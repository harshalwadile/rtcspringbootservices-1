package com.dci.rest.dao;



import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;


import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.dci.rest.bean.DashboardLayout;
import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.utils.CheckString;



@Component
@Repository
public class DashboardDAO extends DataAccessObject{
	
	private static Logger developerLog = Logger.getLogger(DashboardDAO.class);

	public DashboardLayout getUserChartPrefrence(String userName) {
		developerLog.debug("Entering into com.dci.rest.dao.DashboardDAO || Method Name : getUserChartPrivileges() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		DashboardLayout dashboardLayoutObj = new DashboardLayout();
		try {
			if (con == null || con.isClosed()){
				con = getConnection();
			}
			developerLog.warn("call SP_RTC_GETDASHBOARDPREF("+userName+",?,?)");	
			cs = con.prepareCall("{call SP_RTC_GETDASHBOARDPREF(?,?,?)}");
			cs.setString(1,userName);
			cs.registerOutParameter(2, java.sql.Types.INTEGER);
			cs.registerOutParameter(3, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			dashboardLayoutObj.setLayoutId(cs.getInt(2)+"");
			dashboardLayoutObj.setChartOrder(cs.getString(3));

		} catch (Exception e) {
			new DocubuilderException("Exception in om.dci.rest.dao.DashboardDAO || getUserChartPrivileges()",e);
		}finally{
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.DashboardDAO || Method Name : getUserChartPrivileges() ||");
		}
		
		return dashboardLayoutObj;
	}
	public String updateChartPrefrence(String userid,DashboardLayout dashboardLayout) {
		developerLog.debug("Entering into com.dci.rest.dao.DashboardDAO || Method Name : updateChartPrefrence() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		String status = null;
		try {
			if (con == null || con.isClosed()){
				con = getConnection();
			}
			developerLog.warn("call SP_RTC_SAVEDASHBOARDPREF("+userid+","+dashboardLayout.getLayoutId()+","+dashboardLayout.getChartOrder()+",?)");	
			cs = con.prepareCall("{call SP_RTC_SAVEDASHBOARDPREF(?,?,?,?)}");
			cs.setString(1,userid);
			cs.setInt(2,CheckString.getInt(dashboardLayout.getLayoutId()));
			cs.setString(3,dashboardLayout.getChartOrder());
			cs.registerOutParameter(4, java.sql.Types.VARCHAR);
			cs.executeUpdate();
		    status =cs.getString(4);
		} catch (Exception e) {
			new DocubuilderException("Exception in om.dci.rest.dao.DashboardDAO || updateChartPrefrence()",e);
		}finally{
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.DashboardDAO || Method Name : updateChartPrefrence() ||");
		}
		
		return status;
	}


}

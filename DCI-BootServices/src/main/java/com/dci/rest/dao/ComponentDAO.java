package com.dci.rest.dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.dci.rest.common.DciCommon;
import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.model.AssetClass;
import com.dci.rest.model.CommentsBean;
import com.dci.rest.model.ComponentBean;
import com.dci.rest.model.ComponentStatus;
import com.dci.rest.model.SearchCriteria;
import com.dci.rest.model.ContextBean;
import com.dci.rest.model.ContextSchema;
import com.dci.rest.model.DocumentBean;
import com.dci.rest.model.DocumentType;
import com.dci.rest.model.Filter;
import com.dci.rest.model.FootNoteAttributes;
import com.dci.rest.model.Fund;
import com.dci.rest.model.Locale;
import com.dci.rest.model.Order;
import com.dci.rest.model.Placement;
import com.dci.rest.model.ResloveVarBean;
import com.dci.rest.model.UserBean;
import com.dci.rest.model.SelectBean;
import com.dci.rest.model.ShareClass;
import com.dci.rest.model.Style;
import com.dci.rest.model.StyleBean;
import com.dci.rest.model.TableComponent;
import com.dci.rest.model.TableType;
import com.dci.rest.model.Tag;
import com.dci.rest.model.VariableBean;
import com.dci.rest.model.WorkFLStatus;
import com.dci.rest.utils.CheckString;
import com.dci.rest.utils.DateFormat;
import com.dci.rest.utils.StringUtility;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Component
@Repository
public class ComponentDAO  extends DataAccessObject {
	
	private Logger developerLog = Logger.getLogger(ComponentDAO.class);
	
	@Value("${APP_ID}" ) private String appId;
	/**
	 * @Purpose:This method is used to get the list of context from the
	 *               database.
	 * @return Collection
	 */
	
	public ComponentBean getComponentTestData(String user,String client, String componentName) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : getComponent() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		ComponentBean component =new ComponentBean();
		try {
			if (con == null || con.isClosed()){
				con = getConnection();
			}
			developerLog.debug("call SP8_RTC_SEARCHCOMPONENTSBYNAMES ('" + user+"',"+client+","+ componentName+")");
			cs = con.prepareCall("{call SP8_RTC_SEARCHCOMPONENTSBYNAMES(?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(client));
			cs.setString(3, componentName);
			rs = cs.executeQuery();
			while (rs.next()) {
				component.setElementInstanceId(rs.getString("felementinstanceid"));
				component.setType(rs.getString("felement_id"));
				component.setTableType(rs.getString("FTABLETYPE"));
				component.setName(rs.getString("fqualdata_desc"));
				component.setEffectiveDate(DateFormat.expandedDate(rs.getDate("feffectivedate")));
				component.setExpirationDate(DateFormat.expandedDate(rs.getDate("fexpirationdate")));
				component.setBody(rs.getString("felement_detail"));
				component.setCreatedBy(rs.getString("fcreatedby"));
				component.setLastUpdatedBy(rs.getString("flastchangedby"));
				component.setStatusId(rs.getString("fcompstatus"));
				component.setStatusDesc(rs.getString("fcompstatus_desc"));
				component.setLocale(rs.getString("flanguageid"));
				component.setLocaleDesc(rs.getString("flanguage_desc"));
				component.setContext(rs.getString("felementcontextid"));
				component.setCreatedTime(DateFormat.expandedDate(rs.getDate("ftimecreated")));
				/*component.setQualDataTableType(rs.getString("fqualdata_tabletype"));
				component.setDocumentTypeAssociation(rs.getString("fbook_type"));*/
				component.setLastUpdatedTime(DateFormat.expandedDate(rs.getDate("ftimelastchanged")));
				component.setLocaleInd(rs.getString("FPRIMARYLINK"));
				component.setShadowId(rs.getString("fqualdatashadowid"));
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getComponent()",e);
		} finally {
		  releaseConStmts(rs, cs, con, null); 
		  developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : getComponent() ||");
		}
		return component;
		
	}
	public List<ContextBean> getElementContextList(String userId, String clientId, String objectId) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : getElementContextList() ||");
		ArrayList<ContextBean> elementContextList = new ArrayList<ContextBean>();
		CallableStatement cs = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}				
			developerLog.debug("{CALL SP_RTC_ITGetElementContextList(" + userId+ "," + clientId + "," + objectId + ")}");
			cs = con.prepareCall("{CALL SP_RTC_ITGetElementContextList(?,?,?)}");
			cs.setString(1, userId);
			cs.setBigDecimal(2, CheckString.getBigDecimal(clientId));
			cs.setBigDecimal(3, CheckString.getBigDecimal(objectId));
			rs = cs.executeQuery();
			while (rs.next()) {
				ContextBean elementContext = new ContextBean();
				elementContext.setId(rs.getInt("fELementContextId"));
				elementContext.setName(rs.getString("FELEMENTCONTEXT_DESC"));
				elementContextList.add(elementContext);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getElementContextList()",e);
		} finally {
			releaseConStmts(rs, cs, con,null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : getElementContextList()");
		}
		return elementContextList;
	}
	
	
	public List<Filter> getSystemFilterList(String userId, String clientId) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : getSystemFilterList() ||");
		CallableStatement cs = null;
		ResultSet rs = null;
		Connection con = null;
		List<Filter> filterList = new ArrayList<Filter>();
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			System.out.println("{CALL SP8_RTC_GETCOMPFILTER (" + clientId+ "," + userId + ")}");
			cs = con.prepareCall("{CALL SP8_RTC_GETCOMPFILTER (?,?)}");
			cs.setInt(1, CheckString.getInt(clientId));
			cs.setString(2, userId);			
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {
				Filter filter = new Filter();
				filter.setId(rs.getString("FCOMPSEARCH_NAME"));
				filter.setName(rs.getString("FCOMPSEARCH_DESC"));
				filterList.add(filter);				
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getSystemFilterList()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : getSystemFilterList()");
		}
		return filterList;
	}
	
	public String checkComponentName(String user, String clientId, String elementInstanceId, String qualDataDesc) throws Exception {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : checkComponentName() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;

		ObjectMapper mapper = new ObjectMapper();
		ArrayNode componentArray = mapper.createArrayNode();		
		try {
			if (con == null || con.isClosed()){
				con = getConnection();
			}
			if(elementInstanceId == null){
				elementInstanceId = "0";
			}
			developerLog.debug("call SP5_RTC_ITISVALIDNAME('" + user + "'," + clientId + ","+ elementInstanceId + ",'" + qualDataDesc+"')");
			cs = con.prepareCall("{call SP5_RTC_ITISVALIDNAME(?,?,?,?)}");
			cs.setString(1, user);
			cs.setInt(2, new Integer(clientId).intValue());
			cs.setBigDecimal(3, CheckString.getBigDecimal(elementInstanceId));
			cs.setString(4, qualDataDesc);
			rs = cs.executeQuery();

			while (rs != null && rs.next()) {
				ObjectNode objectNode = mapper.createObjectNode();
				System.out.println(rs.getString(2));
				objectNode.put("duplicateName", "Component name is already exist with below data.");
		        objectNode.put("id", rs.getString(1));
		        objectNode.put("name", rs.getString(2));
		        objectNode.put("sdate", rs.getString(4));
		        objectNode.put("edate", rs.getString(5));
		        componentArray.add(objectNode);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || checkComponentName()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : checkComponentName()");
		}
		return componentArray.toString();
	}
	
@SuppressWarnings({ "rawtypes", "unused" })
public Map<String, String> createComponent(String user, String clientId, String bookInstanceId, String bookDetailId, ComponentBean componentObj) throws Exception {
	MDC.put("category","com.dci.rest.dao.ComponentDAO");
	developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : createComponent() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		BigDecimal bookDetail = null;
		BigDecimal elementInstanceId = null;	
		Map<String,String> resultNode = new HashMap<String,String>();
		String ContentLimit = "11000"; //default content limit set.

		String bodytype = componentObj.getType();
		String body = componentObj.getBody();
		String effectiveDate = componentObj.getEffectiveDate(), expirationDate = componentObj.getExpirationDate();
		String displayName = componentObj.getName();
		String global = componentObj.getGlobal();
		String qualDataId = componentObj.getQualDataId(), qualDataCon = componentObj.getQualDataCon(), qualDataTableType = componentObj.getQualDataTableType();
		String comment = componentObj.getComment();
		String locale = componentObj.getLocale();
		
		String footnoteIds = componentObj.getFootnoteIds();
		String entityIds = componentObj.getEntityIds();
		String systemEntityIds = componentObj.getSystemEntityIds();
		String recursiveIds = componentObj.getRecursiveIds();
		
		String footnoteMapping = componentObj.getFootnoteMapping();
		//String ownerName=componentObj.getOwner();
				
		String elementInstanceIdStr = null;
		try {
			if (con == null || con.isClosed()){
				con = getConnection();
			}
			//Commented for later use
			/*if (bookInstanceId == null) {
				if (document != null && document.length > 0) {
					bookInstanceId = document[0];
				}
			}*/
			
			if((body.length()>=Integer.parseInt(ContentLimit)) && !bodytype.equalsIgnoreCase("table") ){
				//throw new Exception("length is greater than 1100 chars");
				resultNode.put("message", "texttoolong");
				resultNode.put("elementId", "0");
				return resultNode;
			}
		//	System.out.println("call SP_RTC_itputElementQual3('"+ user +"','" + clientId + "','" + componentObj.getEffectiveDate() + "','" + componentObj.getExpirationDate() + "','" + bodytype
	//				+ "'," + bookInstanceId + "," + bookDetailId + "," + global + ",'" + displayName + "',null,'default','" + qualDataTableType + "','"+body+"',?,?)");
			developerLog.debug("call SP_RTC_itputElementQual3('"+ user +"','" + clientId + "','" + componentObj.getEffectiveDate() + "','" + componentObj.getExpirationDate() + "','" + bodytype
					+ "'," + bookInstanceId + "," + bookDetailId + "," + global + ",'" + displayName + "',null,'default','" + qualDataTableType + "','"+body+"',?,?)");
			cs = con.prepareCall("{call sp_RTC_itputElementQual3(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			cs.setString(1, user);
			cs.setInt(2, new Integer(clientId).intValue());
			cs.setDate(3, DateFormat.getDateOfString(effectiveDate));
			cs.setDate(4, DateFormat.getDateOfString(expirationDate));
			cs.setString(5, bodytype);
			cs.setBigDecimal(6, CheckString.getBigDecimal(bookInstanceId)); // Book Instance Id
			cs.setBigDecimal(7, CheckString.getBigDecimal(bookDetailId)); // Book Detail Id.
			cs.setInt(8, new Integer(global).intValue()); // Global
			cs.setString(9, displayName);
			cs.setString(10, qualDataId);

			cs.setString(11, qualDataCon);
			cs.setString(12, qualDataTableType);
			cs.setString(13, body);
			cs.registerOutParameter(14, java.sql.Types.BIGINT);
			cs.registerOutParameter(15, java.sql.Types.BIGINT);
			cs.executeUpdate();

			long lbookDetail = cs.getLong(14);
			long lelementInstanceId = cs.getLong(15);
			if (lbookDetail > 0){
				bookDetail = new BigDecimal(lbookDetail);
			}
			if (lelementInstanceId > 0){
				elementInstanceId = new BigDecimal(lelementInstanceId);
				elementInstanceIdStr = CheckString.getStringBDValue(elementInstanceId);
				componentObj.setElementInstanceId(elementInstanceIdStr);
				//Commented for later use
				/*if (document != null) {
					associateElementToBook(user, elementInstanceId, document, null);
				}
				if (content != null) {
					associateElementToContent(user, elementInstanceId, content, null);
				}*/
				
				if(CheckString.isValidString(componentObj.getContext()) || CheckString.isValidString(componentObj.getParentContextId()) ) {
					associateElementToElementContext(user, elementInstanceId, componentObj.getContext(),componentObj.getParentContextId());
				}
				
				if(CheckString.isValidString(comment)){
	                comment = StringUtility.fixSpecialCharforWeb(comment);
	                insertCommentForElement(user, elementInstanceIdStr, comment);
	            }
	            if(CheckString.isValidString(locale)){
	                setComponentLanguage(user, elementInstanceIdStr, locale);
	            }
				
				/*if (contentCategory != null) {
					associateElementToContentCategory(user, elementInstanceId,contentCategory,bodytype, null);
				}*/

				if(CheckString.isValidString(footnoteIds)){
					Map hmFootnotes = CheckString.getFootnoteRefId(body);
					insertRecursiveElement(user, "footnote", bookInstanceId, bookDetailId, elementInstanceId, footnoteIds, hmFootnotes);
				}

				if(CheckString.isValidString(recursiveIds)){
					insertRecursiveElement(user, "recursiveobj", bookInstanceId, bookDetailId, elementInstanceId, recursiveIds, null);
				}
				if(CheckString.isValidString(entityIds)){
					insertRecursiveEntity(user, "entity", entityIds, elementInstanceId, bookInstanceId, null);
				}
				/*if (booktypeids != null && !booktypeids.equals("")) {
					associateElementToBooktype(user, elementInstanceId,booktypeids, null);
				}*/
				if(CheckString.isValidString(systemEntityIds)){
					insertSystemEntity(user, clientId, systemEntityIds, elementInstanceId, CheckString.getBigDecimal(bookDetailId), null, null, null, null);
				}
				
				if(CheckString.isValidString(footnoteMapping)){
					int result = checkFootnoteMappingExistence(user, clientId, elementInstanceIdStr, footnoteMapping);
					if(result == -1){
						resultNode.put("message", "footnoteid already exist");
						resultNode.put("elementId", elementInstanceIdStr);
					}
				}else if(bodytype.equals("footnote")){
					int result = checkFootnoteMappingExistence(user, clientId, elementInstanceIdStr, elementInstanceIdStr);
					if(result == -1){
						resultNode.put("message", "footnoteid already exist");
						resultNode.put("elementId", elementInstanceIdStr);
					}
				}
			}
			
		}catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || createComponent()",e);
		}finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : createComponent()");
		}
		
		if(elementInstanceIdStr == null){
			resultNode.put("message", "error");
			resultNode.put("elementId", "0");
		}else{
			resultNode.put("message", "success");
			resultNode.put("elementId", elementInstanceIdStr);
		}
		return resultNode;
	}

public Map<String, String> createTableComponent(String user, String clientId, TableComponent componentObj) throws Exception {
	MDC.put("category","com.dci.rest.dao.ComponentDAO");
	developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : createComponent() ||");
	Connection con = null;
	CallableStatement cs = null;
	ResultSet rs = null;
	BigDecimal bookDetail = null;
	BigDecimal elementInstanceId = null;	
	Map<String,String> resultNode = new HashMap<String,String>();
	String ContentLimit = "11000"; //default content limit set.

	String bodytype = componentObj.getType();
	String body = componentObj.getBody();
	String effectiveDate = componentObj.getEffectiveDate(), expirationDate = componentObj.getExpirationDate();
	String displayName = componentObj.getName();
	String global = componentObj.getGlobal();
	String qualDataId = componentObj.getQualDataId(), qualDataCon = componentObj.getQualDataCon(), qualDataTableType = componentObj.getTableType();
	String comment = componentObj.getComment();
	String locale = componentObj.getLocale();		
	String footnoteIds = componentObj.getFootnoteIds();
	String entityIds = componentObj.getEntityIds();
	String systemEntityIds = componentObj.getSystemEntityIds();
	String recursiveIds = componentObj.getRecursiveIds();		
	String footnoteMapping = componentObj.getFootnoteMapping();				
	String elementInstanceIdStr = null;
	
	
	try {
		if (con == null || con.isClosed()){
			con = getConnection();
		}
		//Commented for later use
		/*if (bookInstanceId == null) {
			if (document != null && document.length > 0) {
				bookInstanceId = document[0];
			}
		}*/
		
		if((body.length()>=Integer.parseInt(ContentLimit)) && !bodytype.equalsIgnoreCase("table") ){
			//throw new Exception("length is greater than 1100 chars");
			resultNode.put("message", "texttoolong");
			resultNode.put("elementId", "0");
			return resultNode;
		}
		developerLog.debug("call sp_RTC_itputElementQual3('"+ user +"','" + clientId + "','" + componentObj.getEffectiveDate() + "','" + componentObj.getExpirationDate() + "','" + bodytype
				+ "'," + componentObj.getBookInstanceId()+ "," + componentObj.getBookDetailId() + "," + global + ",'" + displayName + "',null,'default','" + qualDataTableType + "','"+body+"',?,?)");
		cs = con.prepareCall("{call sp_RTC_itputElementQual3(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
		cs.setString(1, user);
		cs.setInt(2, new Integer(clientId).intValue());
		cs.setDate(3, DateFormat.getDateOfString(effectiveDate));
		cs.setDate(4, DateFormat.getDateOfString(expirationDate));
		cs.setString(5, bodytype);
		cs.setBigDecimal(6, CheckString.getBigDecimal(componentObj.getBookInstanceId())); // Book Instance Id
		cs.setBigDecimal(7, CheckString.getBigDecimal(componentObj.getBookDetailId())); // Book Detail Id.
		cs.setInt(8, new Integer(global).intValue()); // Global
		cs.setString(9, displayName);
		cs.setString(10, qualDataId);
		cs.setString(11, qualDataCon);
		cs.setString(12, qualDataTableType);
		cs.setString(13, body);
		cs.registerOutParameter(14, java.sql.Types.BIGINT);
		cs.registerOutParameter(15, java.sql.Types.BIGINT);
		cs.executeUpdate();

		long lbookDetail = cs.getLong(14);
		long lelementInstanceId = cs.getLong(15);
		if (lbookDetail > 0){
			bookDetail = new BigDecimal(lbookDetail);
		}
		if (lelementInstanceId > 0){
			elementInstanceId = new BigDecimal(lelementInstanceId);
			elementInstanceIdStr = CheckString.getStringBDValue(elementInstanceId);
			componentObj.setElementInstanceId(elementInstanceIdStr);
			//Commented for later use
			/*if (document != null) {
				associateElementToBook(user, elementInstanceId, document, null);
			}
			if (content != null) {
				associateElementToContent(user, elementInstanceId, content, null);
			}*/
			
			if(CheckString.isValidString(componentObj.getContext())) {
				associateElementToElementContext(user, elementInstanceId, componentObj.getContext(),componentObj.getParentContextId());
			}
			
			if(CheckString.isValidString(comment)){
                comment = StringUtility.fixSpecialCharforWeb(comment);
                insertCommentForElement(user, elementInstanceIdStr, comment);
            }
            if(CheckString.isValidString(locale)){
                setComponentLanguage(user, elementInstanceIdStr, locale);
            }
			
			/*if (contentCategory != null) {
				associateElementToContentCategory(user, elementInstanceId,contentCategory,bodytype, null);
			}*/
			if(CheckString.isValidString(footnoteIds)){
				Map hmFootnotes = CheckString.getFootnoteRefId(body);
				insertRecursiveElement(user, "footnote", componentObj.getBookInstanceId(), componentObj.getBookDetailId(), elementInstanceId, footnoteIds, hmFootnotes);
			}
			if(CheckString.isValidString(recursiveIds)){
				insertRecursiveElement(user, "recursiveobj", componentObj.getBookInstanceId(), componentObj.getBookDetailId(), elementInstanceId, recursiveIds, null);
			}
			if(CheckString.isValidString(entityIds)){
				insertRecursiveEntity(user, "entity", entityIds, elementInstanceId, componentObj.getBookDetailId(), null);
			}
			/*if (booktypeids != null && !booktypeids.equals("")) {
				associateElementToBooktype(user, elementInstanceId,booktypeids, null);
			}*/
			if(CheckString.isValidString(systemEntityIds)){
				insertSystemEntity(user, clientId, systemEntityIds, elementInstanceId, CheckString.getBigDecimal(componentObj.getBookDetailId()), null, null, null, null);
			}
			
			if(CheckString.isValidString(footnoteMapping)){
				int result = checkFootnoteMappingExistence(user, clientId, elementInstanceIdStr, footnoteMapping);
				if(result == -1){
					resultNode.put("message", "footnoteid already exist");
					resultNode.put("elementId", elementInstanceIdStr);
				}
			}
		}
		
	}catch (Exception e) {
		new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || createTableComponent()",e);;
	}finally {
		releaseConStmts(rs, cs, con, null);
		developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : createTableComponent()");
	}
	
	if(elementInstanceIdStr == null){
		resultNode.put("message", "error");
		resultNode.put("elementId", "0");
	}else{
		resultNode.put("message", "success");
		resultNode.put("elementId", elementInstanceIdStr);
	}
	return resultNode;
}

	public Map<String, String> editComponentField(String user, String clientId, Map<String, Object> params) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : editComponentField() ||");
		CallableStatement cs = null;
		Connection con = null;
		String status = "";
		String shadowId = "";
		Map<String,String> editCompStatusMap = new HashMap<String,String>();
		try {
			ComponentBean component = (ComponentBean)params.get("componentObj");
			if(CheckString.isValidString(component.getName()) || CheckString.isValidString(component.getBody()) || CheckString.isValidString(component.getBody())|| CheckString.isValidString(component.getLocale())|| CheckString.isValidString(component.getContext())|| CheckString.isValidString(component.getEffectiveDate())|| CheckString.isValidString(component.getExpirationDate())|| CheckString.isValidString(component.getTableType()) || CheckString.isEmptyString(component.getEffectiveDate()) ||  CheckString.isEmptyString(component.getExpirationDate())||CheckString.isValidString(component.getFcomp_assignee()) || CheckString.isValidString(component.getWorkFlowStatusId()) || CheckString.isValidString(component.getOwner())) {
				if(CheckString.isValidString(component.getBody())) {
					String ContentLimit = "11000"; //default content limit set.
					//	ContentLimit =((PreConfigBean)client.get("RTEContentLimit")).getPropertyValue();
					if((component.getBody().length()>=Integer.parseInt(ContentLimit)) && !component.getType().equalsIgnoreCase("table")) {
						//throw new DCIUserDefinedException(new Exception("update text is too long"),"com.dci.db4.dao.ComponentDAO || method updateComponentData()");
						status = "texttoolong";
						editCompStatusMap.put("status", status);
						return editCompStatusMap;
					} 
					if (component.getType().equals("footnote")) {
						if (component.getBody().indexOf("<p>") < 0)
							//component.setBody("<footnote id=\"" + component.getQualData_Id() + "\"><p>" + component.getBody() + "</p></footnote>");
							 component.setBody("<footnote id=\"" + component.getQualData_Id() + "\">" + component.getBody() + "</footnote>");
						
						else
							component.setBody("<footnote id=\"" + component.getQualData_Id() + "\">" + component.getBody() + "</footnote>");
					} else if (component.getType().equals("note")){
						if (component.getBody().indexOf("<p>") < 0)
							component.setBody("<note><p>" + component.getBody() + "</p></note>");
						else
							component.setBody("<note>" + component.getBody() + "</note>");
					}/* else if (component.getType().equals("bridgehead")) {
						component.setBody("<bridgehead>" + component.getBody() + "</bridgehead>");
					}		*/		
				}
				if(CheckString.isValidString(component.getName()) && component.getName().length() > 128) {
					component.setName(component.getName().substring(0, 128));
				}
				if (con == null || con.isClosed()){
					con = getConnection();
				}	
				developerLog.debug("CALL SP8_RTC_ITUPDATEELEMENT('"+user+"',"+clientId+","+component.getElementInstanceId()+",'"+component.getName()+"','"+component.getBody()+"',"+component.getLocale()+","+component.getContext()+","+DateFormat.getDateOfString(component.getEffectiveDate())+","+DateFormat.getDateOfString(component.getExpirationDate())+","+CheckString.getBigDecimal(component.getBookInstanceId())+","+CheckString.getBigDecimal(component.getBookDetailId())+",'"+component.getFootnoteMapping()+"','"+component.getTableType()+"','"+component.getWorkFlowStatusId()+","+component.getFcomp_assignee()+","+component.getParentContextId()+","+component.getOwner()+",?,?)");
				cs = con.prepareCall("{call SP8_RTC_ITUPDATEELEMENT(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
				cs.setString(1, user);
				cs.setInt(2, CheckString.getInt(clientId));
				cs.setBigDecimal(3, CheckString.getBigDecimal(component.getElementInstanceId()));
				cs.setString(4, component.getName());
				cs.setString(5, component.getBody());
				cs.setString(6, component.getLocale());
				cs.setString(7, component.getContext());
				if(component.getEffectiveDate() !=null)
				if(CheckString.isEmptyString(component.getEffectiveDate())) {
					cs.setString(8,component.getEffectiveDate());
				}else {
				cs.setDate(8, DateFormat.getDateOfString(component.getEffectiveDate()));
				}
				if(component.getEffectiveDate() == null)
				{
					cs.setDate(8, DateFormat.getDateOfString(component.getEffectiveDate()));
				}
				if(component.getExpirationDate() !=null)
				if(CheckString.isEmptyString(component.getExpirationDate())) {
					cs.setString(9,component.getExpirationDate());
				}else {
				cs.setDate(9, DateFormat.getDateOfString(component.getExpirationDate()));
				}
				if(component.getExpirationDate() == null)
				{
					cs.setDate(9, DateFormat.getDateOfString(component.getExpirationDate()));
				}
				cs.setBigDecimal(10,CheckString.getBigDecimal(component.getBookInstanceId()));
				cs.setBigDecimal(11,CheckString.getBigDecimal(component.getBookDetailId()));
				cs.setString(12, component.getTableType());
				cs.setString(13, component.getFootnoteMapping());
				cs.setBigDecimal(14, CheckString.getBigDecimal(component.getWorkFlowStatusId()));
				cs.setString(15, component.getFcomp_assignee());
				cs.setString(16, component.getParentContextId());
				cs.setString(17, component.getOwner());
				cs.registerOutParameter(18, Types.VARCHAR);
				cs.registerOutParameter(19, java.sql.Types.BIGINT);
				cs.executeUpdate();
				status = cs.getString(18);
				shadowId = ""+cs.getBigDecimal(19);
				if(CheckString.isValidString(component.getFcomp_assignee())) {
					editCompStatusMap.put("assignee", component.getFcomp_assignee());
				}
				editCompStatusMap.put("status", status);
				editCompStatusMap.put("shadowId", shadowId);
	
				if(CheckString.isValidString(component.getSystemEntityIds())){
					insertSystemEntity(user, clientId, component.getSystemEntityIds(), CheckString.getBigDecimal(component.getElementInstanceId()), CheckString.getBigDecimal(component.getBookInstanceId()), null, null, null,con);
				}
			} 
		}catch (Exception e){
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || editComponentField()",e);
		} finally {
			releaseConStmts(null, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : editComponentField()");
		}
		return editCompStatusMap;
	}

	public String editComponentData(String user, String clientId, Map<String, Object> params){
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : editComponentData() ||");
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection con = null;
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objectNode = mapper.createObjectNode();
		//HashMap client = (HashMap)MainConfigBean.getDepartmentPreconfigs().get("default");
		String ContentLimit = "11000"; //default content limit set.
	//	ContentLimit =((PreConfigBean)client.get("RTEContentLimit")).getPropertyValue();
		HashMap<String, Object> paramMap = (HashMap<String, Object>)params;
		ComponentBean component = (ComponentBean)paramMap.get("componentObj");
		String overwriteStatus =  paramMap.get("overwriteStatus").toString();
		String bookInstanceId =  paramMap.get("bookInstanceId")==null? null : paramMap.get("bookInstanceId").toString();
		String bookDetailId =  paramMap.get("bookDetailId")==null? null : paramMap.get("bookDetailId").toString();
		String qualDataCond =  paramMap.get("qualDataCond")==null? null : paramMap.get("qualDataCond").toString();
		String qualDataTableType =  paramMap.get("qualDataTableType")==null? null : paramMap.get("qualDataTableType").toString();
		boolean contentchanged =  Boolean.parseBoolean(paramMap.get("contentchanged").toString());
		//String lastUpdatedTime = paramMap.get("lastUpdatedTime")==null? null : paramMap.get("qualDataTableType").toString();
		try {
			String qualDataDesc = (component.getName() != null && component.getName().length() > 128) ? component.getName().substring(0, 128): component.getName();
			String bodytype = component.getType();
			String body=component.getBody();
			if (con == null || con.isClosed()){
				con = getConnection();
			}			
			Date dexpDate = DateFormat.getDateOfString(component.getExpirationDate());
			Date deffDate = DateFormat.getDateOfString(component.getEffectiveDate());
			if (bodytype.equals("footnote")) {
				if (body.indexOf("<p>") < 0)
					body = "<footnote id=\"" + component.getQualData_Id() + "\"><p>" + body + "</p></footnote>";
				else
					body = "<footnote id=\"" + component.getQualData_Id() + "\">" + body + "</footnote>";
			} else if (bodytype.equals("note")){
				if (body.indexOf("<p>") < 0)
					body = "<note><p>" + body + "</p></note>";
				else
					body = "<note>" + body + "</note>";
			} else if (bodytype.equals("bridgehead")) {
				body = "<bridgehead>" + body + "</bridgehead>";
			}
			developerLog.debug("content Limit: "+Integer.parseInt(ContentLimit));
			if((body.length()>=Integer.parseInt(ContentLimit)) && !bodytype.equalsIgnoreCase("table")) {
				//throw new DCIUserDefinedException(new Exception("update text is too long"),"com.dci.db4.dao.ComponentDAO || method updateComponentData()");
				objectNode.put("message", "texttoolong");
				objectNode.put("elementId", "0");
				return objectNode.toString();
			}				
			if(bookDetailId!=null && bookDetailId.equals("")){
				bookDetailId = null;
			}
			developerLog.debug("call SP4_RTC_ITSETELEMENT('" + user + "','"+ component.getElementInstanceId() + "','" + component.getQualDataId() + "','"
					+ bookInstanceId + "','" + bookDetailId + "','" + deffDate+ "','" + dexpDate + "','" + qualDataDesc + "','" + component.getQualData_Id() +"','"
					+qualDataCond+ "','" + qualDataTableType + "','"+body+"','" + component.getComment() + "','"+ contentchanged + "','"+component.getLastUpdatedTime()+"','"+overwriteStatus+"',?,?)");
	
			cs = con.prepareCall("{call SP4_RTC_ITSETELEMENT(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(component.getElementInstanceId()));
			cs.setBigDecimal(3, CheckString.getBigDecimal(component.getQualDataId()));
			cs.setBigDecimal(4, CheckString.getBigDecimal(bookInstanceId));
			cs.setBigDecimal(5, CheckString.getBigDecimal(bookDetailId));
			cs.setDate(6, deffDate);
			cs.setDate(7, dexpDate);
			cs.setString(8, qualDataDesc);
			cs.setString(9, component.getQualData_Id());			
			cs.setString(10, qualDataCond);
			cs.setString(11, qualDataTableType);
			body = CheckString.convertXHtmlToDocBook(body);
			body = CheckString.replaceStringX(body, "<para/>", "");
			if (bodytype.equals("para") || bodytype.equals("note"))
				body += "<para/>";
			cs.setString(12, body);
			cs.setString(13, component.getComment());
			if (contentchanged){
				cs.setNull(14, Types.NULL);
			} else {
				cs.setString(14, "F");
			}
			if(overwriteStatus.equals("Y")){
				cs.setNull(15,Types.NULL);
			}else{
				cs.setTimestamp(15,Timestamp.valueOf(component.getLastUpdatedTime()));
			}
			cs.setString(16, overwriteStatus);
			cs.registerOutParameter(17, java.sql.Types.TIMESTAMP);
			cs.registerOutParameter(18, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			String lastChangedTime = "NA";
			String lastChangedBy = "NA";
			if(cs.getTimestamp(17)!=null){
				lastChangedTime = cs.getTimestamp(17).toString();
			}
			if(cs.getString(18)!=null){
				lastChangedBy = cs.getString(18);
			}
			//objectNode.put ("0", lastChangedTime);
			//objectNode.put ("1", lastChangedBy);
			objectNode.put("message", "updated successfull");
			if(objectNode.size() > 0){
				if(CheckString.isValidString(component.getContext())) {
					associateElementToElementContext(user, CheckString.getBigDecimal(component.getElementInstanceId()), component.getContext(),component.getParentContextId());
				}				
				
	            if(CheckString.isValidString(component.getLocale())){
	                setComponentLanguage(user, component.getElementInstanceId(), component.getLocale());
	            }
				/*
				if((outputList.get(0).equals("NA")) && (outputList.get(1).equals("NA"))){							
					if (dissociatedocument != null) {
						dissociateElementToBook(user, CheckString.getBigDecimal(elementInstanceId), dissociatedocument,null);
					}
					if (dissociatecontent != null) {
						dissociateElementToContent(user, CheckString.getBigDecimal(elementInstanceId), dissociatecontent,null);
					}
					if (dissociatecontentcategory != null) {
						entityDAO.dissociateEntityToContentCategory(user,CheckString.getBigDecimal(elementInstanceId),dissociatecontentcategory,bodytype,null);
					}
					if(lastChangedBy.equals("NA")){
						if (dissociateContextId != null) {
							dissociateElementToElementContext(user, CheckString.getBigDecimal(elementInstanceId), dissociateContextId,null);
						}
					}
					if (document != null && document.length!=0 && !document[0].equals("")) {
						associateElementToBook(user, CheckString.getBigDecimal(elementInstanceId), document, null);
					}
					if (content != null && !content.equals("")) {
						try{
							Integer.parseInt(content[0]);	
							associateElementToContent(user, CheckString.getBigDecimal(elementInstanceId), content, null);
						}catch(Exception e){
						}
					}
					if(lastChangedBy.equals("NA")){
						if (contextId != null) {
							associateElementToElementContext(user, CheckString.getBigDecimal(elementInstanceId), contextId, null);
						}
					}
					if (contentCategory != null && !contentCategory.equals("")) {
						associateElementToContentCategory(user, CheckString.getBigDecimal(elementInstanceId), contentCategory,bodytype, null);
					}
					if (oldfootnoteids != null && !oldfootnoteids.equals(""))
						removeRecursiveElement(user, "footnote",
								bookInstanceId,
								bookDetailId, // bookDetailId of the section
								CheckString.getBigDecimal(elementInstanceId),
								oldfootnoteids, null);
					if (footnoteids != null && !footnoteids.equals("")) {
						java.util.HashMap hmFootnotes =CheckString.getFootnoteRefId(body);
						insertRecursiveElement(user, "footnote",
								bookInstanceId,
								bookDetailId, // bookDetailId of the section
								CheckString.getBigDecimal(elementInstanceId),
								footnoteids,hmFootnotes, null);
						updateRecursiveElementOrder(user, "footnote", bookInstanceId,
								bookDetailId, // modified by gguser on 3rd Aug-2007
								elementInstanceId, footnoteids, null);
					}
					if (oldrecursiveids != null && !oldrecursiveids.equals(""))
						removeRecursiveElement(user, "recursiveobj",
								bookInstanceId,bookDetailId, CheckString.getBigDecimal(elementInstanceId),
								oldrecursiveids, null);
					if (recursiveids != null && !recursiveids.equals("")) {
						insertRecursiveElement(user, "recursiveobj",bookInstanceId,
								bookDetailId, CheckString.getBigDecimal(elementInstanceId),
								recursiveids,null, null);
						updateRecursiveElementOrder(user, "recursiveobj",
								bookInstanceId, bookDetailId, 
								elementInstanceId, recursiveids, null);
					}
					if (oldentityids != null && !oldentityids.equals(""))
						EntityDAO.removeRecursiveEntity(user, "entity", bookInstanceId,
								CheckString.getBigDecimal(elementInstanceId),oldentityids, null);
					if (entityids != null && !entityids.equals("")) {
						EntityDAO.insertRecursiveEntity(user, "entity", entityids,
								CheckString.getBigDecimal(elementInstanceId),
								bookInstanceId, null);
					}
					if(bodytype.equals("footnote")||bodytype.equals("note"))
						eBookDetailId = null;
					updateSystemEntitys(user, clientId, sysEntityids, oldSysentityids,
							CheckString.getBigDecimal(elementInstanceId), CheckString
							.getBigDecimal(bookDetailId), CheckString
							.getBigDecimal(eBookDetailId),null,null, null);
				}
			*/}
		}catch (Exception e){
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || editComponentData()",e);
		} finally {
			releaseConStmts(rs, cs, con,null);
			 developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : editComponentData() ||");
		}
		return objectNode.toString();
	}
	
	
	public ComponentBean getComponent(String user,String client, String componentId) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : getComponent() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		ComponentBean component =new ComponentBean();
		try {
			if (con == null || con.isClosed()){
				con = getConnection();
			}
			developerLog.debug("call SP8_RTC_SEARCHCOMPONENTSBYIDS('" + user+"',"+client+","+ componentId+")");
			cs = con.prepareCall("{call SP8_RTC_SEARCHCOMPONENTSBYIDS(?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(client));
			cs.setString(3, componentId);
			rs = cs.executeQuery();
			while (rs.next()) {
				component.setElementInstanceId(rs.getString("felementinstanceid"));
				component.setType(rs.getString("felement_id"));
				component.setTableType(rs.getString("FTABLETYPE"));
				component.setName(rs.getString("fqualdata_desc"));
				component.setEffectiveDate(DateFormat.expandedDate(rs.getDate("feffectivedate")));
				component.setExpirationDate(DateFormat.expandedDate(rs.getDate("fexpirationdate")));
				component.setBody(rs.getString("felement_detail"));
				component.setCreatedBy(rs.getString("fcreatedby"));
				component.setLastUpdatedBy(rs.getString("flastchangedby"));
				component.setStatusId(rs.getString("fcompstatus"));
				component.setStatusDesc(rs.getString("fcompstatus_desc"));
				component.setLocale(rs.getString("flanguageid"));
				component.setLocaleDesc(rs.getString("flanguage_desc"));
				component.setContext(rs.getString("felementcontextid"));
				component.setParentContextId(rs.getString("FELEMENTCONTEXTID_PARENT"));
				if(!CheckString.isValidString(component.getContext())) {
					component.setContext("-1"); 
				}
				if(!CheckString.isValidString(component.getParentContextId())) {
					component.setParentContextId("-1"); 
				}
				component.setCreatedTime(DateFormat.expandedDate(rs.getDate("ftimecreated")));
				/*component.setQualDataTableType(rs.getString("fqualdata_tabletype"));
				component.setDocumentTypeAssociation(rs.getString("fbook_type"));*/
				component.setLastUpdatedTime(DateFormat.expandedDate(rs.getDate("ftimelastchanged")));
				component.setLocaleInd(rs.getString("FPRIMARYLINK"));
				component.setShadowId(rs.getString("fqualdatashadowid"));
				component.setFootnoteMapping(rs.getString("FMARKER"));
				//component.setAssignee("dcisupportjhf"); 
				if(!CheckString.isValidString(rs.getString("FSTATUSID"))) {
					component.setWorkFlowStatusId(""); 
				}else {
					component.setWorkFlowStatusId(rs.getInt("FSTATUSID")+""); 
				}
				component.setWorkFlowStatus(rs.getString("FSTATUS_ID")); 
				//component.setWorkFlowStatusId(rs.getString("WRK_FLOWSTATUSID")); 
				component.setFcomp_assignee(rs.getString("FCOMP_ASSIGNEE")); 
				component.setOwner(rs.getString("FOWNER")); 
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getComponent()",e);
		} finally {
		  releaseConStmts(rs, cs, con, null); 
		  developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : getComponent() ||");
		}
		return component;
		
	}
	
	public List<Locale> getLanguageList(String clientId) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : getLanguageList() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		ArrayList<Locale> localList = new ArrayList<Locale>();
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}				
			developerLog.debug("{ call SP_RTC_TRGETLANGUAGES(" + clientId+ ",?,?,?) }");
			cs = con.prepareCall("{call SP_RTC_TRGETLANGUAGES(?,?,?,?)}");
			cs.setBigDecimal(1, CheckString.getBigDecimal(clientId));
			cs.registerOutParameter(2, java.sql.Types.CHAR);
			cs.registerOutParameter(3, java.sql.Types.INTEGER);
			cs.registerOutParameter(4, java.sql.Types.VARCHAR);
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {
				Locale local = new Locale();
				local.setId(rs.getString("flanguageid"));
				local.setValue(rs.getString("flanguage"));
				if(!(rs.getString("FDEFAULTID").equals("0"))){
					local.setDefault(true);
				}				
				localList.add(local);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getLanguageList()",e);
		} finally {
		  releaseConStmts(rs, cs, con, null); 
		  developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : getLanguageList() ||");
		}
		return localList;
	}
	public void associateElementToElementContext(String user, BigDecimal elementInstanceId, String elementContextId, String subContextId) throws Exception {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : associateElementToElementContext() ||");
		Connection con = null;
		CallableStatement cs = null;
		try {
			if (con == null || con.isClosed()){
				con = getConnection();
			}
			developerLog.debug("{call SP8_RTC_ITASSOCIATEDISSOCIATE_ELEMENTCONTEXT(" + user+ ","+ elementInstanceId+ ","+ elementContextId + ","+ subContextId + ") }");
			//System.out.println("call SP8_ITASSOCIATEDISSOCIATE_ELEMENTCONTEXT(" + user+ ","+ elementInstanceId+ ","+ elementContextId + ","+ subContextId + ")");
			cs = con.prepareCall("{call SP8_RTC_ITASSOCIATEDISSOCIATE_ELEMENTCONTEXT(?,?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, elementInstanceId);
			cs.setBigDecimal(3, CheckString.getBigDecimal(elementContextId));
			cs.setBigDecimal(4, CheckString.getBigDecimal(subContextId));
			cs.executeUpdate();
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || associateElementToElementContext()",e);
		} finally {
			releaseConStmts(null, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : associateElementToElementContext() ||");
		}
	}
	
	public boolean insertCommentForElement(String user, String elementInstanceId, String comment) throws Exception {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : insertCommentForElement() ||");
		boolean isSuccess = false;
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		try {
			if (con == null || con.isClosed()){
				con = getConnection();
			}
			developerLog.debug("call SP5_RTC_ITSETELEMENTCOMMENT( clientId: " + user + ", elementInstanceId: " + elementInstanceId + ", Comment: " + comment + ")");
			cs = con.prepareCall("{call SP5_RTC_ITSETELEMENTCOMMENT(?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(elementInstanceId));
			cs.setString(3, comment);
			cs.executeUpdate();
			isSuccess = true;
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || insertCommentForElement()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : insertCommentForElement() ||");
		}
		return isSuccess;
	}
	public boolean setComponentLanguage(String userId, String elementInstanceId, String languageId) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : setComponentLanguage() ||");
		Connection con = null;
		CallableStatement cs = null;
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			developerLog.debug("{ call SP_RTC_TRSETCOMPONENTLANGUAGE(" + userId + "," + elementInstanceId + "," + languageId + ",?,?,?) }");
			cs = con.prepareCall("{call SP_RTC_TRSETCOMPONENTLANGUAGE(?,?,?,?,?,?)}");
			cs.setString(1, userId);
			cs.setBigDecimal(2, CheckString.getBigDecimal(elementInstanceId));
			cs.setInt(3, Integer.parseInt(languageId));
			cs.registerOutParameter(4, java.sql.Types.INTEGER);
			cs.registerOutParameter(5, java.sql.Types.VARCHAR);
			cs.registerOutParameter(6, java.sql.Types.VARCHAR);
			cs.executeUpdate();
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || setComponentLanguage()",e);
		} finally {
			releaseConStmts(null, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : setComponentLanguage() ||");
		}

		return true;
	}
	@SuppressWarnings("rawtypes")
	public void insertRecursiveElement(String user, String elementId, String bookInstanceId, String bookSectionDetailId,
		BigDecimal elementInstanceId, String recursiveObjectElementInstanceIds, Map hmFootnoteAlt) throws Exception {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : insertRecursiveElement() ||");
		Connection con = null;
		CallableStatement cs = null;
		try {
			if (con == null || con.isClosed()){
				con = getConnection();
			}
			String PURRECURSIVEELEMENT = "sp_RTC_ftPutRecursiveElement(?,?,?,?,?,?,?)}";
			String[] recursiveObjectElementInstanceIdsArray = CheckString.parseIntegerString(recursiveObjectElementInstanceIds, "*");
			int elementTypeId = 1;
			for (int i = 0; i < recursiveObjectElementInstanceIdsArray.length; i++) {
				String elementInstanceChildId = recursiveObjectElementInstanceIdsArray[i];
				String strKey = elementInstanceChildId;
				boolean recusriveInsertionFlag = true;
				for (int j = 0; j < i; j++){
					if (elementInstanceChildId != null && elementInstanceChildId.equals(recursiveObjectElementInstanceIdsArray[j])){
						recusriveInsertionFlag = false;
					}
				}
				if(elementId.equals("recursiveobj")) {
					elementTypeId = 1;
				}
				if(hmFootnoteAlt != null && hmFootnoteAlt.containsKey(strKey)){
					elementTypeId = (new Integer((String)hmFootnoteAlt.get(strKey))).intValue();
				}
				if (recusriveInsertionFlag) {
					System.out.println("call sp_RTC_ftPutRecursiveElement(" + user + "," + elementId + "," + bookInstanceId + ","
							+ bookSectionDetailId + "," + elementInstanceId + "," + elementInstanceChildId + ","+ elementTypeId +")");
					cs = con.prepareCall("{call " + PURRECURSIVEELEMENT);
					cs.setString(1, user);
					cs.setString(2, elementId);
					cs.setBigDecimal(3, CheckString.getBigDecimal(bookInstanceId));
					cs.setBigDecimal(4, CheckString.getBigDecimal(bookSectionDetailId));
					cs.setBigDecimal(5, elementInstanceId);
					cs.setBigDecimal(6, CheckString.getBigDecimal(elementInstanceChildId));
					cs.setInt(7,elementTypeId);
					cs.executeUpdate();
				}
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || insertRecursiveElement()",e);
		} finally {
			releaseConStmts(null, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : insertRecursiveElement() ||");
		}		
	}
	
	@SuppressWarnings("unused")
	public  void insertRecursiveEntity(String user, String entityId, String entityInstanceChildIds, BigDecimal elementInstanceId, String bookInstanceId, Connection con) throws Exception {
		CallableStatement cs = null;
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		//developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : insertRecursiveEntity() ||");
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			final String PURRECURSIVEELEMENT = "sp_RTC_enPutRecursiveEntity(?,?,?,?,?)}";
			String[] some = CheckString.parseIntegerString(entityInstanceChildIds, "*");
			for (int i = 0; i < some.length; i++) {
				String elementInstanceChildId = some[i];
				boolean first = true;
				for (int j = 0; j < i; j++)
					if (elementInstanceChildId != null && elementInstanceChildId.equals(some[j]))
						first = false;
				System.out.println("call sp_RTC_enPutRecursiveEntity(" + user + "," + entityId + "ref," + elementInstanceChildId + "," + elementInstanceId + "," + bookInstanceId + ")");
				cs = con.prepareCall("{call " + PURRECURSIVEELEMENT);
				cs.setString(1, user);
				cs.setString(2, entityId + "ref");
				cs.setBigDecimal(3, CheckString.getBigDecimal(elementInstanceChildId));
				cs.setBigDecimal(4, elementInstanceId);
				cs.setBigDecimal(5, CheckString.getBigDecimal(bookInstanceId));
				cs.executeUpdate();
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || insertRecursiveEntity()",e);
		} finally {
			releaseConStmts(null, cs, con, null);
			//developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : insertRecursiveEntity() ||");
		}
	}
	
	public  void insertSystemEntity(String user, String clientId, String sysEntityids, BigDecimal elementInstanceId, BigDecimal bookInstId, BigDecimal eleBookDtlId, BigDecimal eleTypId, BigDecimal eleParentDtlId, Connection con)throws Exception {
		CallableStatement cs = null;
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		//developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : insertSystemEntity() ||");
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			String[] entityList = sysEntityids.split("\\*");
			String entityId = "";
			String fundId = "";
			String classId = "";
			String sequenceId = "";
			for (int i = 0; i < entityList.length; i++) {
				String eachEntityId = entityList[i];
				if (eachEntityId != null && eachEntityId.length() > 0) {
					String[] entityDtlList = eachEntityId.split("\\~");
					if (entityDtlList.length > 0 && entityDtlList[0] != null)
						entityId = entityDtlList[0].toString();
					if (entityDtlList.length > 1 && entityDtlList[1] != null)
						fundId = entityDtlList[1].toString();
					else if (entityDtlList.length > 1 && entityDtlList[1] != null
							&& entityDtlList[1].equalsIgnoreCase("00"))
						fundId = null;
					if (fundId.equals("-00"))
						fundId = null;
					if (entityDtlList.length > 2 && entityDtlList[2] != null)
						classId = entityDtlList[2].toString();
					else if (entityDtlList.length > 2 && entityDtlList[2] != null
							&& entityDtlList[2].equalsIgnoreCase("00"))
						classId = null;
					if (classId.equals("00"))
						classId = null;
					if (entityDtlList.length > 3 && entityDtlList[1] != null)
						sequenceId = entityDtlList[3].toString();				
					if (!entityId.equals("") && !sequenceId.equals("")) {
						System.out.println("call sp_RTC_VPUTVARIABLEDETAIL(" + user + "," + clientId + "," + eleBookDtlId + "," + elementInstanceId + "," + entityId + "," + fundId + "," + classId + "," + sequenceId + "," + eleTypId + "," + eleParentDtlId + ")");
						cs = con.prepareCall("{call SP_RTC_VPUTVARIABLEDETAIL(?,?,?,?,?,?,?,?,?,?)}");
						cs.setString(1, user);
						cs.setBigDecimal(2, CheckString.getBigDecimal(clientId));
						cs.setBigDecimal(3, eleBookDtlId);
						cs.setBigDecimal(4, elementInstanceId);
						cs.setBigDecimal(5, CheckString.getBigDecimal(entityId));
						cs.setBigDecimal(6, CheckString.getBigDecimal(fundId));
						cs.setBigDecimal(7, CheckString.getBigDecimal(classId));
						cs.setBigDecimal(8, CheckString.getBigDecimal(sequenceId));
						cs.setBigDecimal(9, eleTypId);
						cs.setBigDecimal(10, eleParentDtlId);
						cs.executeUpdate();
					}
				}
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || insertSystemEntity()",e);
		} finally {
			releaseConStmts(null, cs, con, null);
			//developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : insertSystemEntity() ||");
		}
	}	
	
	public int checkFootnoteMappingExistence(String user, String clientId, String elementInstanceId, String footnoteId) throws Exception{
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : checkFootnoteMappingExistence() ||");
		Connection con = null;
		CallableStatement cs = null;
		int return_code= -1;		
		try{
			if(con == null || con.isClosed()){
				con = getConnection();
			}
			if(!footnoteId.equals("")){
				cs = con.prepareCall("{call SP_RTC_ITASSOCIATE_ELEMENT2MARKER(?,?,?,?,?)}");
				System.out.println("call SP_RTC_ITASSOCIATE_ELEMENT2MARKER("+user+ ", " +clientId+ ", " +elementInstanceId+ ", " +footnoteId+ " )" );
				cs.setString(1, user);
				cs.setString(2, clientId);
				if(elementInstanceId==null){
					cs.setInt(3,0);
				}else{
					cs.setInt(3,new Integer(elementInstanceId).intValue());
				}				
				cs.setString(4,footnoteId);
				cs.registerOutParameter(5,java.sql.Types.INTEGER);
				cs.executeUpdate();
				return_code = cs.getInt(5);
			}else{
				return_code=0;
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || checkFootnoteMappingExistence()",e);
		} finally {
			releaseConStmts(null, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : checkFootnoteMappingExistence() ||");
		}
		return return_code;
	}
	
	public int dissociateFootnoteMapping(String user, String clientId, String elementInstanceId, String prevFootnoteId) throws Exception{
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : dissociateFootnoteMapping() ||");
		Connection con = null;
		CallableStatement cs = null;
		int return_code = -1;		
		try{  
			//if(CheckString.isValidString(prevFootnoteId)){
				if(con == null || con.isClosed()){
					con = getConnection();
				}
				cs = con.prepareCall("{call SP_RTC_ITDISSOCIATE_ELEMENT2MARKER(?,?,?,?)}");
				developerLog.debug("call SP_RTC_ITDISSOCIATE_ELEMENT2MARKER("+user+ ", " +clientId+ ", " +elementInstanceId+ ", " +prevFootnoteId+ " )" );
				cs.setString(1, user);
				cs.setString(2, clientId);		
				cs.setInt(3,new Integer(elementInstanceId).intValue());
				cs.setString(4,prevFootnoteId);
				return_code = cs.executeUpdate();
			//}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || dissociateFootnoteMapping()",e);
		} finally {
			releaseConStmts(null, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : dissociateFootnoteMapping() ||");
		}
		return return_code;
	}
	
	public String dissociateComponent(String user, String clientId, String elementInsId)throws Exception {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : dissociateComponent() ||");
		CallableStatement cs = null;
		Connection con = null;
		String status="";
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("call SP8_RTC_ITDISSOCIATE_ELEMENT('" + user+ "'," + clientId + "," + elementInsId + ")");
			cs = con.prepareCall("{call SP8_RTC_ITDISSOCIATE_ELEMENT(?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(clientId));
			cs.setBigDecimal(3, CheckString.getBigDecimal(elementInsId));
			cs.executeUpdate(); 
			status="success";
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || dissociateComponent()",e);
			return "error";
		} finally {
			releaseConStmts(null, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : dissociateComponent() ||");
		}
		return status;
	}
	
	public String setDocTypeAssociation(String user, String elementId, String docTypeIds)throws Exception {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : setDocTypeAssociation() ||");
		CallableStatement cs = null;
		Connection con = null;
		String status="";
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			System.out.println("call sp_RTC_itAssociate_Element2BKType('" + user+ "'," + elementId + ",'" + docTypeIds + "',?)");
			cs = con.prepareCall("{call sp_RTC_itAssociate_Element2BKType(?,?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(elementId));
			cs.setString(3, docTypeIds);
			cs.registerOutParameter(4, Types.VARCHAR);
			cs.executeUpdate();
			status = (cs.getString(4).equals("0")) ? "success":"error";			
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || setDocTypeAssociation()",e);
		} finally {
			releaseConStmts(null, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : setDocTypeAssociation() ||");
		}
		return status;
	}
	
	public String setFundAssociation(String user,String elementInstanceId, String contentId)throws Exception {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : setFundAssociation() ||");
		CallableStatement cs = null;
		Connection con =null;
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			System.out.println("call sp8_RTC_itAssociate_Element2Content("+ user + "," + elementInstanceId + ",'"+ contentId + "')");
			cs = con.prepareCall("{call sp8_RTC_itAssociate_Element2Content(?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(elementInstanceId));
			cs.setString(3, contentId);
			cs.executeUpdate();
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || setFundAssociation()",e);
		} finally {
			releaseConStmts(null, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : setFundAssociation() ||");
		}
		return "success";
	}
	
	public String setAssetClassAssociation(String user,String elementInstanceId, String contentCategoryId,String bodytype)throws Exception {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : setAssetClassAssociation() ||");
		CallableStatement cs = null;
		Connection con = null;
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			
			System.out.println("call SP_RTC_ITMAPCOMP2ASSETCLASS("+ user+ ","+ elementInstanceId+ ","+ contentCategoryId+ "," + bodytype +  ")");
			cs = con.prepareCall("{call SP_RTC_ITMAPCOMP2ASSETCLASS(?,?,?,?)}");
			cs.setString(1, user);
			cs.setString(2, elementInstanceId);
			cs.setString(3, contentCategoryId);
			cs.setString(4,bodytype);
			cs.executeUpdate();
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || processAssetClassAssociation()",e);
		} finally {
			releaseConStmts(null, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO ||MethodName:processAssetClassAssociation ||");
		}
		return "success";
	}
	
	public String setDocumentAssociation(String user,String elementInstanceId, String bookInstanceId) throws Exception {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : setDocumentAssociation() ||");
		CallableStatement cs = null;
		Connection con = null;
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			
			System.out.println("call sp8_RTC_itAssociate_Element2Book("+ user + "," + elementInstanceId + ",'"+ bookInstanceId+ "')");
			cs = con.prepareCall("{call sp8_RTC_itAssociate_Element2Book(?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(elementInstanceId));
			cs.setString(3,bookInstanceId);
			cs.executeUpdate();
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || setDocumentAssociation()",e);
		} finally {
			releaseConStmts(null, cs, con,null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO ||MethodName : setDocumentAssociation ||");
		}
		return "success";
	}
	
	public ComponentBean getComponentData(String elementinstanceid) throws Exception {   
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : getComponentData() ||");
		CallableStatement cs = null;
		Connection con =null;
		ResultSet rs = null;
		ComponentBean component = null;
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			System.out.println("{call SP_RTC_ITGETELEMENTQUALDETAIL("+ elementinstanceid + ")}");
			cs = con.prepareCall("{call SP_RTC_ITGETELEMENTQUALDETAIL(?)}");
			cs.setBigDecimal(1, CheckString.getBigDecimal(elementinstanceid));
			rs = cs.executeQuery();
			if (rs != null && rs.next()) {
				component = new ComponentBean();
				component.setQualDataId(CheckString.getStringBDValue(rs.getBigDecimal("fqualdataid")));
				component.setElementInstanceId(CheckString.getStringBDValue(rs.getBigDecimal("felementinstanceid")));
				component.setType(rs.getString("felement_id"));				
				component.setGlobal(rs.getInt("fglobal") + "");
				component.setQualData_Id(rs.getString("fQualData_id"));
				component.setQualDataCon(rs.getString("fQualData_cond"));
				component.setBody(rs.getString("fQualData"));
				component.setName(rs.getString("fQualData_desc"));
				component.setQualDataTableType(rs.getString("fQualData_tabletype"));
				component.setEffectiveDate(DateFormat.expandedDate(rs.getDate("feffectivedate")));
				component.setExpirationDate(DateFormat.expandedDate(rs.getDate("fexpirationdate")));
				component.setCreatedBy(rs.getString("fcreatedby"));
				component.setLastUpdatedBy(rs.getString("flastchangedby"));
				component.setCreatedTime(DateFormat.expandedTS(rs.getTimestamp("fTimeCreated")));
				component.setLastUpdatedTime(DateFormat.expandedTS(rs.getTimestamp("fTimeLastChanged")));
				component.setLastUpdatedTime(rs.getTimestamp("fTimeLastChanged").toString());
				component.setOwner(rs.getString("fowner"));
				if (rs.getBigDecimal("FQUANTDATAID") == null) {
					component.setQuantDataId(null);
				} else {
					component.setQuantDataId(String.valueOf(rs.getBigDecimal("FQUANTDATAID")));
				}
			}
		} catch (Exception e){
			developerLog.error("Exiting from com.dci.rest.dao.ComponentDAO ||MethodName : getComponentData ||" +e,e);
			//new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getComponentData()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO ||MethodName : getComponentData ||");
		}
		return component;
	}
	
	public void convertComponent(String user , String clientId, String elementInstanceId, String elementFrom, String elementTo){
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : convertComponent() ||");
		CallableStatement cs = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP4_RTC_ITSETTEXT2SUBHEAD ("+user+","+clientId+","+elementInstanceId+","+elementFrom+","+elementTo+")}");
			cs = con.prepareCall("{call SP4_RTC_ITSETTEXT2SUBHEAD(?,?,?,?,?)}");
			cs.setString(1, user);
			cs.setInt(2, CheckString.getInt(clientId));
			cs.setBigDecimal(3,CheckString.getBigDecimal(elementInstanceId));
			cs.setString(4,elementFrom);
			cs.setString(5,elementTo);
			cs.executeUpdate();
		}catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || convertComponent()",e);
		}finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || MethodName : convertComponent ||");
		}			
	}	
	
	
	public Map<String, Object> setTagAssociation(String userID,int clientID,String elementInstId,String updatedVal) throws Exception {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		CallableStatement cs = null;
		Connection con = null;
		ResultSet rs = null;
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : setTagAssociation() ||");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try{
			con = getConnection();
			developerLog.debug("CALL sp8_RTC_PUTCOMPTAG ('"+userID+"',"+clientID+","+elementInstId+",'"+updatedVal+"',?)");
			cs = con.prepareCall("{CALL sp8_RTC_PUTCOMPTAG(?,?,?,?,?)}");
			cs.setString(1, userID);
			cs.setInt(2, clientID);
			cs.setString(3, elementInstId);
			if(CheckString.isValidString(updatedVal)) {
				cs.setString(4, updatedVal);
			}else {
				cs.setNull(4,java.sql.Types.INTEGER);
			}
			
			cs.registerOutParameter(5, java.sql.Types.VARCHAR);				
			cs.executeUpdate();
			resultMap.put("tagAssociationStatus", cs.getString(5));
		}catch (Exception e){
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || setTagAssociation()",e);
		}finally{
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : setTagAssociation() ||");
		}
		return resultMap;
	}
	
	
	public ArrayList<DocumentBean> getDocumentList(String user, String clientId, String componentId) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		ArrayList<DocumentBean> DocList = new ArrayList<DocumentBean>();
		try {
			con = getConnection();
			developerLog.debug("{call sp_RTC_utGetBookInstanceList(" + user + ","+ clientId + ","+componentId+")}");
			cs = con.prepareCall("{call sp_RTC_utGetBookInstanceList(?,?,?)}");
			cs.setString(1, user);
			cs.setInt(2, new Integer(clientId).intValue());
			if(CheckString.isValidString(componentId)) {
				cs.setBigDecimal(3, CheckString.getBigDecimal(componentId));
			}else {
				cs.setNull(3, Types.NULL);
			}			
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {
				DocumentBean document = new DocumentBean();
				document.setId(rs.getString("fbookinstanceid"));
				document.setName(rs.getString("fbookinstance_description"));
				document.setEffDate(DateFormat.expandedDate(rs.getDate("feffectivedate")));
				document.setExpDate(DateFormat.expandedDate(rs.getDate("fexpirationdate")));
				document.setStatusId(rs.getString("fbookinstance_status"));
				document.setStatusDesc(rs.getString("fstatus_description"));				
				document.setDetails((document.getName() + " ("+ document.getStatusDesc() + ")"));				
				DocList.add(document);
			}
		} catch (Exception e) {			
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getDocumentList()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : getDocumentList() ||");
		}
		return DocList;
	}
	
	public ArrayList<DocumentType> getBookTypeList(String user, String clientId) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		ArrayList<DocumentType> doctypeList = new ArrayList<DocumentType>();
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP_RTC_utGetBook_Type_List('" + user + "',"+ clientId + ")}");
			cs = con.prepareCall("{call SP_RTC_utGetBook_Type_List('" + user + "',"+ clientId + ")}");
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {
				DocumentType docType = new DocumentType(rs.getString("fbooktypeid"),rs.getString("fbooktype_display"),rs.getString("fbook_type"));
				doctypeList.add(docType);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getBookTypeList()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : getBookTypeList() ||");
		}
		return doctypeList;
	}

	public List<ComponentStatus> getCompStatusList() {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : getCompStatusList() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		ComponentStatus compStatus;
		List<ComponentStatus> statusList = new ArrayList<ComponentStatus>();
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			developerLog.debug("{call SP7_RTC_TRGETCOMPSTATUSIDENTITY() }");
			cs = con.prepareCall("{call SP7_RTC_TRGETCOMPSTATUSIDENTITY()}");			
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {
				compStatus = new ComponentStatus();
				compStatus.setId(rs.getInt("FCOMPSTATUS"));
				compStatus.setName(rs.getString("FCOMPSTATUS_DESC"));
				statusList.add(compStatus);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getCompStatusList()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : getCompStatusList() ||");
		}

		return statusList;
	}
	public ArrayList<Fund> getContentList(String user, String clientId, String componentId) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		ArrayList<Fund> FundsArray = new ArrayList<Fund>();
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			System.out.println("Conee : "+con);
			developerLog.debug("{call SP8_RTC_UTGETCONTENTLIST('" + user + "',"+ clientId + ","+componentId+")}");
			cs = con.prepareCall("{call SP8_RTC_UTGETCONTENTLIST('" + user + "',"+ clientId + ","+componentId+")}");
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {
				Fund fund = new Fund();
				fund.setId(rs.getInt("fcontentid"));
				fund.setName(rs.getString("fcontent_id"));
				//fund.setAssetClassId(rs.getString("FCONTENTCATEGORYID"));
				fund.setAssetClassName(rs.getString("FCONTENTCATEGORY_DESCRIPTION"));
				FundsArray.add(fund);
			}
		} catch (Exception e) {			
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getContentList()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : getContentList() ||");
		}
		return FundsArray;
	}
	

	
	public ArrayList<AssetClass> getContentCategoryList(String user, String clientId, String contentCategory) throws Exception {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		if (contentCategory == null || contentCategory.length() == 0) 
			throw new Exception("Invalid Content Category");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		ArrayList<AssetClass> assetList = new ArrayList<AssetClass>();
		try {
			if(con == null || con.isClosed()) {
				con = getConnection();
			}
			developerLog.debug("{call SP_RTC_UTGETANYCONTENTCATEGORYLIST('"+user+"',"+clientId+",'"+contentCategory+"')}");
			cs = con.prepareCall("{call SP_RTC_UTGETANYCONTENTCATEGORYLIST('"+user+"',"+clientId+",'"+contentCategory+"')}");
			rs = cs.executeQuery();
			while(rs != null && rs.next()) {
				AssetClass assetClass = new AssetClass(rs.getString("fcontentcategoryid"),rs.getString("fcontentcategory_id"));
				assetList.add(assetClass);
			}
		} catch(Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getContentCategoryList()",e);
		}finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : getContentCategoryList() ||");
		}	
		return assetList;
	}
	
	public List<ComponentBean> getRevisionHistoryForComponent(String user, String elementInstanceId) throws Exception {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : getRevisionHistoryForComponent() ||");
		Connection con = null;
		List<ComponentBean> historyList = new ArrayList<ComponentBean>();
		CallableStatement cs = null;
		ResultSet rs = null;
		UserBean userBean ;
		try {
			if (con == null || con.isClosed()){
				con = getConnection();
			}
			developerLog.debug("call SP_RTC_UTGETREVHISFORELEMENTINSTANCE(" + user+ "," + elementInstanceId + ")");
			cs = con.prepareCall("{call SP_RTC_UTGETREVHISFORELEMENTINSTANCE(?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(elementInstanceId));
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {
				ComponentBean componentBean = new ComponentBean();
				userBean = new UserBean();
				componentBean.setCommentCount(rs.getString("fcommentcount"));
				componentBean.setName(rs.getString("fqualdata_desc"));
				componentBean.setBody(rs.getString("fqualdata"));
				componentBean.setType(rs.getString("felement_id"));
				componentBean.setCreatedBy(rs.getString("fcreatedby"));
				componentBean.setCreatedTime(DateFormat.expandedTS(rs.getTimestamp("ftimecreated")));
				componentBean.setQualDataId(rs.getString("fqualdataid"));
				componentBean.setVersion(rs.getString("fqualdataversion"));
				componentBean.setShadowId(rs.getString("fqualdatashadowid"));
				componentBean.setLastUpdatedBy(rs.getString("FLASTCHANGEDBY"));
				componentBean.setLastUpdatedTime(DateFormat.expandedTS(rs.getTimestamp("FTIMELASTCHANGED")));
				userBean.setFirstName(rs.getString("FFIRST_NAME"));
				userBean.setLastName(rs.getString("FLAST_NAME"));
				componentBean.setUser(userBean);				
				if (componentBean.getType().equals("footnote")) {
					componentBean.setBody(CheckString.xdocBookToHtml(CheckString.stripOffTag(componentBean.getBody(),"footnote")));
				} else if (componentBean.getType().equals("note")) {
					componentBean.setBody(CheckString.xdocBookToHtml(CheckString.stripOffTag(componentBean.getBody(),"note")));
					
				}
				componentBean.setBody(CheckString.removeCRLF(CheckString.replaceStringX(componentBean.getBody(), "'","&#39")));
				/*if (componentBean.getBody().length() > 250) {
					componentBean.setDisplayFlag(false);
				}else{
					componentBean.setDisplayFlag(true);
				}	*/	
				historyList.add(componentBean);
			}
		} catch (Exception e){
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getRevisionHistoryForComponent()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : getRevisionHistoryForComponent() ||");
		}
		return historyList;
	}
	
	public List<CommentsBean> getCommentsForQualDataShadowId(String user, String qualDataShadowId) throws Exception {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		List<CommentsBean> commentList = new ArrayList<CommentsBean>();
		CallableStatement cs = null;
		ResultSet rs = null;
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : getCommentsForQualDataShadowId() ||");
		try {
			if (con == null || con.isClosed()){
				con = getConnection();
			}
			developerLog.debug("call sp8_RTC_utGetCommmentsForQualdatashadowId("+ user + "," + qualDataShadowId + ")");
			cs = con.prepareCall("{call sp8_RTC_utGetCommmentsForQualdatashadowId(?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(qualDataShadowId));
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {
				CommentsBean commentBean = new CommentsBean();
				UserBean userBean = new UserBean();
				commentBean.setComment(StringUtility.replaceControlCharacters(DciCommon.replaceSpecialCharacters(rs.getString("FCOMMENT_DESC")),null));
				commentBean.setCreatedby(rs.getString("FCREATEDBY"));
				commentBean.setCreatedtime(DateFormat.expandedTS(rs.getTimestamp("ftimecreated")));
				userBean.setFirstName(rs.getString("FFIRST_NAME"));
				userBean.setLastName(rs.getString("FLAST_NAME"));
				commentBean.setUser(userBean);
				commentBean.setShadowId(qualDataShadowId);
				commentList.add(commentBean);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getCommentsForQualDataShadowId()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : getCommentsForQualDataShadowId() ||");
		}
		return commentList;
	}
	public List<ComponentBean> getSecondaryComponentsForPrimaryComponent(String clientId,String elementinstanceid) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : getSecondaryComponentsForPrimaryComponent() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<ComponentBean> linkageComponentList = new ArrayList<ComponentBean>();
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
				developerLog.debug("{ call SP7_RTC_TRGETCOMPLINKS("
					+ clientId + "," + elementinstanceid + ",?,?,?) }");
			cs = con
					.prepareCall("{call SP7_RTC_TRGETCOMPLINKS (?,?,?,?,?)}");
			cs.setInt(1, new Integer(clientId).intValue());
			cs.setBigDecimal(2, CheckString.getBigDecimal(elementinstanceid));
			cs.registerOutParameter(3, java.sql.Types.CHAR);
			cs.registerOutParameter(4, java.sql.Types.INTEGER);
			cs.registerOutParameter(5, java.sql.Types.VARCHAR);
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {
				String elementInstanceId = rs.getString("FELEMENTINSTANCEID");
				if (elementInstanceId != null && !elementInstanceId.equals("")) {
					ComponentBean linkageComponentObj = new ComponentBean();
					linkageComponentObj.setElementInstanceId(elementInstanceId);
					linkageComponentObj.setLocale(rs.getString("FLANGUAGEID"));
					linkageComponentObj.setLocaleDesc(rs.getString("FLANGUAGE"));
					
					linkageComponentObj.setStatusId(rs.getString("FSECONDARYLANGSTATUS"));
					linkageComponentObj.setStatusDesc(rs.getString("FVALUE"));
					linkageComponentObj.setName(rs.getString("FQUALDATA_DESC"));
					linkageComponentObj.setBody(rs.getString("fQualData"));
					linkageComponentObj.setEffectiveDate(DateFormat.expandedDate(rs.getDate("feffectivedate")));
					linkageComponentObj.setExpirationDate(DateFormat.expandedDate(rs.getDate("fexpirationdate")));
					
					linkageComponentList.add(linkageComponentObj);
				}
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getSecondaryComponentsForPrimaryComponent()",e);
		} finally {
			releaseConStmts(rs, cs, con,null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : getSecondaryComponentsForPrimaryComponent() ||");
		}

		return linkageComponentList;
	}
	public String setPrimaryComponent(String userId, String elementInstanceId) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : setPrimaryComponent() ||");
		Connection con = null;
		CallableStatement cs = null;
		String primaryFlag="";
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			developerLog.debug("{ call SP_RTC_TRSETPRIMARYCOMPONENT(" + userId+ "," + elementInstanceId + ",?,?,?) }");
			cs = con.prepareCall("{call SP_RTC_TRSETPRIMARYCOMPONENT(?,?,?,?,?)}");
			cs.setString(1, userId);
			cs.setBigDecimal(2, CheckString.getBigDecimal(elementInstanceId));
			cs.registerOutParameter(3, java.sql.Types.CHAR);
			cs.registerOutParameter(4, java.sql.Types.INTEGER);
			cs.registerOutParameter(5, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			primaryFlag = cs.getString(5);
		} catch (Exception e) {
			developerLog.error("Exception in com.dci.rest.dao.ComponentDAO || Method Name : setPrimaryComponent() :"+e,e);
		} finally {
			releaseConStmts(null, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : setPrimaryComponent() ||");
		}
		return primaryFlag;
	}
	public String unSetPrimaryComponent(String userId, String elementInstanceId) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO|| Method Name : unSetPrimaryComponent() ||");
		Connection con = null;
		CallableStatement cs = null;
		String primaryFlag="";
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			developerLog.debug("{ call SP_RTC_TRUNSETPRIMARYCOMPONENT(" + userId + "," + elementInstanceId + ",?,?,?) }");
			cs = con.prepareCall("{call SP_RTC_TRUNSETPRIMARYCOMPONENT(?,?,?,?,?)}");
			cs.setString(1, userId);
			cs.setBigDecimal(2, CheckString.getBigDecimal(elementInstanceId));
			cs.registerOutParameter(3, java.sql.Types.CHAR);
			cs.registerOutParameter(4, java.sql.Types.INTEGER);
			cs.registerOutParameter(5, java.sql.Types.VARCHAR);
			cs.executeUpdate();

			primaryFlag = cs.getString(5);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || unSetPrimaryComponent()",e);
			return "error";
		} finally {
			releaseConStmts(null, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : unSetPrimaryComponent() ||");

		}
		return primaryFlag;
	}
	
	public List<DocumentType> getDcoTypeList(String user, String clientId,String elementInstanceId) throws Exception {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO|| Method Name : getDcoTypeList() ||");
		CallableStatement cs = null;
		ResultSet rs = null;
		Connection con = null;
		List<DocumentType> docTypeList = new ArrayList<DocumentType> ();
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			
			developerLog.debug("{call SP_RTC_UTGETELEMENTBKTYPELIST('" + user+ "'," + clientId + "," + elementInstanceId + ")}");
			cs = con.prepareCall("{call SP_RTC_UTGETELEMENTBKTYPELIST(?,?,?)}");
			cs.setString(1, user);
			cs.setInt(2, CheckString.getInt(clientId));
			cs.setBigDecimal(3, CheckString.getBigDecimal(elementInstanceId));
			rs = cs.executeQuery();
			while (rs.next()) {
				DocumentType docType = new DocumentType(rs.getString("fbooktypeId"),null, rs.getString("fbook_type"));
				docTypeList.add(docType);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getDcoTypeList()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : getDcoTypeList() ||");
		}
		return docTypeList;
	}
	
	public List<AssetClass> getAssetClassList(String user, String clientId, String componentId) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO|| Method Name : getAssetClassList() ||");
		List<AssetClass> assetClassList = new ArrayList<AssetClass>();
		CallableStatement cs = null;		  
		ResultSet rs = null;
		Connection con = null;
		try {
			if(con == null || con.isClosed()) {
				con = getConnection();
			}
		  	developerLog.debug("SP8_RTC_GETCOMPONENTASSETCLASS('"+user+"',"+clientId+","+ componentId +")");
			cs = con.prepareCall("{call SP8_RTC_GETCOMPONENTASSETCLASS(?,?,?)}");
			cs.setString(1, user);	
			cs.setInt(2, new Integer(clientId).intValue());
			cs.setBigDecimal(3,CheckString.getBigDecimal(componentId));
			rs = cs.executeQuery();
			while(rs != null && rs.next()) {
				AssetClass assetClass = new AssetClass(rs.getString("fassetclassid"), rs.getString("fassetclass"));
				assetClassList.add(assetClass);
			}
		  } catch (Exception e) {
			  new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getAssetClassList()",e);
		  } finally {
			  releaseConStmts(rs, cs, con,null); 
			  developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : getAssetClassList() ||");
		  }	
		return  assetClassList;		
	}

	public boolean isPrimaryComponent(String elementInstanceId) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : isPrimaryComponent() ||");
		Connection con = null;
		CallableStatement cs = null;
		boolean isPrimary = false;

		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			developerLog.debug("{ call SP_RTC_TRISPRIMARYCOMPONENT ("
					+ elementInstanceId + ",?,?,?,?) }");
			cs = con.prepareCall("{call SP_RTC_TRISPRIMARYCOMPONENT (?,?,?,?,?)}");
			cs.setBigDecimal(1, CheckString.getBigDecimal(elementInstanceId));
			cs.registerOutParameter(2, java.sql.Types.INTEGER);
			cs.registerOutParameter(3, java.sql.Types.CHAR);
			cs.registerOutParameter(4, java.sql.Types.INTEGER);
			cs.registerOutParameter(5, java.sql.Types.VARCHAR);
			cs.executeUpdate();

			int primaryFlag = cs.getInt(2);
			if (primaryFlag == 1) {
				isPrimary = true;
			}

		} catch (Exception e) {
			  new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || isPrimaryComponent()",e);
		} finally {
			  releaseConStmts(null, cs, con,null); 
			  developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : isPrimaryComponent() ||");
		}

		return isPrimary;
	}
	public boolean disassociateSecondaryComonent(String userId,
			String secondaryCompId, String primaryCompId) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering from com.dci.rest.dao.ComponentDAO || Method Name : disassociationElementToElement ||");
		Connection con = null;
		CallableStatement cs = null;
		boolean isAssociated = false;

		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			/*developerLog.debug("{ call SP_TRDISASSOCIATE_ELEMENT2ELEMENT ("
					+ userId + "," + secondaryCompId + "," + primaryCompId
					+ ", ?,?,?) }");
			cs = con
					.prepareCall("{call SP_TRDISASSOCIATE_ELEMENT2ELEMENT(?,?,?,?,?,?)}");*/
			developerLog.debug("{ call SP_RTC_TRDELINKCOMP ("
					+ userId + "," + secondaryCompId + "," + primaryCompId
					+ ", ?,?,?) }");
			cs = con
					.prepareCall("{call SP_RTC_TRDELINKCOMP(?,?,?,?,?,?)}");
			cs.setString(1, userId);
			cs.setBigDecimal(2, CheckString.getBigDecimal(secondaryCompId));
			cs.setBigDecimal(3, CheckString.getBigDecimal(primaryCompId));
			cs.registerOutParameter(4, java.sql.Types.CHAR);
			cs.registerOutParameter(5, java.sql.Types.INTEGER);
			cs.registerOutParameter(6, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			String associateFlag = cs.getString(6);
			if (associateFlag.equalsIgnoreCase("success!")) {
				isAssociated = true;
			}

		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || disassociateSecondaryComonent()",e);
		} finally {
			  releaseConStmts(null, cs, con,null); 
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : disassociateSecondaryComonent() ||");
		}

		return isAssociated;
	}
	public boolean setAssociationElementToElement(String userId,
			String clientId, String childId, String parentId) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : setAssociationElementToElement ||");
		Connection con = null;
		CallableStatement cs = null;
		boolean isAssociated = false;

		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}			
			developerLog.debug("{ call SP7_RTC_TRLINKCOMPONENTS ("
					+ userId + "," + clientId + "," + childId + "," + parentId
					+ ", ?,?,?) }");
			cs = con
					.prepareCall("{call SP7_RTC_TRLINKCOMPONENTS(?,?,?,?,?,?,?)}");
			cs.setString(1, userId);
			cs.setInt(2, new Integer(clientId).intValue());
			cs.setBigDecimal(3, CheckString.getBigDecimal(childId));
			cs.setBigDecimal(4, CheckString.getBigDecimal(parentId));
			cs.registerOutParameter(5, java.sql.Types.CHAR);
			cs.registerOutParameter(6, java.sql.Types.INTEGER);
			cs.registerOutParameter(7, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			String associateFlag = cs.getString(7);
			if (associateFlag.equalsIgnoreCase("success!")) {
				isAssociated = true;
			}

		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || disassociateSecondaryComonent()",e);
		} finally {
			releaseConStmts(null, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : disassociateSecondaryComonent() ||");
		}

		return isAssociated;
	}
	public ComponentBean getTableData(String elementinstanceid, String bookInstanceId, String bookDetailId) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : getTableData ||");
		CallableStatement cs = null;
		ResultSet rs = null;
		Connection con = null;
		ComponentBean comp = null;
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			developerLog.debug("{call SP4_RTC_ITGETELEMENTQUALDETAILTABLE("+ elementinstanceid + "," + bookInstanceId + ","+ bookDetailId + ")}");
			cs = con.prepareCall("{call SP4_RTC_ITGETELEMENTQUALDETAILTABLE(?,?,?)}");
			cs.setBigDecimal(1, CheckString.getBigDecimal(elementinstanceid));
			cs.setBigDecimal(2, CheckString.getBigDecimal(bookInstanceId));
			cs.setBigDecimal(3, CheckString.getBigDecimal(bookDetailId));
			rs = cs.executeQuery();
			if (rs != null && rs.next()) {
				comp = new ComponentBean();
				comp.setElementInstanceId(rs.getString("felementinstanceid"));
				comp.setBody(rs.getString("fQualData"));
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getTableData()",e);
		}finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : getTableData() ||");
		}
		return comp;
	}
	
	public String addToFavoriteLibrary(String userId, int clientId,String selectedComponents, int favoriteSearchLmt) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		Map<String,String> statusCode = new HashMap<String,String>();
 		String message = "";
		developerLog.debug("Entering into ccom.dci.rest.dao.ComponentDAO || Method Name : addToFavoriteLibrary() ||");
		CallableStatement cs = null;
		try {
			if (con == null || con.isClosed())
				con = getConnection();

			developerLog.debug("{call SP5_RTC_FPUTCOMPFAVORITES  (" + userId + ","
					+ clientId + "," + selectedComponents + ","
					+ favoriteSearchLmt + ")");
			cs = con.prepareCall("{call SP5_RTC_FPUTCOMPFAVORITES(?,?,?,?,?,?)}");
			cs.setString(1, userId);
			cs.setInt(2, clientId);
			cs.setString(3, selectedComponents);
			cs.setInt(4, favoriteSearchLmt);
			cs.registerOutParameter(5, java.sql.Types.BIGINT);
			cs.registerOutParameter(6, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			/*
			 * if (cs.getInt(5) != 0) { message = cs.getString(6); }
			 */
			 message = cs.getString(6);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || addToFavoriteLibrary()",e);
		} finally {
			releaseConStmts(null, cs, con, null);
			developerLog.debug("Exiting into com.dci.rest.dao.ComponentDAO || Method Name : addToFavoriteLibrary()");
		}
		return message;
	}
	
	public List<ComponentBean> searchLinkageComponent(String userName, String clientId, SearchCriteria searchParams) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : searchLinkageComponent() ||");
		ComponentBean component;
		String isFromPrimary = searchParams.getIsFromPrimary() != null ? (String) searchParams.getIsFromPrimary() : ""; 
		String compTypeList = searchParams.getCompType() != null ? (String) searchParams.getCompType() : ""; 
		String languageIdList = searchParams.getLocale() != null ? (String) searchParams.getLocale() : "";
		String searchString = searchParams.getSearchText() != null ? (String) searchParams.getSearchText(): "";
		String isExact = searchParams.getExactSearchFlag() != null ? (String) searchParams.getExactSearchFlag() : "";
		String counter = searchParams.getCount() != null ? (String) searchParams.getCount() : "200";
		String createdBy = searchParams.getCreatedBy() != null ? (String) searchParams.getCreatedBy().toLowerCase() : "";
		String lastChangedBy = searchParams.getLastChangedBy() != null ? (String) searchParams.getLastChangedBy().toLowerCase() : "";
		String orAndParam = searchParams.getOrAndParam() != null ? (String) searchParams.getOrAndParam() : "";
		String effectiveDate = searchParams.getEffDate() != null ? (String) searchParams.getEffDate() : "";
		String expirationDate = searchParams.getExpDate() != null ? (String) searchParams.getExpDate() : "";
		String elementInstanceId = searchParams.getComponentId();
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<ComponentBean> searchResult = new ArrayList<ComponentBean>();
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			developerLog.debug("{ call SP7_RTC_TRSEARCHCOMPONENTS ('" + userName + "'," + clientId + "," + isFromPrimary + ",'"
					+ compTypeList + "','" + languageIdList + "','" + searchString + "','" + isExact + "'," + counter + ",'"
					+ createdBy + "','" + lastChangedBy + "','" + orAndParam + "','" + effectiveDate + "','" + expirationDate +"',"+elementInstanceId+",?,?,?) }");
			cs = con.prepareCall("{call SP7_RTC_TRSEARCHCOMPONENTS(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			cs.setString(1, userName);

			cs.setInt(2, new Integer(clientId).intValue());
			cs.setInt(3, new Integer(isFromPrimary).intValue());

			if (compTypeList.equals("") || compTypeList == null) {
				cs.setNull(4, Types.NULL);
			} else {
				cs.setString(4, compTypeList);
			}
			if (languageIdList.equals("") || languageIdList == null) {
				cs.setNull(5, Types.NULL);
			} else {
				cs.setString(5, languageIdList);
			}
			if (searchString.equals("") || searchString == null) {
				cs.setNull(6, Types.NULL);
			} else {
				cs.setString(6, searchString);
			}
			if (isExact.equals("") || isExact == null) {
				cs.setNull(7, Types.NULL);
			} else {
				cs.setString(7, isExact);
			}
			if (counter.equals("") || counter == null) {
				cs.setNull(8, Types.NULL);
			} else {
				cs.setInt(8, new Integer(counter).intValue());
			}			
			if (createdBy.equals("") || createdBy == null) {
				cs.setNull(9, Types.NULL);
			} else {
				cs.setString(9, createdBy);
			}
			if (lastChangedBy.equals("") || lastChangedBy == null) {
				cs.setNull(10, Types.NULL);
			} else {
				cs.setString(10, lastChangedBy);
			}
			cs.setString(11, orAndParam);
			if (effectiveDate.equals("") || effectiveDate == null) {
				cs.setNull(12, Types.NULL);
			} else {
				cs.setDate(12, DateFormat.getDateOfString(effectiveDate));
			}
			if (expirationDate.equals("") || expirationDate == null) {
				cs.setNull(13, Types.NULL);
			} else {
				cs.setDate(13, DateFormat.getDateOfString(expirationDate));
			}
			cs.setInt(14, new Integer(elementInstanceId).intValue());
			cs.registerOutParameter(15, java.sql.Types.CHAR);
			cs.registerOutParameter(16, java.sql.Types.INTEGER);
			cs.registerOutParameter(17, java.sql.Types.VARCHAR);
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {
				component = new ComponentBean();
				component.setElementInstanceId(rs.getString("FELEMENTINSTANCEID"));
				component.setType(rs.getString("FELEMENT_ID"));
				component.setEffectiveDate(rs.getString("FEFFECTIVEDATE"));
				component.setExpirationDate(rs.getString("FEXPIRATIONDATE"));
				component.setName(rs.getString("FQUALDATA_DESC"));
				component.setBody(rs.getString("FELEMENT_DETAIL"));
				component.setCreatedBy(rs.getString("FCREATEDBY"));
				component.setLastUpdatedBy(rs.getString("FLASTCHANGEDBY"));
				component.setPrimaryIndicator(rs.getString("FISPRIMARY_IND"));
				component.setLocale(rs.getString("FLANGUAGEID"));
				component.setLocaleDesc(rs.getString("FLANGUAGE_DESC"));
				component.setStatusId(rs.getString("FSECONDARYLANGSTATUS"));
				component.setStatusDesc(rs.getString("SECONDARYLANGSTATUS_DESC"));
				searchResult.add(component);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || searchLinkageComponent()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting into com.dci.rest.dao.ComponentDAO || Method Name : searchLinkageComponent()");
		}
		return searchResult;
	}

	public  boolean deactivateComponentAssociation(String userId,String clientId,String componentId,String type) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : deactivateComponentAssociation ||");
		CallableStatement cs = null;
		boolean isSuccess = false;
		Connection con = null;
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
				developerLog.debug("call SP8_RTC_ITREMOVEALLUSAGE(" + userId	+ "," +clientId+ "," +  componentId+")");
			cs = con.prepareCall("{call SP8_RTC_ITREMOVEALLUSAGE(?,?,?)}");
			cs.setString(1, userId);
			cs.setInt(2, Integer.parseInt(clientId));
			cs.setString(3, componentId);
			cs.executeUpdate();
			isSuccess = true;
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || deactivateComponentAssociation()",e);
		} finally {
			releaseConStmts(null, cs, con, null);
			developerLog.debug("Exiting into com.dci.rest.dao.ComponentDAO || Method Name : deactivateComponentAssociation()");
		}
		return isSuccess;
	}
	
	public List<String> getVersionBlkLining(String user, String qualdatashadowid) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : getVersionBlkLining ||");
		CallableStatement cs = null;	
		Connection con = null;
		List<String> colAlObj = new ArrayList<String>(); 
		try {
			if(con == null || con.isClosed()) {
				con = getConnection();
			}
			developerLog.debug("call sp4_RTC_utGetXMLForBlkLining("+user+","+qualdatashadowid+",?,?)");
			cs = con.prepareCall("{call sp4_RTC_utGetXMLForBlkLining(?,?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2,CheckString.getBigDecimal(qualdatashadowid));
			cs.registerOutParameter(3,java.sql.Types.VARCHAR);
			cs.registerOutParameter(4,java.sql.Types.VARCHAR);
			cs.execute();
			String xml1 = cs.getString(3);
			String xml2 = cs.getString(4);
			colAlObj.add(xml2); //add old xml first
			colAlObj.add(xml1);	// followed by new xml
		  } catch (Exception e) {
			  new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getVersionBlkLining()",e);
		  } finally {
				releaseConStmts(null, cs, con, null);
				developerLog.debug("Exiting into com.dci.rest.dao.ComponentDAO || Method Name : getVersionBlkLining()");
		  }
		return colAlObj;
	}
	
	public List<VariableBean> getVariables(String user, String clientId, String bookInstanceId,String scope) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : getVariables ||");
		CallableStatement cs = null;	
		Connection con = null;
		ResultSet rs = null;
		List<VariableBean> variables = new ArrayList<VariableBean>();
		try {
			if (con == null || con.isClosed()){
				con = getConnection();
			}
			developerLog.debug("{call SP8_RTC_VGETVARIABLE('" + user + "'," + clientId + "," + bookInstanceId+ "," + scope + ")");
			cs = con.prepareCall("{call SP8_RTC_VGETVARIABLE(?,?,?,?)}");	
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(clientId));
			cs.setBigDecimal(3, CheckString.getBigDecimal(bookInstanceId));
			if(CheckString.isValidString(scope)) {
				cs.setInt(4, CheckString.getInt(scope));
			}else {
				cs.setBigDecimal(4, null);
			}
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {
				if(!CheckString.isValidString(bookInstanceId)) {
					VariableBean variable = new VariableBean();
					variable.setId(rs.getInt("FVARIABLEID"));
					variable.setName(rs.getString("FAVARIABLENAME"));
					variable.setDescription(rs.getString("FAVARIABLE_DESCRIPTION"));
					variable.setScope(rs.getInt("FSCOPEID"));
					variable.setDisplayType(rs.getString("FDISPLAYTYPEID"));
					variable.setUsedCount(rs.getInt("VAR_USED_COUNT"));
					variable.setCreatedBy(rs.getString("FCREATEDBY"));
					variable.setUpdatedBy(rs.getString("FLASTCHANGEDBY"));
					variable.setSystemVar(rs.getInt("FISUSERDEFINED")+"");
					SelectBean select = new SelectBean();
					if (variable.getScope() ==1 || variable.getScope() ==0 || variable.getScope() ==3 || variable.getScope() ==2){
						select.setId("-00");
					}else{
						select.setId("00");
					}
					select.setValue("-None-");
					variable.getFundOptions().add(select);
					variable.getClassOptions().add(select);
					variable.setStyleOptions(getVariableStyleObject());
					variables.add(variable);
				}				
			}
		} catch (Exception e) {
			 new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getVariables()",e);
		}finally {
			releaseConStmts(rs, cs, con,null);
			developerLog.debug("Exiting into com.dci.rest.dao.ComponentDAO || Method Name : getVariables()");
		}
		return variables;
	}
	
	private List<SelectBean> getVariableStyleObject(){
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : getVariableStyleObject ||");
		List<SelectBean> sytleObject = new ArrayList<SelectBean>();	 	
		SelectBean select = null;
		select =new SelectBean();
		select.setId("default");
		select.setValue("default");
		select.setSelected(true);
		sytleObject.add(select);
		select =new SelectBean();
		select.setId("lcase");
		select.setValue("Lower Case");
		sytleObject.add(select);
		select =new SelectBean();
		select.setId("ucase");
		select.setValue("Upper Case");
		sytleObject.add(select);
	 	return sytleObject;
	 }
	public List<ComponentBean> getImages(String user, String clientId, String type, String documentId, String effdate, String expdate,String context,String elementInstId) {
		CallableStatement cs = null;	
		Connection con = null;
		ResultSet rs = null;
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : getImages ||");
		List<ComponentBean> images = new ArrayList<ComponentBean>();
		try {
			if (con == null || con.isClosed()){
				con = getConnection();
			}
			developerLog.debug("{call sp7_RTC_itGetGlobalElementQualList('" + user + "'," + clientId + "," + type + "," + documentId + ","+effdate+","+expdate+","+context+","+elementInstId + ")}");
			cs = con.prepareCall("{call sp7_RTC_itGetGlobalElementQualList(?,?,?,?,?,?,?,?)}");
			cs.setString(1, user);
			cs.setInt(2, new Integer(clientId).intValue());
			cs.setString(3, type);
			cs.setBigDecimal(4, CheckString.getBigDecimal(documentId));
			cs.setNull(5, Types.NULL);
			cs.setNull(6, Types.NULL);
			if(CheckString.isValidString(context)) {
				cs.setBigDecimal(7, CheckString.getBigDecimal(context));
			}else {
				cs.setBigDecimal(7, null);
			}
			if(CheckString.isValidString(elementInstId)) {
				cs.setBigDecimal(8, CheckString.getBigDecimal(elementInstId));
			}else {
				cs.setBigDecimal(8, null);
			}
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {
				ComponentBean image = new ComponentBean();
				image.setElementInstanceId(rs.getString("FElementInstanceId"));
				image.setType(rs.getString("FElement_Id"));
				image.setQualData_Id(rs.getString("FQualData_Id"));
				image.setQualDataCon(rs.getString("FQualData_Cond"));
				image.setBody(rs.getString("FQualData"));
				image.setName(rs.getString("FQualData_Desc"));
				image.setQualDataId(CheckString.getStringBDValue(rs.getBigDecimal("FQualDataId")));
				image.setEffectiveDate(DateFormat.expandedDate(rs.getDate("fEffectiveDate")));
				image.setExpirationDate(DateFormat.expandedDate(rs.getDate("fExpirationDate")));
				images.add(image);
			}
		} catch (Exception e) {
			 new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getImages()",e);
		}finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting into com.dci.rest.dao.ComponentDAO || Method Name : getImages()");
		}
		return images;
	}
	
	public List<StyleBean> getInlineStyles(String user, String clientId, String compType) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : getInlineStyles ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		ArrayList<StyleBean> styleList = new ArrayList<StyleBean>();
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			developerLog.debug("call SP8_RTC_GETINLINESTYLES('" + user + "'," + clientId + "," + compType + ")");
			cs = con.prepareCall("{call SP8_RTC_GETINLINESTYLES(?,?,?)}");
			cs.setString(1, user);
			cs.setInt(2, new Integer(clientId).intValue());
			if (compType == null) {
				cs.setNull(3, Types.NULL);
			} else {
				cs.setString(3, compType);
			}
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {
				StyleBean styleObj = new StyleBean();
				styleObj.setType(rs.getString("FELEMENT_ID"));
				styleObj.setStyleName(rs.getString("FSTYLE_NAME"));
				styleObj.setInternalValue(rs.getString("FINTERNALVALUE"));
				styleList.add(styleObj);
			}
		} catch (Exception e) {
			 new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getInlineStyles()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting into com.dci.rest.dao.ComponentDAO || Method Name : getInlineStyles()");
		}
		return styleList;
	}
	
	public List<ComponentBean> getFavorites(String userid, String deptid, int bookInstanceId, int favoriteLimit) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : getFavorites ||");
		List<ComponentBean> searchresultlist = new ArrayList<ComponentBean>();
		CallableStatement cs = null;
		ResultSet rs = null;
		Connection con = null;
		ComponentBean searchresult = new ComponentBean();

		try {
			if (con == null || con.isClosed())
				con = getConnection();
			cs = con.prepareCall("{CALL SP7_RTC_FGETCOMPFAVORITES (?,?,?,?)}");
			developerLog.debug("{ SP7_RTC_FGETCOMPFAVORITES('" + userid + "',"
					+ deptid + "," + bookInstanceId + "," + favoriteLimit
					+ ")}");
			int deptid2 = CheckString.getInt(deptid);
			cs.setString(1, userid);
			cs.setInt(2, deptid2);
			if (bookInstanceId != 0) {
				cs.setInt(3, bookInstanceId);
			} else {
					cs.setNull(3, Types.NULL);
			}
			cs.setInt(4, favoriteLimit);
			rs = cs.executeQuery();
			while (rs.next()) {
				if(bookInstanceId == 0){
					searchresult.setElementInstanceId(rs.getString("FELEMENTINSTANCEID"));
					searchresult.setType(rs.getString("FELEMENT_ID"));
					searchresult.setName(rs.getString("FQUALDATA_DESC"));
					searchresult.setEffectiveDate(rs.getString("FEFFECTIVEDATE"));
					searchresult.setExpirationDate(DateFormat.expandedDate(rs.getDate("FEXPIRATIONDATE")));
					searchresult.setContext(rs.getString("FELEMENTCONTEXT_DESC"));
					searchresult.setBody(rs.getString("FELEMENT_DETAIL"));
					searchresult.setCreatedBy(rs.getString("FCREATEDBY"));
					searchresult.setLastUpdatedBy(rs.getString("FLASTCHANGEDBY"));
					searchresult.setStatusId(rs.getString("FCOMPSTATUS"));
					searchresult.setStatusDesc(rs.getString("FCOMPSTATUS_DESC"));
					searchresult.setLocale(rs.getString("FLANGUAGEID"));
					searchresult.setLocaleDesc(rs.getString("FLANGUAGE"));
					searchresult.setLocaleInd(rs.getString("FPRIMARYLINK"));
					searchresultlist.add(searchresult);
				}else{
					searchresult.setElementInstanceId(rs.getString("FELEMENTINSTANCEID"));// element id
					searchresult.setType(rs.getString("FELEMENT_ID"));// element name
					searchresult.setName(rs.getString("FQUALDATA_DESC"));// element diplay name
					searchresult.setAddmore(rs.getString("FADDMORE"));// the Add moew
					searchresult.setBody(rs.getString("FELEMENT_DETAIL"));
					searchresult.setEffectiveDate(rs.getString("FEFFECTIVEDATE"));
					searchresult.setExpirationDate(rs.getString("FEXPIRATIONDATE"));
					searchresult.setfHeaderFooterInd(rs.getString("FHEADERFOOTERIND"));
					searchresult.setLocale(rs.getString("FLANGUAGEID"));
					searchresult.setLocaleDesc(rs.getString("FLANGUAGE"));
					searchresult.setLocaleInd(rs.getString("FPRIMARYLINK"));
					searchresultlist.add(searchresult);
				}
			}
		} catch (Exception e) {
			 new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getFavorites()",e);
		} finally {
			releaseConStmts(rs, cs, con,null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : getFavorites()");
		}
		return searchresultlist;
	}
	
	public Map<String, String> createUserFilter(String user,String client,Filter filter, String criteria,int searchType) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : createUserFilter ||");
		CallableStatement cs = null;
		Connection con = null;
		Map<String, String> status = new HashMap<String, String>();
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			developerLog.debug("Call SP7_RTC_SAVECOMPSEARCH('"+user+"',"+client+",'"+filter.getName()+"','"+filter.getDescription()+"','"+criteria+"',"+searchType+","+(filter.getPublicFlag().equalsIgnoreCase("true")?1:0)+",?,?)");
			cs = con.prepareCall("{call SP7_RTC_SAVECOMPSEARCH(?,?,?,?,?,?,?,?,?)}");
			cs.setString(1, user);
			cs.setInt(2, CheckString.getInt(client));
			cs.setString(3, filter.getName());
			cs.setString(4, filter.getDescription());
			cs.setString(5,criteria);
			cs.setInt(6, searchType);
			cs.setInt(7, filter.getPublicFlag().equalsIgnoreCase("true")?1:0);
			cs.registerOutParameter(8, Types.INTEGER);
			cs.registerOutParameter(9, Types.VARCHAR);
			cs.executeUpdate();
			status.put("id", cs.getString(8));
			status.put("statusMessage", cs.getString(9));
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || createUserFilter()",e);
		} finally {
			releaseConStmts(null, cs, con, "com.dci.rest.dao.ComponentDAO || Method Name : createUserFilter()");
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : createUserFilter()");
		}
		return status;
	}
	public String updateFilterSearch(String user, String userId, String deptId, Filter filter,String criteriaVal,int searchType) throws Exception {
		CallableStatement cs = null;
		Connection con = null;
		MDC.put("category","com.dci.db4.dao.DashboardDAO");
		String message = "success";
		try{
			con = getConnection();
			developerLog.debug("CALL SP7_RTC_UPDATECOMPSEARCH('"+user+"',"+deptId+","+filter.getId()+",'"+filter.getName()+"','"+filter.getDescription()+"','"+criteriaVal+"',"+searchType+","+(filter.getPublicFlag().equalsIgnoreCase("true")?1:0)+",?)");
			cs = con.prepareCall("{CALL SP7_RTC_UPDATECOMPSEARCH(?,?,?,?,?,?,?,?,?)}");
			cs.setString(1, user);
			cs.setInt(2, CheckString.getInt(deptId));
			cs.setInt(3, CheckString.getInt(filter.getId()));
			cs.setString(4, filter.getName());
			cs.setString(5, filter.getDescription());
			cs.setString(6, criteriaVal);
			cs.setInt(7, searchType);
			cs.setInt(8, filter.getPublicFlag().equalsIgnoreCase("true")?1:0);
			cs.registerOutParameter(9, Types.VARCHAR);
			cs.executeUpdate();
			message = cs.getString(9);	
			developerLog.debug("Filter Update Message--->"+message);
		
		
		}catch (Exception e){
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || updateFilterSearch()",e);
		}finally{
			releaseConStmts(null, cs, con, "com.dci.db4.dao.DashboardDAO || MethodName:updateFilterSearch()");
		}
		return message;
	}

	public String removeFavoriteLibrary(String userId, int clientId,String selectedComponents) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		String message = "";
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : removeFavoriteLibrary() ||");
		CallableStatement cs = null;
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP5_RTC_FDELETECOMFAVORITES  (" + userId
					+ "," + clientId + "," + selectedComponents + ")");
			cs = con.prepareCall("{call SP5_RTC_FDELETECOMFAVORITES(?,?,?)}");
			cs.setString(1, userId);
			cs.setInt(2, clientId);
			cs.setString(3, selectedComponents);
			cs.executeUpdate();
			message = "sucess";
		} catch (Exception e) {
			message = "error";
			e.printStackTrace();
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || removeFavoriteLibrary()",e);
		} finally {
			releaseConStmts(null, cs, con, null); 
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : removeFavoriteLibrary()");
		}
		return message;
    }
	public List<Tag> getComponentTags(String user, String clientId,String elementInstId) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : getComponentTags() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		ArrayList<Tag> tagList = new ArrayList<Tag>();
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP8_RTC_GETELEMENTELEMENTTAG('" + user + "',"+ clientId + ","+elementInstId+")}");
			cs = con.prepareCall("{call SP8_RTC_GETELEMENTELEMENTTAG(?,?,?)}");
			cs.setString(1, user);	
			cs.setInt(2, new Integer(clientId).intValue());
			if(CheckString.isNumericValue(elementInstId)){
				cs.setBigDecimal(3,CheckString.getBigDecimal(elementInstId));				
			}else{
				cs.setNull(3, Types.NULL);
			}			
			rs = cs.executeQuery();
			while (rs.next()) {
				Tag tag = new Tag();
				tag.setId(CheckString.getInt(rs.getString("FTAGID")));
				tag.setName(rs.getString("FTAG_DESC"));
				tag.setSelected(rs.getString("FUSEDIND").equals("1")?true:false);
				if(CheckString.isNumericValue(elementInstId) && !tag.isSelected()){
					continue;
				}
				tagList.add(tag);
			}			
		} catch (Exception e) {			
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getComponentTags()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : getComponentTags()");
		}
		return tagList;
	}
	public String createTag(String user, String clientId,String tagName) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		CallableStatement cs = null;
		String message="";
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : createTag() ||");
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP8_RTC_CREATEELEMENTTAG('" + user + "',"+ clientId + ",'"+tagName+"','Y',?,?)}");
			cs = con.prepareCall("{call SP8_RTC_CREATEELEMENTTAG(?,?,?,?,?,?)}");
			cs.setString(1, user);	
			cs.setInt(2, new Integer(clientId).intValue());
			cs.setString(3, tagName);
			cs.setString(4, "Y");
			cs.registerOutParameter(5, java.sql.Types.BIGINT);
			cs.registerOutParameter(6, java.sql.Types.VARCHAR);					
			cs.executeUpdate();
			message = cs.getString(5);	
		} catch (Exception e) {			
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || createTag()",e);
		} finally {
			releaseConStmts(null, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : createTag()");
		}
		return message;
	}
	
	public List<Filter> getUserFilters(String user, String client, int filterType) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<Filter> userFilters = new ArrayList<Filter>();
		Filter filter = null;
		ObjectMapper mapper = null;
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : getUserFilters() ||");
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP7_RTC_GETCOMPSEARCHLIST('" + user + "'," + filterType + "," + client + ")}");
			cs = con.prepareCall("{call SP7_RTC_GETCOMPSEARCHLIST(?,?,?)}");
			cs.setString(1, user.toLowerCase());
			cs.setInt(2, filterType);
			cs.setInt(3, CheckString.getInt(client));			
			rs = cs.executeQuery();
			while(rs.next()) {
				filter = new Filter();
				mapper = new ObjectMapper();
				filter.setId(rs.getString("fsearchid"));
				filter.setName(rs.getString("fsearch_name"));
				filter.setDescription(rs.getString("fsearch_desc"));
				filter.setCreator(rs.getString("fcreatedby"));
				filter.setPublicFlag(rs.getString("fsearch_status").equals("1")?"true":"false");
				filter.setCriteria(mapper.readValue(rs.getString("fsearch_data"), SearchCriteria.class));
				userFilters.add(filter);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getUserFilters()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : getUserFilters()");
		}
		return userFilters;
	}
	
	public String createSecondaryComponent(String userId, String clientId, ComponentBean component) throws Exception {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("com.dci.rest.dao.ComponentDAO || Method Name : createSecondaryComponent() ||");
		Connection con = null;
		CallableStatement cs = null;
		String secondaryComponentInstanceId = null;
		String primaryComponentId = component.getPrimaryComponentId();
		String displayName = component.getName();
		String effectiveDate = component.getEffectiveDate();
		String expirationDate = component.getExpirationDate(); 
		String langId = component.getLocale();
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			developerLog.debug("{ call SP7_RTC_TRCREATE2NDCOMP ('" + userId + "'," + clientId + "," + primaryComponentId + "," + displayName + ",'" + effectiveDate + "','" + expirationDate
							+ "','" + langId + "')}");
			cs = con.prepareCall("{call SP7_RTC_TRCREATE2NDCOMP (?,?,?,?,?,?,?,?)}");
			cs.setString(1, userId);
			cs.setInt(2, new Integer(clientId).intValue());
			cs.setBigDecimal(3, CheckString.getBigDecimal(primaryComponentId));
			cs.setString(4, displayName);
			if ( effectiveDate == null) {
				cs.setNull(5, Types.NULL);
			} else {
				cs.setDate(5, DateFormat.getDateOfString(effectiveDate));
			}
			if (expirationDate == null) {
				cs.setNull(6, Types.NULL);
			} else {
				cs.setDate(6, DateFormat.getDateOfString(expirationDate));
			}
			cs.setInt(7, Integer.parseInt(langId));
			cs.registerOutParameter(8, java.sql.Types.BIGINT);
			cs.executeUpdate();
			long secondaryComponentInstanceIdILong = cs.getLong(8);
			if (secondaryComponentInstanceIdILong > 0) {
				secondaryComponentInstanceId = String.valueOf(secondaryComponentInstanceIdILong);
				developerLog.debug("secondaryComponentInstanceId is || Method Name : createSecondaryComponent() || "+ secondaryComponentInstanceId);
			}

			
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || createSecondaryComponent()",e);
		} finally {
			releaseConStmts(null, cs, con,null);
		}
		return secondaryComponentInstanceId;
	}


	public List<TableType> getTableTypes(String user, String client, String booktype) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("com.dci.rest.dao.ComponentDAO || Method Name : getTableTypes() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs =null;
		List<TableType> tableTypes = new ArrayList<TableType>();
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			developerLog.debug("{ call SP_RTC_EXGETTABLEQUALLIST('" + user + "'," + client + "," + booktype+ ")}");
			cs = con.prepareCall("{call SP_RTC_EXGETTABLEQUALLIST (?,?,?)}");
			cs.setString(1, user);
			cs.setInt(2, CheckString.getInt(client));
			if(CheckString.isValidString(booktype)) {
				cs.setString(3,booktype);
			}else {
				cs.setNull(3, Types.NULL);
			}
			rs = cs.executeQuery();
			while (rs.next()) {
				TableType tableType = new TableType();
				tableType.setId(rs.getInt("ftableidentityid"));
				tableType.setInternalName(rs.getString("fqualdata_tabletype"));
				tableType.setDisplayName(rs.getString("FBOOKTYPE_DISPLAY"));
				tableType.setName(rs.getString("ftable_description"));
				tableType.setDocType(rs.getString("fbook_type"));
				tableType.setFlexiColInd(rs.getString("flexcol_ind"));
				tableType.setHeaderFooterInd(rs.getString("fheaderfooterind"));
				tableTypes.add(tableType);
			}
			
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getTableTypes()",e);
		}finally {
			releaseConStmts(rs, cs, con,null);
		}
		return tableTypes;
	}
	public List<SelectBean> getElementStylesList(String userId,String clientId, String strElementTypeId,String strElementAtt,String strTableType,String bookInstanceId,String strElementType) 
	{
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		List<SelectBean> elementStyleList = new ArrayList<SelectBean>();
		SelectBean elementStyleObj;
		Connection con = null;
		CallableStatement cs = null;		  
		ResultSet rs = null;
		developerLog.debug("Entering in to com.dci.rest.dao.ComponentDAO || Method Name : getElementStylesList() ||");
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			
			developerLog.debug("{CALL SP_RTC_GETELEMENTSTYLE("+userId+","+clientId+","+strElementTypeId+","+strElementAtt+","+strTableType+","+bookInstanceId+","+strElementType+")}");
			cs = con.prepareCall("{CALL SP_RTC_GETELEMENTSTYLE(?,?,?,?,?,?,?)}");
			cs.setString(1, userId);
			cs.setInt(2, CheckString.getInt(clientId));
			cs.setString(3, strElementTypeId);
			cs.setString(4, strElementAtt);
			cs.setString(5, strTableType);
			cs.setString(6, bookInstanceId);
			cs.setString(7, strElementType);
			rs = cs.executeQuery();
			while(rs.next()) {
				elementStyleObj  = new SelectBean();
				elementStyleObj.setId(rs.getString("FSTYLE"));
				elementStyleObj.setValue(rs.getString("FSTYLE_DESCRIPTION"));
				elementStyleList.add(elementStyleObj);
			}
		}catch (SQLException e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getElementStylesList()",e);
		} finally {
			releaseConStmts(rs, cs, con,null);
		}
		return elementStyleList;
	}
	
	public String setComponentStatus(String userId, String compId, String compStatus, String reasonCode, String comment) {
		MDC.put("category", "com.dci.rest.dao.ComponentDAO");
		developerLog.warn("Entering into com.dci.rest.dao.ComponentDAO || Method Name : setComponentStatus() ||");
		Connection con = null;
		CallableStatement cs = null;
		String returnString = null;
		//			ResultSet rs = null;
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			developerLog
					.debug("{ call SP7_RTC_TRSETCOMPSTATUS ('" + userId + "',"
							+ compId + "," + compStatus + ","
							+ reasonCode + ",'" + comment + "')}");
			cs = con.prepareCall("{call SP7_RTC_TRSETCOMPSTATUS(?,?,?,?,?,?,?,?)}");
			cs.setString(1, userId);			
			cs.setBigDecimal(2, CheckString.getBigDecimal(compId));
			cs.setInt(3, Integer.parseInt(compStatus));
			cs.setString(4, reasonCode);
			cs.setString(5, comment);			
			cs.registerOutParameter(6, java.sql.Types.CHAR);
			cs.registerOutParameter(7, java.sql.Types.INTEGER);
			cs.registerOutParameter(8, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			returnString = cs.getString(8);
			//developerLog.warn("cs.getString(6)=========" + cs.getString(6));
			//developerLog.warn("cs.getInt(7)===========" + cs.getInt(7));
			//developerLog.warn("cs.getString(8)========" + cs.getString(8));
		}catch (SQLException e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || setComponentStatus()",e);
		} finally {
			releaseConStmts(null, cs, con,null);
		}
		return returnString;
	}
	public ArrayList<String> createEditContext(String user,String clientId,String contextName,String contextId){		
		Connection con = null;
		CallableStatement cs = null;
		ArrayList<String> result =new ArrayList<String>(2);
		try {
			if(con == null || con.isClosed())con = getConnection();
		  	developerLog.debug("call SP8_RTC_PUTELEMENTCONTEXT('" + user + "',"+ clientId + ",'" + contextName + "',"+contextId+",?,?)");
			cs = con.prepareCall("{call SP8_RTC_PUTELEMENTCONTEXT(?,?,?,?,?,?)}");
			cs.setString(1, user);
			cs.setInt(2, new Integer(clientId).intValue());
			cs.setString(3, contextName);
			if(contextId == null){
				cs.setNull(4, Types.NULL);
			}else{
				cs.setBigDecimal(4,CheckString.getBigDecimal(contextId));
			}
			cs.registerOutParameter(5, Types.BIGINT);
			cs.registerOutParameter(6, Types.VARCHAR);
			cs.execute();
			result.add(0,cs.getInt(5)+"");
			result.add(1,cs.getString(6));
		}catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || createEditContext()",e);
	    }finally{
	    	releaseConStmts(null, cs, con,null);
	    }
	    return  result;
	}
	
	public  List<ComponentBean> getGlobalElementQualListContext(String user, String clientId, String elementId, String documentId,
			String effdate, String expdate, String context,String elementInstId) {
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<ComponentBean> footnoteList = new ArrayList<ComponentBean>();
		try {
			if (con == null || con.isClosed()){
				con = getConnection();
			}
	
			developerLog.debug("{call sp7_RTC_itGetGlobalElementQualList("
					+ user + "," + clientId + "," + elementId + ","
					+ documentId + ",null,null" + "," + context +","+elementInstId+ ")}");
			cs = con.prepareCall("{call sp7_RTC_itGetGlobalElementQualList(?,?,?,?,?,?,?,?)}");
			cs.setString(1, user);
			cs.setInt(2, new Integer(clientId).intValue());
			cs.setString(3, elementId);
			cs.setBigDecimal(4, CheckString.getBigDecimal(documentId));
			cs.setNull(5, Types.NULL);
			cs.setNull(6, Types.NULL);
			cs.setBigDecimal(7, CheckString.getBigDecimal(context));
			cs.setBigDecimal(8, CheckString.getBigDecimal(elementInstId));
		
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {

				String elemType = rs.getString(3);
				StringBuffer url = new StringBuffer("");

				StringBuffer data = new StringBuffer("");
				ComponentBean footnoteBean = new ComponentBean();
				footnoteBean.setElementInstanceId(CheckString.getStringBDValue(rs.getBigDecimal("FElementInstanceId")));
				footnoteBean.setType(rs.getString("FElement_Id"));
				footnoteBean.setQualData_Id(rs.getString("FQualData_Id"));
				footnoteBean.setQualDataCon(rs.getString("FQualData_Cond"));
				footnoteBean.setBody(rs.getString("FQualData"));
				footnoteBean.setName(rs.getString("FQualData_Desc"));
				footnoteBean.setQualDataId(CheckString.getStringBDValue(rs.getBigDecimal("FQualDataId")));
				footnoteBean.setEffectiveDate(DateFormat.expandedDate(rs.getDate("fEffectiveDate")));
				footnoteBean.setExpirationDate(DateFormat.expandedDate(rs.getDate("fExpirationDate")));
				footnoteBean.setFEffective_Ind(rs.getString("FEFFECTIVE_IND"));
				if (footnoteBean.getType().equals("graphic")) {
					String qualdata = CheckString.getAttributeValueOfTag(footnoteBean.getBody(), "graphic", "fileref");
					qualdata = CheckString.replaceStringX(qualdata, "file:", "");
					footnoteBean.setAttachment("openpdf('" + qualdata + "')");
				}
				footnoteBean.setFootnoteMapping(rs.getString("FMARKER"));
				footnoteList.add(footnoteBean);
			}
			System.out.println(footnoteList.size());
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getGlobalElementQualListContext()",e);
		} finally {
			releaseConStmts(null, cs, con,null);
		}
		return footnoteList;
	}
	public  List<FootNoteAttributes> getFootnoteDetails(String user,String clientId,String documentId) {
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		String fattribute = "";
		List<FootNoteAttributes> finallist = new ArrayList<FootNoteAttributes>();
		Order orderObj;Placement placementObj;Style style;
		List<Order> orderList = new ArrayList<Order>();
		List<Placement> placementList = new ArrayList<Placement>();
		List<Style> styleList = new ArrayList<Style>();
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			 
			developerLog.debug("{CALL sp4_RTC_FGETFOOTNOTEATTRIBUTE ( "+user+" , "+clientId+", "+documentId+" )}");
			cs = con.prepareCall("{CALL SP4_RTC_FGETFOOTNOTEATTRIBUTE (?,?,?)}");
			cs.setString(1, user);
			cs.setInt(2,new Integer(clientId).intValue());
			cs.setString(3,documentId);
			rs = cs.executeQuery();
			
		
			while(rs != null && rs.next()){
				FootNoteAttributes f_att = new FootNoteAttributes();
				fattribute = rs.getString(3);
				if((fattribute.equals("Order"))||(fattribute=="Order")){
					orderObj = new Order();
					orderObj.setId(rs.getString(5));
					orderObj.setValue(rs.getString(4));
					orderList.add(orderObj);
				}
				if((fattribute.equals("Placement"))||(fattribute=="Placement")){
					placementObj = new Placement();
					placementObj.setId(rs.getString(5));
					placementObj.setValue(rs.getString(4));
					placementList.add(placementObj);
					}
				if((fattribute.equals("Style"))||(fattribute=="Style")){
					style = new Style();
					style.setId(rs.getString(5));
					style.setValue(rs.getString(4));
					styleList.add(style);
				}	
				f_att.setFbooktype(rs.getString(2));
				f_att.setOrder(orderList);
				//f_att.setOrder_attr(order_attr);
				f_att.setPlacement(placementList);
				//f_att.setPlacement_attr(placement_attr);
				f_att.setStyle(styleList);
				//f_att.setStyle_attr(style_attr);
				finallist.add(f_att);	
			}
		} 
		catch(Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getFootnoteDetails()",e);
		} finally {
			releaseConStmts(null, cs, con,null);
		}
		return finallist;
	}


	public List<Fund> getFundListOnAssetClass(String user, String clientId, String assetClassIds){
		List<Fund> fundList = new ArrayList<Fund>();
		boolean local = true;
		Connection con = null;
		CallableStatement cs = null;		  
		ResultSet rs = null;
		 try {
			BigDecimal bd = null;
			if (con == null || con.isClosed())
				con = getConnection();
			else
				local = false;
			developerLog.debug("{CALL SP4_RTC_GETFUNDSBYASSETCLASS('"+user+"',"+clientId+",'"+assetClassIds+"')}");
			cs = con.prepareCall("{CALL SP4_RTC_GETFUNDSBYASSETCLASS(?,?,?)}");
			cs.setString(1, user);	
			cs.setInt(2, new Integer(clientId).intValue());
			cs.setString(3,assetClassIds);
			rs = cs.executeQuery();
			while(rs.next()) {
				Fund mb = new Fund();
				mb.setId(rs.getInt(1));
				mb.setsId(rs.getInt(1)+"*"+rs.getString(2));
				mb.setDescription(rs.getString(3));
				mb.setValue(mb.getId()+"");
				mb.setLabel(mb.getDescription());	
				mb.setName(rs.getString(3));
				mb.setAssetClassId(rs.getInt(4)+"");
				mb.setAssetClassName(rs.getString("FCONTENTCATEGORY_DESCRIPTION"));		
				fundList.add(mb);
			}			
		 } catch (SQLException e) {
				new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getFundListOnAssetClass()",e);
			} finally {
				releaseConStmts(null, cs, con,null);
			}

	   return fundList;		
	}
	public  int checkForDuplicateFootnoteId(String user, String clientId,  String footnoteId){
		MDC.put("category", "com.dci.rest.dao.ComponentDAO");
		CallableStatement cs=null;
		Connection con = null;
		int return_code= 0;		
			try{
				if(con == null || con.isClosed())
					con = getConnection();
			cs = con.prepareCall("{call SP_RTC_ITASSOCIATE_ELEMENT2MARKER(?,?,?,?,?)}");
			developerLog.debug("call SP_RTC_ITASSOCIATE_ELEMENT2MARKER("+user+ ", " +clientId+ ", 0, " +footnoteId+ " )" );
			cs.setString(1, user);
			cs.setString(2, clientId);		
			cs.setInt(3,0);
			cs.setString(4,footnoteId);
			cs.registerOutParameter(5,java.sql.Types.INTEGER);
			cs.executeUpdate();
			return_code = cs.getInt(5);
			} catch (Exception e) {
				new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || checkForDuplicateFootnoteId()",e);
			 
			}finally {
				releaseConStmts(null, cs, con,null);
		}
			return return_code;
	}
	
	public  String setComponentAssignee(String user, String clientId, String elementInstanceId,ComponentBean component){
		MDC.put("category", "com.dci.rest.dao.ComponentDAO");
		CallableStatement cs=null; //You can change default Status of component as per requirement
		Connection con = null;
		String return_code = null;	
		String ownerName=component.getOwner();
			try{
				if(con == null || con.isClosed())
					con = getConnection();
			cs = con.prepareCall("{call SP8_RTC_ITPUTELEMENT2USER  (?,?,?,?,?,?,?)}");
			developerLog.debug("call SP8_RTC_ITPUTELEMENT2USER  ("+user+ ", " +clientId +","+elementInstanceId+ ","+component.getWorkFlowStatusId()+","+component.getFcomp_assignee()+ ","+ownerName+",? )" );
			cs.setString(1, user);
			cs.setInt(2, CheckString.getInt(clientId));		
			cs.setInt(3,CheckString.getInt(elementInstanceId));
			cs.setInt(4,CheckString.getInt(component.getWorkFlowStatusId()));
			cs.setString(5,component.getFcomp_assignee());
			cs.setString(6,ownerName);
			cs.registerOutParameter(7,Types.VARCHAR);
			cs.executeUpdate();
			return_code = cs.getString(7);
			} catch (Exception e) {
				new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || setComponentAssignee()",e);
			 
			}finally {
				releaseConStmts(null, cs, con,null);
		}
			return return_code;
	}
	public List<ComponentStatus> getComponentStatusList(String user,String client,String statusId,String schemaId) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : getComponentStatusList() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		ComponentStatus compStatus;
		List<ComponentStatus> statusList = new ArrayList<ComponentStatus>();
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			developerLog.debug("{call SP8_RTC_GETSTATUS("+user+","+client+","+statusId+","+schemaId+")}");
			cs = con.prepareCall("{call SP8_RTC_GETSTATUS (?,?,?,?)}");	
			cs.setString(1, user);		
			cs.setInt(2,CheckString.getInt(client));
			cs.setBigDecimal(3, CheckString.getBigDecimal(statusId));
			cs.setBigDecimal(4, CheckString.getBigDecimal(schemaId));
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {
				compStatus = new ComponentStatus();
				compStatus.setId(rs.getInt("FCOMPSTATUS"));
				compStatus.setName(rs.getString("FCOMP_STATUS"));
				//compStatus.setsId(CheckString.getInt(rs.getString("FSTATUSID")));
				statusList.add(compStatus);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getComponentStatusList()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : getComponentStatusList() ||");
		}

		return statusList;
	}
	public ArrayList<UserBean> getCreatedUpdatedBy(String userid, String clientId) throws Exception {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : getCreatedUpdatedBy() ||");
		CallableStatement cs = null;
		ResultSet rs = null;
		Connection con = null;
		ArrayList<UserBean> userlist = new ArrayList<UserBean>();
			try {
			if (con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call  SP5_RTC_GETUSERLIST(" + userid + "," + clientId + ")}");
			cs = con.prepareCall("{call SP5_RTC_GETUSERLIST(?,?)}");
			cs.setString(1, userid);
			cs.setInt(2, CheckString.getInt(clientId));
			rs = cs.executeQuery();
			 UserBean slb;
				while (rs != null && rs.next()) {
					slb = new UserBean();
					slb.setUserName(rs.getString("FUSERID"));
					slb.setUserId(rs.getString("FUSERID"));
					if ((rs.getString("FCREATE_IND") != null) && (rs.getString("FCREATE_IND").equals("1"))) {
					slb.setCreatedbyInd(true);
					}
					if ((rs.getString("FMODIFIED_IND") != null) && (rs.getString("FMODIFIED_IND").equals("1"))) {
					slb.setUpdatedbyInd(true);
					}
						userlist.add(slb);
				}
		} catch (SQLException e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getCreatedUpdatedBy()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : getCreatedUpdatedBy() ||");
		}
		return userlist;
	}
	
	
	public ArrayList<UserBean> getAuthUsers(String userid, String clientId) throws Exception {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : getAuthUsers() ||");
		CallableStatement cs = null;
		ResultSet rs = null;
		Connection con = null;
		ArrayList<UserBean> userlist = new ArrayList<UserBean>();
			try {
			if (con == null || con.isClosed())
				con = getAuthConnection();
			developerLog.debug("{call  SP7_RTC_DSGETUSERLIST(" + userid + "," + appId + "," + clientId + ")}");
			cs = con.prepareCall("{call SP7_RTC_DSGETUSERLIST(?,?,?)}");
			cs.setString(1, userid);
			cs.setInt(2, CheckString.getInt(appId));
			cs.setInt(3, CheckString.getInt(clientId));
			rs = cs.executeQuery();
			 UserBean slb;
				while (rs != null && rs.next()) {
					slb = new UserBean();

					slb.setUserName(rs.getString("FUSER_ID"));
					slb.setUserId(rs.getString("FUSER_ID"));
					if(rs.getString("FSTATUS_DESC").equals("Active")) {
						userlist.add(slb);
					}
				}
		} catch (SQLException e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getAuthUsers()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : getAuthUsers() ||");
		}
		return userlist;
	}

	public Map<String, String> bulkdataUpdate(String userid, String clientId,List<ComponentBean> componentBeanList) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : bulkdataUpdate() ||");
		CallableStatement cs = null;
		Connection con = null;
		String status = "";
		String shadowId = "";
		Map<String,String> editCompStatusMap = new HashMap<String,String>();
		try {
				if (con == null || con.isClosed()){
					con = getConnection();
				}	
				for (ComponentBean component : componentBeanList) {
					if(CheckString.isValidString(component.getLocale())|| CheckString.isValidString(component.getContext())|| CheckString.isValidString(component.getEffectiveDate())|| CheckString.isValidString(component.getExpirationDate())|| CheckString.isValidString(component.getFcomp_assignee()) ||CheckString.isValidString(component.getWorkFlowStatusId())) {

				developerLog.debug("CALL SP8_RTC_ITUPDATEELEMENT('"+userid+"',"+clientId+","+component.getElementInstanceId()+",'"+component.getName()+"','"+component.getBody()+"',"+component.getLocale()+","+component.getContext()+","+DateFormat.getDateOfString(component.getEffectiveDate())+","+DateFormat.getDateOfString(component.getExpirationDate())+","+CheckString.getBigDecimal(component.getBookInstanceId())+","+CheckString.getBigDecimal(component.getBookDetailId())+",'"+component.getFootnoteMapping()+"','"+component.getTableType()+"','"+component.getWorkFlowStatusId()+","+component.getFcomp_assignee()+","+component.getParentContextId()+","+Types.NULL+",?,?)");
				cs = con.prepareCall("{call SP8_RTC_ITUPDATEELEMENT(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
				cs.setString(1, userid);
				cs.setInt(2, CheckString.getInt(clientId));
				cs.setBigDecimal(3, CheckString.getBigDecimal(component.getElementInstanceId()));
				cs.setString(4, component.getName());
				cs.setString(5, component.getBody());
				cs.setString(6, component.getLocale());
				cs.setString(7, component.getContext());
				
				cs.setDate(8, DateFormat.getDateOfString(component.getEffectiveDate()));
				cs.setDate(9, DateFormat.getDateOfString(component.getExpirationDate()));
				cs.setBigDecimal(10,CheckString.getBigDecimal(component.getBookInstanceId()));
				cs.setBigDecimal(11,CheckString.getBigDecimal(component.getBookDetailId()));
				cs.setString(12, component.getTableType());
				cs.setString(13, component.getFootnoteMapping());
				cs.setBigDecimal(14, CheckString.getBigDecimal(component.getWorkFlowStatusId()));
				cs.setString(15, component.getFcomp_assignee());
				cs.setString(16, component.getParentContextId());
				cs.setNull(17, Types.NULL);
				cs.registerOutParameter(18, Types.VARCHAR);
				cs.registerOutParameter(19, java.sql.Types.BIGINT);
				cs.executeUpdate();
				}
				}
				status = cs.getString(18);
				shadowId = ""+cs.getBigDecimal(19);
				editCompStatusMap.put("status", status);
				editCompStatusMap.put("shadowId", shadowId);
		}catch (Exception e){
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || bulkdataChange()",e);
		} finally {
			releaseConStmts(null, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : bulkdataChange()");
		}
		return editCompStatusMap;
	}
	public List<ComponentBean> getAffectedComponent(String userId, String clientId, String fimportstatusid) {
		MDC.put("category"," com.dci.db4.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : getAffectedComponent() ||");
		CallableStatement cs = null;
		ResultSet rs = null;
		Connection con = null;
		List<ComponentBean> affectedComponents = new ArrayList<ComponentBean>();
		 try {
			con = getConnection();
			developerLog.debug("{CALL SP8_RTC_GETIMPORTELEMENTPERSTATUSID("+userId+","+clientId +","+ fimportstatusid+")}");
			cs = con.prepareCall("{CALL SP8_RTC_GETIMPORTELEMENTPERSTATUSID(?,?,?)}");
			cs.setString(1, userId);
			cs.setString(2, clientId);
			cs.setInt(3, Integer.parseInt(fimportstatusid));

			rs = cs.executeQuery();
			while(rs.next()) {
				ComponentBean affectedTableBean = new ComponentBean();
				affectedTableBean.setName(rs.getString("fqualdata_desc"));
				affectedTableBean.setElementInstanceId(rs.getString("felementinstanceid"));
				affectedTableBean.setCreatedTime(DateFormat.expandedDate(rs.getTimestamp("ftimecreated")));
				affectedTableBean.setCreatedBy(rs.getString("fcreatedby"));
				affectedTableBean.setEffectiveDate(DateFormat.expandedTS(rs.getTimestamp("fstarttime")));
				affectedTableBean.setExpirationDate(DateFormat.expandedTS(rs.getTimestamp("fcompletiontime")));
				affectedTableBean.setType(rs.getString("felement_id"));
				affectedTableBean.setLastUpdatedBy(rs.getString("flastchangedby"));
				affectedTableBean.setLastUpdatedTime(DateFormat.expandedDate(rs.getTimestamp("ftimelastchanged")));
				String fund = rs.getString("fcontent_id") == null ? " , " : rs.getString("fcontent_id").toString()+",";
				affectedTableBean.setFund(fund.split(","));
				affectedTableBean.setUnixtimestamplastUpdatedTime(DateFormat.getUnixTimestamp(rs.getTimestamp("ftimelastchanged")));
				affectedComponents.add(affectedTableBean);
			}
		 }catch (Exception e){
				new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getAffectedComponent()",e);
			} finally {
				releaseConStmts(null, cs, con, null);
				developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : getAffectedComponent()");
			}	
		 return affectedComponents;
	}
	
	public WorkFLStatus getStatusDefaultAssignee(String userId, String clientId, String statusId,String schemaId) {
		MDC.put("category"," com.dci.db4.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : getStatusDefaultAssignee() ||");
		CallableStatement cs = null;
		ResultSet rs = null;
		Connection con = null;
		WorkFLStatus defaultAssingee = new WorkFLStatus();
		 try {
			con = getConnection();
			developerLog.debug("{CALL SP8_RTC_GETSTATUSASSIGNEE ("+userId+","+ schemaId+","+ statusId+")}");
			cs = con.prepareCall("{CALL SP8_RTC_GETSTATUSASSIGNEE (?,?,?)}");
			cs.setString(1, userId);
			cs.setString(2, schemaId);
			cs.setInt(3, Integer.parseInt(statusId));
			rs = cs.executeQuery();
			while(rs.next()) {
				defaultAssingee.setAssignee(rs.getString("FASSIGNEE"));
			}
		 }catch (Exception e){
				new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getStatusDefaultAssignee()",e);
			} finally {
				releaseConStmts(null, cs, con, null);
				developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : getStatusDefaultAssignee()");
			}	
		 return defaultAssingee;
	}
	public String getQualDataByShadowId(String user, String qualDataShadowId)  {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		String qualdata = null;
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : getQualDataByShadowId() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			
			developerLog.debug("{call SP_RTC_ITGETELEMENTQUALSHADOWDETAIL("+qualDataShadowId+")}");
			cs = con.prepareCall("{call SP_RTC_ITGETELEMENTQUALSHADOWDETAIL(?)}");
			cs.setBigDecimal(1, CheckString.getBigDecimal(qualDataShadowId));
			rs = cs.executeQuery();
			if (rs != null && rs.next()) {
				qualdata = rs.getString("fqualdata");			
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getQualDataByShadowId()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : getQualDataByShadowId() ||");
		}
		return qualdata;	
	}
	public List<ContextBean> getContentCatByGroup(String user,String client,String catagoryId)  {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<ContextBean> contextBeansList = new ArrayList<ContextBean>();
		ContextBean contextBean;
		ComponentStatus subCategory;
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : getContentCatByGroup() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP8_RTC_GETGROUPCONTEXTCATEGORY("+user+","+client+","+catagoryId+")}");
			cs = con.prepareCall("{call SP8_RTC_GETGROUPCONTEXTCATEGORY(?,?,?)}");
			cs.setBigDecimal(1, CheckString.getBigDecimal(client));
			cs.setString(2, user);
			cs.setBigDecimal(3, CheckString.getBigDecimal(catagoryId));
			rs = cs.executeQuery();
			while (rs.next()) {
				contextBean = new ContextBean();
				subCategory = new ComponentStatus();
				contextBean.setId(rs.getInt("FELEMENTCONTEXTID_PARENT"));
				contextBean.setName(rs.getString("FELEMENTPRIMARYCONTEXT"));
				contextBean.setsId(rs.getInt("FELEMENTCONTEXTID")+"");
				contextBean.setSname(rs.getString("FELEMENTSUBCONTEXT"));
				contextBean.setOrder(rs.getInt("FORDER_PARENT"));
				contextBean.setsOrder(rs.getInt("FORDER_CHILD"));
				contextBean.setGroupIdAssociation(rs.getString("SELECTED_IND"));
				contextBeansList.add(contextBean);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getContentCatByGroup()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : getContentCatByGroup() ||");
		}
		return contextBeansList;	
	}
	public List<ContextBean> getContentCategory(String user,String client,String catagoryId)  {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<ContextBean> contextBeansList = new ArrayList<ContextBean>();
		ContextBean contextBean;
		ComponentStatus subCategory;
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : getContentCategory() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP8_RTC_GETELEMENTCONTEXTCATEGORY("+user+","+client+","+catagoryId+")}");
			cs = con.prepareCall("{call SP8_RTC_GETELEMENTCONTEXTCATEGORY(?,?,?)}");
			cs.setBigDecimal(1, CheckString.getBigDecimal(client));
			cs.setString(2, user);
			cs.setBigDecimal(3, CheckString.getBigDecimal(catagoryId));
			rs = cs.executeQuery();
			while (rs.next()) {
				contextBean = new ContextBean();
				subCategory = new ComponentStatus();
				contextBean.setId(rs.getInt("FELEMENTCONTEXTID_PARENT"));
				contextBean.setName(rs.getString("FELEMENTPRIMARYCONTEXT"));
				contextBean.setsId(rs.getInt("FELEMENTCONTEXTID")+"");
				contextBean.setSname(rs.getString("FELEMENTSUBCONTEXT"));
				contextBean.setsOrder(rs.getInt("FORDER"));
				contextBean.setOrder(rs.getInt("FORDER_PARENT"));
				contextBeansList.add(contextBean);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getContentCategory()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : getContentCategory() ||");
		}
		return contextBeansList;	
	}
	public List<ContextBean> getContentCategoryForSystemFilters(String user,String client,String catagoryId)  {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
		Map<Integer,ContextBean>  resultMap=new LinkedHashMap<>();
		List<ContextBean> finalContextBeansList = new ArrayList<ContextBean>();
		
		ContextBean contextBean;
		ContextBean subCatBean;
		ComponentStatus subCategory;
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : getContentCategory() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP8_RTC_GETELEMENTCONTEXTCATEGORY("+user+","+client+","+catagoryId+")}");
			cs = con.prepareCall("{call SP8_RTC_GETELEMENTCONTEXTCATEGORY(?,?,?)}");
			cs.setBigDecimal(1, CheckString.getBigDecimal(client));
			cs.setString(2, user);
			cs.setBigDecimal(3, CheckString.getBigDecimal(catagoryId));
			rs = cs.executeQuery();
			while (rs.next()) {
				contextBean = new ContextBean();
				subCategory = new ComponentStatus();
				subCatBean = new ContextBean();
				
				//contextBean.setId(rs.getInt("FELEMENTCONTEXTID_PARENT"));
				//contextBean.setName(rs.getString("FELEMENTPRIMARYCONTEXT"));
				int subContxtId=rs.getInt("FELEMENTCONTEXTID");
				String subContxtName=rs.getString("FELEMENTSUBCONTEXT");
				//subCatBean.setId(rs.getInt("FELEMENTCONTEXTID"));
				//subCatBean.setName(rs.getString("FELEMENTSUBCONTEXT"));	
//				contextBean.setsOrder(rs.getInt("FORDER"));
//				contextBean.setOrder(rs.getInt("FORDER_PARENT"));
				//contextBeansList.add(contextBean);
				//subCatBean !=null && ;
				if(subContxtId != 0) {
					if(resultMap.containsKey(rs.getInt("FELEMENTCONTEXTID_PARENT")))
					{
						ContextBean contextBeansSecond=resultMap.get(rs.getInt("FELEMENTCONTEXTID_PARENT"));
						List<ContextBean> secondSubCatList=contextBeansSecond.getSubCatList();
						ContextBean newSubContext=new ContextBean();
						newSubContext.setId(subContxtId);
						newSubContext.setName(subContxtName);
						secondSubCatList.add(newSubContext);
						contextBeansSecond.setSubCatList(secondSubCatList);
						resultMap.put(rs.getInt("FELEMENTCONTEXTID_PARENT"),contextBeansSecond);
					}
					else
					{
						List<ContextBean> contextBeansList = new ArrayList<ContextBean>();
						ContextBean newSubContext=new ContextBean();
						newSubContext.setId(rs.getInt("FELEMENTCONTEXTID"));
						newSubContext.setName(rs.getString("FELEMENTSUBCONTEXT"));
						contextBeansList.add(newSubContext);
						
						ContextBean newContext=new ContextBean();
						newContext.setId(rs.getInt("FELEMENTCONTEXTID_PARENT"));
						newContext.setName(rs.getString("FELEMENTPRIMARYCONTEXT"));
						newContext.setSubCatList(contextBeansList);
						resultMap.put(rs.getInt("FELEMENTCONTEXTID_PARENT"),newContext);
					}
					
					
					//contextBeansList.add(subCatBean);
				}
				else
				{
					List<ContextBean> contextBeansList = new ArrayList<ContextBean>();
					ContextBean newContextBean=new ContextBean();
					newContextBean.setId(rs.getInt("FELEMENTCONTEXTID_PARENT"));
					newContextBean.setName(rs.getString("FELEMENTPRIMARYCONTEXT"));
					newContextBean.setSubCatList(contextBeansList);
					resultMap.put(rs.getInt("FELEMENTCONTEXTID_PARENT"), newContextBean);
					
				}
			}
			
			for (Map.Entry<Integer, ContextBean> entry : resultMap.entrySet()) {
				finalContextBeansList.add(entry.getValue());
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getContentCategory()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : getContentCategory() ||");
		}
		return finalContextBeansList;	
	}

	public List<ComponentStatus> availableToPrimary(String user,String client,String catagoryId)  {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		List<ComponentStatus> contextBeansList = new ArrayList<ComponentStatus>();
		ComponentStatus subCategory;
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : availableToPrimary() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP8_RTC_GETSUBCATEGORY("+client+","+user+","+catagoryId+")}");
			cs = con.prepareCall("{call SP8_RTC_GETSUBCATEGORY(?,?,?)}");
			cs.setBigDecimal(1, CheckString.getBigDecimal(client));
			cs.setString(2, user);
			cs.setBigDecimal(3, CheckString.getBigDecimal(catagoryId));
			rs = cs.executeQuery();
			while (rs.next()) {
				subCategory = new ComponentStatus();
				subCategory.setName(rs.getString("FELEMENTCONTEXT_DESC"));
				subCategory.setId(rs.getInt("FELEMENTCONTEXTID"));
				contextBeansList.add(subCategory);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || availableToPrimary()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : availableToPrimary() ||");
		}
		return contextBeansList;	
	}
	
	public Map<String, String> createPrimaryCatogory(String user,String client,String contentCategory,ContextBean contextBean)  {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		Map<String,String> result = new HashMap<String,String>();
		String crtUpdtStatus = null;String id = null;
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : createPrimarySecondaryCatogory() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP8_RTC_CREATECATEGORY  ("+user+","+client+","+contextBean.getName()+","+contentCategory+")}");
			cs = con.prepareCall("{call SP8_RTC_CREATECATEGORY (?,?,?,?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(client));
			cs.setString(3, contextBean.getName());
			cs.setBigDecimal(4, CheckString.getBigDecimal(contentCategory));
			cs.registerOutParameter(5, Types.VARCHAR);
			cs.registerOutParameter(6, java.sql.Types.BIGINT);
			
			cs.executeUpdate();
			crtUpdtStatus = cs.getString(5);
		    id = cs.getString(6);
		    result.put("createStatus", crtUpdtStatus);
		    result.put("id", id);

		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || createPrimarySecondaryCatogory()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : createPrimarySecondaryCatogory() ||");
		}
		return result;	
	}
	
	
	public String removeCategory(String user,String client,String prentCatId,String contentCategoryId)  {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		String removeCatStatus = null;
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : removeCategory() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP8_RTC_REMOVECATEGORY("+client+","+user+","+contentCategoryId+","+prentCatId+","+null+")}");
			cs = con.prepareCall("{call SP8_RTC_REMOVECATEGORY(?,?,?,?,?)}");
			cs.setBigDecimal(1, CheckString.getBigDecimal(client));
			cs.setString(2, user);
			cs.setBigDecimal(3, CheckString.getBigDecimal(contentCategoryId));
			cs.setBigDecimal(4, CheckString.getBigDecimal(prentCatId));
			cs.registerOutParameter(5, Types.VARCHAR);
			cs.executeUpdate();
			removeCatStatus = cs.getString(5);	

		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || removeCategory()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : removeCategory() ||");
		}
		return removeCatStatus;	
	}
	public Map<String, String> createSecondaryCatogory(String user,String client,String contentCategory,ComponentStatus subCotegory)  {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		Map<String,String> result = new HashMap<String,String>();
		CallableStatement cs = null;
		ResultSet rs = null;String id = null;
		String crtUpdtStatus = null;
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : createSecondaryCatogory() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP8_RTC_CREATECATEGORY  ("+user+","+client+","+subCotegory.getName()+","+contentCategory+")}");
			cs = con.prepareCall("{call SP8_RTC_CREATECATEGORY (?,?,?,?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(client));
			cs.setString(3, subCotegory.getName());
			cs.setBigDecimal(4, CheckString.getBigDecimal(contentCategory));
			//cs.setBigDecimal(4, CheckString.getBigDecimal(subCotegory.getOrder()));
			cs.registerOutParameter(5, Types.VARCHAR);
			cs.registerOutParameter(6, java.sql.Types.BIGINT);
			cs.executeUpdate();
			crtUpdtStatus = cs.getString(5);
		    id = cs.getString(6);
		    result.put("createStatus", crtUpdtStatus);
		    result.put("id", id);


		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || createSecondaryCatogory()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : createPrimarySecondaryCatogory() ||");
		}
		return result;	
	}
	public Map<String, String> editSubContentCategory(String user,String client,String categoryId,ContextBean contextBean)  {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		Map<String,String> result = new HashMap<String,String>();
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : editSubContentCategory() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			
			developerLog.debug("{call SP8_RTC_UPDATECATEGORY("+user+","+client+","+categoryId+","+contextBean.getName()+",?)}");
			cs = con.prepareCall("{call SP8_RTC_UPDATECATEGORY(?,?,?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(client));
			cs.setBigDecimal(3, CheckString.getBigDecimal(categoryId));
			cs.setString(4, contextBean.getName());
			cs.registerOutParameter(5, Types.VARCHAR);
			cs.executeUpdate();
		    result.put("updateStatus", cs.getString(5));
		    } catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || editSubContentCategory()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : editSubContentCategory() ||");
		}
		return result;	
	}	
	public Map<String, String> orderContentCatogory(String user,String client,ContextBean contextBean)  {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		Map<String,String> result = new HashMap<String,String>();
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : orderContentCatogory() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			if(contextBean.getId() == 0) {
				contextBean.setIds(null);
			}else {
				contextBean.setIds(contextBean.getId()+"");
			}
			developerLog.debug("{call SP8_RTC_REORDERCATEGORY("+user+","+client+","+CheckString.getBigDecimal(contextBean.getIds())+","+contextBean.getOrderIds()+",?)}");
			cs = con.prepareCall("{call SP8_RTC_REORDERCATEGORY(?,?,?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(client));
			cs.setBigDecimal(3, CheckString.getBigDecimal(contextBean.getIds()));
			cs.setString(4, contextBean.getOrderIds());
			cs.registerOutParameter(5, Types.VARCHAR);
			cs.executeUpdate();
		    result.put("orderStatus", cs.getString(5));
		    } catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || orderContentCatogory()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : orderContentCatogory() ||");
		}
		return result;	
	}
	
	public Map<String, String> createCategorySchema(String user,String client,ContextSchema contextSchema)  {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		Map<String,String> result = new HashMap<String,String>();
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : createCategorySchema() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();

			developerLog.debug("{call SP9_RTC_CREATEAPISCHEMA("+user+","+client+","+contextSchema.getName()+","+contextSchema.getDescription()+",?)}");
			cs = con.prepareCall("{call SP9_RTC_CREATEAPISCHEMA(?,?,?,?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(client));
			cs.setString(3, contextSchema.getName());
			cs.setString(4,contextSchema.getDescription());
			cs.registerOutParameter(5, Types.VARCHAR);
			cs.registerOutParameter(6, java.sql.Types.BIGINT);
			cs.executeUpdate();
		    result.put("createStatus", cs.getString(5));
		    result.put("id", cs.getString(6));
		    } catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || createCategorySchema()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : createCategorySchema() ||");
		}
		return result;	
	}
	public Map<String, String> editContentCategory(String user,String client,int categoryId,ContextBean contextBean)  {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		String qualdata = null;
		Map<String,String> result = new HashMap<String,String>();
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : editContentCategory() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			
			developerLog.debug("{call SP8_RTC_UPDATECATEGORY("+user+","+client+","+contextBean.getId()+","+contextBean.getName()+",?)}");
			cs = con.prepareCall("{call SP8_RTC_UPDATECATEGORY(?,?,?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(client));
			cs.setBigDecimal(3, CheckString.getBigDecimal(categoryId));
			cs.setString(4, contextBean.getName());
			cs.registerOutParameter(5, Types.VARCHAR);
			cs.executeUpdate();
		    result.put("updateStatus", cs.getString(5));
		    } catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || editContentCategory()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : editContentCategory() ||");
		}
		return result;	
	}
	public Map<String, String> addPermissionToContentCategory(String user,String client,ContextBean contextBean)  {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		Map<String,String> result = new HashMap<String,String>();
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : addAuthContentCategory() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP8_RTC_ADDCONTEXTTOGROUP("+client+","+user+","+appId+","+contextBean.getGroupId()+","+contextBean.getContextCategory()+")}");
			cs = con.prepareCall("{call SP8_RTC_ADDCONTEXTTOGROUP(?,?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(client));
			//cs.setInt(3, CheckString.getInt(appId));
			cs.setString(3, contextBean.getGroupId());
			cs.setString(4, contextBean.getContextCategory());
			cs.executeUpdate();
		    } catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || addAuthContentCategory()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : addAuthContentCategory() ||");
		}
		return result;	
	}
	public Map<String, String> authEditSubContentCatogory(String user,String client,int categoryId,ContextBean contextBean)  {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		String qualdata = null;
		Map<String,String> result = new HashMap<String,String>();
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : authEditSubContentCatogory() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			
			developerLog.debug("{call SP8_RTC_UPDATECATEGORY("+user+","+client+","+contextBean.getId()+","+contextBean.getName()+",?)}");
			cs = con.prepareCall("{call SP8_RTC_UPDATECATEGORY(?,?,?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(client));
			cs.setBigDecimal(3, CheckString.getBigDecimal(categoryId));
			cs.setString(4, contextBean.getName());
			cs.registerOutParameter(5, Types.VARCHAR);
			cs.executeUpdate();
		    result.put("updateStatus", cs.getString(5));
		    } catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || authEditSubContentCatogory()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : authEditSubContentCatogory() ||");
		}
		return result;	
	}
	public Map<String, List<ComponentBean>> getFundVariableList(String user, String clientId, String fund, ComponentBean componentBean,String fundId)  {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : getVariableList() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		Map<String,List<ComponentBean>> variableMap = new HashMap<String,List<ComponentBean>>();
		ComponentBean keyDistractorVar,toContributors;
		List<ComponentBean>  keyDistractorVarList = new ArrayList<ComponentBean>();
		List<ComponentBean>  toContributorsList = new ArrayList<ComponentBean>();
		try {
			if (con == null || con.isClosed()){
				con = getConnection();
			}
			developerLog.debug("call SP7_RTC_GETVARBYSCOPENOBJECT('" + user + "'," + clientId + ","+ fundId+ ",null'" + 1+"2')");
			cs = con.prepareCall("{call SP7_RTC_GETVARBYSCOPENOBJECT(?,?,?,?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(clientId));
			cs.setBigDecimal(3, CheckString.getBigDecimal(fundId.trim()));
			cs.setString(4, null);
			cs.setBigDecimal(5, CheckString.getBigDecimal(1));
			cs.setBigDecimal(6, CheckString.getBigDecimal(2));
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {
				String variableName = rs.getString("FAVARIABLENAME");
				String variableValue = rs.getString("FRESOLVE_VALUE");
				if (variableName.startsWith("Key_Detractor") && CheckString.isValidString(variableValue)) {
						keyDistractorVar = new ComponentBean();
						keyDistractorVar.setVariableId(rs.getString("FVARIABLEID"));
						keyDistractorVar.setVariableName(rs.getString("FAVARIABLENAME"));
						keyDistractorVar.setVariableValue(rs.getString("FRESOLVE_VALUE"));
						keyDistractorVar.setFundsAssociationId(fundId);
						keyDistractorVarList.add(keyDistractorVar);
				} else if (variableName.startsWith("Top_Contributor") && CheckString.isValidString(variableValue)) {
					toContributors = new ComponentBean();
					toContributors.setVariableId(rs.getString("FVARIABLEID"));
					toContributors.setVariableName(rs.getString("FAVARIABLENAME"));
					toContributors.setVariableValue(rs.getString("FRESOLVE_VALUE"));
					toContributors.setFundsAssociationId(fundId);
					toContributorsList.add(toContributors);
				}
			}
			variableMap.put("Key_Detractor", keyDistractorVarList);
			variableMap.put("Top_Contributor", toContributorsList);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getVariableList()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.ComponentDAO || Method Name : getVariableList()");
		}
		return variableMap;
	}
	
	public List<ShareClass> getShareClassByFund(String user,String client,String fundId)  {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		ShareClass shareClassObj;
		String associatedShareClassFlag = "Y";
		List<ShareClass> shareClassList = new ArrayList<ShareClass>();
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : getShareClassByFund() ||");
		try {
				if(con == null || con.isClosed())
					con = getConnection();
				shareClassObj = new ShareClass();
				developerLog.debug("{call SP8_RTC_CNTGETCONTENTCLASSASSOCDETAIL("+user+","+client+","+fundId+",Y)}");
				cs = con.prepareCall("{call SP8_RTC_CNTGETCONTENTCLASSASSOCDETAIL(?,?,?,?)}");
				cs.setString(1, user);
				cs.setBigDecimal(2, CheckString.getBigDecimal(client));
				cs.setString(3, fundId);
				cs.setString(4, associatedShareClassFlag);
				rs = cs.executeQuery();
				while (rs.next()) { 
					shareClassObj = new ShareClass();					
					shareClassObj.setName(rs.getString("FCONTENTCLASS"));
					shareClassObj.setId(rs.getString("FCONTENTCLASSID"));
					shareClassList.add(shareClassObj);
				  }
		    } catch (Exception e) {
		    	new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || getShareClassByFund()",e);
			} finally {
				releaseConStmts(rs, cs, con, null);
				developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : getShareClassByFund() ||");
			}	
		return shareClassList;
	}
	
	public Map<String, String> resolveComponentVariables(String user,String client,ResloveVarBean resloveVarBean,String componentId)  {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		Map<String,String> result = new HashMap<String,String>();
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : resolveComponentVariables() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP9_RTC_PUTUPDATEVAR2CONTENTCLASS ("+user+","+client+","+componentId+","+resloveVarBean.getVariableId()+","+resloveVarBean.getSequence()+","+resloveVarBean.getFundId()+","+resloveVarBean.getClassId()+",?)}");
			cs = con.prepareCall("{call SP9_RTC_PUTUPDATEVAR2CONTENTCLASS (?,?,?,?,?,?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(client));
			cs.setBigDecimal(3, CheckString.getBigDecimal(componentId));
			cs.setBigDecimal(4, CheckString.getBigDecimal(resloveVarBean.getVariableId()));
			cs.setInt(5, CheckString.getInt(resloveVarBean.getSequence()));
			cs.setBigDecimal(6, CheckString.getBigDecimal(resloveVarBean.getFundId()));
			cs.setBigDecimal(7,CheckString.getBigDecimal(resloveVarBean.getClassId()));
			cs.registerOutParameter(8, Types.VARCHAR);
			cs.executeUpdate();
		    result.put("resolveStatus", cs.getString(8));
		    } catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || resolveComponentVariables()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : resolveComponentVariables() ||");
		}
		return result;	
	}
	
	public List<ResloveVarBean> previewVariableValue(String user,String client,String componentId)  {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		ResloveVarBean resloveVarBean;
		List<ResloveVarBean> variableList = new ArrayList<ResloveVarBean>();
		developerLog.debug("Entering into com.dci.db4.dao.ComponentDAO || Method Name : previewVariableValue() ||");
		try {
				if(con == null || con.isClosed())
					con = getConnection();
				resloveVarBean = new ResloveVarBean();
				developerLog.debug("{call SP9_RTC_GETVAR2CONTENTCLASS("+user+","+client+","+componentId+")}");
				cs = con.prepareCall("{call SP9_RTC_GETVAR2CONTENTCLASS(?,?,?)}");
				cs.setString(1, user);
				cs.setBigDecimal(2, CheckString.getBigDecimal(client));
				cs.setBigDecimal(3, CheckString.getBigDecimal(componentId));
				rs = cs.executeQuery();
				while (rs.next()) { 
					resloveVarBean = new ResloveVarBean();
					resloveVarBean.setVariableId(rs.getString("FVARIABLEID"));
					resloveVarBean.setResolveVal(rs.getString("FRESOLVE_VALUE"));
					resloveVarBean.setSequence(rs.getString("FSEQUENCEID"));
					resloveVarBean.setFundId(rs.getString("FCONTENTID"));
					resloveVarBean.setClassId(rs.getString("FCONTENTCLASSID"));
					variableList.add(resloveVarBean);
				  }
		    } catch (Exception e) {
		    	new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || previewVariableValue()",e);
			} finally {
				releaseConStmts(rs, cs, con, null);
				developerLog.debug("Exiting from com.dci.db4.dao.ComponentDAO || Method Name : previewVariableValue() ||");
			}	
		return variableList;
	}
	
	public List<CommentsBean> getAllCommentsOfComponent(String userId,String clientId,String eleInstanceId) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentDAO || Method Name : getAllCommentsOfComponent() ||");
		List<CommentsBean> commentList = new ArrayList<CommentsBean>(); 
		CallableStatement cs = null;
		ResultSet rs = null;
		Connection con =null;
		SimpleDateFormat changeToDate = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP8_RTC_GETQUALDATASHADOWCOMMENTS('" + userId + "'," + clientId +"," + eleInstanceId +")}");
		    cs = con.prepareCall("{call SP8_RTC_GETQUALDATASHADOWCOMMENTS(?,?,?)}");
			cs.setString(1, userId);
			cs.setInt(2, CheckString.getInt(clientId));
			cs.setBigDecimal(3,CheckString.getBigDecimal(eleInstanceId));		
			rs = cs.executeQuery();
			while(rs.next()){
				CommentsBean commentBean = new CommentsBean();
				UserBean userBean = new UserBean();
				commentBean.setComment(StringUtility.replaceControlCharacters(DciCommon.replaceSpecialCharacters(rs.getString("FCOMMENT_DESC")),null));
				commentBean.setCreatedby(rs.getString("FCREATEDBY"));
				commentBean.setCreatedtime(DateFormat.expandedTS(rs.getTimestamp("ftimecreated")));
				userBean.setFirstName(rs.getString("FFIRST_NAME"));
				userBean.setLastName(rs.getString("FLAST_NAME"));
				commentBean.setUser(userBean);
				commentBean.setShadowId(rs.getString("FSHADOWID"));
				commentList.add(commentBean);
			}
		} catch (Exception e) {
			new DocubuilderException("com.dci.rest.dao.ComponentDAO || getAllCommentsOfComponent()",e);
		} finally {
			releaseConStmts(rs, cs, con, "com.dci.rest.dao.ComponentDAO || getAllCommentsOfComponent()");
		}
		return commentList;
	}
}


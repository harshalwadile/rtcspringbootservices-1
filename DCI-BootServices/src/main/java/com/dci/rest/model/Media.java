package com.dci.rest.model;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Media {
	
	List<MediaBean> media;
	List<MediaAlbum> albums;
	
	public List<MediaBean> getMedia() {
		return media;
	}
	public void setMedia(List<MediaBean> media) {
		this.media = media;
	}
	public List<MediaAlbum> getAlbums() {
		return albums;
	}
	public void setAlbums(List<MediaAlbum> albums) {
		this.albums = albums;
	}

}

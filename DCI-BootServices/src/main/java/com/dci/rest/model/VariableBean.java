package com.dci.rest.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class VariableBean {
	private String name;
	private int id;
	private int scope;
	private String description;
	private String displayType;
	private int usedCount;
	private String createdBy;
	private String updatedBy;
	private String systemVar;
	private List<Object> fundOptions = new ArrayList<Object>();
	private List<Object> classOptions = new ArrayList<Object>();
	private List<SelectBean> styleOptions = new ArrayList<SelectBean>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getScope() {
		return scope;
	}
	public void setScope(int scope) {
		this.scope = scope;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDisplayType() {
		return displayType;
	}
	public void setDisplayType(String displayType) {
		this.displayType = displayType;
	}
	public int getUsedCount() {
		return usedCount;
	}
	public void setUsedCount(int usedCount) {
		this.usedCount = usedCount;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public List<Object> getFundOptions() {
		return fundOptions;
	}
	public void setFundOptions(List<Object> fundOptions) {
		this.fundOptions = fundOptions;
	}
	public List<SelectBean> getStyleOptions() {
		return styleOptions;
	}
	public void setStyleOptions(List<SelectBean> styleOptions) {
		this.styleOptions = styleOptions;
	}
	public List<Object> getClassOptions() {
		return classOptions;
	}
	public void setClassOptions(List<Object> classOptions) {
		this.classOptions = classOptions;
	}
	public String getSystemVar() {
		return systemVar;
	}
	public void setSystemVar(String systemVar) {
		this.systemVar = systemVar;
	}

	
}

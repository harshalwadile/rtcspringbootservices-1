package com.dci.rest.model;

import javax.validation.constraints.NotNull;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.dci.rest.model.DciValidation.ValidationCreateComp;
import com.dci.rest.model.DciValidation.ValidationCreateLinkageComponent;
import com.dci.rest.model.DciValidation.ValidationCreateTableComp;
import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class TableComponent extends ComponentBean{	
	private String rows;
	private String columns;
	
	private String dataType;
	private String tableType;
	private String quantData;
	private String qualData;
	private String owner;
	
	public String getRows() {
		return rows;
	}
	public void setRows(String rows) {
		this.rows = rows;
	}
	public String getColumns() {
		return columns;
	}
	public void setColumns(String columns) {
		this.columns = columns;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getTableType() {
		return tableType;
	}
	public void setTableType(String tableType) {
		this.tableType = tableType;
	}
	
	public String getQuantData() {
		return quantData;
	}
	public void setQuantData(String quantData) {
		this.quantData = quantData;
	}
	public String getQualData() {
		return qualData;
	}
	public void setQualData(String qualData) {
		this.qualData = qualData;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
}
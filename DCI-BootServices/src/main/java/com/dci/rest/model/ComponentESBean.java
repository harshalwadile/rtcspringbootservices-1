package com.dci.rest.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class ComponentESBean {

	private String fcomp_status;
	private String fcompstatusid;
	private String type;
	private String expiration_date;
	private String effective_date;
	private String qual_data_desc;
	private String felementcontext_desc;
	private String felementcontextid;
	private String component_type;
	private String felementinstanceid;
	
	public String getFcomp_status() {
		return fcomp_status;
	}
	public void setFcomp_status(String fcomp_status) {
		this.fcomp_status = fcomp_status;
	}
	public String getFcompstatusid() {
		return fcompstatusid;
	}
	public void setFcompstatusid(String fcompstatusid) {
		this.fcompstatusid = fcompstatusid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getExpiration_date() {
		return expiration_date;
	}
	public void setExpiration_date(String expiration_date) {
		this.expiration_date = expiration_date;
	}
	public String getEffective_date() {
		return effective_date;
	}
	public void setEffective_date(String effective_date) {
		this.effective_date = effective_date;
	}
	public String getQual_data_desc() {
		return qual_data_desc;
	}
	public void setQual_data_desc(String qual_data_desc) {
		this.qual_data_desc = qual_data_desc;
	}
	public String getFelementcontext_desc() {
		return felementcontext_desc;
	}
	public void setFelementcontext_desc(String felementcontext_desc) {
		this.felementcontext_desc = felementcontext_desc;
	}
	public String getFelementcontextid() {
		return felementcontextid;
	}
	public void setFelementcontextid(String felementcontextid) {
		this.felementcontextid = felementcontextid;
	}
	public String getComponent_type() {
		return component_type;
	}
	public void setComponent_type(String component_type) {
		this.component_type = component_type;
	}
	public String getFelementinstanceid() {
		return felementinstanceid;
	}
	public void setFelementinstanceid(String felementinstanceid) {
		this.felementinstanceid = felementinstanceid;
	}

}

package com.dci.rest.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class UserBean {
	
	private String userName = null;
	private String userId = null;
	
	private boolean createdbyInd;
	private boolean updatedbyInd;
	
	private String firstName;
	private String lastName;
		
	
	public final String getUserName() {
		return userName;
	}
	public final void setUserName(String userName) {
		this.userName = userName;
	}
	public final boolean isCreatedbyInd() {
		return createdbyInd;
	}
	public final void setCreatedbyInd(boolean createdbyInd) {
		this.createdbyInd = createdbyInd;
	}
	public final boolean isUpdatedbyInd() {
		return updatedbyInd;
	}
	public final void setUpdatedbyInd(boolean updatedbyInd) {
		this.updatedbyInd = updatedbyInd;
	}
	public final String getUserId() {
		return userId;
	}
	public final void setUserId(String userId) {
		this.userId = userId;
	}
	public final String getFirstName() {
		return firstName;
	}
	public final void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public final String getLastName() {
		return lastName;
	}
	public final void setLastName(String lastName) {
		this.lastName = lastName;
	}


}

package com.dci.rest.model;

public interface DciValidation {

    interface ValidationCreateComment {
        // validation group marker interface
    }
    interface ValidationCreateComp {
        // validation group marker interface
    }
    interface ValidationCreateUpdateInd {
        // validation group marker interface
    }
    interface ValidationCreateFilter {
        // validation group marker interface
    }
    interface ValidationCreateTag {
        // validation group marker interface
    }
    interface ValidationSetTag {
        // validation group marker interface
    }
    interface ValidationSearchLinkage {
        // validation group marker interface
    }
    interface ValidationDeactivateComp {
        // validation group marker interface
    }
    interface ValidationCreateLinkageComponent {
        // validation group marker interface
    }
    interface ValidationCreateTableComp {
        // validation group marker interface
    }
    interface ValidationAppasese {
        // validation group marker interface
    }
    interface ValidateStatusId {
        // validation group marker interface
    }
    
    interface ValidationCreateContext {
        // validation group marker interface
    }
    interface ValidationGetFundByAsset {
        // validation group marker interface
    }
    
}

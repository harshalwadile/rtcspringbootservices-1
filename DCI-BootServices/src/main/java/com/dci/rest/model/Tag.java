package com.dci.rest.model;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.dci.rest.model.DciValidation.ValidationCreateComp;
import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class Tag implements DciValidation{
	private int id=0;
	@NotNull(groups = {ValidationCreateTag.class})
	private String name;
	public boolean selected;
	 
	List<Style> category;

	public final boolean isSelected() {
		return selected;
	}
	public final void setSelected(boolean selected) {
		this.selected = selected;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public final String getName() {
		return name;
	}
	public final void setName(String name) {
		this.name = name;
	}
	public List<Style> getCategory() {
		return category;
	}
	public void setCategory(List<Style> category) {
		this.category = category;
	}


}

package com.dci.rest.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class FootNoteAttributes {
	private String fbooktype;
	private String fattribute;
	
	private List<Order> order;
	private ArrayList order_attr;
	
	private List<Placement> placement;
	private ArrayList placement_attr;
	
	private  List<Style> style;
	private ArrayList style_attr;
	public String getFbooktype() {
		return fbooktype;
	}
	public void setFbooktype(String fbooktype) {
		this.fbooktype = fbooktype;
	}
	public String getFattribute() {
		return fattribute;
	}
	public void setFattribute(String fattribute) {
		this.fattribute = fattribute;
	}

	public ArrayList getOrder_attr() {
		return order_attr;
	}
	public void setOrder_attr(ArrayList order_attr) {
		this.order_attr = order_attr;
	}
	public ArrayList getPlacement_attr() {
		return placement_attr;
	}
	public void setPlacement_attr(ArrayList placement_attr) {
		this.placement_attr = placement_attr;
	}
	public ArrayList getStyle_attr() {
		return style_attr;
	}
	public void setStyle_attr(ArrayList style_attr) {
		this.style_attr = style_attr;
	}
	public List<Order> getOrder() {
		return order;
	}
	public void setOrder(List<Order> order) {
		this.order = order;
	}
	public List<Placement> getPlacement() {
		return placement;
	}
	public void setPlacement(List<Placement> placement) {
		this.placement = placement;
	}
	public List<Style> getStyle() {
		return style;
	}
	public void setStyle(List<Style> style) {
		this.style = style;
	}

}

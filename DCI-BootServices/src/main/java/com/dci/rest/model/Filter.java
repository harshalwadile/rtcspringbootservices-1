package com.dci.rest.model;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Filter  implements DciValidation{
	private String id;
	@NotNull(groups = {ValidationCreateFilter.class})
	private String name;
	private String description;
	@NotNull(groups = {ValidationCreateFilter.class})
	private String publicFlag;
	private List<Object> items;
	@NotNull(groups = {ValidationCreateFilter.class})
	private SearchCriteria criteria;
	@NotNull(groups = {ValidationCreateFilter.class})
	private String creator;
	List<Fund> funds;
 	/*public Filter(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}*/
	private Object pendingwork;
	
	public Object getPendingwork() {
		return pendingwork;
	}
	public void setPendingwork(Object pendingwork) {
		this.pendingwork = pendingwork;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public List<Object> getItems() {
		return items;
	}

	public void setItems(List<Object> items) {
		this.items = items;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public SearchCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(SearchCriteria criteria) {
		this.criteria = criteria;
	}
	
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getPublicFlag() {
		return publicFlag;
	}
	public void setPublicFlag(String publicFlag) {
		this.publicFlag = publicFlag;
	}
	public List<Fund> getFunds() {
		return funds;
	}
	public void setFunds(List<Fund> funds) {
		this.funds = funds;
	}
	
}

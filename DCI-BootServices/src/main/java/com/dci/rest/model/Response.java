package com.dci.rest.model;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class Response<T> {
	@Autowired
	private Status Status;
	private T result;

	public final T getResult() {
		return result;
	}
	public final void setResult(T result) {
		this.result = result;
	}
	public final Status getStatus() {
		return Status;
	}
	public final void setStatus(Status status) {
		Status = status;
	}
	
	
	

}

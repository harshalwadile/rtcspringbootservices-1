package com.dci.rest.utils;

import org.w3c.dom.*; 
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;

import com.dci.rest.model.FieldAttribute;

import org.apache.xerces.dom.*;
import org.apache.xerces.parsers.*;

import org.apache.xpath.objects.*;
import javax.xml.transform.*;
import org.apache.xpath.*;
import org.apache.xml.utils.*;
import org.apache.xpath.compiler.XPathParser;
import org.apache.xpath.compiler.Compiler;
import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.xml.serialize.*;
import java.math.*;

public class DBXmlManager {
	
	/**
	 * @Purpose: this mehtod is used to get XML String
	 * @return String
	 * @throws Exception
	 
	 * */
	public static String getXMLString(Document doc) throws Exception    {
		  String str = null;
		  if (doc == null) return str;
		  StringWriter docOut = new StringWriter();
		  try  {
		      OutputFormat format = new OutputFormat(doc);
		      format.setOmitXMLDeclaration(false);
		      format.setOmitComments(true);
        		 XMLSerializer serializer = new XMLSerializer(docOut, format);
	         serializer.serialize(doc);
    	    } catch (IOException e) {
				throw new Exception(e.getMessage());
			} 
    	    String xmlString = "";
    	    if(docOut.toString().contains("inlinestyle")){
    	    	String[] xmlStringArray = docOut.toString().split("<inlinestyle");
    	    	xmlString = xmlStringArray[0];
    	    	for(int i=1;i<xmlStringArray.length;i++){
	    	    	String afterCondition = xmlStringArray[i].substring(xmlStringArray[i].indexOf("condition=\"")+11);
	    	    	String conditionValue = afterCondition.substring(0,afterCondition.indexOf("\""));
	    	    	String afterSize = xmlStringArray[i].substring(xmlStringArray[i].indexOf("size=\"")+6);
	    	    	String sizeValue = afterSize.substring(0,afterSize.indexOf("\""));
	    	    	Pattern pattern = Pattern.compile("condition=\"[^\"]+\" size=\"[^\"]+\"");
	    	    	Matcher matcher = pattern.matcher(xmlStringArray[i]);
	    	    	xmlStringArray[i] =matcher.replaceAll("size=\""+sizeValue+"\" condition=\""+conditionValue+"\"");
	    	    	xmlString += "<inlinestyle" + xmlStringArray[i];
    	    	}
    	    }else if(docOut.toString().contains("type=\"button\"")){
    	    	xmlString = formatXmlString (docOut.toString(),"footnoteref");
    	    } else{
    	    	xmlString = docOut.toString();
    	    }
		return xmlString.replace("\"bold\"","'bold'");
	}	
	
	/**
	 * @Purpose: this mehtod is used to getDOMFromXMLString
	 * @return DocumentImpl
	 * @throws Exception
	 
	 * */
	@SuppressWarnings("unused")
	public static DocumentImpl getDOMFromXMLString(String str) throws Exception {
	if(str == null)
		return null;
			InputSource is = new InputSource(new StringReader(str));			
			DOMParser xreader = new DOMParser();
			try {
			  
			  is.setEncoding("UTF-8");
              DefaultHandler handler = new DefaultHandler();

					xreader.setFeature("http://apache.org/xml/features/dom/defer-node-expansion", false);
					xreader.parse(is);
				} catch(SAXNotRecognizedException saxe) {
					saxe.printStackTrace();

				} catch (SAXNotSupportedException e) {
					e.printStackTrace();
				} catch (IOException io) {
					io.printStackTrace();
				} catch (SAXException sax) {
					sax.printStackTrace();
				}
					return (DocumentImpl)xreader.getDocument();
   }									

	/**
	 * @Purpose: this mehtod is used to addElement  with int value
	 * @return Element
	 
	 * */
	public static Element addElement(DocumentImpl xmlDocument, String xmlTag, int value) {
		return addElement(xmlDocument, xmlDocument.getDocumentElement(), xmlTag, new Integer(value));
	}
	
	/**
	 * @Purpose: this mehtod is used to addElement with String value
	 * @return Element
	 
	 * */
	public static Element addElement(DocumentImpl xmlDocument, String xmlTag, String value) {
		return addElement(xmlDocument, xmlDocument.getDocumentElement(), xmlTag, value);
	}

	/**
	 * @Purpose: this mehtod is used to addElement with double value
	 * @return Element
	 
	 * */
	public static Element addElement(DocumentImpl xmlDocument, Element parentElement, String xmlTag, double value) {
		return 	addElement(xmlDocument, parentElement, xmlTag, new Double(value).toString());
	}
	
	/**
	 * @Purpose: this mehtod is used to addElement with long value
	 * @return Element
	 
	 * */
	public static Element addElement(DocumentImpl xmlDocument, Element parentElement, String xmlTag, long value) {
		return addElement(xmlDocument, parentElement, xmlTag, new Long(value).toString());
	}
	
	/**
	 * @Purpose: this mehtod is used to addElement with int value
	 * @return Element
	 * 
	 * */
	public static Element addElement(DocumentImpl xmlDocument, Element parentElement, String xmlTag, int value) {
		return addElement(xmlDocument, parentElement, xmlTag, new Integer(value));
	}

	/**
	 * @Purpose: this mehtod is used to addElement with Integer value
	 * @return Element
	 
	 * */
	public static Element addElement(DocumentImpl xmlDocument, Element parentElement, String xmlTag, Integer value) {
		if (value == null) {
			return addElement(xmlDocument, parentElement, xmlTag);											
		} else {
			return addElement(xmlDocument, parentElement, xmlTag, value.toString());
		}
	}
	/**
	 * @Purpose: this mehtod is used to addElement with Double value
	 * @return Element
	 
	 * */
	public static Element addElement(DocumentImpl xmlDocument, Element parentElement, String xmlTag, Double value) {    
		if (value == null) {
			return addElement(xmlDocument, parentElement, xmlTag);
		} else {
			return addElement(xmlDocument, parentElement, xmlTag, value.toString());
		}
	}
	
	/**
	 * @Purpose: this mehtod is used to addElement with BigDecimal value
	 * @return Element
	 
	 * */
	public static Element addElement(DocumentImpl xmlDocument, Element parentElement, String xmlTag, BigDecimal value) {    
		if (value == null) {
			return addElement(xmlDocument, parentElement, xmlTag);
		} else {
			return addElement(xmlDocument, parentElement, xmlTag, value.toString());
		}
	}	
	
	/**
	 * @Purpose: this mehtod is used to addElement with Date  value
	 * @return Element
	 
	 * */
	public static Element addElement(DocumentImpl xmlDocument, Element parentElement, String xmlTag, java.sql.Date value) {
		if (value == null) {
			return addElement(xmlDocument, parentElement, xmlTag, "");
		} else {
			return addElement(xmlDocument, parentElement, xmlTag, value.toString());
		}
	}

	/**
	 * @Purpose: this mehtod is used to addElement with Time  value
	 * @return Element
	 
	 * */
	public static Element addElement(DocumentImpl xmlDocument, Element parentElement, String xmlTag, java.sql.Time value) {
		if (value == null) {
			return addElement(xmlDocument, parentElement, xmlTag, "");
		} else {
			return addElement(xmlDocument, parentElement, xmlTag, value.toString());
		}
	}

	/**
	 * @Purpose: this mehtod is used to addElement with StringBuffer  value
	 * @return Element
	 
	 * */
	public static Element addElement(DocumentImpl xmlDocument, Element parentElement, String xmlTag, StringBuffer value) {
		if (value == null) {
			return addElement(xmlDocument, parentElement, xmlTag, "");
		} else {
			return addElement(xmlDocument, parentElement, xmlTag, value.toString());
		}
	}
	
	/**
	 * @Purpose: this mehtod is used to addElement with String value
	 * @return Element
	 
	 * */
	public static Element addElement(DocumentImpl xmlDocument, Element parentElement, String xmlTag, String value) {
		Element element = xmlDocument.createElement(xmlTag); 
		parentElement.appendChild(element); 
		if(value != null) {
	        Text text = xmlDocument.createTextNode(value); 
    	    element.appendChild(text); 
		}
		return element;
	}
	
	/**
	 * @Purpose: this mehtod is used to addElement with String value
	 * @return Element
	 
	 * */	
	public static Element addElement(DocumentImpl xmlDocument, Element parentElement, String xmlTag){
		Element element = xmlDocument.createElement(xmlTag);
		parentElement.appendChild(element);
		return element;				
		}

	/**
	 * @Purpose:getNodeValue to get table body.
	 * @return Element
	 
	 * */
   @SuppressWarnings("unused")
public static String getNodeValue(Node node) {
   			try {
   					if (node == null) return "";
   					String root = "xx"+node.getNodeName();
   					String start = "<"+root+">"+"<"+node.getNodeName()+">";
   					String end = "</"+node.getNodeName()+">"+"</"+root+">";
					Document tempDoc = new DocumentImpl();
					Element elem = tempDoc.createElement(root);
					tempDoc.appendChild(elem);
					Node nd = tempDoc.importNode(node,true);
					elem.appendChild(nd);
					String resultstr = getXMLString(tempDoc);
					resultstr = CheckString.stripOffTag(resultstr, node.getNodeName());
					return resultstr;
   				} catch (Exception e) {
   						return "";
   				}
			}
			
   /**
	 * @Purpose: this method is used to prepareXmlDOM
	 * @return DocumentImpl
	 
	 * */
	public static DocumentImpl prepareXmlDOM(String str) {
			DocumentImpl doc = null;
			try {
			String xmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
			xmlHeader +="<!DOCTYPE book PUBLIC \"-//OASIS//DTD DocBook XML V4.1.2//EN\" \"http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd\">";
			str =  xmlHeader + str;
			doc =  getDOMFromXMLString(str);		
			} catch (Exception e) {
					e.printStackTrace();
			}
			return doc;
	}	
	
	
	/**
	 * @Purpose: this method is used to getTextNodeValue
	 * @return String
	 
	 * */
	public static String getTextNodeValue(Node node, String xPath) throws SAXException {
		Node textNode = getTextNode(node, xPath);
		if(textNode != null && textNode.getNodeType() == Node.TEXT_NODE) {
			return textNode.getNodeValue();
		} else {
			return null;
		}
	}

	/**
	 * @Purpose: this method is used to getTextNode
	 * @return Node
	 
	 * */
	public static Node getTextNode(Node node, String xPath) throws SAXException {
		NodeList list = null;
		Node textNode = getSingleNode(node, xPath);
		if(textNode != null && (list = textNode.getChildNodes()) != null && list.getLength() > 0 && list.item(0).getNodeType() == Node.TEXT_NODE) {
			return list.item(0);
		} else {
			return null;
		}
	}

	/**
	 * @Purpose: this method is used to getSingleNode
	 * @return Node
	 
	 * */
	public static Node getSingleNode(Node node, String xPath) throws SAXException {
	    NodeList list = getNodeList(node, xPath);
		if(list.getLength() > 0) {
			return list.item(0);
		} else {
			return null;
		}
	}

	/**
	 * @Purpose: this method is used to getNodeList
	 * @return NodeList
	 
	 * */
	public static NodeList getNodeList(Node node, String xPath) throws SAXException {
		try {
			XObject list = execute(node, xPath);
		    return (NodeList)list.nodelist();
		} catch(Exception e) {
				e.printStackTrace();
				throw new SAXException(e.getMessage());
			}
	}

	/**
	 * @Purpose: this method is used to execute parser
	 * @return XObject
	 
	 * */
	public static XObject execute(Node node, String xPath) throws SAXException {
		try {	
			XPathContext xpathSupport = new XPathContext(); // shivaji
			PrefixResolverDefault prefixResolver = null;
			if(node.getNodeType() == Node.DOCUMENT_NODE) {
				prefixResolver = new PrefixResolverDefault(((Document)node).getDocumentElement());
			} else {
				prefixResolver = new PrefixResolverDefault(node);
			}
			XPath xpath = null;
			  xpath = new XPath( xPath, xpathSupport.getSAXLocator(), prefixResolver, XPath.SELECT);
			XPathParser parser = new XPathParser(xpathSupport.getErrorListener(), xpathSupport.getSAXLocator());
			parser.initXPath(new Compiler(), xPath, prefixResolver);
			return xpath.execute(xpathSupport, node, prefixResolver);
			} catch(TransformerException te) {
						new SAXException(te.getMessage());
			}
		return null;
	}
	
	/**
	 * @Purpose: this method is used to getElementAttributes
	 * @return ArrayList
	 
	 * */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ArrayList getElementAttributes(Element element) {
		ArrayList col = null;
		NamedNodeMap  nnm = element.getAttributes();
		if (nnm == null || nnm.getLength() ==0) return null;
		col = new ArrayList(); 
		for(int i=0;i<nnm.getLength();i++) {
			Node attribute = nnm.item(i);
			col.add(new FieldAttribute(attribute.getNodeName(),attribute.getNodeValue()));
		}
		return col;
	}
	
	/**
	 * @Purpose: this method is used to formatXmlString
	 * @return String
	 
	 * */
	public static String formatXmlString (String source,String symbol){
		String patternToCheck = "<footnoteref alt=\"[0-9]+\" idref=\"[0-9]+\" order=\"[0-9]+\" size=\"[0-9]+\" type=\"button\" value=\"[0-9]+\"/>";
		Pattern pattern = Pattern.compile(patternToCheck);
    	Matcher matcher = pattern.matcher(source);
		Document doc ;
		while(matcher.find()){
			try {
				doc = DBXmlManager.getDOMFromXMLString("<root>"+matcher.group()+"</root>");
				Element rootElement = doc.getDocumentElement();
				NodeList imageNode = rootElement.getElementsByTagName(symbol);	
				Element imageElement = (Element) imageNode.item(0);
				String alt = imageElement.getAttribute("alt");
				String order = imageElement.getAttribute("order");
				String size = imageElement.getAttribute("size");
				String value = imageElement.getAttribute("value");
				String idref = imageElement.getAttribute("idref");
				String newString = "<footnoteref type=\"button\" alt=\""+alt+"\" size=\""+size+"\" value=\""+value+"\"  idref=\""+idref+"\" order=\""+order+"\" />";
				source = source.replace(matcher.group(), newString);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return source;
	}
	
	/**
	 * @Purpose: this method is used to formatXmlString
	 * @return String
	 
	 * */
	public static String formatXmlString (String source){
		// <input [^>]+>
		// Below if condition is written by Ajeet Singh to solve footnote tag conversion problem in IE - code starts here
		if(source.indexOf("INPUT")!=-1 || source.indexOf("input")!=-1){
			String patternToCheck = "<input [^>]+>";
			Pattern pattern = Pattern.compile(patternToCheck, Pattern.CASE_INSENSITIVE);
	    	Matcher matcher = pattern.matcher(source);
			Document doc ;			
			if(matcher.find()){
				try {
					do{
						String cellValue = matcher.group().replace(">", " />").replace("type=image", "type=\"image\"");
						String[] patternToCheck2 = {"title=[0-9]+","alt=[0-9]+","size=[0-9]+","value=[0-9]+"};
						for(int i=0;i<patternToCheck2.length;i++){
							Pattern pattern2 = Pattern.compile(patternToCheck2[i]);
					    	Matcher matcher2 = pattern2.matcher(matcher.group());
					    	while(matcher2.find()){
					    		cellValue = cellValue.replace(matcher2.group(), matcher2.group().replace("=","=\"")+"\"");
					    	}
						}
						doc = DBXmlManager.getDOMFromXMLString("<root>"+cellValue+"</root>");
						Element rootElement = doc.getDocumentElement();
						NodeList imageNode = null;	
						if(cellValue.indexOf("INPUT") != -1){
							imageNode = rootElement.getElementsByTagName("INPUT");
						}else if(cellValue.indexOf("input") != -1){
							imageNode = rootElement.getElementsByTagName("input");
						}
						Element imageElement = (Element) imageNode.item(0);
						String alt = imageElement.getAttribute("alt");
						String order = imageElement.getAttribute("order");
						String size = imageElement.getAttribute("size");
						String value = imageElement.getAttribute("value");
						String src = "htmlarea/images/footnote.gif";
						String title = imageElement.getAttribute("title");
						String imageElementSrc = imageElement.getAttribute("src");
						String imageElementType = imageElement.getAttribute("type");
						String newString = "<input type=\"image\" alt=\""+alt+"\" size=\""+size+"\" value=\""+value+"\"  src=\""+src+"\" title=\""+title+"\" order=\""+order+"\" />";
						if(imageElementType.trim().equals("image") && imageElementSrc.indexOf("htmlarea/images/footnote.gif") != -1){
							source = source.replace(matcher.group(), newString);
						}
					}while(matcher.find());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		//Above if condition is written by Ajeet Singh to solve footnote tag conversion problem in IE - code ends here
		if(source.indexOf("INPUT")!=-1){
			String patternToCheck = "<INPUT title=[0-9]+ type=image alt=[0-9]+ src=\"[^\"]+\" size=[0-9]+ value=[0-9]+ order=\"[0-9]+\">";
			Pattern pattern = Pattern.compile(patternToCheck);
	    	Matcher matcher = pattern.matcher(source);
			Document doc ;			
			if(matcher.find()){
				try {
					do{
						String cellValue = matcher.group().replace(">", " />").replace("type=image", "type=\"image\"");
						String[] patternToCheck2 = {"title=[0-9]+","alt=[0-9]+","size=[0-9]+","value=[0-9]+"};
						for(int i=0;i<patternToCheck2.length;i++){
							Pattern pattern2 = Pattern.compile(patternToCheck2[i]);
					    	Matcher matcher2 = pattern2.matcher(matcher.group());
					    	while(matcher2.find()){
					    		cellValue = cellValue.replace(matcher2.group(), matcher2.group().replace("=","=\"")+"\"");
					    	}
						}
						doc = DBXmlManager.getDOMFromXMLString("<root>"+cellValue+"</root>");
						Element rootElement = doc.getDocumentElement();
						NodeList imageNode = rootElement.getElementsByTagName("INPUT");	
						Element imageElement = (Element) imageNode.item(0);
						String alt = imageElement.getAttribute("alt");
						String order = imageElement.getAttribute("order");
						String size = imageElement.getAttribute("size");
						String value = imageElement.getAttribute("value");
						String src = "htmlarea/images/footnote.gif";
						String title = imageElement.getAttribute("title");
						String newString = "<input type=\"image\" alt=\""+alt+"\" size=\""+size+"\" value=\""+value+"\"  src=\""+src+"\" title=\""+title+"\" order=\""+order+"\" />";
						source = source.replace(matcher.group(), newString);
					}while(matcher.find());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}else{
				patternToCheck = "<INPUT title=[0-9]+ value=[0-9]+ alt=[0-9]+ src=\"[^\"]+\" size=[0-9]+ type=image order=\"[0-9]+\">";
				pattern = Pattern.compile(patternToCheck);
		    	matcher = pattern.matcher(source);	
		    	while(matcher.find()){
					try {
							String cellValue = matcher.group().replace(">", " />").replace("type=image", "type=\"image\"");
							String[] patternToCheck2 = {"title=[0-9]+","alt=[0-9]+","size=[0-9]+","value=[0-9]+"};
							for(int i=0;i<patternToCheck2.length;i++){
								Pattern pattern2 = Pattern.compile(patternToCheck2[i]);
						    	Matcher matcher2 = pattern2.matcher(matcher.group());
						    	while(matcher2.find()){
						    		cellValue = cellValue.replace(matcher2.group(), matcher2.group().replace("=","=\"")+"\"");
						    	}
							}
							doc = DBXmlManager.getDOMFromXMLString("<root>"+cellValue+"</root>");
							Element rootElement = doc.getDocumentElement();
							NodeList imageNode = rootElement.getElementsByTagName("INPUT");	
							Element imageElement = (Element) imageNode.item(0);
							String alt = imageElement.getAttribute("alt");
							String order = imageElement.getAttribute("order");
							String size = imageElement.getAttribute("size");
							String value = imageElement.getAttribute("value");
							String src = "htmlarea/images/footnote.gif";
							String title = imageElement.getAttribute("title");
							String newString = "<input type=\"image\" alt=\""+alt+"\" size=\""+size+"\" value=\""+value+"\"  src=\""+src+"\" title=\""+title+"\" order=\""+order+"\" />";
							source = source.replace(matcher.group(), newString);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
		    }
		}
		if(source.indexOf("IMG")!=-1){
			String patternToCheck = "<IMG id=[0-9~]+ title=[^;]+; src=\"[^\"]+\" type=\"[a-z]+\" condition=\"[a-z]+\" sequenceid=\"[0-9]+\">";
			Pattern pattern = Pattern.compile(patternToCheck);
	    	Matcher matcher = pattern.matcher(source);
			Document doc ;			
			while(matcher.find()){
				try {					
					String cellValue = matcher.group().replace(">", " />");
					String[] patternToCheck2 = {"title=[^;]+;","id=[0-9~]+"};
					for(int i=0;i<patternToCheck2.length;i++){
						Pattern pattern2 = Pattern.compile(patternToCheck2[i]);
				    	Matcher matcher2 = pattern2.matcher(matcher.group());
				    	while(matcher2.find()){
				    		cellValue = cellValue.replace(matcher2.group(), matcher2.group().replace("=","=\"")+"\"");
				    	}
					}
					doc = DBXmlManager.getDOMFromXMLString("<root>"+cellValue+"</root>");
					Element rootElement = doc.getDocumentElement();
					NodeList imageNode = rootElement.getElementsByTagName("IMG");	
					Element imageElement = (Element) imageNode.item(0);
					String id = imageElement.getAttribute("id");
					String title = imageElement.getAttribute("title");
					String src = "htmlarea/images/entity.gif";
					String condition = imageElement.getAttribute("condition");
					String sequenceid = imageElement.getAttribute("sequenceid");
					String type = imageElement.getAttribute("type");
					String newString = "<img id=\""+id+"\" title=\""+title+"\" src=\""+src+"\" type=\""+type+"\" sequenceid=\""+sequenceid+"\" condition=\""+condition+"\" />";
					source = source.replace(matcher.group(), newString);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return source;
	}
}
package com.dci.rest.utils;

import java.util.*;
import java.sql.Timestamp;
import java.text.*;
public class DateFormat {
	
	public static String expandedDate(java.sql.Date p_Date) {
	//	System.out.print("From DB : "+p_Date);
		if( p_Date == null)
			return null;
		SimpleDateFormat dformatter; 
		dformatter = new SimpleDateFormat("MM/dd/yyyy");
	//	System.out.println("    ** After conversion : "+dformatter.format(p_Date).toString()); 
		return dformatter.format(p_Date).toString();
	}
	
	public static java.sql.Date getDateOfString(String p_StringDate) {
		if (p_StringDate == null || p_StringDate.equals(""))
			return null;
		else  {
			StringTokenizer st = new StringTokenizer(p_StringDate,"/-");
			String scatteredDate[] = new String[3];
	
			for(int i=0;i<3;i++) {
				scatteredDate[i] = st.nextToken();
			}
			if (scatteredDate[2].length()<4){
				if (CheckString.getInt(scatteredDate[2]) > 75)
					scatteredDate[2] = "19"+scatteredDate[2];
				else 
					scatteredDate[2] = "20"+scatteredDate[2];
			}	
			return java.sql.Date.valueOf(scatteredDate[2] + "-" + scatteredDate[0] + "-" + scatteredDate[1]);
		}

	}
	
	/**  To convert the data format.
	*/		
		public static String expandedTS(Timestamp p_Date) {
			if( p_Date == null)
				return "";
			SimpleDateFormat dformatter; 
			dformatter = new SimpleDateFormat("MM/dd/yyyy h:mm a"); 
			return dformatter.format(p_Date).toString();
		}

		public static String getCurrentDateMDY() {
			Date dt = new Date(System.currentTimeMillis());
			return expandedDate(dt);
		}

		/**  To convert the data format.
		 */
		public static String expandedDate(Date p_Date) {
			if (p_Date == null)
				return "";
			SimpleDateFormat dformatter;
			dformatter = new SimpleDateFormat("MM/dd/yyyy");
			return dformatter.format(p_Date).toString();
		}
		
		 public static java.sql.Date getDateofDB(String p_StringDate) {
			if (p_StringDate == null || p_StringDate.equals(""))
				return null;
			else  {
				return java.sql.Date.valueOf(p_StringDate);
			}

		 }
		 public static String getDateInt(String date){
			if (date == null) return null;
			StringTokenizer st = new StringTokenizer(date,"/-");
			String scatteredDate[] = new String[3];
			for(int i=0;i<3;i++) {
				scatteredDate[i] = st.nextToken();
			}
			String integer = scatteredDate[2]+scatteredDate[0]+scatteredDate[1];
			return integer;
		 }
		 public static String changeDateFormat(String date) {
			 String newdate = null ;
			 if(date!=null) {
				String sailingDate=date.trim();
				String ar[] =sailingDate.split( "-" );
				if (ar.length>=2)
				newdate = ar[1]+ "/" +ar[2]+ "/" +ar[0];
				else
				newdate=date;
			 }
			 return newdate;
		 }
		 
		 /**  To convert the data format.
			*/		
		public static String expandedDateMMddYY(java.sql.Date p_Date) {
			if( p_Date == null)
				return "";
			SimpleDateFormat dformatter; 
			dformatter = new SimpleDateFormat("MM/dd/yy"); 
			return dformatter.format(p_Date).toString();
		}
		public static String getCurrentDateTime() {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			return dateFormat.format(date);
			
		}
		public static long getUnixTimestamp(Date d) {
			long unixTime = (long)d.getTime()/1000;
			return unixTime;
			
		}
}

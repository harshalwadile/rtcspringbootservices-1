/*
 * DocuBuilder
 * Copyright (c) 2005, 2004 Data Communique International, Inc.
 * All Rights Reserved.
 * 
 * This code is Intellectual Property ("IP") of 
 * Data Communique International, Inc. ("DCI").
 *  
 * Any form of distribution or redistribution to any 
 * party other than DCI is not permitted unless authorized 
 * by DCI in writing.
 *  
 * All users (programmers, consultanst or vendors appointed 
 * by DCI) of this software must return to
 * 		Data Comunique International
 * 		330 Washington Ave
 * 		Carlstadt, NJ - 07072
 * any improvements or extensions that they make 
 * and grant the IP rights to DCI.
 * 
 */
package com.dci.rest.utils;

// SAX
import org.w3c.dom.*;
import org.xml.sax.*;

import com.dci.rest.dao.DataAccessObject;

import javax.xml.transform.*;

import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.dom.DOMSource;
import java.io.*;
import java.sql.*;
// big faceless.
import org.faceless.report.*;
import org.faceless.pdf2.*;

/**
 * @author spunukollu
 *
 *         To change this generated comment edit the template variable
 *         "typecomment": Window>Preferences>Java>Templates. To enable and
 *         disable the creation of type comments go to
 *         Window>Preferences>Java>Code Generation.
 */
public class GeneratePDF {

	public void createPDF() {

	}

	/**
	 *
	 * method processXSLFile
	 * 
	 * @param String
	 * @param String
	 * @param Writer
	 * @return
	 * 
	 */
	public void processXSLFile(String xmlDocument, String stylesheet, Writer out) throws IOException, SAXException {
		try {
			TransformerFactory tFactory = TransformerFactory.newInstance();
			StreamSource xmlSource = new StreamSource(xmlDocument);
			StreamSource xslSource = new StreamSource(stylesheet);
			Transformer transformer = tFactory.newTransformer(xslSource);
			StreamResult resultTarget = new StreamResult(out);
			transformer.transform(xmlSource, resultTarget);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SAXException(e.getMessage() + "ProcessXSLFile Error - XML,XSL, Writer ");
		}
	}

	/**
	 * 
	 * method processXSLFile
	 * 
	 * @param Document
	 * @param String
	 * @param Writer
	 * @return
	 * 
	 */
	public void processXSLFile(Document document, String stylesheet, Writer out) throws IOException, SAXException {

		try {
			TransformerFactory tFactory = TransformerFactory.newInstance();

			DOMSource xmlSource = new DOMSource(document);
			StreamSource xslSource = new StreamSource(stylesheet);
			Transformer transformer = tFactory.newTransformer(xslSource);
			StreamResult resultTarget = new StreamResult(out);
			transformer.transform(xmlSource, resultTarget);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SAXException(e.getMessage() + "ProcessXSLFile Error -DOM,XSL,Writer");
		} finally {

		}
	}

	/**
	 *
	 * method processXSLFile
	 * 
	 * @param Document
	 * @param InputStream
	 * @param Writer
	 * @return
	 * 
	 */
	public void processXSLFile(Document document, InputStream stylesheet, Writer out) throws IOException, SAXException {

		try {
			TransformerFactory tFactory = TransformerFactory.newInstance();
			DOMSource xmlSource = new DOMSource(document);
			StreamSource xslSource = new StreamSource(stylesheet);
			Transformer transformer = tFactory.newTransformer(xslSource);
			StreamResult resultTarget = new StreamResult(out);
			transformer.transform(xmlSource, resultTarget);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SAXException(e.getMessage() + "ProcessXSLFile Error -DOM,XSL,Writer");
		} finally {

		}
	}

	/**
	 * 
	 * method generatePDFbyFaceLess
	 * 
	 * @param Document
	 * @param String
	 * @param String
	 * @return
	 * 
	 */
	public void generatePDFbyFaceLess(Document document, String stylesheetPath, String targetPath)
			throws IOException, SAXException {
		java.io.Writer out = new StringWriter();
		processXSLFile(document, stylesheetPath, out);
		ReportParser rp = ReportParser.getInstance();
		ReportParser.setLicenseKey("GB2GB5D7794HH1H");
		String str = CheckString.fixSpecialCharforWeb(out.toString());
		InputSource is = new InputSource(new StringReader(str));
		is.setEncoding("utf-8");
		is.setSystemId("http://www.w3.org/1999/XSL/Format");
		PDF pdf = rp.parse(is);
		OutputStream secondout = new FileOutputStream(targetPath);
		pdf.render(secondout);
		secondout.close();
	}

	/**
	 * 
	 * method generatePDFbyFaceLess
	 * 
	 * @param Document
	 * @param InputStream
	 * @param OutputStream
	 * @return
	 * 
	 */
	public void generatePDFbyFaceLess(Document document, String stylesheetPath, OutputStream targetPath)
			throws IOException, SAXException {
		java.io.Writer out = new StringWriter();
		processXSLFile(document, stylesheetPath, out);
		ReportParser rp = ReportParser.getInstance();
		ReportParser.setLicenseKey("GB2GB5D7794HH1H");
		String str = CheckString.fixSpecialCharforWeb(out.toString());
		InputSource is = new InputSource(new StringReader(str));
		is.setEncoding("utf-8");
		is.setSystemId("http://www.w3.org/1999/XSL/Format");
		PDF pdf = rp.parse(is);
		pdf.render(targetPath);

	}

	/**
	 * 
	 * method generatePDFbyFaceLess
	 * 
	 * @param Document
	 * @param InputStream
	 * @param OutputStream
	 * @return
	 * 
	 */
	public void generatePDFbyFaceLess(Document document, InputStream stylesheetPath, OutputStream targetPath)
			throws IOException, SAXException {
		try {
			java.io.Writer out = new StringWriter();
			processXSLFile(document, stylesheetPath, out);
			ReportParser rp = ReportParser.getInstance();
			ReportParser.setLicenseKey("GB2GB5D7794HH1H");
			String str = out.toString();
			InputSource is = new InputSource(new StringReader(str));
			is.setEncoding("utf-8");
			is.setSystemId("http://www.w3.org/1999/XSL/Format");
			PDF pdf = rp.parse(is);
			pdf.render(targetPath);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SAXException(e.getMessage() + " ->> ProcessXSLFile Error - Generating PDF");
		}

	}

	/**
	 * 
	 * method copyFile
	 * 
	 * @param String
	 * @param String
	 * @return boolean
	 * 
	 */
	public static boolean copyFile(String sourcepath, String targetpath) {

		if (sourcepath == null || targetpath == null)
			return false;
		InputStream bis = null;
		BufferedOutputStream bos = null;
		try {
			bis = new FileInputStream(sourcepath);
			bos = new BufferedOutputStream(new FileOutputStream(targetpath));
			byte[] buff = new byte[2048];
			int bytesRead;

			while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
				bos.write(buff, 0, bytesRead);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (bis != null)
					bis.close();
				if (bos != null)
					bos.close();
			} catch (Exception e) {
			}
		}
		return true;
	}

	/**
	 * 
	 * method stringToFile
	 * 
	 * @param String
	 * @param String
	 * @return boolean
	 * 
	 */
	public boolean stringToFile(String text, String path) {
		try {

			ByteArrayInputStream bis = new ByteArrayInputStream(text.getBytes());
			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(path));
			byte[] buff = new byte[2048];
			int bytesRead;
			while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
				bos.write(buff, 0, bytesRead);
			}
			bis.close();
			bos.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static void main(String args[]) {
		try {

			Connection con = null;
			CallableStatement cs = null;
			ResultSet rs = null;
			try {
				con = new DataAccessObject().getConnection();
				cs = con.prepareCall("{call docubuildr.sp_utgetdocumentlist('1')}");
				rs = cs.executeQuery();
				StringBuffer sb = new StringBuffer("");
				while (rs != null && rs.next()) {
					sb.append(rs.getString(3));
				}

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (rs != null)
					rs.close();
				if (cs != null)
					cs.close();
				if (con != null)
					con.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

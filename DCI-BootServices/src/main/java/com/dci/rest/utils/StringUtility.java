
package com.dci.rest.utils;


import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtility {
	
	
	public static String fixEdgarFileName(String text) {
		int i = 0;
		Character c = null;
		char[] ctext = null;	
		StringBuffer newText = new StringBuffer("");
		if ((text == null) || (text.trim().length() == 0)) {		
			return newText.toString();
		} else {
			text = CheckString.removeCRLF(text);
			for(i=0;i<text.trim().length();i++) {
				ctext = text.trim().substring(i, i+1).toCharArray();
				c = new Character(ctext[0]);
				if(ctext[0]> 96 && ctext[0] < 123){
					newText.append(c);
				}
			}
		}	
		return newText.toString();
	}
	/**
	 * Insert the method's description here.
	 * Creation date: (3/15/2002 2:50:46 PM)
	 * @return java.lang.String
	 * @param text java.lang.String
	 */
	public static String fixSpecialCharforWeb(String text) {
	  text = CheckString.replaceSkypeIconWithCleanText(text);
		text = CheckString.fixAsciiChars(text);
		text=text.replaceAll("&shy;","-").replaceAll("�", "-").replaceAll("<font[^>]*>", "").replaceAll("</font>", "");//Code added for DB-3434(&shy;) and DBP-452(<font tag removing)
		return text;
	}
	
	/**
	 * @Purpose: this method will get returnValue
	 * @return String
	 */
	public static  String substituteAll(String source, String toBeReplaced, String replacement)
	{
		String returnValue = source;
		while(returnValue.indexOf(toBeReplaced) >= 0)
		{
			returnValue = substituteFirst(returnValue, toBeReplaced, replacement);
		}
		return(returnValue);
	}
	
	/**
	 * @Purpose: this method will get returnValue
	 * @return String
	 */
	public static String substituteFirst(String source, String toBeReplaced, String replacement)
		{
			StringBuffer returnValue = new StringBuffer(source);
			int hit = source.indexOf(toBeReplaced);
			if(hit >= 0)
			{
				returnValue = new StringBuffer(source.substring(0, hit));
				returnValue.append(replacement);
				returnValue.append(source.substring(hit + toBeReplaced.length()));
			}
			return(returnValue.toString());
		}
	
	/**
	 * @Purpose: this method will get substituteEntityName
	 * @return String
	 */
	public static String substituteEntityName(String str){
	
		String [] entities = { "&tilde;", "&shy;", "&ndash;","&bull;","&euro;", "&mdash;","&yen;","&pound;","&dagger;","&trade;","&reg;","&copy;","&frac12;","&frac34;","&frac14;","&eacute;","&egrave;"};
				String [] replace = { "&#732;","&#173;","&#8211;","&#8226;","&#8364;", "&#8212;","&#165;","&#163;","&#8224;","&#8482;","&#174;","&#169;","&#189;","&#190;","&#188;","&#233;","&#232;"};
				for (int i =0; i< entities.length; i++){

					str = substituteAll(str, entities[i],replace[i]);			
				}
		return str;
	}
	
	/**
	 * @Purpose: this method will get fixEmp
	 * @return String
	 */
	public static String fixEmp(String str){
	
		String [] string = { "<emphasis >","</emphasis >"};
				String [] replace = { "<em>",        "</em>"};
				for (int i =0; i< string.length; i++){
					str = str.replaceAll(string[i],replace[i]);			
				}
		return str;
	}
	
	/**
	 * @Purpose: this method will get after removeHyper
	 * @return String
	 */
	public static String removeHyper(String source, String tag){
		if (source == null) return source;
		if(tag==null || tag.length()==0)
			tag = "<a href";
		int start = source.indexOf(tag);
		if ( start ==-1)
			return source;
		StringBuffer sb = new StringBuffer("");
		sb.append(source.substring(0,start));
		sb.append(source.substring(source.indexOf(">",start)+1));
		if (sb.toString().indexOf("</a>")>-1)
			sb.replace(sb.toString().indexOf("</a>"),sb.toString().indexOf("</a>")+"</a>".length(),"");		
		return removeHyper(sb.toString(), tag); 
	}
	
	/**
	 * @Purpose: this method will get fixSpecialAttributes
	 * @return String
	 */
	public static String fixSpecialAttributes(String source){
		source = CheckString.replaceStringX(source,"value="," value=");
		source = CheckString.replaceStringX(source,"title="," title=");
		source = CheckString.replaceStringX(source,"type="," type=");
		source = CheckString.replaceStringX(source,"idref="," idref=");
		source = CheckString.replaceStringX(source,"src="," src=");
		source = CheckString.replaceStringX(source,"order="," order=");
		source = CheckString.replaceStringX(source,"alt="," alt=");
		source = CheckString.replaceStringX(source,"size="," size=");
		source = CheckString.replaceStringX(source,"  "," ");
		return source;
	}
	
	/**
	 * @Purpose: this method will get List
	 * @return ArrayList
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ArrayList getList(String[] str) {
		if(str == null)
			return new ArrayList(); 
		ArrayList list = new ArrayList();
		for(int i=0; i<str.length; i++) {
			list.add(str[i]);
		}
		return list;
	}

	/**
	 * @Purpose: this method will get TrimmedQualDataTill50
	 * @return String
	 */
	public static String getTrimmedQualDataTill50(String qualData){
			if(qualData != null && qualData.length()>50){
				int charCount=0;			
				boolean isTagStart=false;
				StringBuffer tempQualData=new StringBuffer();
				for(int i=0; i<qualData.length()&& charCount<50; i++)
				{
					if(qualData.charAt(i) == '<')
						isTagStart=true;
					else if(qualData.charAt(i) == '>')
						{ isTagStart=false; charCount--;} 
					else if(isTagStart==false)
						charCount++;						
					tempQualData.append(qualData.charAt(i));
				}				
				qualData = tempQualData.toString();
				qualData = qualData + "...";
			}
			return  qualData; 
		}
	
	/**
	 * @Purpose: this method will get TrimmedQualDataTill100
	 * @return String
	 */
	public static String getTrimmedQualDataTill100(String qualData){

		if(qualData != null && qualData.length()>100){
			int charCount=0;			
			boolean isTagStart=false;
			StringBuffer tempQualData=new StringBuffer();
			for(int i=0; i<qualData.length()&& charCount<100; i++)
			{
				if(qualData.charAt(i) == '<')
					isTagStart=true;
				else if(qualData.charAt(i) == '>')
					{ isTagStart=false; charCount--;} 
				else if(isTagStart==false)
					charCount++;						
				tempQualData.append(qualData.charAt(i));
			}				
			qualData = tempQualData.toString();
			qualData = qualData + "...";
		}
		return  qualData; 
	}
	
	/**
	 * @Purpose: this method will get TrimmedQualData
	 * @return String
	 */
	public static String getTrimmedQualData(String qualData){
	
		if(qualData != null && qualData.length()> 150){

			int charCount=0;			
			boolean isTagStart=false;
			StringBuffer tempQualData=new StringBuffer();
			for(int i=0; i<qualData.length()&& charCount<150; i++)
			{
				if(qualData.charAt(i) == '<')
					isTagStart=true;
				else if(qualData.charAt(i) == '>')
					isTagStart=false;
				else if(isTagStart==false)
					charCount++;						
				tempQualData.append(qualData.charAt(i));
			}				
			qualData = tempQualData.toString();
		qualData = qualData + "...";
		}
		return  qualData; 
	}
	
	/**
	 * @Purpose: this method will get TrimmedQualDataTill300
	 * @return String
	 */
	public static String getTrimmedQualDataTill300(String qualData){

		if(qualData != null && qualData.length()>300){
			int charCount=0;			
			boolean isTagStart=false;
			StringBuffer tempQualData=new StringBuffer();
			for(int i=0; i<qualData.length()&& charCount<300; i++)
			{
				if(qualData.charAt(i) == '<')
					isTagStart=true;
				else if(qualData.charAt(i) == '>')
					{ isTagStart=false; charCount--;} 
				else if(isTagStart==false)
					charCount++;						
				tempQualData.append(qualData.charAt(i));
			}				
			qualData = tempQualData.toString();
			qualData = qualData + "...";
		}
		return  qualData; 
	}
	
	
	/**
	 * @Purpose:The method gettext is used to remove all the html tags from a chunk of data via regular expression
	 * @return String
	 */
	@SuppressWarnings({ "unused", "static-access" })
	public String  removeHtmlTags(String body) throws Exception {
		
		Pattern pattern = null;
        Matcher matcher = null;
        String regex;
        String strTagLess = null;
        strTagLess = body;
        int intWorkFlow=-1;
        if(strTagLess!=null){
        	if (intWorkFlow == -1) {
                regex = "<[^>]*>";
                // removes all html tags
                pattern = pattern.compile(regex);
                strTagLess = pattern.matcher(strTagLess).replaceAll(" ");
          }

          if (intWorkFlow > 0 && intWorkFlow < 3) {
                regex = "[<]";
                // matches a single <
                pattern = pattern.compile(regex);
                strTagLess = pattern.matcher(strTagLess).replaceAll("<");
                regex = "[>]";
                // matches a single >
                pattern = pattern.compile(regex);
                strTagLess = pattern.matcher(strTagLess).replaceAll(">");
          }
        }else{
        	
        	strTagLess="";
        }
		return strTagLess;
	}

	/**
	 * @Purpose:to remove unnecessary  spaces between text.
	 * @author Saswat
	 * @name removeSpaces
	 * @return String
	 */
	@SuppressWarnings("unused")
	public String  removeSpaces(String body) throws Exception {
		Pattern pattern = null;
        Matcher matcher = null;
        String regex;
        String strTagLess = null;
        strTagLess = body;
        strTagLess= strTagLess.replaceAll("<p>", "").replaceAll("</p>", "").replaceAll("<br>", "").replaceAll("<br/>", "").replaceAll("<br />", "").replaceAll("^\\s+", "").replaceAll("\\s+$", "").replaceAll("\\b\\s{2,}\\b", " ");
		return strTagLess;
	}
	
	public static void main(String[] args){
		String str = "<note><para>Please consider the investment objectives, risk, charge and expenses of the fund carefully before investing.&nbsp; The"+
				"prospectus contains this and other information about the fund.&nbsp; To obtain a prospectus, contact your Morgan Stanley  Financial Advisor or download one at <a href=\"http://www.morganstanley.com\">www.morganstanley.com</a>."+
				"Please read the prospectus carefully before investing. <a href=\"mailto:shivaji5@hotmail.com\">shivaji5@hotmail.com</a></para> <para>Distributor: Morgan Stanley Distributor Inc.&nbsp;</para> <para>Investment Adviser: Morgan Stanley Investment Advisors Inc.</para></note><para/>";
		str = "<p>  <p><font size=\"1\" condition=\"booksubhead\">APRIL 25,  <strong>2006<img id=\"46\"  title=\"anotherentity;\"   src=\"htmlarea/images/entity.gif\"   type=\"...</a>";
		
		getTrimmedQualData(str);
		}
	public static final String [] entitylist = { "38"     ,"148","149"    ,"152"   ,"173"  ,"150"    ,"151"    ,"128"    ,"165"   ,"163"   ,"189"   ,"188"   ,"190"   ,"134"    ,"153"    ,"174"   ,"169"   ,"233"   ,"232"};
	public static final String [] replacelist = {"&#038;", "\"","&#8226;","&#732;","&#173;","&#8211;","&#8212;","&#8364;","&#165;","&#163;","&#189;","&#188;","&#190;","&#8224;","&#8482;","&#174;","&#169;","&#233;","&#232;"};

	/**
	 * @Purpose:this method is used to substituteEntity.
	 * @return String
	 */
	public static String substituteEntity(String str){
		if (str == null || str.trim().length()==0)
			return str;
		str = fixSpecialCharforWeb(str);			
		return str;
	}
    
	/**
	 * @Purpose:this method is used to makeEntity.
	 * @return String
	 */
	public static String makeEntity(String str){
		if (str == null || str.trim().length()==0)
			return str;
		for(int i=0;i<replacelist.length;i++)	
		str = str.replaceAll(replacelist[i],((char)Integer.parseInt(entitylist[i]))+"");			
		return str;
	}	
	
	/**
	 * @Purpose:this method is used to getList.
	 * @return String[]
	 */
	/*public static String[] getList(Collection arrays) {
		if(arrays == null)
			return null; 
		String[] str = new String[arrays.size()];
		Iterator array = arrays.iterator(); 	
		for(int i=0; i<arrays.size(); i++) {
			str[i]=((MasterBean)array.next()).getSid();
		}
		return str;
	}*/
	
	public static String getNumberSequenceFromRange(String sequence){
		String finalStr = "";
		if(sequence != null && !sequence.trim().equals("")){
			String[] splitArr = sequence.split(",");
			for(int index = 0; index < splitArr.length; index++){
				String s = splitArr[index];
				if(s.indexOf("-") != -1){
					String[] splitTokenArr = s.split("-");
					int loopFromIndex = Integer.parseInt(splitTokenArr[0]);
					int loopToIndex = Integer.parseInt(splitTokenArr[1]);
					for(int tokenIndex = loopFromIndex; tokenIndex <= loopToIndex; tokenIndex++){
						if(finalStr.equals("")){
							finalStr = Integer.toString(tokenIndex);
						}else{
							finalStr = finalStr +","+ Integer.toString(tokenIndex);
						}
					}
				}else{
					if(finalStr.equals("")){
						finalStr = s;
					}else{
						finalStr = finalStr +","+ s;
					}
				}
			}
		}
		return finalStr;
		
	}
	
	public static String blackLiningStringOperations(String blacklining_xml, boolean cDataFlag){
		//blacklining_xml = blacklining_xml.replaceAll("color=", "color:"); 
		if(cDataFlag){
			blacklining_xml = "<![CDATA[" + blacklining_xml + "]]>";
		}
		return blacklining_xml;
	}
	
	@SuppressWarnings("rawtypes")
	public static String replaceControlCharacters(String inputStr, Map otherParams){
		if(inputStr != null && !inputStr.trim().equals("")){
			inputStr = inputStr.replaceAll("[\u0000-\u001f]", "");
		}
		return inputStr;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static String arrangeSortedNumbersInRange(String sequence){
		String[] splitArr = sequence.split(",");
		int num = Integer.parseInt(splitArr[0]);
		List rangeList = new ArrayList();
		String rangeItem = "";
		boolean rangeBreak = false;
		for(int index = 0; index < splitArr.length; index++){
			String s = splitArr[index];
			int numInArr = Integer.parseInt(s);
			boolean lastItemTaken = false;
			if(num == numInArr){
				if(rangeItem.equals("")){
					rangeItem = numInArr + ",";
				}else{
					rangeItem = rangeItem + numInArr + ",";
				}
				if(index != (splitArr.length - 1)){
					num++;
				}else if(index == (splitArr.length - 1)){
					lastItemTaken = true;
					rangeBreak = true;
				}
			}else{
				rangeBreak = true;
				num = numInArr;
				num++;
			}
			if(rangeBreak){
				rangeBreak = false;
				if(rangeItem.equals("")){
				}else{
					if(rangeItem.lastIndexOf(",") == rangeItem.length()-1){
						rangeItem = rangeItem.substring(0,rangeItem.length()-1);
					}
					rangeList.add(rangeItem);
				}
				if(index == (splitArr.length - 1)){
					if(!lastItemTaken){
						rangeList.add(""+numInArr);
					}
				}else{
					rangeItem = numInArr+",";
				}
			}
		}
		String finalStr = "";
		for(Object s : rangeList){
			String sortedStr = (String) s;
			String[] sortedStrArr = sortedStr.split(",");
			if(sortedStrArr.length == 1){
				if(finalStr.equals("")){
					finalStr = sortedStrArr[0];
				}else{
					finalStr += "," + sortedStrArr[0];
				}
			}else{
				if(finalStr.equals("")){
					finalStr = sortedStrArr[0] + "-" + sortedStrArr[sortedStrArr.length - 1];
				}else{
					finalStr += "," + sortedStrArr[0] + "-" + sortedStrArr[sortedStrArr.length - 1];
				}
			}
		}
		return finalStr;
	}
}


package com.dci.rest.utils;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.tidy.Tidy;

import com.dci.rest.config.DocuBuilder;


@SuppressWarnings("unchecked")
public class CheckString {

	
		
/**
 * Insert the method's description here.
 * Creation date: (3/15/2002 2:50:46 PM)
 * @return java.lang.String
 * @param text java.lang.String
 */
public static String fixEscapeChar(String text) {
	int i = 0;
	String newText = "";
	//String temp = "";
	if ((text == null) || (text.trim().length() == 0)) {
		newText = "";
		return newText;
	} else {
		for(i=0;i<text.trim().length();i++) {
			if (text.trim().substring(i, i+1).equals("")) {
				newText = newText + "''";;
			}
			if (text.trim().substring(i, i+1).equals("'")) {
				newText = newText + "''";
			} else {
				newText = newText + text.trim().substring(i, i+1);
			}
		}
	}
//	InfoLog.log(InfoLog.DEBUG, "CheckString:fixEscapeChar","text: " + text + " newText: " + newText);
	return newText;
}
/**
 * Insert the method's description here.
 * Creation date: (3/15/2002 2:50:46 PM)
 * @return java.lang.String
 * @param text java.lang.String
 */
public static String fixEscapeCharforWeb(String text) {
	int i = 0;
	String newText = "";
	if ((text == null) || (text.trim().length() == 0)) {
		newText = "";
		return newText;
	} else {
		for(i=0;i<text.trim().length();i++) {
			if (text.trim().substring(i, i+1).equals("'")) {
				newText = newText + "&#039;";
			} else {
				newText = newText + text.trim().substring(i, i+1);
			}
		}
	}
//	developerLog.debug("text: " + text + " newText: " + newText);
	return newText;
}
/**
 * Insert the method's description here.
 * Creation date: (10/30/2002 4:08:37 PM)
 * @return java.lang.String
 * @param text java.lang.String
 */
public static String fixReturnChar(String text) {
	int i = 0;
	String newText = "";
	if ((text == null) || (text.trim().length() == 0)) {
		newText = "";
		return newText;
	} else {
		for(i=0;i<text.trim().length();i++) {
			if (text.trim().substring(i, i+1).equals("<")) {
				newText = newText + "\r";
				i = i + 3;
			} else {
				newText = newText + text.trim().substring(i, i+1);
			}
		}
	}
//	InfoLog.log(InfoLog.DEBUG, "CheckString:fixReturnChar", "text: " + text + " newText: " + newText);
	return newText;
}
/**
 * Insert the method's description here.
 * Creation date: (10/30/2002 4:08:37 PM)
 * @return java.lang.String
 * @param text java.lang.String
 */
public static String fixReturnChar(String text, boolean reverse) {
	int i = 0;
	String newText = "";
	if ((text == null) || (text.trim().length() == 0)) {
		newText = "";
		return newText;
	} else {
		if (reverse) {
			for(i=0;i<text.trim().length();i++) {
				if (text.trim().substring(i, i+1).equals("<")) {
					newText = newText + "\r";
					i = i + 3;
				} else {
					newText = newText + text.trim().substring(i, i+1);
				}
			}
		} else {
			for(i=0;i<text.trim().length();i++) {
				if (text.trim().substring(i, i+1).equals("\r")) {
					newText = newText + "<br>";
					i = i + 1;
				} else {
					newText = newText + text.trim().substring(i, i+1);
				}
			}
		}
	}
	//InfoLog.log(InfoLog.DEBUG, "CheckString:fixReturnChar", "text: " + text + " newText: " + newText);
	return newText;
}
/**
 * Insert the method's description here.
 * Creation date: (4/3/2002 10:57:06 AM)
 * @return java.lang.String
 * @param text java.lang.String
 */
public static String fixSingleQuote(String text) {
	int i = 0;
	String newText = "";
	if ((text == null) || (text.trim().length() == 0)) {
		newText = "";
		return newText;
	} else {
		for(i=0;i<text.trim().length();i++) {
			if (text.trim().substring(i, i+1).equals("\'")) {
				newText = newText + "\"";
			} else {
				newText = newText + text.trim().substring(i, i+1);
			}
		}
	}
//	InfoLog.log(InfoLog.DEBUG, "CheckString:fixEscapeChar","text: " + text + " newText: " + newText);
	return newText;
}
/**
 * Insert the method's description here.
 * Creation date: (3/15/2002 2:50:46 PM)
 * @return java.lang.String
 * @param text java.lang.String
 */
public static String fixSpecialCharforWeb(String text) {
	int i = 0;
	Character c = null;
	char[] ctext = null;
	String newText = "";
	boolean changed = false;
	if ((text == null) || (text.trim().length() == 0)) {
		newText = "";
		return newText;
	} else {
		text = removeCRLF(text);
		for(i=0;i<text.trim().length();i++) {
			ctext = text.trim().substring(i, i+1).toCharArray();
			c = new Character(ctext[0]);
			//Single quote
			if ((text.trim().substring(i, i+1).equals("'")) || (c.hashCode() == 8217) || (text.trim().substring(i, i+1).equals("�")) || (c.hashCode() == 146) || (c.hashCode() == 145)) {
				newText = newText + "'";
				changed = true;
			}
			//Double quotes
			if ((c.hashCode() == 8220) || (c.hashCode() == 8221) || (c.hashCode() == 147) || (c.hashCode() == 148)) {
				newText = newText + "\"";
				changed = true;
			}
			if ((i+2 <= text.length()) && (!text.substring(i,i+2).equalsIgnoreCase("&#"))) {
				// bullet point
				if ((c.hashCode() == 8226) || (c.hashCode() == 149)){
					newText = newText + "&#8226;";
					changed = true;
				}
				// tilde
				if ((c.hashCode() == 732) || (c.hashCode() == 152)){
					newText = newText + "&#732;";
					changed = true;
				}
				// Soft Hypen
				if (c.hashCode() == 173){
					newText = newText + "&#173;";
					changed = true;
				}
				// En-Dash
				if ((c.hashCode() == 8211) || (c.hashCode() == 150)) {
					newText = newText + "&#8211;";
					changed = true;
				}
				// Em-Dash
				if ((c.hashCode() == 8212) || (c.hashCode() == 151)) {
					newText = newText + "&#8212;";
					changed = true;
				}
				// Euro Sign
				if ((c.hashCode() == 8364) || (c.hashCode() == 128)) {
					newText = newText + "&#8364;";
					changed = true;
				}
				// Yen Sign
				if (c.hashCode() == 165) {
					newText = newText + "&#165;";
					changed = true;
				}
				// Pound Sign
				if (c.hashCode() == 163) {
					newText = newText + "&#163;";
					changed = true;
				}
				// 1/2 sign
				if (c.hashCode() == 189) {
					newText = newText + "&#189;";
					changed = true;
				}
				// 1/4 sign
				if (c.hashCode() == 188) {
					newText = newText + "&#188;";
					changed = true;
				}
				// 3/4 sign
				if (c.hashCode() == 190) {
					newText = newText + "&#190;";
					changed = true;
				}
				// Sword/dagger
				if ((c.hashCode() == 8224) || (c.hashCode() == 134)) {
					newText = newText + "&#8224;";
					changed = true;
				}
				// Trademark
				if ((c.hashCode() == 8482) || (c.hashCode() == 153)) {
					newText = newText + "&#8482;";
					changed = true;
				}
				// Ampersand & 
				if ((text.trim().substring(i, i+1).equals("&")) || (c.hashCode() == 38)) {
					newText = newText + "&#038;";
					changed = true;
				}
				//Registered mark
				if ((text.trim().substring(i, i+1).equals("�")) || (c.hashCode() == 174)) {
					newText = newText + "&#174;";
					changed = true;
				}
				//Copyright mark
				if ((text.trim().substring(i, i+1).equals("�")) || (c.hashCode() == 169)) {
					newText = newText + "&#169;";
					changed = true;
				}
				// Question.
				if (c.hashCode() == 63 && c.toString().equals("?")){
//					developerLog.debug(c+"          found");
					newText = newText + "?";
					changed = true;
				} else 
				// Normal dash
/*				if (c.hashCode() == 63){
					newText = newText + "-";
					changed = true;
				}*/
				//� as in Data Communiqu�
				if ((text.trim().substring(i, i+1).equals("�")) || (c.hashCode() == 233)) {
					newText = newText + "&#233;";
					changed = true;
				}
				//� 
				if ((text.trim().substring(i, i+1).equals("�")) || (c.hashCode() == 232)) {
					newText = newText + "&#232;";
					changed = true;
				}
			}
			if (!changed) {
				newText = newText + text.trim().substring(i, i+1);
			}
			
			changed = false;
			
		}
	}
	//developerLog.debug("text: " + text + " newText: " + newText);
	return newText;
}

@SuppressWarnings("unused")
public static String reverseFixSpecialCharforWeb(String text) {
	int i = 0;
	Character c = null;
	char[] ctext = null;
	String newText = "";
	boolean changed = false;
	if ((text == null) || (text.trim().length() == 0)) {
		newText = "";
		return newText;
	} else {
		//bullet point
		
		text = removeCRLF(text);
		newText=text;
		// En-Dash
		newText =replaceString(newText, "&#8211;", "�");
		// Em-Dash
		newText =replaceString(newText, "&#8212;", "�");
		// Euro Sign
		newText =replaceString(newText, "&#8364;", "�");
		// Yen Sign
		newText =replaceString(newText, "&#165;", "�");
		// Pound Sign
		newText =replaceString(newText, "&#163;", "�");
		// 1/2 sign
		newText =replaceString(newText, "&#189;", "�");
		// 1/4 sign
		newText =replaceString(newText, "&#188;", "�");
		// 3/4 sign
		newText =replaceString(newText, "&#190;", "�");
		// Sword/dagger
		newText =replaceString(newText, "&#8224;", "�");
		// Trademark
		newText =replaceString(newText, "&#8482;", "�");
		// Ampersand & 
		newText =replaceString(newText, "&#038;", "&");
		//Registered mark
		newText =replaceString(newText, "&#174;", "�");
		//Copyright mark
		newText =replaceString(newText, "&#169;", "�");
		//� 
		newText =replaceString(newText, "&#232;", "�");
	}
	//developerLog.debug("text: " + text + " newText: " + newText);
	return newText;
}

/**
 * Used to fix all special characters to render the pdf document.  
 * Creation date: (4/27/2004 2:50:46 PM)
 * @return java.lang.String
 * @param text java.lang.String
 */
public static String fixSpecialCharforRender(String text) {
	int i = 0;
	Character c = null;
	char[] ctext = null;
	String newText = "";
	boolean changed = false;
	if ((text == null) || (text.trim().length() == 0)) {
		newText = "";
		return newText;
	} else {
		text = removeCRLF(text);
		for(i=0;i<text.trim().length();i++) {
			ctext = text.trim().substring(i, i+1).toCharArray();
			c = new Character(ctext[0]);
			//Single quote
			if ((text.trim().substring(i, i+1).equals("'")) || (c.hashCode() == 8217) || (text.trim().substring(i, i+1).equals("�")) || (c.hashCode() == 146) || (c.hashCode() == 145)) {
				newText = newText + "'";
				changed = true;
			}
			//Double quotes
			if ((c.hashCode() == 8220) || (c.hashCode() == 8221) || (c.hashCode() == 147) || (c.hashCode() == 148)) {
				newText = newText + "\"";
				changed = true;
			}
			if ((i+2 <= text.length()) && (!text.substring(i,i+2).equalsIgnoreCase("&#"))) {
				// bullet point
				if ((c.hashCode() == 8226) || (c.hashCode() == 149)){
					newText = newText + "&#8226;";
					changed = true;
				}
				// tilde
				if ((c.hashCode() == 732) || (c.hashCode() == 152)){
					newText = newText + "&#732;";
					changed = true;
				}
				// Soft Hypen
				if (c.hashCode() == 173){
					newText = newText + "&#173;";
					changed = true;
				}
				// En-Dash
				if ((c.hashCode() == 8211) || (c.hashCode() == 150)) {
					newText = newText + "&#8211;";
					changed = true;
				}
				// Em-Dash
				if ((c.hashCode() == 8212) || (c.hashCode() == 151)) {
					newText = newText + "&#8212;";
					changed = true;
				}
				// Euro Sign
				if ((c.hashCode() == 8364) || (c.hashCode() == 128)) {
					newText = newText + "&#8364;";
					changed = true;
				}
				// Yen Sign
				if (c.hashCode() == 165) {
					newText = newText + "&#165;";
					changed = true;
				}
				// Pound Sign
				if (c.hashCode() == 163) {
					newText = newText + "&#163;";
					changed = true;
				}
				// 1/2 sign
				if (c.hashCode() == 189) {
					newText = newText + "&#189;";
					changed = true;
				}
				// 1/4 sign
				if (c.hashCode() == 188) {
					newText = newText + "&#188;";
					changed = true;
				}
				// 3/4 sign
				if (c.hashCode() == 190) {
					newText = newText + "&#190;";
					changed = true;
				}
				// Sword/dagger
				if ((c.hashCode() == 8224) || (c.hashCode() == 134)) {
					newText = newText + "&#8224;";
					changed = true;
				}
				// Trademark
				if ((c.hashCode() == 8482) || (c.hashCode() == 153)) {
					newText = newText + "&#8482;";
					changed = true;
				}
				// Ampersand & 
//				if ((text.trim().substring(i, i+1).equals("&")) || (c.hashCode() == 38)) {
//					newText = newText + "&#038;";
//					changed = true;
//				}
				//Registered mark
				if ((text.trim().substring(i, i+1).equals("�")) || (c.hashCode() == 174)) {
					newText = newText + "&#174;";
					changed = true;
				}
				//Copyright mark
				if ((text.trim().substring(i, i+1).equals("�")) || (c.hashCode() == 169)) {
					newText = newText + "&#169;";
					changed = true;
				}
				// Question.
				if (c.hashCode() == 63 && c.toString().equals("?")){
//					developerLog.debug(c+"          found");
					newText = newText + "?";
					changed = true;
				} else 
				// Normal dash
/*				if (c.hashCode() == 63){
					newText = newText + "-";
					changed = true;
				}*/
				//� as in Data Communiqu�
				if ((text.trim().substring(i, i+1).equals("�")) || (c.hashCode() == 233)) {
					newText = newText + "&#233;";
					changed = true;
				}
				//� 
				if ((text.trim().substring(i, i+1).equals("�")) || (c.hashCode() == 232)) {
					newText = newText + "&#232;";
					changed = true;
				}
			}
			if (!changed) {
				newText = newText + text.trim().substring(i, i+1);
			}
			
			changed = false;
			
		}
	}
	//developerLog.debug("text: " + text + " newText: " + newText);
	return newText;
}
/**
 * Insert the method's description here.
 * Creation date: (5/21/2003 2:56:30 PM)
 * @return java.lang.String
 * @param text java.lang.String
 */
public static String getLastWord(String text, boolean reverse) {
	String newText = "";
	int i = 0;
	boolean done = false;
	if ((text == null) || (text.trim().length() == 0)) {
		return newText;
	}
	i = text.trim().length();
	while((!done) && (!text.trim().substring(i-1,i).equalsIgnoreCase(" ")) && (!text.trim().substring(i-1,i).equalsIgnoreCase("\r")) && (!text.trim().substring(i-1,i).equalsIgnoreCase("\t")) && ((!text.trim().substring(i-1,i).equalsIgnoreCase("\n")))) {
		if (i > 1) {
			newText = text.trim().substring(i-1, i) + newText;
			i--;
		} else {
			newText = text.trim();
			i = 1;
			done = true;
		}
	}
	return newText;
}
/**
 * Insert the method's description here.
 * Creation date: (5/21/2003 2:56:30 PM)
 * @return java.lang.String
 * @param text java.lang.String
 */
public static String getWord(String text) {
	String newText = "";
	if ((text == null) || (text.trim().length() == 0)) {
		return newText;
	}
	for(int i=0;i<text.trim().length();i++) {
		if (text.trim().substring(i,i+1).equalsIgnoreCase(" ")) {
		} else {
			newText = newText + text.trim().substring(i, i+1);
		}
	}
	return newText;
}
/**
 * Insert the method's description here.
 * Creation date: (5/21/2003 2:56:30 PM)
 * @return java.lang.String
 * @param text java.lang.String
 */
public static String getWord(String text, boolean reverse) {
	String newText = "";
	int i = 0;
	if ((text == null) || (text.trim().length() == 0)) {
		return newText;
	}
	while(!text.trim().substring(i,i+1).equalsIgnoreCase(" ")) {
		newText = newText + text.trim().substring(i, i+1);
		i++;
	}
	if (!reverse) {
		newText = text.trim().substring(newText.trim().length(), text.trim().length());
	}
	return newText;
}
/**
 * Insert the method's description here.
 * Creation date: (5/21/2003 2:56:30 PM)
 * @return java.lang.String
 * @param text java.lang.String
 */
public static String getWordsFromPara(String text, boolean reverse) {
	String newText = "";
	boolean found = false;
	Character c = null;
	char[] ctext = null;
	int i = 0;
	if ((text == null) || (text.trim().length() == 0)) {
		return newText;
	}
	if (reverse) {
		found = true;
	}
	text = removeCRLF(text);
	for(i=0;i<text.trim().length();i++) {
		ctext = text.trim().substring(i, i+1).toCharArray();
		c = new Character(ctext[0]);
		if (text.trim().substring(i, i+1).equals(":")) {
			if (!reverse) {
				if (!found) {
					found = true;
				}
			} else {
				if (found) {
					found = false;
				}
			}
		}
		if (!found) {
			if ((c.hashCode() != 8230) || (c.hashCode() != 13)) {
				newText = newText + text.trim().substring(i, i+1);
			}
		}
		if ((text.trim().substring(i, i+1).equals("\r")) || (text.trim().substring(i, i+1).equals("\n")) || (c.hashCode() == 133) || (c.hashCode() == 8230)) {
			found = !found;
			if (reverse) {
				newText = newText.trim().substring(0,newText.trim().indexOf(":")) + newText.trim().substring(newText.trim().indexOf(":")+1, newText.trim().length());
			}
			newText = newText + "\r";
		}
	}
	if (reverse) {
		newText = newText.trim().substring(0,newText.trim().indexOf(":")) + newText.trim().substring(newText.trim().indexOf(":")+1, newText.trim().length());
	}

	return newText;
}
/**
 * Insert the method's description here.
 * Creation date: (4/3/2002 10:57:06 AM)
 * @return java.lang.String
 * @param text java.lang.String
 */
public static String insertCommentaryParaFormat(String text, String pFormat, boolean bullet) {
	int i = 0; 
	Character c = null;
	char[] ctext = null;
	String newText = "";
	boolean newline = false;
	boolean newpara = false;
	String textformat = "<p requote=\"true\" border=\"0\" padding=\"0\" margin=\"0\" margin-top=\"4\" align=\"left\" font-family=\"text\" font-size=\"9.5\" color=\"black\" letter-spacing=\"-0.3\">";
	if ((text == null) || (text.trim().length() == 0)) {
		newText = "";
		return newText;
	} else {
		ctext = text.trim().substring(i, i+1).toCharArray();
		c = new Character(ctext[0]);
		for(i=0;i<text.trim().length();i++) {
			if ( (text.trim().substring(i, i+1).equals("\r")) || (c.hashCode() == 133)) {
				newpara = true;
			} else {
				newpara = false;
			}
			if ((text.trim().substring(i, i+1).equals("\t"))) {
				newline = true;
			} else {
				newline = false;
			}
			if (newline || newpara) {
				if ((pFormat != null) && (pFormat.trim().length() > 0)) {
					newText = newText + "</p>";
				} 
				if (bullet) {
					newText = newText + "</li>";
				}
				if (text.trim().length() > i + 2) {
					if (newpara) {
						newText = newText + textformat;
					} else {
						newText = newText + pFormat;
					}
				}
			} else {
//				developerLog.debug(c.charValue() + " " + c.getNumericValue(ctext[0]) + " " + c.hashCode());
				if (c.hashCode() == 26) {
					newText = newText + "";
				} else {
					newText = newText + text.trim().substring(i, i+1);
				}
			}
		}
		if ((pFormat != null) && (pFormat.trim().length() > 0)) {
			newText = newText + "</p>";
		}
		if (bullet) {
			newText = newText + "</li>";
		}
	}
//	InfoLog.log(InfoLog.DEBUG, "CheckString:fixEscapeChar","text: " + text + " newText: " + newText);
	return newText;
}
/**
 * Insert the method's description here.
 * Creation date: (4/3/2002 10:57:06 AM)
 * @return java.lang.String
 * @param text java.lang.String
 */
public static String insertParaFormat(String text, String pFormat, boolean bullet) {
	int i = 0;
	Character c = null;
	char[] ctext = null;
	String newText = "";
	//String textformat = "<p requote=\"true\" border=\"0\" padding=\"0\" margin=\"0\" margin-top=\"3\" align=\"left\" font-family=\"text\" font-size=\"9.5\" color=\"black\" letter-spacing=\"-0.3\">";
	String benchNoteFormat = "<p requote=\"true\" border=\"0\" padding=\"0\" margin=\"0\" margin-top=\"2\" margin-right=\"4\" padding-left=\"6\" padding-right=\"6\" align=\"left\" font-family=\"text\" font-size=\"8\" color=\"black\" letter-spacing=\"-0.3\">";
	if ((text == null) || (text.trim().length() == 0)) {
		if ((pFormat != null) && (pFormat.trim().length() > 0)) {
			newText = "</p>";
		} else {
			newText = "";
		}
		return newText;
	} else {
		for(i=0;i<text.trim().length();i++) {
			ctext = text.trim().substring(i, i+1).toCharArray();
			c = new Character(ctext[0]);
			if ((text.trim().substring(i, i+1).equals("\r")) || (text.trim().substring(i, i+1).equals("\n")) || (c.hashCode() == 133)) {
				if ((pFormat != null) && (pFormat.trim().length() > 0)) {
					newText = newText + "</p>";
				} 
				if (bullet) {
					newText = newText + "</li>";
				}
				if (text.trim().length() > i + 2) {
					if (text.trim().indexOf("Benchmark") > -1) {
						newText = newText + benchNoteFormat;
					} else {
						newText = newText + pFormat;
					}
				}
			} else {
				ctext = text.trim().substring(i, i+1).toCharArray();
				c = new Character(ctext[0]);
//				developerLog.debug(c.charValue() + " " + c.getNumericValue(ctext[0]) + " " + c.hashCode());
				if (c.hashCode() == 26) {
					newText = newText + "";
				} else {
					newText = newText + text.trim().substring(i, i+1);
				}
			}
		}
		if ((pFormat != null) && (pFormat.trim().length() > 0)) {
			if (pFormat.indexOf("<b>")>0) {
				newText = newText + "</b>";
			}
			newText = newText + "</p>";
		}
		if (bullet) {
			newText = newText + "</li>";
		}
	}
//	InfoLog.log(InfoLog.DEBUG, "CheckString:fixEscapeChar","text: " + text + " newText: " + newText);
	return newText;
}
/**
 * Insert the method's description here.
 * Creation date: (4/3/2002 10:57:06 AM)
 * @return java.lang.String
 * @param text java.lang.String
 */
public static String insertParaFormat(String text, String pFormat, boolean bullet, String item) {
	int i = 0;
	Character c = null;
	char[] ctext = null;
	String newText = "";
	//String textformat = "<p requote=\"true\" border=\"0\" padding=\"0\" margin=\"0\" margin-top=\"3\" align=\"left\" font-family=\"text\" font-size=\"9.5\" color=\"black\" letter-spacing=\"-0.3\">";
	String benchNoteFormat = "<p requote=\"true\" border=\"0\" padding=\"0\" margin=\"0\" margin-top=\"1.5\" margin-right=\"4\" padding-left=\"6\" padding-right=\"6\" align=\"left\" font-family=\"text\" font-size=\"8\" color=\"black\" letter-spacing=\"-0.3\">";
	if ((text == null) || (text.trim().length() == 0)) {
		if ((pFormat != null) && (pFormat.trim().length() > 0)) {
			newText = "</p>";
		} else {
			newText = "";
		}
		return newText;
	} else {
		for(i=0;i<text.trim().length();i++) {
			ctext = text.trim().substring(i, i+1).toCharArray();
			c = new Character(ctext[0]);
			if ((text.trim().substring(i, i+1).equals("\r")) || (text.trim().substring(i, i+1).equals("\n")) || (c.hashCode() == 133)) {
				if ((pFormat != null) && (pFormat.trim().length() > 0)) {
					newText = newText + "</p>";
				} 
				if (bullet) {
					newText = newText + "</li>";
				}
				if (text.trim().length() > i + 2) {
					if ((item.trim().indexOf("Details_Text") > -1) && (text.trim().indexOf("Benchmark") > -1)) {
						newText = newText + benchNoteFormat;
						//InfoLog.log(InfoLog.DEBUG, "CheckString:fixEscapeChar","text: " + text.trim() + " newText: " + newText.trim() + "item: " + item);
					} else {
						newText = newText + pFormat;
					}
				}
			} else {
				ctext = text.trim().substring(i, i+1).toCharArray();
				c = new Character(ctext[0]);
//				developerLog.debug(c.charValue() + " " + c.getNumericValue(ctext[0]) + " " + c.hashCode());
				if (c.hashCode() == 26) {
					newText = newText + "";
				} else {
					newText = newText + text.trim().substring(i, i+1);
				}
			}
		}
		if ((pFormat != null) && (pFormat.trim().length() > 0)) {
			if (pFormat.indexOf("<b>")>0) {
				newText = newText + "</b>";
			}
			newText = newText + "</p>";
		}
		if (bullet) {
			newText = newText + "</li>";
		}
	}
//	InfoLog.log(InfoLog.DEBUG, "CheckString:fixEscapeChar","text: " + text + " newText: " + newText);
	return newText;
}
/**
 * Insert the method's description here.
 * Creation date: (4/3/2002 10:57:06 AM)
 * @return java.lang.String
 * @param text java.lang.String
 */
public static String insertParaFormat(String text, String pFormat, boolean bullet, String item, boolean benchmark) {
	int i = 0;
	Character c = null;
	char[] ctext = null;
	String newText = "";
	//String textformat = "<p requote=\"true\" border=\"0\" padding=\"0\" margin=\"0\" margin-top=\"3\" align=\"left\" font-family=\"text\" font-size=\"9.5\" color=\"black\" letter-spacing=\"-0.3\">";
	String benchNoteFormat = "<p requote=\"true\" border=\"0\" padding=\"0\" margin=\"0\" margin-top=\"1.5\" margin-right=\"4\" padding-left=\"6\" padding-right=\"3\" align=\"left\" font-family=\"text\" font-size=\"8\" color=\"black\" letter-spacing=\"-0.3\">";
	if ((text == null) || (text.trim().length() == 0)) {
		if ((pFormat != null) && (pFormat.trim().length() > 0)) {
			newText = "</p>";
		} else {
			newText = "";
		}
		return newText;
	} else {
		text = removeCRLF(text);
		for(i=0;i<text.trim().length();i++) {
			ctext = text.trim().substring(i, i+1).toCharArray();
			c = new Character(ctext[0]);
			if ((text.trim().substring(i, i+1).equals("\r")) || (text.trim().substring(i, i+1).equals("\n")) || (c.hashCode() == 133)) {
				if ((pFormat != null) && (pFormat.trim().length() > 0)) {
					newText = newText + "</p>";
				} 
				if (bullet) {
					newText = newText + "</li>";
				}
				if (text.trim().length() > i + 2) {
					if (benchmark) {
						newText = newText + benchNoteFormat;
						//InfoLog.log(InfoLog.DEBUG, "CheckString:fixEscapeChar","text: " + text.trim() + " newText: " + newText.trim() + "item: " + item);
					} else {
						newText = newText + pFormat;
					}
				}
			} else {
				ctext = text.trim().substring(i, i+1).toCharArray();
				c = new Character(ctext[0]);
//				developerLog.debug(c.charValue() + " " + c.getNumericValue(ctext[0]) + " " + c.hashCode());
				if (c.hashCode() == 26) {
					newText = newText + "";
				} else {
					newText = newText + text.trim().substring(i, i+1);
				}
			}
		}
		if ((pFormat != null) && (pFormat.trim().length() > 0)) {
			if (pFormat.indexOf("<b>")>0) {
				newText = newText + "</b>";
			}
			newText = newText + "</p>";
		}
		if (bullet) {
			newText = newText + "</li>";
		}
	}
//	InfoLog.log(InfoLog.DEBUG, "CheckString:fixEscapeChar","text: " + text + " newText: " + newText);
	return newText;
}
/**
 * Insert the method's description here.
 * Creation date: (8/16/2001 11:49:50 AM)
 * @return boolean
 * @param x java.lang.String
 */
public static boolean isCharacter(String x) {
	String characters="-.,abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ";
	boolean result = false;
	// is x a String or a character?
	if(x.trim().length()>1) {
	  for(int j=0;j<x.trim().length();j++) {
		// call isCharacter recursively for each character
		result=isCharacter(x.substring(j,j+1));
		if(!result) return result;
	  }
	  return result;
	}
	else {
	  // if x is a character return true
	  if(characters.indexOf(x)>=0) return true;
	  return false;
	}
}
/**
 * Insert the method's description here.
 * Creation date: (8/14/2001 3:05:27 PM)
 * @return boolean
 * @param x java.lang.String
 */
public static boolean isNumeric(String x) {
	String numbers="0123456789";
	boolean number = false;
	// is x a String or a character?
	if(x.trim().length()>1) {
	  // remove negative sign
//	  x=Math.abs(x)+""; 
	  for(int j=0;j<x.trim().length();j++) {
		// call isNumeric recursively for each character
		number=isNumeric(x.substring(j,j+1));
		if(!number) return number;
	  }
	  return number;
	}
	else {
	  // if x is number return true
	  if(numbers.indexOf(x)>=0) return true;
	  return false;
	}
}
/**
 * Insert the method's description here.
 * Creation date: (6/11/2003 12:39:46 PM)
 * @return java.lang.String
 * @param text java.lang.String
 * used only in the table.
 */
public static String removeCRLFx(String text) {
	String newText = "";
	Character c = null;
	char[] ctext = null;
	if ((text == null) || (text.trim().length() == 0)) {
		return newText;
	} else {
		for (int i = 0; i<text.length();i++) {
			ctext = text.substring(i, i+1).toCharArray();
			c = new Character(ctext[0]);
			if ((c.hashCode() == 10) || (c.hashCode() == 13)) {
						newText = newText + "";
			} else {

				newText = newText + text.substring(i,i+1);
			}	
		}
	}
	return newText;
}
public static String removeCRLF(String text) {
	String newText = "";
	Character c = null;
	char[] ctext = null;
	if ((text == null) || (text.trim().length() == 0)) {
		return newText;
	} else {
		for (int i = 0; i<text.length();i++) {
			ctext = text.substring(i, i+1).toCharArray();
			c = new Character(ctext[0]);
//			developerLog.debug(c.hashCode()+"             "+c);
			if ((c.hashCode() == 10) || (c.hashCode() == 13)) {
//				if (text.trim().length() > i+2) {
//					ctext = text.trim().substring(i+1, i+2).toCharArray();
//					c = new Character(ctext[0]);
//					if ((c.hashCode() == 13) || (c.hashCode() == 10)) {
						newText = newText + " ";
//						i++;
//					} else {
//						newText = newText + text.trim().substring(i,i+1);
//					}
//				}
			} else {
//				developerLog.debug(newText);
				newText = newText + text.substring(i,i+1);
//				newText = newText + text.trim().substring(i,i+1); // change to avoid type = ype
			}	
		}
	}
	
	return newText;
}
public static String removeSpaceCR(String text) {
	String newText = "";
	//Character c = null;
	//char[] ctext = null;
	if ((text == null) || (text.trim().length() == 0)) {
		return newText;
	} else {
		for(int i=0;i<text.trim().length();i++) {
			if (text.trim().substring(i, i+2).equals(" \r")) {
				newText = newText + "\r";
				i++;
			} else {
				newText = newText + text.trim().substring(i, i+1);
			}
		}
	}
	
	return newText;
}
/**
 * Insert the method's description here.
 * Creation date: (4/21/2003 4:45:39 PM)
 * @return java.lang.String
 * @param text java.lang.String
 */
public static String removeExtraSpaces(String text) {
	String newText = "";
	//Character c = null;
	/*char[] ctext = null;
	boolean space1 = false;
	boolean space2 = false;
	int i = 0;*/
	if ((text != null) && (text.trim().length() > 2)) {
		newText = text;
	} else {
		return text;
	}
	while (newText.trim().indexOf("  ") > -1) {
		newText = newText.trim().substring(0,newText.trim().indexOf("  ")) + " " + newText.trim().substring(newText.trim().indexOf("  ") + 2, newText.trim().length());
	}
	return newText;
}
/**
 * Insert the method's description here.
 * Creation date: (4/3/2002 10:57:06 AM)
 * @return java.lang.String
 * @param text java.lang.String
 */
public static String removeQuotes(String text) {
	int i = 0;
	String newText = "";
	if ((text == null) || (text.trim().length() == 0)) {
		newText = "";
		return newText;
	} else {
		for(i=0;i<text.trim().length();i++) {
			if (text.trim().substring(i, i+1).equals("\"")) {
				newText = newText + "";
			} else {
				newText = newText + text.trim().substring(i, i+1);
			}
		}
	}
//	InfoLog.log(InfoLog.DEBUG, "CheckString:fixEscapeChar","text: " + text + " newText: " + newText);
	return newText;
}
/**
 * Insert the method's description here.
 * Creation date: (6/24/2003 1:06:20 PM)
 * @return java.lang.String
 * @param source java.lang.String
 * @param find java.lang.String
 * @param replace java.lang.String
 */
public static String replaceString(String source, String find, String replace) {
	String result = "";
	if ((source == null) || (source.trim().length() == 0)) {
		return result;
	} else {
		result = source;
		while (source.trim().indexOf(find) > -1) {
			result = source.trim().substring(0,source.trim().indexOf(find)) + replace + source.trim().substring(source.trim().indexOf(find)+find.length(), source.trim().length());
			source = result;
		}

	}
	//InfoLog.log(InfoLog.DEBUG, "CheckString:replaceString","source: " + source + " result: " + result);
	return result;
}

public static String replaceStringX(String source, String find, String replace) {
	String result = "";
	if ((source == null) || (source.trim().length() == 0)) {
		return result;
	} else {
		StringBuffer sb = new StringBuffer("");
		result = source;
//		developerLog.debug(source);
		while (source.indexOf(find) > -1) {
			sb.append(source.substring(0,source.indexOf(find)) + replace );			
//			developerLog.debug(sb.toString());
			result = source.substring(source.indexOf(find)+find.length(), source.length());
			source = result;
		}
		sb.append(source);
		result = sb.toString();
	}
	//InfoLog.log(InfoLog.DEBUG, "CheckString:replaceString","source: " + source + " result: " + result);
	return result;
}


//Html Markup list.
//private static String html[] = {"<P>"   ,"</P>",   "<STRONG>",              "</STRONG>",  "<SUB>",      "</SUB>",       "<SUP>",         "</SUP>",        "<EM>",        "</EM>"      ,"<LI>",             "</LI>",              "<OL>",        "</OL>",         "<UL>",          "</UL>",           "<BR>"    };

// DocBook Markup list
//private static String db[] =   {"<para>","</para>","<emphasis role='bold'>","</emphasis>","<subscript>","</subscript>", "<superscript>", "</superscript>","<emphasis >","</emphasis >","<listitem><para >","</para ></listitem>","<orderedlist>","</orderedlist>","<itemizedlist>","</itemizedlist>","<para/>" }; //doc book

/**
 * Method to format the text from the Html markups to DocBook markups
 *
public static String convertToDocBook(String source) {
	source = fixSpecialCharforWeb(source);
	if (source == null || source.trim().length() ==0) return source;
	for(int i=0; i<html.length;i++)		
	source = replaceString(source, html[i], db[i]);
	return source;
  }

  
public static String docBookToHtml(String source) {
	source = fixSpecialCharforWeb(source);
	if (source == null || source.trim().length() ==0) return source;
	for(int i=0; i<html.length;i++)		
	source = replaceString(source, db[i], html[i]);
	return source;
  }
*/
	private static String htmlorder[] = {"<P>"   ,"</P>",   "<STRONG>",              "</STRONG>",  "<SUB>",      "</SUB>",       "<SUP>",         "</SUP>",        "<EM>",        "</EM>" };
	
	// DocBook Markup list
	private static String dborder[] =   {"<listitem><para>","</para></listitem>","<emphasis role='bold'>","</emphasis>","<subscript>","</subscript>", "<superscript>", "</superscript>","<emphasis >","</emphasis >" }; //doc book
	  
	/**
	 * Method to format the text from the Html markups to DocBook markups
	 * */
	public static String orderToDocBook(String source) {
		source = fixSpecialCharforWeb(source);
		if (source == null || source.trim().length() ==0) return source;
		for(int i=0; i<htmlorder.length;i++)		
		source = replaceString(source, htmlorder[i], dborder[i]);
		return source;
	}


	/**
	 * Method to format the text from the Html markups to DocBook markups
	 * */
	 
 	private static String htmlorder2[] = { "</inlinestyle>", "</emphasis>","<sbr/>","<para>"   ,"</para>","<para/>", "<note>"   ,"</note>","<P>"   ,"</P>",   "<STRONG>",              "</STRONG>",  "<SUB>",      "</SUB>",       "<SUP>",         "</SUP>",        "<EM>",        "</EM>" };
	public static String htmltotext(String source) {
		source = fixSpecialCharforWeb(source);
		//source= stripOffhalfTag(source, "inlinestyle");
		source= stripOffhalfTag(source, "img");
		//source= stripOffhalfTag(source, "emphasis");
	
		
		if (source == null || source.trim().length() ==0) return source;
	
		for(int i=0; i<htmlorder2.length;i++)		
		source = replaceString(source, htmlorder2[i], "");
		return source;
	}
/**
 * Method to format the text from the DocBook markups to Html markups
 * */
	public static String orderToHtml(String source) {
		source = fixSpecialCharforWeb(source);
		if (source == null || source.trim().length() ==0) return source;
		for(int i=0; i<htmlorder.length;i++)		
		source = replaceString(source, dborder[i], htmlorder[i]);
		return source;
	}

	
	
	public static String convertToXHTML(String str) {
		String convertedSource = null;
		if (str == null || str.length()==0) return str;
		final String BODY = "<body>";
		final String eBODY = "</body>";
		final String P = "<P>";
		final String eP = "</P>";
		if(str.indexOf(P) <0 || str.indexOf(P) >3) 
			str = P+ str +eP;
		InputStream is = new ByteArrayInputStream(str.getBytes());
		Tidy tidy = new Tidy();
		tidy.setXHTML(true);
		tidy.setShowWarnings(false);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		tidy.parse(is,baos);
		String html = baos.toString();
		int start = html.indexOf(BODY);
		int end = html.indexOf(eBODY);
		convertedSource = html.substring(start+BODY.length(),end);
		return convertedSource;
	}
	
	public static int checkStringOccurence(String originalString,String occurence) {
		int lastIndex = 0;
		int count =0;
		String findStr = occurence;
		while(lastIndex != -1){

	       lastIndex = originalString.indexOf(findStr,lastIndex);

	       if( lastIndex != -1){
	    	   count ++;
	    	   lastIndex+=findStr.length();
	       }
		}
		return count;
	}
	
	// not adding para for the actual text.
	public static String convertToXHTMLx(String str) {
      String convertedSource = "";

		int count = 0;
		count = checkStringOccurence(str, "<p> </p>");// This condition is written for firefox - In the parameter "str" para comes in LOWERCASE
		if(count == 0){
			count = checkStringOccurence(str, "<P> </P>");// This condition is written for IE8 - In the parameter "str" para comes in UPPERCASE
		}
		
		if (str == null || str.trim().length()==0) return str; 
		final String BODY = "<body>";
		final String eBODY = "</body>";	
		InputStream is;
    try {
      is = new ByteArrayInputStream(str.getBytes("UTF-8"));
   
		Tidy tidy = new Tidy();
		tidy.setXHTML(true);
		tidy.setShowWarnings(true);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		tidy.setInputEncoding("UTF-8");
	    tidy.setOutputEncoding("UTF-8");
	    tidy.setWraplen(Integer.MAX_VALUE);//added for dbct-45
	  
		tidy.parse(is,baos);
		String html = baos.toString("UTF-8");
		if(html.indexOf("<a\r\nhref")!=-1)
			html = html.replace("<a\r\nhref", "<a href");
		if(html.indexOf("<span\r\nclass")!=-1)
			html = html.replace("<span\r\nclass", "<span class");
		if(html.indexOf("<img\r\nid=")!=-1)
			html = html.replace("<img\r\nid=", "<img id=");
		if(html.indexOf("<input\r\ntype")!=-1){
			html = html.replaceAll("<input\r\ntype", "<input type");
		}
		if(html.indexOf("\r\nsrc")!=-1){
			html = html.replaceAll("\r\nsrc", " src");
		}
		if(html.indexOf("<\r\n")!=-1){
			html = html.replaceAll("<\r\n", "<");
		}
		int start = html.indexOf(BODY);
		int end = html.indexOf(eBODY);
		if(start!= -1 || end!= -1){
				convertedSource = html.substring(start+BODY.length(),end);
			}
		else{
				convertedSource = str;
			}
		try {
			baos.flush();
			baos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(count > 0){
			convertedSource = convertedSource.replaceAll("<p></p>", "<p>&nbsp;</p>");
		}
	      return convertedSource;

    } catch (UnsupportedEncodingException e1) {
      e1.printStackTrace();
    }
    return convertedSource;

	}


	
	//Html Markup list.
	//alignment tags in html format i.e (<p style="text-align:center">) for center,left,right and justify added as per requirement Done by: Madhavi on 7 Nov 2009
	//private static String xhtml[] = {"<li>",            "</li>", "<li style=\"text-align: left;\"","<li style=\"text-align: center;\"","<li style=\"text-align: right;\"","<li style=\"text-align: justify;\"",           "<strong>",              "</strong>",  "<sub>",      "</sub>",       "<sup>",         "</sup>",        "<em>",        "</em>"      ,"<li>",            "<li style=\"LIST-STYLE-TYPE: none\">","<li style=\"list-style: none\">","</li>",              "<ol>",        "</ol>",         "<ul",          "</ul>",               "<br />" ,"<tr",  "</tr>",     "<td",   "</td>",         "<input"," type=\"image\"",  "  type=\"image\"",             "<p>"   ,"</p>"  ,"<strong>","<u style=\"text-decoration: underline;\">","<!-- pagebreak -->","src=\"htmlarea/images/footnote.gif\" title=\"","src=\"htmlarea/images/footnote.gif\"  title=\"","<input type=\"image\"","<p style=\"text-align: left;\"","<p style=\"text-align: right;\"","<p style=\"text-align: center;\"","<p style=\"text-align: justify;\"","<b>","</b>","<i>","</i>"};
	private static String xhtml[] = {"<li>",            "</li>", "<li style=\"text-align: left;\"","<li style=\"text-align: center;\"","<li style=\"text-align: right;\"","<li style=\"text-align: justify;\"",           "<strong>",              "</strong>",  "<sub>",      "</sub>",       "<sup>",         "</sup>",        "<em>",        "</em>"      ,"<li style=\"list-style-type: none;\">",            "<li style=\"LIST-STYLE-TYPE: none\">","<li style=\"list-style: none\">","</li>",              "<ol>",        "</ol>",         "<ul",          "</ul>",               "<br />" ,"<tr",  "</tr>",     "<td",   "</td>",         "<input"," type=\"image\"",  "  type=\"image\"",             "<p>"   ,"</p>"  ,"<strong>","<u style=\"text-decoration: underline;\">","<!-- pagebreak -->","src=\"htmlarea/images/footnote.gif\" title=\"","src=\"htmlarea/images/footnote.gif\"  title=\"","<input type=\"image\"","<p style=\"text-align: left;\"","<p style=\"text-align: right;\"","<p style=\"text-align: center;\"","<p style=\"text-align: justify;\"","<b>","</b>","<i>","</i>"};
	
	// DocBook Markup list
//	alignment tags in docbook format i.e (<para align:center>) for center,left,right and justify added as per requirement to take effect Done by: Madhavi on 7 Nov 2009
	private static String xdb[] =   {"<listitem><para>","</para></listitem>","<listitem align=\"left\"><para","<listitem align=\"center\"><para","<listitem align=\"right\"><para","<listitem align=\"justify\"><para","<emphasis role='bold'>","</emphasis>","<subscript>","</subscript>", "<superscript>", "</superscript>","<emphasis >","</emphasis >","<listitem><para >","<listitem><para >"                  ,"<listitem><para >"                  ,"</para ></listitem>","<orderedlist>","</orderedlist>","<itemizedlist","</itemizedlist>","<sbr/>" ,"<row","</row>","<entry","</entry>", "<footnoteref"," type=\"button\""," type=\"button\"","<para>","</para>","<emphasis role=\"bold\">.","<u>","<img id=\"1\" src=\"htmlarea/images/pbreak.gif\"  type=\"pagebreak\" />","idref=\"","idref=\"","<input type=\"button\"","<para align=\"left\"","<para align=\"right\"","<para align=\"center\"","<para align=\"justify\"","<emphasis role='bold'>","</emphasis>","<emphasis >","</emphasis >"}; //doc book
	
	//Start : Function By Aziza : 30th Jan 2009
	private static String convert[] =   {"<!-- pagebreak -->"};
	
	private static String reconvert[] =   {"<img id=\"1\" src=\"htmlarea/images/pbreak.gif\"  type=\"pagebreak\" />"};
	
	public static String convertPageBreak(String source) {
		if (source == null || source.trim().length() ==0) return source;
		for(int i=0; i<convert.length;i++)		
			source = replaceStringX(source, convert[i], reconvert[i]);
		return source;
	  }
	//End : Function By Aziza : 30th Jan 2009
	
	//Start : Function By Tejas : 5th Mar 2009
	private static String convertHR[] =   {"<hr />"};
	
	private static String reconvertHR[] =   {"<img class=\"hrClass\" src=\"images/hr.gif\" >"};
	public static String convertHorizontalRule(String source) {
		if (source == null || source.trim().length() ==0) return source;
		for(int i=0; i<convertHR.length;i++)		
			source = replaceStringX(source, convertHR[i], reconvertHR[i]);
		return source;
	  }
	//End : Function By Tejas : 5th Mar 2009
	
	//REVHIS REVHISBLK Changes Starts
	//Html Markup list.
	private static String xhtmlblack[] = {"<p"   ,"</p>",   "<strong",              "</strong>",  "<sub",      "</sub>",       "<sup",         "</sup>",        "<em",        "</em>"      ,"<li"			  ,		"<li revisionFlag=\"added\""			,	    "<li revisionFlag=\"changed\""				,		"<li revisionFlag=\"deleted\""				,		"</li>",         			"<ol",        		"</ol",         		"<ul",          	"</ul>",           		"<br />" ,		"<tr",  	"</tr>",     "<td",   	"</td>",        "<input",      		"<font",   			"</font>",			"<span",	  "</span>"};
	
	// DocBook Markup list
	private static String xdbblack[] =   {"<para","</para>","<emphasis role='bold'","</emphasis>","<subscript","</subscript>", "<superscript", "</superscript>","<emphasis ","</emphasis >","<listitem><para ",		"<listitem revisionFlag=\"added\"><para ",		"<listitem revisionFlag=\"changed\"><para ",		"<listitem revisionFlag=\"deleted\"><para ",		"</para ></listitem>",		"<orderedlist",		"</orderedlist",		"<itemizedlist",	"</itemizedlist>",		"<para/>" ,		"<row",		"</row>",	"<entry",	"</entry>", 	"<footnoteref",		"<inlinestyle", 	"</inlinestyle>",	"<phrase",	  "</phrase>"}; //doc book
	
	
	private static String replacewith[] = {""};
	
	// DocBook Markup list
	private static String replace[] =   {"<para/>"}; //For removing the unnecessary characters
	//REVHIS REVHISBLK Changes ENDS
	
	/**
	 * Method to format the text from the Html markups to DocBook markups
	 * */
	public static String convertXHtmlToDocBook(String source) {
		// Start : Added By Aziza on 16th July 2009 for "@" problem
		String patternToCheck = "<span class=\"[^\"]+\">";
		Pattern patternToCheck2 = Pattern.compile(patternToCheck);
		Matcher matcher = patternToCheck2.matcher(source);
		
		while(matcher.find()){
		    String spanTagContent = ""+matcher.group();
		    String newContent = spanTagContent.replaceAll("<span class=\"","<inlinestyle size=\"");
		    newContent = newContent.replaceAll("@","\" condition=\"");
		    source = source.replaceAll(spanTagContent,newContent);
		}
		source = source.replaceAll("</span>","</inlinestyle>").replaceAll("<span","<inlinestyle");
		
		// End : Added By Aziza on 16th July 2009 for "@" problem
		
		if (source == null || source.trim().length() ==0) return source;
		
		source = addBlockquote(source);//Added by Anil to add <blockquote> tag for skipped levels of list items as client needs it.
		/*Added for DBP-345 Bullets Numbers Overlaying Text in HTML-s*/
		//source = source.replaceAll("<li> <p\\b[^>]*>","<li>").replaceAll("<li><p\\b[^>]*>", "<li>").replaceAll("</p></li>","</li>").replaceAll("</p> </li>","</li>");
		source = replaceAllPTagsWithinLi(source); // Commented and Updated the code to fix DB-4124
		/*Added for DBP-345 Bullets Numbers Overlaying Text in HTML-e*/
		for(int i=0; i<xhtml.length;i++)	{			
			source = replaceString(source, xhtml[i], xdb[i]);			
		}
		return source;
	  }
	
	/**
	 * Method to format the text from the DocBook markups to Html markups
	 * */
	  
	public static String xdocBookToHtml(String source) {
		// Start : Added By Aziza on 16th July 2009 for "@" problem
		String patternToCheck = "<inlinestyle size=\"[^\"]+\" condition=\"[^\"]+\">";
		Pattern patternToCheck2 = Pattern.compile(patternToCheck);
		Matcher matcher = patternToCheck2.matcher(source);
		
		while(matcher.find()){
		    String spanTagContent = ""+matcher.group();
		    String newContent = spanTagContent.replaceAll("<inlinestyle size=\"","<span class=\"");
		    newContent = newContent.replaceAll("\" condition=\"","@");
		    source = source.replaceAll(spanTagContent,newContent);
		}
		source = source.replaceAll("</inlinestyle>","</span>").replaceAll("<inlinestyle","<span");
		// End : Added By Aziza on 16th July 2009 for "@" problem
		
		
		if (source == null || source.trim().length() ==0) return source;
		
		source = removeBlockquote(source);//Added by Anil to remove <blockquote> tag while reverse process.
		for(int i=0; i<xhtml.length;i++)		
		source = replaceStringX(source, xdb[i], xhtml[i]);
		return source;
	  }
	  
	
	//REVHIS REVHISBLAC Changes Starts 
	public static String xdocBookToHtmlBlack(String source) {
//	  source = fixSpecialCharforWeb(source);
	  if (source == null || source.trim().length() ==0) return source;
	  for(int i=0; i<xhtmlblack.length;i++)		
	  source = replaceStringX(source, xdbblack[i], xhtmlblack[i]);
	  return source;
	}
		
	public static String removeExtraChars(String source) {
		//source = fixSpecialCharforWeb(source);
		  if (source == null || source.trim().length() ==0) return source;
		  for(int i=0; i<replacewith.length;i++)		
		  source = replaceStringX(source, replace[i], replacewith[i]);
		  return source;
	}
	  
	//	REVHIS REVHISBLAC Changes Ends

  
  public static BigDecimal getBigDecimal(String value) {
		if (value == null || value.equals("") || value.equalsIgnoreCase("null"))
			return null;
		return new BigDecimal(value);	
  }
  public static BigDecimal getBigDecimal(Integer value) {
		if (value == null || value.toString().trim().equals(""))
			return null;
		return new BigDecimal(value.toString().trim());	
  }
  
  public static int getInt(String value) {
		if (value == null || value.equals("") || value.equalsIgnoreCase("null"))
			return 0;
		return new Integer(value).intValue();	
  }
  
  @SuppressWarnings("unlikely-arg-type")
public static String getStringBDValue(BigDecimal value) {
		if (value == null || value.equals(""))
			return null;
		return value.longValue()+"";	
  }

	public static String[] filterArrayString(String[] originalStr, String withStr[]) {
		if (originalStr == null) return null;
		if (withStr == null) return originalStr;
	
		for (int i=0;i<originalStr.length;i++) {
			for (int j=0;j<withStr.length;j++) {
				if ((originalStr[i] != null && withStr[j] != null ) && originalStr[i].equals(withStr[j])) 
					originalStr[i] = null;
			}
		}
		return originalStr;
	}
		
	@SuppressWarnings("rawtypes")
	public static String[] parseIntegerString(String elem, String delimiter)	{
		if (elem == null) return null;
		if (delimiter == null || delimiter.length() ==0) delimiter = "*";
		StringTokenizer st = new StringTokenizer(elem,"*");
		ArrayList al = new ArrayList();		
	
		while (st.hasMoreElements()) 
			al.add(st.nextElement());
	
		String[] array =  new String[al.size()];
		for (int i=0;i<al.size();i++) 
			array[i] = (String)al.get(i);
		return array;
	}
	
	@SuppressWarnings("rawtypes")
	public static String[] parseDelimitedString(String elem, String delimiter)	{
				if (elem == null) return null;
				if (delimiter == null || delimiter.length() ==0) delimiter = "*";
				StringTokenizer st = new StringTokenizer(elem,delimiter);
				ArrayList al = new ArrayList();		

				while (st.hasMoreElements()) 
					al.add(st.nextElement());

				String[] array =  new String[al.size()];
				for (int i=0;i<al.size();i++) 
					array[i] = (String)al.get(i);
				return array;
		}
	@SuppressWarnings("rawtypes")
	public static String[] parseDelimitedStringX(String elem, String delimiter)	{
				if (elem == null) return null;
				if (delimiter == null || delimiter.length() ==0) delimiter = "*";

				ArrayList al = new ArrayList();		
				while (elem.indexOf(delimiter)> -1) {
				
					al.add(elem.substring(0,elem.indexOf(delimiter)));
					elem = elem.substring(elem.indexOf(delimiter)+delimiter.length(),elem.length());
				}
				if (elem != null && elem.length()>0)
					al.add(elem);
				String[] array =  new String[al.size()];
				for (int i=0;i<al.size();i++) 
					array[i] = (String)al.get(i);
				return array;
		}
			
	public static String parseIntegerArrayString(String[] elem, String delimiter)	{
			if (elem == null) return null;
			if (delimiter == null || delimiter.length() ==0) delimiter = "*";
			String str = "";
			for (int i=0;i<elem.length;i++) 
				if (elem[i] != null && elem[i].length() >0 )
					str += (i==0? elem[i] : delimiter+elem[i]);
			return str;
	}
	
	/**
	 * @Purpose:This method changes the footnote values
	 * @return String
	 * */
	public static String changeFootnoteValues(String oldbody){	
		String newbody = "";
		newbody = CheckString.removeExtraSpaces(oldbody);	
		String patternToCheck = "";		
		String tagName = "footnoteref";
		//String attributeName = "value";
		patternToCheck = "<footnoteref type=\"button\" alt=\"[0-9]+\" size=\"[0-9]+\" value=\"[0-9]+\" order=\"[0-9]+\" />";
		Pattern pattern = Pattern.compile(patternToCheck);
		Matcher matcher = pattern.matcher(newbody);
		Document documentObj ;
		while(matcher.find()){
			try {
				documentObj = DBXmlManager.getDOMFromXMLString("<root>"+matcher.group()+"</root>");
				Element rootElement = documentObj.getDocumentElement();
				NodeList imageNode = rootElement.getElementsByTagName(tagName);	
				
				Element imageElement = (Element) imageNode.item(0);
				String alt = imageElement.getAttribute("alt");
				String order = imageElement.getAttribute("order");
				String size = imageElement.getAttribute("size");
				String value = imageElement.getAttribute("value");
				String newString = "<footnoteref type=\"button\" alt=\""+alt+"\" size=\""+size+"\" value=\""+value+"\" idref=\""+value+"\" order=\""+order+"\" />";
				newbody = newbody.replace(matcher.group(), newString);
			} catch (Exception e) {
				
				e.printStackTrace();
			}
		}
		return newbody;
	}
	
	/*
	 *  remove the tag and return string
	 */
			
	
	public static String stripOffTag(String source, String tag) {
		if (source == null || source.length() ==0) return source;
		String stag = "<"+tag;
		String etag = "</"+tag+">";
		int found = source.indexOf(stag);
		//int endTag = source.indexOf(etag);
		if (found == -1) return source;
		found += stag.length();
		
		String halfSource = "";
		if (source.length() >= found){
			halfSource = source.substring(found,source.length());
		}
		
		String result = "";
		if ((halfSource.length() >= halfSource.lastIndexOf(etag)) && (halfSource.lastIndexOf(etag) >= halfSource.indexOf(">")+1)){
			result = halfSource.substring(halfSource.indexOf(">")+1,halfSource.lastIndexOf(etag)); //05/25/2004 to lastIndex of
		}
		
		return result;
	}
	
	
	public static String stripOffhalfTag(String source, String tag) {
		if (source == null || source.length() ==0) return source;
		String stag = "<"+tag;
		String etag = ">";
		if(tag.equals("img")){
			
			stag = "<"+tag;
			etag = "/>";
		}
		int found = source.indexOf(stag);
		if (found == -1) return source;
		found += stag.length();
		String halfSource = source.substring(found,source.length());
		String result = halfSource.substring(halfSource.indexOf(etag)+1,halfSource.lastIndexOf(etag)); //05/25/2004 to lastIndex of
		return result;
	}
	
	public static String getXmlNodeValue(String source, String tag) {
		if (source == null || source.length() ==0) return source;
		String stag = "<"+tag;
		String etag = "</"+tag+">";
		int found = source.indexOf(stag);
		if (found == -1) return "";
		found += stag.length();
		String halfSource = source.substring(found,source.length());
		String result = halfSource.substring(halfSource.indexOf(">")+1,halfSource.indexOf(etag));
		return result;
	}
	

	
	public static String getAttributeValueOfTag(String sourcexml, String tagname, String attribute ) {
		if (sourcexml == null || sourcexml.length() ==0) return "";
		String stag = "<"+tagname;
		int found = sourcexml.indexOf(stag);
		if (found < 0) return "";
		found += stag.length();
		String halfSource = sourcexml.substring(found,sourcexml.length());
		int end = 	halfSource.indexOf(">");
		if (found > end) return "";
		String attributes = halfSource.substring(0, end);
		String result="";
		if(attributes.indexOf(attribute+"=\"") > -1) {
			String temp = attributes.substring(attributes.indexOf(attribute+"=\"")+(attribute+"=\"").length(),attributes.length());
			//developerLog.debug(temp);
			result = temp.substring(0, temp.indexOf("\""));
		}
		return result;
		
	}
	
	public static String removeSectfromSearchResult(String text) {
		String result = null;
		if (text == null) {
			result =  null;
		}
		else if(text.indexOf("<sect") == -1) {
			result = text;
		}
		else {
			if (text.trim().substring(0,5).equalsIgnoreCase("<sect")) {
				int title = text.trim().indexOf("<title>");
				int endtitle = text.trim().indexOf("<titleabbrev>");
				if (endtitle < title) {
					endtitle = text.trim().length();
				}
				result =  text.trim().substring(title,endtitle);
			} else {
				result = text;
			}
		}
		return result;
	}

	public static boolean lookup(String lvalue, String[] strings){
		if (lvalue == null || lvalue.length() ==0) return false;
		if (strings == null ||(strings != null && strings.length == 0)) return false;
		for(int i=0;i<strings.length;i++)
			if(lvalue.equals(strings[i]))
				return true;
		return false;
	}
	
	public static String xmlToText(String str) {
		if (str == null || str.length()==0) return str;
		str = replaceStringX(str,"&amp;","&");
		return str;
	}
	
	//REVHIS REVHISBLAC Changes Starts

	
	
	
	public static String getBlackLiningHTML(String blacklinedXML) throws Exception{
		
		String blackLinedHTML= null;
		try{
			blackLinedHTML = rplBlackLiningHTML(blacklinedXML,"revisionflag=\"added\"","color:blue");
			blackLinedHTML = rplBlackLiningHTML(blackLinedHTML,"revisionflag=\"changed\"","color:yellow");
			blackLinedHTML = rplBlackLiningHTML(blackLinedHTML,"revisionflag=\"deleted\"","text-decoration:line-through;color:red");
			blackLinedHTML = xdocBookToHtmlBlack(blackLinedHTML);
		}
		catch(Exception e){
			throw new Exception("Error in getBlackLiningHTML");
		}
		return blackLinedHTML;
		
	}
	
	
	
	public static String getDeltaBlackLiningHTML(String blacklinedXML) throws Exception{
		
		String blackLinedHTML= blacklinedXML;
		try{
			if ((blackLinedHTML == null) || (blackLinedHTML.trim().length() == 0)) 
				return blacklinedXML;
			
			String spanblue = "<span style=\"color:blue;\">";
			String spanred = "<span style=\"text-decoration:line-through;color:red;\">";
			String spanend = "</span>";
			//node level add/delete tags
			String[] tagList = new String[] {"<deltaxml:PCDATAnew>","</deltaxml:PCDATAnew>","<deltaxml:new>","</deltaxml:new>","<deltaxml:PCDATAold>","</deltaxml:PCDATAold>","<deltaxml:old>","</deltaxml:old>","<deltaxml:PCDATAmodify>","</deltaxml:PCDATAmodify>","<deltaxml:exchange>","</deltaxml:exchange>"};
			String[] mapList = new String[] {spanblue              ,spanend                ,spanblue        ,spanend          ,spanred               ,spanend                ,spanred        ,spanend          ,""                       ,""                        ,""				   ,""                    };
			for(int i=0;i<tagList.length;i++) {
				blackLinedHTML = CheckString.replaceStringX(blackLinedHTML,tagList[i],mapList[i]);
			}
			String[] atagList = new String[] {"deltaxml:delta=\"add\"", "deltaxml:delta=\"delete\""};
			String[] astyleList = new String[] {spanblue, spanred};

			for(int i=0;i<atagList.length;i++) {
				blackLinedHTML = CheckString.parseDeltaXmlTags(blackLinedHTML, atagList[i], astyleList[i]);
			}
			//developerLog.debug("blackLinedHTML"+blackLinedHTML);
		}
		catch(Exception e){
			throw new Exception("Error in getBlackLiningHTML");
		}
		return blackLinedHTML;
		
	}

	public static String parseDeltaXmlTags(String blackLinedHTML, String find, String style) throws Exception {
		StringBuffer sb = null;
		int index = 0, subindex1 = 0, subindex2 = 0;
		String ctag = "";
		String etag = ""; 
		while (blackLinedHTML.indexOf(find) > -1) {
			sb = new StringBuffer();
			index = blackLinedHTML.indexOf(find);
			//developerLog.debug("index:"+index);
			subindex1 = blackLinedHTML.indexOf(">", index);
			//developerLog.debug("subindex1:"+subindex1);
			sb.append(blackLinedHTML.substring(0,index));
			//developerLog.debug("sb1:"+sb.toString());
			etag = blackLinedHTML.substring(index+find.length(),(index+find.length()+2)); 
			ctag = blackLinedHTML.substring(blackLinedHTML.lastIndexOf(">",index)+2,index).trim();
			//developerLog.debug("{ctag,etag} {"+ctag+","+etag+"}");
			
			if("/>".equals(etag)) {
				subindex2 = index+find.length();
			} else {
				sb.append(blackLinedHTML.substring(index+find.length(),subindex1+1));
				//developerLog.debug("sb2:"+sb.toString());
				sb.append(style);	
				subindex2 = blackLinedHTML.indexOf("</"+ctag+">",subindex1);
				sb.append(blackLinedHTML.substring(subindex1+1,subindex2));
				//developerLog.debug("sb4:"+sb.toString());
				sb.append("</span>");
			}
			//developerLog.debug("subindex2:"+subindex2);
			sb.append(blackLinedHTML.substring(subindex2,blackLinedHTML.length()));						
			//developerLog.debug("sb5:"+sb.toString());
			blackLinedHTML = sb.toString();
			//developerLog.debug("blackLinedHTML:"+blackLinedHTML);
		}
		return blackLinedHTML;
	} 
 	
	public static String rplBlackLiningHTML(String blacklinedXML,String txtOld,String txtNew) throws Exception{
		
		int i =0;
		int k = 0;
		int l = 0;
		int m = 0;
		int n = 0;
		int p = 0;
		int q = 0;
		//int r = 0;
		int a = 0;
		String substr = null;
		String changedsubstr = null;
		String changedsubstr1 = null;
		StringBuffer sb = new StringBuffer();

		try {
			//blacklinedXML=null;
			a = blacklinedXML.indexOf(txtOld,i);
			if ( a != -1) {
				for(i=0;i<blacklinedXML.length();) {
					k = blacklinedXML.indexOf(txtOld,i);
					if (k != -1) {
						l =	blacklinedXML.lastIndexOf("<",k);  	
						m = blacklinedXML.indexOf (">",k);
						substr = blacklinedXML.substring(l,m+1);
						changedsubstr = substr;
						n=substr.indexOf("style=");
						if (n < 0) {
							changedsubstr=changedsubstr.replaceFirst(txtOld," style=\""+txtNew+"\"");
						} else {
							p = substr.indexOf("\"",n);
							q = substr.indexOf("\"",p+1);
							changedsubstr1=substr.substring(p,q+1);
							//if((r=changedsubstr.indexOf(59,q-3)) != -1){
							//	changedsubstr=changedsubstr.replaceFirst(txtOld,"");
							//	StringBuffer sb1 = new StringBuffer(changedsubstr);
							//	sb1.insert(r+1,txtNew);
							//	changedsubstr=sb1.toString();
							//}
							//else{
							StringBuffer sb1 = new StringBuffer(changedsubstr1);
							sb1.insert((q-p),";"+txtNew);
							changedsubstr=changedsubstr.replaceFirst(changedsubstr1,sb1.toString());
							changedsubstr=changedsubstr.replaceFirst(txtOld,"");
							//}
						}
						sb.append(blacklinedXML.substring(i,l));
						sb.append(changedsubstr);
						i=m+1;
					} else {
						sb.append(blacklinedXML.substring(m+1));
						i=blacklinedXML.length();
					}
				}
				blacklinedXML= sb.toString();
			}/* else {
				//developerLog.debug("The following attribute is not found ;"+txtOld);
			}*/
		} catch(Exception e) {
			throw new Exception("Error in rplBlackLiningHTML");
		}
		return blacklinedXML;
	//	}
	}
	/**
	 * @Purpose:This method returns the blacklined XML
	 * Aug 29,2008
	 * @return String
	 * @throws Exception
	 * */
	public static String getBlackLiningXML(String oldXML, String newXML) {
		String blacklinedXML = null;
		String blacklinedHTML = null;
		try {
			System.out.println("oldXML is " + oldXML);
			System.out.println("newXML is " + newXML);
			if (isValidString(oldXML) && isValidString(newXML) && (!oldXML.equals("<para></para>") || !newXML.equals("<para></para>"))) {
				Map<String, String> xmlContainer = new HashMap<String, String>();
				xmlContainer.put("oldXML", oldXML);
				xmlContainer.put("newXML", newXML);
				formatBlankXML(xmlContainer);
				oldXML = xmlContainer.get("oldXML");
				newXML = xmlContainer.get("newXML");
				oldXML = StringUtility.replaceControlCharacters(oldXML, null);
				oldXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE book SYSTEM 'http://dev.datacom-usa.com/dtd//dcidocbook.dtd' []>"
						+ oldXML;
				newXML = StringUtility.replaceControlCharacters(newXML, null);
				newXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE book SYSTEM 'http://dev.datacom-usa.com/dtd//dcidocbook.dtd' []>"
						+ newXML;
				blacklinedXML = DeltaXml.doDiff(removeExtraChars(oldXML), removeExtraChars(newXML));
				blacklinedHTML = getDeltaBlackLinedHTML(blacklinedXML);
				blacklinedHTML = CheckString.replaceString(blacklinedHTML, "&amp;", "&");
			} else {
				blacklinedHTML = "<?xml version=\"1.0\" encoding=\"utf-8\"?><span style=\"color:red;\">Error in blackline</span>";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return blacklinedHTML;
	}

	/*Added by Chaitanya to get blank XML in correct format.DBCT-58.03-06-2015*/
	public static void formatBlankXML(Map<String,String> xmlContainer){
		if(xmlContainer.get("oldXML").equalsIgnoreCase("<para></para>")){
			if(xmlContainer.get("newXML").indexOf("<bridgehead>") != -1){
				xmlContainer.put("oldXML", "<bridgehead><para></para></bridgehead>");
			}else if(xmlContainer.get("newXML").indexOf("<note>") != -1){
				xmlContainer.put("oldXML", "<note><para></para></note><para/>");
			}
		}else if(xmlContainer.get("newXML").equalsIgnoreCase("<para></para>")){
			if(xmlContainer.get("oldXML").indexOf("<bridgehead>") != -1){
				xmlContainer.put("newXML", "<bridgehead><para></para></bridgehead>");
			}else if(xmlContainer.get("oldXML").indexOf("<note>") != -1){
				xmlContainer.put("newXML", "<note><para></para></note><para/>");
			}
		}
	}	

	public static String getDeltaBlackLinedHTML(String blacklinedXML) throws Exception {
		String blackLinedHTML = null;
		GeneratePDF transformXML = new GeneratePDF();
		String styleSheet = DocuBuilder.SHAREPATH + "/docubuilder/stylesheets/Common/BlackLineTransform.xsl";
		org.w3c.dom.Document doc = DBXmlManager.getDOMFromXMLString(blacklinedXML);
		// OutputStream xmlStream = new ByteArrayOutputStream();
		Writer writer = new StringWriter();
		transformXML.processXSLFile(doc, styleSheet, writer);
		blackLinedHTML = writer.toString();
		writer.close();
		return blackLinedHTML;
	}

	@SuppressWarnings("unused")
	public static String convertStyleIndents(String body) {
		
		//developerLog.debug(body+"i m the new body");
		String finalstring="";
		String startswithless ="";
		String tempbody="";
		int count=0;
		int lesscount=0;
		String middlebody="";
		String finaldatabasebody="";
		String enter="";
		int entercount=0;
		String checkforp="";
		String skip="";
		int skipcount=0;
		int indexextrap=0;
		String removeextrapstring="";
		
		if(body.contains("<para"))
			return body;
		
		for(int i=0;i<body.length();i++) {
			char letter=body.charAt(i);
			if((i+2)<=body.length()-1 && (""+body.charAt(i)+body.charAt(i+1)+body.charAt(i+2)).equals("<p>") ) {
			   skip="yes";
			   indexextrap = i;
			}
			if(skip.equals("yes")) {
				// finaldatabasebody=finaldatabasebody+letter;
				removeextrapstring=removeextrapstring+letter; 
				if(i!=body.length()-1 && (""+body.charAt(i)+body.charAt(i+1)).equals("/p") ) {
					skipcount=i+2;
	 			}
				if(skipcount==i && skipcount!=0) {
					skip="";
					skipcount=0;
					indexextrap=0;
				   //developerLog.debug("removeextrapstring"+removeextrapstring);
				   if(removeextrapstring.contains("<em") ||removeextrapstring.contains("<emphasis") || removeextrapstring.contains("<u") || removeextrapstring.contains("<sup") || removeextrapstring.contains("<sub") || removeextrapstring.contains("<strong")){
					   if(removeextrapstring.contains("<br />")){
						   //removeextrapstring=removeextrapstring.replace("<p>","").replace("</p>","").trim();
					   }
				   }
				   finaldatabasebody=finaldatabasebody+removeextrapstring;
				   removeextrapstring="";
				}
			} else {
			   
	           if(i!=body.length()-1 && (""+body.charAt(i)+body.charAt(i+1)).equals("<p") ) {
	            	checkforp="yes";
			   }
			   if(!checkforp.equals("yes")) {	
				  if(i!=body.length()-1 && ""+body.charAt(i)+body.charAt(i+1)!="<p" ) {
					   enter="no";
					   
				   }
	              if(i!=body.length()-1 && body.charAt(i+1)=='>' && enter.equals("no")) {
	            	  entercount++;
				   }
	              if(entercount<=2 &&  enter.equals("no")) {
	            	  finaldatabasebody=finaldatabasebody+letter; 
	              }
			   }
			   if(checkforp.equals("yes")) {
				   if(letter=='<') {
					   startswithless="yes";
					   lesscount++;
				   }
				   if(startswithless=="" ) {
					   finaldatabasebody=finaldatabasebody+letter;
				   }
				   if(letter=='>') {
					   count++;
				   }
				   if(count>0 && (i+3)<=body.length()-1 && !(""+body.charAt(i)+body.charAt(i+1)+body.charAt(i+2)+body.charAt(i+3)).equals("</p>")) {
					   middlebody=middlebody+letter;
					   //developerLog.debug("middlebody"+middlebody);
					   startswithless="no";
				   } else {
					   startswithless="yes";
				   }
				   if(startswithless=="yes" || startswithless=="no" ) {
						tempbody=tempbody+letter;
						//developerLog.debug("tempbody"+tempbody);
						if(startswithless=="yes") {
							if(count>0) {
								String temporary="";
								for(int x=0;x<middlebody.length();x++) {
						    	   if(middlebody.charAt(x)=='$' || middlebody.charAt(x)=='?' || middlebody.charAt(x)=='(' || middlebody.charAt(x)==')' || middlebody.charAt(x)=='{' 
						    			   || middlebody.charAt(x)=='}' || middlebody.charAt(x)=='[' || middlebody.charAt(x)==']' || middlebody.charAt(x)==':' 
						    			   || middlebody.charAt(x)=='*' || middlebody.charAt(x)=='+' || middlebody.charAt(x)=='\\'){
						    		   temporary=temporary+"\\"+middlebody.charAt(x);
						    	   } else {
						    		   temporary=temporary+middlebody.charAt(x);
						    	   }
						    	}
								/* Code written by Ajeet Singh to solve the indent special character issue - starts here 
								 * Jira issue id - (WF-59) Issue with Indent and Saving Component */
								//withoutmiddlebody = tempbody.split(temporary.substring(0,temporary.length()-2));
								String tempStr = temporary.substring(0,temporary.length()-2);
								String withoutmiddlebody[] = null;
								if(tempStr.lastIndexOf("\\") != -1 && tempStr.lastIndexOf("\\") == (tempStr.length() - 1)){
									tempStr = tempStr.substring(0,tempStr.length()-1);
									withoutmiddlebody = tempbody.split(temporary.substring(0,temporary.length()-3));
								}else{
									withoutmiddlebody = tempbody.split(temporary.substring(0,temporary.length()-2));
								}
								/* Code written by Ajeet Singh to solve the indent special character issue - ends here */
								withoutmiddlebody[0] = withoutmiddlebody[0] + ">";
								
								tempbody = withoutmiddlebody[0]+withoutmiddlebody[1];
								if(tempbody.contains("style")) {
									if(tempbody.contains("px")) {
										String tempPattern = "";
										Pattern checkJustifyPattern = Pattern.compile("text-align: [^>]+; ");
										Matcher matchStyleJustify = checkJustifyPattern.matcher(tempbody);
										if(matchStyleJustify.find()) {
											tempPattern = "align=\""+matchStyleJustify.group().substring(matchStyleJustify.group().indexOf(":")+1, matchStyleJustify.group().indexOf(";")).trim()+"\"";
											tempbody = matchStyleJustify.replaceAll("");
										}	
										
										
										String total = tempbody.substring(tempbody.indexOf(":")+1,tempbody.indexOf("px")).trim();
										int totalcount=Integer.parseInt(total);
										for(int j=0;j<totalcount/30;j++) {
											finalstring=finalstring+"<blockquote dir=\"ltr\" style=\"MARGIN-RIGHT: 0px\">";
										}
										if(totalcount!=0) {
											if(tempPattern != "")
												finalstring=finalstring+"<para "+tempPattern+">";
											else 
												finalstring=finalstring+"<para>";
										}
										 if(tempbody.contains("class")) {
											  String classstring=tempbody.substring(tempbody.indexOf("class=\"")+7,tempbody.indexOf("style")-1).trim();	
											  String classarray[]=classstring.split("@");
											  String sizearray[]=classarray[1].split("\"");
											  finalstring=finalstring+"<inlinestyle size=\""+classarray[0].trim()+" condition=\""+sizearray[0]+"\"\">";
										 }
										finalstring=finalstring+middlebody.substring(1,middlebody.length());
										if(tempbody.contains("class")) {
											finalstring=finalstring+"</inlinestyle>";
										}
										if(totalcount!=0) {
										 finalstring=finalstring+"</para>";
										}
										for(int j=0;j<totalcount/30;j++) {
											finalstring=finalstring+"</blockquote>";
										}
										
									} else{										
											tempbody = tempbody.substring(tempbody.indexOf("<"), tempbody.indexOf(">")+1);
											String aligntype = "";
											if(tempbody.contains(";"))
												aligntype = tempbody.substring(tempbody.indexOf(":")+1,tempbody.indexOf(";")).trim();
											else 
												aligntype = tempbody.substring(tempbody.indexOf(":")+1,tempbody.indexOf("\">")).trim();
											
											Pattern checkJustifyPattern = Pattern.compile("<p style=\"text-align: [A-Za-z(;+)]+\">");
											
											Matcher matchStyleJustify = checkJustifyPattern.matcher(tempbody.toLowerCase());
											if(matchStyleJustify.find()) {
												//finalstring = matchStyleJustify.replaceAll("<para align=\""+aligntype+"\">");
												finalstring = matchStyleJustify.replaceAll("<para align=\""+aligntype+"\">");
												finalstring=finalstring+middlebody.substring(1,middlebody.length());
												finalstring = finalstring+"</para>";//</para>
											}
										
									 }
								} else if(tempbody.contains("class")) {
								    finalstring="<para>";
								    String classstring=tempbody.substring(tempbody.indexOf("class=\"")+7,tempbody.indexOf(">")-1).trim();	
								    String classarray[]=classstring.split("@");
								    if(classarray[1].contains("\"")) {
								    	String sizearray[]=classarray[1].split("\"");
								    	finalstring=finalstring+"<inlinestyle size=\""+classarray[0].trim()+" condition=\""+sizearray[0]+"\" \">";	
								    } else {
								    	finalstring=finalstring+"<inlinestyle size=\""+classarray[0]+"\" condition=\""+classarray[1]+"\">";
								    }
								    finalstring=finalstring+middlebody.substring(1,middlebody.length());
								    finalstring=finalstring+"</inlinestyle></para>";
								}
									
								//developerLog.debug("finaldatabasebody-->"+finaldatabasebody+"finalstring-->"+finalstring);
								finaldatabasebody=finaldatabasebody+finalstring;
								i=i+3;
								finalstring="";
								tempbody="";
								middlebody="";
								count=0;
								lesscount=0;
								startswithless="";
								checkforp="";
							}
						}
					}
				}
			   	if(entercount==2) {
	          	  enter="";
	          	  entercount=0;
		        }
		   }
		}
		
        return finaldatabasebody;
	}
	
	public static String converStyleIndentReverse(String body) {

		String tempbody="";
		String finaldatabasebody="";
		String skip="";
		int skipcount=0;
		int blockcount=0;
		//body=body.replaceAll("<blockquote>","");
		//body=body.replaceAll("</blockquote>","");
		
		for(int i=0;i<body.length();i++) {
			char letter=body.charAt(i);
			boolean blockquoteSkip = false;
			if((i+12)<=body.length()-1 && (body.substring(i, i+12).equals("<blockquote>") )){
				blockquoteSkip = true;
				//tempbody +=<blockquote>;
			}
			if(!blockquoteSkip && (i+2)<=body.length()-1 && (""+body.charAt(i)+body.charAt(i+1)+body.charAt(i+2)).equals("<bl") && !skip.equals("yes")) {
				skip="yes";
				finaldatabasebody="";
			}
			if(!blockquoteSkip &&skip.equals("yes")) {
				finaldatabasebody=finaldatabasebody+letter;
				//developerLog.debug(finaldatabasebody);
				if((i+2)<=body.length()-1 && (""+body.charAt(i)+body.charAt(i+1)+body.charAt(i+2)).equals("<bl") ) {
					blockcount++;
				}
				if(i!=body.length()-1 && (""+body.charAt(i)+body.charAt(i+1)).equals("/p") ) { //will never come here
					//developerLog.debug(i);
					skipcount=i+2;;
				}
			   
				if(skipcount==i && i!=0) {
					skip="";
					skipcount=0;
					//developerLog.debug(skipcount+"finaldatabasebody----------->"+finaldatabasebody);
					if(Pattern.compile("<blockquote dir=\"ltr\" style=\"MARGIN-RIGHT: 0px\">").matcher(finaldatabasebody).find()) {
						int padding=(blockcount)*30;
						finaldatabasebody=finaldatabasebody.replaceAll("<blockquote dir=\"ltr\" style=\"MARGIN-RIGHT: 0px\">","");
						finaldatabasebody=finaldatabasebody.trim();
						
						String justifyContent = "";
						if(Pattern.compile("style=\"padding-left: 30px;\" style=\"text-align:  [^>]+;\"").matcher(finaldatabasebody).find())  {
							Pattern pattern = Pattern.compile(" style=\"text-align: [^>]+;\"");
							Matcher matcher = pattern.matcher(finaldatabasebody);
							Pattern p = Pattern.compile("text-align: [^>]+;");
							while(matcher.find()) {
								String replaceStr = matcher.group();
								Matcher m = p.matcher(replaceStr);
								if(m.find()) {
									justifyContent = m.group();
								}			
								finaldatabasebody = matcher.replaceAll("");
							}
						}
						
						
						//developerLog.debug("finaldatabasebody"+finaldatabasebody);
						if(justifyContent != "") {
							tempbody=tempbody+finaldatabasebody.substring(0,2)+" style=\""+justifyContent+" padding-left: "+padding+"px;\""+finaldatabasebody.substring(2,finaldatabasebody.length());
						} else {
							tempbody=tempbody+finaldatabasebody.substring(0,2)+" style=\"padding-left: "+padding+"px;\""+finaldatabasebody.substring(2,finaldatabasebody.length());
						}
						padding=0;
						blockcount=0;
						
						//Code Change for blockcode before p tag bug
						Pattern black = Pattern.compile("</blockquote>");
						Matcher bm = black.matcher(tempbody);
						if(bm.find()) {
							bm.replaceFirst("");
						}
						
						//tempbody = tempbody.replaceAll("</blockquote>", "");
					} else if(Pattern.compile("<blockquote>").matcher(finaldatabasebody).find()) {
						tempbody=tempbody+finaldatabasebody;
					}
				}
			}else{
				tempbody=tempbody+letter;
	       }
		}
		//return tempbody;If it is normal <blockquote></blockquote> & so it does not come into the if loop and returns with the tempbody which is null.
		if(tempbody.equals("")){
			tempbody=body;
		}
		return tempbody;
	}
	
	/**
	 * @author: Karan
	 * @purpose: fixes the special characters  copied from db3
	 */
	
	public static String fixSpecialChar(String strData) {
//	System.out.println("1 DATA :"+strData);
		if(strData.indexOf("&#039;")>-1)
			strData=replaceStringX(strData,"&#039;","&prime;");
		else if(strData.indexOf("&#39;")>-1)
			strData=replaceStringX(strData,"&#39;","&prime;");
		else if(strData.indexOf("'")>-1)
			strData=replaceStringX(strData,"'","&prime;");	
			
		while(strData.indexOf("<")>-1 && strData.indexOf(">") > -1) {
			int stIndx=strData.indexOf("<");
			int endInx=strData.indexOf(">");
			if(stIndx==0)
				strData=strData.substring(endInx+1,strData.length());
			else if(stIndx > 0 && endInx == strData.length())
				strData=strData.substring(0,stIndx);
			else
				strData = strData.substring(0,stIndx)+strData.substring(endInx+1,strData.length());
		}
//	System.out.println("2 DATA :"+strData);	
		return 	strData;			
	}
	/**
	 * @author: Karan
	 * @purpose: fixes the special characters  copied from db3
	 */
	
	public static String fixSpecialChar(String strData,String replaceWith) {
//	System.out.println("1 DATA :"+strData);
		if(strData.indexOf("&#039;")>-1)
			strData=replaceStringX(strData,"&#039;",replaceWith);
		else if(strData.indexOf("&#39;")>-1)
			strData=replaceStringX(strData,"&#39;",replaceWith);
		else if(strData.indexOf("'")>-1)
			strData=replaceStringX(strData,"'",replaceWith);	
			
		while(strData.indexOf("<")>-1 && strData.indexOf(">") > -1) {
			int stIndx=strData.indexOf("<");
			int endInx=strData.indexOf(">");
			if(stIndx==0)
				strData=strData.substring(endInx+1,strData.length());
			else if(stIndx > 0 && endInx == strData.length())
				strData=strData.substring(0,stIndx);
			else
				strData = strData.substring(0,stIndx)+strData.substring(endInx+1,strData.length());
		}
//	System.out.println("2 DATA :"+strData);	
		return 	strData;			
	}
	
	public static String toRemoveSpansOfSpellCheck(String body) {
		
		boolean isSpanTagOpened = false;
        boolean isSpanTagClosed = false;
        boolean isSpanStartedClosing= false;
        String  contentToRemoveSpanTags="";
        int     startIndexOfContentToSubstring = 0;
        int     endIndexOfContentToSubstring = 0;
        String  ContentWithoutSpanTagsWithClassJ="";
        try {
			for(int i=0;i<body.length();i++) {
			   char letter=body.charAt(i);
			   if((i+2)<=body.length()-1 && (""+body.charAt(i)+body.charAt(i+1)+body.charAt(i+2)).equals("<sp") ) {
				  isSpanTagOpened=true;
 			   }
			   if(isSpanTagOpened) {
				   contentToRemoveSpanTags=contentToRemoveSpanTags+letter;
				   if(letter=='>') {
					   if(contentToRemoveSpanTags.contains("class=\"j1\"") || contentToRemoveSpanTags.contains("class=\"j2\"") || contentToRemoveSpanTags.contains("class=\"j3\"") || contentToRemoveSpanTags.contains("class=\"j4\"") || contentToRemoveSpanTags.contains("class=\"j5\"") || contentToRemoveSpanTags.contains("class=\"j6\"")){
					   
					   } else {
						   ContentWithoutSpanTagsWithClassJ=ContentWithoutSpanTagsWithClassJ+contentToRemoveSpanTags;
						   isSpanTagOpened = false; 
						   contentToRemoveSpanTags="";
					   }
				   }
			   } else {
				   ContentWithoutSpanTagsWithClassJ=ContentWithoutSpanTagsWithClassJ+letter;
			   }
			   if((i+2)<=body.length()-1 && (""+body.charAt(i)+body.charAt(i+1)+body.charAt(i+2)).equals("/sp") && isSpanTagOpened) {
				   endIndexOfContentToSubstring=contentToRemoveSpanTags.length()-2;
				   isSpanStartedClosing =true;
 			   }
			   if(isSpanStartedClosing) {
				   if(letter=='>') {
					   isSpanTagClosed=true;
					   isSpanTagOpened=false;
					   isSpanStartedClosing =false;
				   }
			   }
			   if(isSpanTagClosed) {
				   startIndexOfContentToSubstring=contentToRemoveSpanTags.indexOf('>',0)+1;
				   String SpanWithClassJRemoved = contentToRemoveSpanTags.substring(startIndexOfContentToSubstring,endIndexOfContentToSubstring);
				   ContentWithoutSpanTagsWithClassJ=ContentWithoutSpanTagsWithClassJ+SpanWithClassJRemoved;
				   startIndexOfContentToSubstring=0;
				   endIndexOfContentToSubstring=0;
				   isSpanTagClosed=false;
				   contentToRemoveSpanTags="";
			   }
				   
			}
			return ContentWithoutSpanTagsWithClassJ;
        } catch(Exception e) {
        	return body;
        }
    }
	
	@SuppressWarnings("rawtypes")
	public static HashMap getFootnoteRefId(String strData) {
		HashMap footnoteValues = new HashMap();
		try {
			strData="<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE book SYSTEM 'http://dev.datacom-usa.com/dtd//dcidocbook.dtd' []><book>"+strData+"</book>";	
			Document dom=(DocumentImpl)DBXmlManager.getDOMFromXMLString(StringUtility.substituteEntityName(strData));
			NodeList listFootnoteRefs= dom.getElementsByTagName("footnoteref");
			for(int i=0;i<listFootnoteRefs.getLength();i++) {
				Element foonoteref =(Element)listFootnoteRefs.item(i); 
				String strrefid = foonoteref.getAttribute("idref");
				String straltid =  foonoteref.getAttribute("alt");
				if((strrefid != null && !strrefid.equals("")) && (straltid != null && !straltid.equals(""))) {
					footnoteValues.put(strrefid,straltid);
				}
			}
		} catch(Exception e){
			e.printStackTrace();
		}
		return footnoteValues;
	}
	
	
	/**
	 *  This method strips off the unclosed tags like '</' or '<' and returns the proper formatted String
	 *  @author pranjal
	 *  @param String
	 */
				
	public static String formatHTMLString(String htmlString) {
		
		if(!(htmlString.lastIndexOf(">") > htmlString.lastIndexOf("</") && (htmlString.lastIndexOf(">") > htmlString.lastIndexOf("<")))) {
			
			if(htmlString.lastIndexOf("</") > htmlString.lastIndexOf("<")){
				if(htmlString.lastIndexOf("</")!=-1){
					htmlString = htmlString.substring(0, htmlString.lastIndexOf("</"));
					//System.out.println(htmlString);
				}
			}else{
				if(htmlString.lastIndexOf("<")!=-1){
					htmlString = htmlString.substring(0, htmlString.lastIndexOf("<"));
					//System.out.println(htmlString);
				}
			}
		}
		
		return htmlString;
	}

	public static String addBlockquote(String source){
		source = source.replaceAll("<ol> <","<ol><").replaceAll("> <ol>","><ol>").replaceAll("<ul> <","<ul><").replaceAll("> <ul>","><ul>").replaceAll("</li> <","</li><").replaceAll("> </li>","></li>");
		source = source.replaceAll("<ol>\r\n<","<ol><").replaceAll(">\r\n<ol>","><ol>").replaceAll("<ul>\r\n<","<ul><").replaceAll(">\r\n<ul>","><ul>").replaceAll("</li>\r\n<","</li><").replaceAll(">\r\n</li>","></li>");
		source = source.replaceAll("<ol>\n<","<ol><").replaceAll(">\n<ol>","><ol>").replaceAll("<ul>\n<","<ul><").replaceAll(">\n<ul>","><ul>").replaceAll("</li>\n<","</li><").replaceAll(">\n</li>","></li>");
		if(source.indexOf("<ol><li style=\"list-style-type: none;\">") == -1 && source.indexOf("<ul><li style=\"list-style-type: none;\">") == -1){
			return source;
		}else{
			if(source.indexOf("<ol><li style=\"list-style-type: none;\">") != -1){
				source = addBlcokQuoteForOL(source);
			}
			if(source.indexOf("<ul><li style=\"list-style-type: none;\">") != -1){
				source = addBlcokQuoteForUL(source);
			}
			return source;
		}
	}
	
	public static String addBlcokQuoteForOL(String content){
		String source = content;
		try{		
			String actualContent = "";
			String replacedContent = "";
			Map<String, String> replacedContentMap = new HashMap<String, String>();
			boolean secondLevelFollowedWithLI = false;
			
			String countString = "<ol><li style=\"list-style-type: none;\">";//to check no. of occurrence.
			int ind = 0;
			int count = 0;
			while (true) {
			    int pos = source.indexOf(countString, ind);
			    if (pos < 0){
			    	break;
			    }
			    count++;
			    ind = pos + 1;
			}
			
			while(count > 0){			
				count--;
				int replaceCount = new Integer(0);
				String checkForLIIndent = null;			 
				checkForLIIndent = "<ol><li style=\"list-style-type: none;\">(.+?)</li></ol>";
				Pattern pattern = Pattern.compile(checkForLIIndent);
				Matcher matcher = pattern.matcher(source);
				pattern = Pattern.compile(checkForLIIndent);
				matcher = pattern.matcher(source);
				matcher.groupCount();
				while(matcher.find()){
				    String matchedContent = matcher.group();
				    replaceCount = new Integer(0);
				    pattern = Pattern.compile("<ol><li>");
				    matcher = pattern.matcher(matchedContent);
					while(matcher.find()) {
						replaceCount++;
						if(replaceCount > 1){
							matchedContent = matchedContent +"</li></ol>";
						}
					}
					replaceCount = new Integer(0);
				    pattern = Pattern.compile("<ol><li style=\"list-style-type: none;\">");
				    matcher = pattern.matcher(matchedContent);
					while(matcher.find()) {
						replaceCount++;					
						matchedContent = matchedContent +"</li></ol>";					
					}
				    String replaceWith = matchedContent.replaceAll("<ol><li style=\"list-style-type: none;\">", "<blockquote>");
				    replaceWith = replaceWith +"</blockquote>";		    
				    boolean skipFlag = true;
				    while(replaceCount > 0){
				    	if(skipFlag){
				    		replaceWith = replaceWith.replace("</li></ol></blockquote>", "</blockquote>");
				    		skipFlag = false;
				    	}else{
				    		replaceWith = replaceWith.replace("</li></ol></blockquote>", "</blockquote></blockquote>");
				    	}	    	
				    	replaceCount--;
				    }
				    if(source.indexOf(matchedContent) != -1 ){
				    	source = source.replace(matchedContent, replaceWith);
				    }else{//if second level follwed with the list items, no block quote conversion required.
				    	actualContent =  matchedContent.substring(0, matchedContent.length()-5);//to remove last </ol>
				    	replacedContent = actualContent.replaceAll(" ", "");
				    	source = source.replace(actualContent, replacedContent);
				    	replacedContentMap.put(actualContent, replacedContent);
				    	secondLevelFollowedWithLI = true;
				    }
				}
			}
			if(secondLevelFollowedWithLI){
				for (Map.Entry<String, String> entry : replacedContentMap.entrySet()) {
					source = source.replace(entry.getValue(), entry.getKey());
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		 return source;
	}
	
	public static String addBlcokQuoteForUL(String content){
		String source = content;
		try {			
			String actualContent = "";
			String replacedContent = "";
			Map<String, String> replacedContentMap = new HashMap<String, String>();
			boolean secondLevelFollowedWithLI = false;
			String countString = "<ul><li style=\"list-style-type: none;\">";//to check no. of occurrence.
			int ind = 0;
			int count = 0;
			while (true) {
			    int pos = source.indexOf(countString, ind);
			    if (pos < 0){
			    	break;
			    }
			    count++;
			    ind = pos + 1;
			}
			
			while(count > 0){			
				count--;
				int replaceCount = new Integer(0);
				String checkForLIIndent = null;			 
				checkForLIIndent = "<ul><li style=\"list-style-type: none;\">(.+?)</li></ul>";
				Pattern pattern = Pattern.compile(checkForLIIndent);
				Matcher matcher = pattern.matcher(source);
				pattern = Pattern.compile(checkForLIIndent);
				matcher = pattern.matcher(source);
				matcher.groupCount();
				while(matcher.find()){
				    String matchedContent = matcher.group();
				    replaceCount = new Integer(0);
				    pattern = Pattern.compile("<ul><li>");
				    matcher = pattern.matcher(matchedContent);
					while(matcher.find()) {
						replaceCount++;
						if(replaceCount > 1){
							matchedContent = matchedContent +"</li></ul>";
						}
					}
					replaceCount = new Integer(0);
				    pattern = Pattern.compile("<ul><li style=\"list-style-type: none;\">");
				    matcher = pattern.matcher(matchedContent);
					while(matcher.find()) {
						replaceCount++;					
						matchedContent = matchedContent +"</li></ul>";					
					}
				    String replaceWith = matchedContent.replaceAll("<ul><li style=\"list-style-type: none;\">", "<blockquote>");
				    replaceWith = replaceWith +"</blockquote>";		    
				    boolean skipFlag = true;
				    while(replaceCount > 0){
				    	if(skipFlag){
				    		replaceWith = replaceWith.replace("</li></ul></blockquote>", "</blockquote>");
				    		skipFlag = false;
				    	}else{
				    		replaceWith = replaceWith.replace("</li></ul></blockquote>", "</blockquote></blockquote>");
				    	}	    	
				    	replaceCount--;
				    }
				    if(source.indexOf(matchedContent) != -1 ){
				    	source = source.replace(matchedContent, replaceWith);
				    }else{//if second level follwed with the list items, no block quote conversion required.
				    	actualContent =  matchedContent.substring(0, matchedContent.length()-5);//to remove last </ul>
				    	replacedContent = actualContent.replaceAll(" ", "");
				    	source = source.replace(actualContent, replacedContent);
				    	replacedContentMap.put(actualContent, replacedContent);
				    	secondLevelFollowedWithLI = true;
				    }			    	
				}
			}
			if(secondLevelFollowedWithLI){
				for (Map.Entry<String, String> entry : replacedContentMap.entrySet()) {
					source = source.replace(entry.getValue(), entry.getKey());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return source;	
	}

	/*
	 * the following method replaces <blockquotetag> tag with matched list item .
	 * @author Anil
	 */
	public static String removeBlockquote(String source){
		if(source.indexOf("<blockquote><itemizedlist>") == -1 && source.indexOf("<blockquote><orderedlist>") == -1){
			return source;
		}else{
			try{
				Pattern pattern = null;		
				String checkForLIIndentReverse = "<blockquote>(.+?)</blockquote>";
				pattern = Pattern.compile(checkForLIIndentReverse);
				Matcher matcher = pattern.matcher(source);
				while(matcher.find()){
					String  matchedContent = matcher.group();
					if(matchedContent.indexOf("<blockquote><itemizedlist>") != -1){//for unorder list.
						String replaceWith = matchedContent.replaceAll("<blockquote><itemizedlist>", "<ul><li style=\"list-style-type: none;\"><itemizedlist>");
						while(replaceWith.indexOf("<blockquote><ul><li style=\"list-style-type: none;\">") != -1){
							replaceWith = replaceWith.replace("<blockquote><ul><li style=\"list-style-type: none;\">", "<ul><li style=\"list-style-type: none;\"><ul><li style=\"list-style-type: none;\">");
						}
						//source = source.replaceAll(matchedContent, replaceWith);commented and added below by Anil to solve ABC-85 (if list item has special char like perentheses)
						source = source.replace(matchedContent, replaceWith);
						if(source.indexOf("</itemizedlist></blockquote>") != -1){
							source = source.replaceFirst("</itemizedlist></blockquote>", "</itemizedlist></li></ul>");
							while(source.indexOf("</li></ul></blockquote>") != -1){
								source = source.replace("</li></ul></blockquote>", "</li></ul></li></ul>");
							}
						} 
					}else if(matchedContent.indexOf("<blockquote><orderedlist>") != -1){//for order list.				
						String replaceWith = matchedContent.replaceAll("<blockquote><orderedlist>", "<ol><li style=\"list-style-type: none;\"><orderedlist>");;
						while(replaceWith.indexOf("<blockquote><ol><li style=\"list-style-type: none;\">") != -1){
							replaceWith = replaceWith.replace("<blockquote><ol><li style=\"list-style-type: none;\">", "<ol><li style=\"list-style-type: none;\"><ol><li style=\"list-style-type: none;\">");
						}
						//source = source.replaceAll(matchedContent, replaceWith);commented and added below by Anil to solve ABC-85 (if list item has special char like perentheses)
						source = source.replace(matchedContent, replaceWith);
						if(source.indexOf("</orderedlist></blockquote>") != -1){ 
							source = source.replaceFirst("</orderedlist></blockquote>", "</orderedlist></li></ol>");
							while(source.indexOf("</li></ol></blockquote>") != -1){
								source = source.replace("</li></ol></blockquote>", "</li></ol></li></ol>");
							}
						} 
					}			
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			return source;	
		}
	}
	public static String replaceVaribaleNames(String inputData){
		String source = inputData;
		try {
			if(source != null && !source.trim().equals("")){
				Pattern pattern = Pattern.compile("<img[^>]*src=\"htmlarea\\/images\\/entity.gif\"[^>]*>", Pattern.CASE_INSENSITIVE&Pattern.MULTILINE);
				Matcher matcher = pattern.matcher(source);
				while(matcher.find()){
					source =source.replace(matcher.group(),"<span title=\"Variable\" class=\"sectionGridVarName\"><span class=\"sectionGridCircleSpanIcon\"><i class=\"material-icons\" style=\"font-size: 6px !important;\">&#xE3FA;</i></span>"+getAttributeValueOfTag(matcher.group(), "img", "title").replace(";", "")+"<span class=\"sectionGridVariableSpanIcon\"><i class=\"material-icons\" style=\"font-size: 11px !important; vertical-align: middle;\">&#xE24A;</i></span></span>&nbsp;");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return source;
	}
			
	/*Added for Resolve variable enhancement- s*/
	public static String replaceVariableImgTagWithSpan(String inputData){
		String source = inputData;
		try {
			//Pattern patternForAttr = Pattern.compile("[\"'](?:(?![\"']?\\s+(?:\\S+)=|[>\"'])).+?[\"']");
			Pattern patternForAttr = Pattern.compile("([a-zA-Z]+)=[\"']?((?:.(?![\"']?\\s+(?:\\S+)=|[>\"']))+.)[\"']?");
			Matcher matcherForAttr = patternForAttr.matcher(source);
			while(matcherForAttr.find()){
				//if(!matcherForAttr.group().trim().endsWith("\"")){
				source =source.replace(matcherForAttr.group(),matcherForAttr.group().trim()+" ").trim();
				//}
			}
			source = source.trim().replaceAll(" +", " ");
			if(source != null && !source.trim().equals("")){
				//Pattern pattern = Pattern.compile("(?=\\btype=\"sentity\").*?(?=\\bsequenceid=\"[0-9]+\")", Pattern.CASE_INSENSITIVE&Pattern.MULTILINE);
				Pattern pattern = Pattern.compile("<img[^>]*type=\"sentity\"[^>]*>", Pattern.CASE_INSENSITIVE&Pattern.MULTILINE);
				//Pattern pattern = Pattern.compile("(?=type=\"sentity\")", Pattern.CASE_INSENSITIVE&Pattern.MULTILINE);
				//<img.*?(?=.*id=\"[0-9~-]+\").*?(?=.*src=\"htmlarea\/images\/entity\.gif\").*?(?=.*type=\"sentity\").*?(?=.*sequenceid=\"[0-9]+\").*?\/>
				Matcher matcher = pattern.matcher(source);
				while(matcher.find()){
					Document documentObj = DBXmlManager.getDOMFromXMLString("<root>"+matcher.group()+"</root>");
					Element rootElement = documentObj.getDocumentElement();
					NodeList imageNode = rootElement.getElementsByTagName("img");	
					Element imageElement = (Element) imageNode.item(0);
					String varId = imageElement.getAttribute("id");
					String varSeqId = imageElement.getAttribute("sequenceid");
					String varIdForClick = varId.trim().replace("\"","");
					varIdForClick = varIdForClick.split("~")[0];
					varIdForClick = varIdForClick +"*#*"+varSeqId;
					source = source.replace(matcher.group(),"<span id=\""+varId+"\" sequenceid=\""+varSeqId+"\" customid=\""+varIdForClick+"\" onclick=\"selectResolveVariableInGrid(this,'"+varIdForClick+"')\" title=\"Variable\" class=\"sectionGridVarNameResolveGrid\"><span class=\"sectionGridCircleSpanIcon\"><i class=\"material-icons\" style=\"font-size: 6px !important;\">&#xE3FA;</i></span>"+CheckString.getAttributeValueOfTag(matcher.group(), "img", "title").replace(";", "")+"<span class=\"sectionGridVariableSpanIcon\"><i class=\"material-icons\" style=\"font-size: 11px !important; vertical-align: middle;\">&#xE24A;</i></span></span>&nbsp;");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return source;
	}
	/*Added for Resolve variable enhancement- e*/
	
	
	/*Added for skype icon issue- s*/
	//replace the span with the text content of the span and delete the remaining elements(a>img)
	public static String replaceSkypeIconWithCleanText(String inputData){
		String source = inputData;
		try {
			String aTag = "<a[^>]+title=\"Call[^\"]+\">(.+?)</a>";
			Pattern pattern = Pattern.compile(aTag);
			Matcher matcher = pattern.matcher(source);			
			while(matcher.find()){
			    String spanTagContent = matcher.group();
			    source = source.replace(spanTagContent,"");
			}
			String inlineTag = "<inlinestyle[^>]+style=\"white-space: nowrap[^\"]+\">(.+?)</inlinestyle>";
			pattern = Pattern.compile(inlineTag);
			matcher = pattern.matcher(source);
			while(matcher.find()){
			    String spanTagContent = matcher.group();
			    source = source.replace(spanTagContent,matcher.group(1));
			}
			String sapnTag = "<span[^>]+style=\"white-space: nowrap[^\"]+\">(.+?)</span>";
			pattern = Pattern.compile(sapnTag);
			matcher = pattern.matcher(source);
			while(matcher.find()){
			    String spanTagContent = matcher.group();
			    source = source.replace(spanTagContent,matcher.group(1));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return source;
	}
	/*Added for skype icon issue- e*/



	/**	fixAsciiChars - replace certain chars which are not rendered.  
	 * @param text
	 * @return String
	 */
	public static String fixAsciiChars(String text) { 
		Character c = null;
		StringBuilder strBuild = new StringBuilder();
		if ((text == null) || (text.trim().length() == 0)) {
			return "";
		} else {
			String trimmdStr = text.trim();
			for(int i=0;i<trimmdStr.length();i++) {
				c = new Character(trimmdStr.charAt(i));
				if ((c.hashCode() == 8208)){ //HYPHEN
					strBuild.append("-");
				}else if ((c.hashCode() == 8195)){
					strBuild.append(" "); //EM SPACE
				}else{
					strBuild.append(trimmdStr.charAt(i));
				}
			}
		}
		return strBuild.toString();
	}
	
	private static String replaceAllPTagsWithinLi(String source) {
	  try {
	    String allLiTags = "<li>([\\s\\S]\\*?)</li>";
	    Pattern pattern = Pattern.compile(allLiTags);
	    Matcher matcher = pattern.matcher(source);          
	    while(matcher.find()){
	      source = source.replaceAll(matcher.group(),matcher.group().replaceAll("<p\\b[^>]\\*>(?!<img)", "").replaceAll("(?<!type=\"columnbreak\" \\/>)</p\\b[^>]\\*>", ""));
	    }
	  } catch (Exception e) {
	    e.printStackTrace();
	  }
	  return source;
	}
	
	public static String checkNull(String source){
		return source==null ? "" : source;
	}
	public static int getTrimLength(String body, int limit){
		boolean closed=true;
        String contentWithoutHTMLTags="";
        int finalcount=limit;
		for(int i=0;i<body.length();i++){
			if(body.charAt(i)=='<'){
			   closed=false;
			}
			if(i!=0 && body.charAt(i-1)=='<'){
			   closed=false;
			}
			if(i!=0 && body.charAt(i-1)=='>'){
				if(body.charAt(i)!='<' ){
					closed=true;
				}
			}
		   
			if(closed){
				contentWithoutHTMLTags=contentWithoutHTMLTags+body.charAt(i);
			  
				if(contentWithoutHTMLTags.length()>limit){
					finalcount=i;
					break;
				}
			}
		}
		return finalcount;
	}
	public static boolean isValidString(String source){
		return source!=null && !source.equals("") ? true : false;
	}
	public static boolean isEmptyString(String source){
		
		return source!=null && source.equals("") ? true : false;
	}
	public static boolean isNumericValue(String numString) {
		try {
			if(numString==null ||numString.equals("")) {
				return false;
			}
			double d = Double.parseDouble(numString);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}
}

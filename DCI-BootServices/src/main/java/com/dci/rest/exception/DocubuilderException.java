package com.dci.rest.exception;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import com.dci.rest.common.DciCommon;

public class DocubuilderException extends Exception{

		private String exceptionMsg;
		private Logger developerLog = Logger.getLogger(DocubuilderException.class);
		
		public DocubuilderException(){
		}
		
		public DocubuilderException(String module, Throwable ex,String errorCode){
			
			classifyException(module, ex);
		
		}
		
		public DocubuilderException(String module, Throwable ex){
			
			classifyException(module, ex);
		
		}
		
		private void classifyException(String module, Throwable ex){
			
			if (ex instanceof SQLException) {
				
				dbException(module, ex);
				
			}else if (ex instanceof RuntimeException) {
				
				runtimeException(module, ex);
				
			}else if (ex instanceof Error) {
				
				systemException(module, ex);
				
			}else if (ex instanceof IOException) {
				
				ioException(module, ex);
				
			}

		}
		
		public void dbException(String module, Throwable ex) {
			exceptionMsg = new DocubuilderDBException(ex, module).getMsg();
			printLogMsg(ex,exceptionMsg);
		}
		
		public void runtimeException(String module, Throwable ex) {
			
			exceptionMsg = new DocubuilderRunTimeException(ex, module).getMsg();
			printLogMsg(ex,exceptionMsg);
		}
		public void systemException(String module, Throwable ex) {
			
			exceptionMsg = new DocubuilderSystemException(ex, module).getMsg();
			printLogMsg(ex,exceptionMsg);
		}
		public void ioException(String module, Throwable ex) {
			
			exceptionMsg = new DocubuilderIOException(ex, module).getMsg();
			printLogMsg(ex,exceptionMsg);
		}
		public void printLogMsg(Throwable ex, String exceptionMsg){
			
			Writer result = new StringWriter();
		    final PrintWriter printWriter = new PrintWriter(result);
		    ex.printStackTrace(printWriter);
		    MDC.put("category",exceptionMsg);
		    developerLog.error(result.toString());
		}
		public String getErrorMsg(String clientId,String errorType, String errorCode){
			 
			  DciCommon dciCommon = new DciCommon();
			  String errorMessage=dciCommon.getErrorMessages(clientId, errorType, errorCode);
			  return errorMessage;
					
		}

}

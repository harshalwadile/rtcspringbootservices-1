package com.dci.rest.exception;

import java.util.HashMap;

public class ErrorMessagesBean {
	/**
     * Error type to check if it is dberror or systemerror
     */
    String errorType="";
    
    String departmentName="";

    /**
     * errorDetail which will store the messageid and message value.
     */
    HashMap errorDetail;
    
    
   

    /**
     * A public constructor where the object of the HashMap is made.
     */
    public ErrorMessagesBean() {
    	
    	 errorDetail = new HashMap();
    	 
    }

   
   /**
     * Returns the errorType
     * 
     * @return the error Type
     */
    public String getErrorType() {
        return errorType;
    }

    /**
     * Sets the errorType
     * 
     * @param errorType
     *            The errorType to set.
     */
    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    /**
	 *@return HashMap
	 *TODO to get the messageid and message value.
	 */
	public  HashMap getErrorDetail() {
		return errorDetail;
	}
	
	/**
	 *@param k
	 *@param v
	 *TODO to set the name and value of the message. 
	 */
	public  void setErrorDetail(String messageid,String messagecode,String messageseverity) {
		
		errorDetail.put(messageid,new ErrorDetail(messagecode,messageseverity));
		
	}
	

	public String getDepartmentName() {
		return departmentName;
	}


	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	
	public class ErrorDetail{
		private String messagevalue = null;
		private String messageseverity = null;
		
		ErrorDetail(String messagecode,String messageseverity){
			setMessagevalue(messagecode);
			setMessageseverity(messageseverity);
		}
		
		public String getMessagevalue() {
			return messagevalue;
		}
		public void setMessagevalue(String messagevalue) {
			this.messagevalue = messagevalue;
		}
		public String getMessageseverity() {
			return messageseverity;
		}
		public void setMessageseverity(String messageseverity) {
			this.messageseverity = messageseverity;
		}	 
	}
}

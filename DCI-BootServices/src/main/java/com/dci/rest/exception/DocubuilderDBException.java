package com.dci.rest.exception;
import java.sql.*;

public class DocubuilderDBException extends DocubuilderException{
	
	String msg = "|| Exception Details ||"+"\n"+"|| DBException in Module Name: ";
	
	public DocubuilderDBException(Throwable ex, String module) {
		
		if(ex instanceof BatchUpdateException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof DataTruncation) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else if(ex instanceof SQLException) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
		
		}else if(ex instanceof SQLWarning) {
			
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
			
		}else{
			msg = msg + "" + module  +"\n"+ "|| Exception Class : " +ex.getClass().getName()+" ||";
		}
		
	}
	
	/**
     * Prints in the log what message is returned.
     */		
	public String getMsg(){
		return msg;
	}

}

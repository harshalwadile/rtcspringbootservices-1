package com.dci.rest.controller.library;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dci.rest.model.Response;
import com.dci.rest.model.SearchCriteria;
import com.dci.rest.service.library.ComponentSearchService;
@SuppressWarnings("unchecked")
@RestController
@RequestMapping({"${SEARCH}","${SEARCH_WITH_CLIENT}"})
public class ComponentSearchController  extends ControllerPreprocess  {
	
	@Autowired ComponentSearchService searchService;
	
	private Logger developerLog = Logger.getLogger(ComponentSearchController.class);

	@RequestMapping(value = "${SEARCH_CRITERIA}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> searchComponents(@PathVariable Map<String, Object> pathVariablesMap, @RequestBody SearchCriteria searchCriteria,HttpServletRequest request){
		MDC.put("category", "com.dci.rest.controller.library.ComponentSearchController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentSearchController || Method Name : searchComponents() || input : "+pathVariablesMap +","+searchCriteria);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = searchService.searchComponents(searchCriteria, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Response : "+responseVO);
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentSearchController || Method Name : searchComponents() || response : "+responseVO);
		return responseVO;		
	}

	@RequestMapping(value = {"${STATISTIC}","${STATISTIC_SPEC}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getLibraryStatistics(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.controller.library.ComponentSearchController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentSearchController || Method Name : getLibraryStatistics() ||  input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = searchService.getLibraryStatistics(pathVariablesMap,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentSearchController || Method Name : getLibraryStatistics() || response : "+responseVO);
		return responseVO;
	}
	@RequestMapping(value = "${SEARCH_LINKAGE_COMPONENT}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> searchLinkageComponent(@Validated(SearchCriteria.ValidationSearchLinkage.class) @RequestBody SearchCriteria searchCriteria,BindingResult bindingresult, @PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category", "com.dci.rest.controller.library.ComponentSearchController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentSearchController || Method Name : searchLinkageComponent() || input : "+pathVariablesMap +","+searchCriteria);
		Response<Object> responseVO = new Response<Object>();
	try {
		//pathVariablesMap = setUserDetails(pathVariablesMap);
		responseVO = searchService.searchLinkageComponent(searchCriteria,bindingresult,pathVariablesMap,request);
	} catch (Exception e) {
		e.printStackTrace();
	}
	developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentSearchController || Method Name : searchLinkageComponent() || response : "+responseVO);
	return responseVO;		
}
	@RequestMapping(value = "${GET_SEARCH_LINKAGE_COMPONENT}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getLinkageComponent(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request){
		MDC.put("category", "com.dci.rest.controller.library.ComponentSearchController");
		developerLog.debug("Entering into com.dci.rest.controller.library.ComponentSearchController || Method Name : searchLinkageComponent() || input : "+pathVariablesMap +",");
		Response<Object> responseVO = new Response<Object>();
	try {
		responseVO = searchService.getLinkageComponent(pathVariablesMap,request);
	} catch (Exception e) {
		e.printStackTrace();
	}
	developerLog.debug("Exiting from com.dci.rest.controller.library.ComponentSearchController || Method Name : searchLinkageComponent() || response : "+responseVO);
	return responseVO;		
}
	
}

package com.dci.rest.bdo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dci.rest.common.ComponentCommons;
import com.dci.rest.dao.WorkFlowDAO;
import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.model.Response;
import com.dci.rest.model.Schema;
import com.dci.rest.model.Status;
import com.dci.rest.model.Transition;
import com.dci.rest.model.WorkFLStatus;
import com.dci.rest.utils.CheckString;

@Component
public class WorkFlowBDO extends ComponentCommons{
	
	@Autowired
	WorkFlowDAO workFlowDAO;
		
	public Response<Object> getAllSchemaList(HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.WorkFlowBDO");
		List<Schema> schemaList = new ArrayList<Schema>();
		List<Schema> finalSchemaList = new ArrayList<Schema>();
		List<WorkFLStatus> workFLStatusList;
		Map<String, String> schemaMap = new HashMap<String, String>();
		Schema schemaObj;
		WorkFLStatus workFLStatus;
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			schemaList = workFlowDAO.getAllSchemaList(userName, null);
			for (Schema schema : schemaList) {
				schemaMap.put(schema.getName(), "");
			}
			for (Map.Entry<String, String> entry : schemaMap.entrySet()) {
				schemaObj = new Schema();
				workFLStatusList = new ArrayList<WorkFLStatus>();
				for (Schema schema : schemaList) {
					workFLStatus = new WorkFLStatus();
					if (entry.getKey().equals(schema.getName())) {
						schemaObj.setName(entry.getKey());
						schemaObj.setCreator(schema.getCreator());
						schemaObj.setId(schema.getId());
						schemaObj.setDescription(schema.getDescription());
						workFLStatus.setName(schema.getfStatusName());
						workFLStatus.setDescription(schema.getfStatusDesc());
						workFLStatus.setAssignee(schema.getfStatusAssigee());
						schemaObj.setType(schema.getType());
						///workFLStatus.setsId(schema.getsId());
						workFLStatus.setOrder(CheckString.getInt(schema.getfOrder()));
						workFLStatus.setId(CheckString.getInt(schema.getfStatus()));
						workFLStatus.setType(CheckString.getInt(schema.getStatusType()));
						workFLStatusList.add(workFLStatus);
					}
				}
				if(!workFLStatusList.isEmpty()) {
					schemaObj.setStatusList(workFLStatusList);
					}
				finalSchemaList.add(schemaObj);
			}
			return response(200,"SUCCESS",null,finalSchemaList);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.WorkFlowBDO || getAllSchemaList()", e);
			return response(500,"ERROR",internalServerError,null);
		}
	}

	public Response<Object> getSchemaDetails(String schemaId, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.WorkFlowBDO");
		List<Schema> schemaList = new ArrayList<Schema>();
		List<Transition> transitionList;
		List<WorkFLStatus> workFLStatusList = new ArrayList<WorkFLStatus>();
		WorkFLStatus workFLStatus = null;
		Schema schemaObj = new Schema();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			schemaList = workFlowDAO.getAllSchemaList(userName, schemaId);

			for (Schema schema : schemaList) {
				workFLStatus = new WorkFLStatus();
				schemaObj.setName(schema.getName());
				schemaObj.setCreator(schema.getCreator());
				schemaObj.setId(schema.getId());
				schemaObj.setDescription(schema.getDescription());
				workFLStatus.setName(schema.getfStatusName());
				workFLStatus.setDescription(schema.getfStatusDesc());
				workFLStatus.setAssignee(schema.getfStatusAssigee());
				//workFLStatus.setsId(schema.getsId());
				workFLStatus.setOrder(CheckString.getInt(schema.getfOrder()));
				workFLStatus.setId(CheckString.getInt(schema.getfStatus()));
				workFLStatus.setType(CheckString.getInt(schema.getStatusType()));
				workFLStatusList.add(workFLStatus);
			}
			for (WorkFLStatus workFLStatusObj : workFLStatusList) {
				transitionList = new ArrayList<Transition>();
				transitionList = workFlowDAO.getStatusTransiton(userName, CheckString.getInt(schemaId),
						workFLStatusObj.getId());
				workFLStatusObj.setTransition(transitionList);
			}
			schemaObj.setStatusList(workFLStatusList);
			return response(200,"SUCCESS",null,schemaObj);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.WorkFlowBDO || getStatusTransiton()", e);
			return response(500,"ERROR",internalServerError,schemaId);
		}
	}

	public Response<Object> getStatusTransiton(String schemaId, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.WorkFlowBDO");
		List<Schema> schemaList = new ArrayList<Schema>();
		List<Transition> transitionList;
		List<WorkFLStatus> workFLStatusList = new ArrayList<WorkFLStatus>();
		WorkFLStatus workFLStatus = null;
		Schema schemaObj = new Schema();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			schemaList = workFlowDAO.getAllSchemaList(userName, schemaId);

			for (Schema schema : schemaList) {
				workFLStatus = new WorkFLStatus();
				schemaObj.setName(schema.getName());
				schemaObj.setId(schema.getId());
				schemaObj.setDescription(schema.getDescription());
				workFLStatus.setName(schema.getfStatusDesc());
				//workFLStatus.setsId(schema.getsId());
				workFLStatus.setOrder(CheckString.getInt(schema.getfOrder()));
				workFLStatus.setId(CheckString.getInt(schema.getfStatus()));
				workFLStatus.setType(CheckString.getInt(schema.getStatusType()));
				workFLStatusList.add(workFLStatus);
			}
			for (WorkFLStatus workFLStatusObj : workFLStatusList) {
				transitionList = workFlowDAO.getStatusTransiton(userName, CheckString.getInt(schemaId),
						workFLStatusObj.getId());
				if (!transitionList.isEmpty()) {
					workFLStatusObj.setTransition(transitionList);
				}
			}
			schemaObj.setStatusList(workFLStatusList);
			return response(200, "SUCCESS", null, schemaObj);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.WorkFlowBDO || getStatusTransiton()", e);
			return response(500, "ERROR", internalServerError, schemaId);
		}
	}

	public Response<Object> createSchema(Schema schema, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.WorkFlowBDO");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			String updateStatus = workFlowDAO.createEditSchema(userName, clientId, null, schema);
			if (updateStatus.equals("INSERTED SUCCESSFULLY")) {
				return response(200, "SUCCESS", null, updateStatus);
			} else if (updateStatus.equals("EXISTING SCHEMA")) {
				return response(409, "ERROR", "Duplicate Schema Name", schema);
			}
			return response(500, "ERROR", internalServerError, schema);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.WorkFlowBDO || createSchema()", e);
			return response(500, "ERROR", internalServerError, schema);
		}
	}
	
	public Response<Object> editSchema(String schemaId, Schema schema, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.WorkFlowBDO");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			String updateStatus = workFlowDAO.createEditSchema(userName, clientId, schemaId, schema);
			if (updateStatus.equals("UPDATED SUCCESSFULLY")) {
				return response(200, "SUCCESS", null, updateStatus);
			} else if (updateStatus.equals("EXISTING SCHEMA")) {
				return response(409, "ERROR", "Duplicate Schema Name", schema);
			}
			return response(500, "ERROR", internalServerError, schema);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.WorkFlowBDO || editSchema()", e);
			return response(500, "ERROR", internalServerError, schema);
		}
	}
	public Response<Object> editSchemaStatus(String schemaId,Schema schema, HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.WorkFlowBDO");
		String updateStatus = null;
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			
			List<WorkFLStatus> statusList = schema.getStatusList();		
			for (WorkFLStatus workFLStatus : statusList) {
				updateStatus =  workFlowDAO.editSchemaStatus(userName,clientId,workFLStatus.getId(),workFLStatus.getName(),workFLStatus.getDescription(),workFLStatus.getAssignee(),workFLStatus.getOrder(),schemaId,workFLStatus);
				//updateSchemaStatus.put("updateSchema", updateStatus);
			}
			for (WorkFLStatus workFLStatus : statusList) {
					updateStatus =  workFlowDAO.editSchemaTransition(userName,clientId,schemaId,workFLStatus.getId(),workFLStatus.getTransitionIds());
					//updateSchemaStatus.put("updateSchemaTransition", updateStatus);
			}
			return response(200,"SUCCESS",null,updateStatus);		
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.WorkFlowBDO || editSchema()", e);
			return response(500,"ERROR",internalServerError,schemaId);
		}
	}
	
	
}

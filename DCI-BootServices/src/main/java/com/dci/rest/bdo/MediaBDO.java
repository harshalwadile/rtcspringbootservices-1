package com.dci.rest.bdo;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import com.dci.rest.common.ComponentCommons;
import com.dci.rest.dao.ComponentDAO;
import com.dci.rest.dao.MediaDAO;
import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.model.APIModel;
import com.dci.rest.model.AssetClass;
import com.dci.rest.model.ComponentBean;
import com.dci.rest.model.DocumentBean;
import com.dci.rest.model.Fund;
import com.dci.rest.model.MediaBean;
import com.dci.rest.model.Response;
import com.dci.rest.model.Schema;
import com.dci.rest.model.Tag;
import com.dci.rest.model.WorkFLStatus;
import com.dci.rest.utils.CheckString;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Component
public class MediaBDO extends ComponentCommons{

	private Logger developerLog = Logger.getLogger(MediaBDO.class);
	
	@Autowired
	MediaDAO mediaDao;

	public Response<Object> createMedia(HttpServletRequest request, MediaBean media, BindingResult bindingResult) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : createMedia() ||");
		Map<String,String> result = new HashMap<String,String>();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			result = mediaDao.createMedia(userName,clientId,media);
			if(result.get("createStatus").equals("INSERTED SUCCESSFULLY"))
			{
	        /*for (Tag tag : mediaTags) {
					tagId = new Tag();
					tagId.setId(Integer.parseInt(mediaDao.createMediaTag(userName,clientId,result.get("id"),tag.getName())));
					mediaTagIds.add(tagId);
				}
				for (Tag tag : mediaTagIds) {
					tagAssociation = mediaDao.associateMediaTag(userName,clientId,result.get("id"),tag.getId());
					result.put("tagAssociation", tagAssociation);
				}
				if(result.get("createStatus").equals("Media Name Already Exits")) {
					return response(409, "ERROR", "Media Already Exist", media);
				}
				if(tagIds.length()>0) {
					result.put("tagAssociation", tagAssociation);
				}*/
				return response(200, "SUCCESS", "", result);
			}
						
			if(result.get("createStatus").equals("MEDIA ALREADY EXISTED")) {
				return response(409, "ERROR", "Media Already Exist", media);
			}
			return response(500, "ERROR", internalServerError, media);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || createMedia()", e);
			return response(500, "ERROR", internalServerError, null);
		}
	}
	
	public Response<Object> createAssoMedia(Map<String, String> pathVariablesMap,HttpServletRequest request, MediaBean media, BindingResult bindingResult) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : createAssoMediaTag() ||");
		Map<String,List<Tag>> result = new HashMap<String,List<Tag>>();
		StringBuffer tagIds = null;
		String tagDisassociation = null;
		String tagAssociation = null;
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			String mediaId = pathVariablesMap.get("mediaId");
			List<Tag> mediaTags = media.getTags();
			List<Tag> mediaTagIds = new  ArrayList<Tag>();
			Tag tagId = null;
			
			tagDisassociation = mediaDao.disassociateMediaTag(userName,clientId,mediaId,null);
			for (Tag tag : mediaTags) {
				tagIds = new StringBuffer();
				tagId = new Tag();
				tagId.setId(Integer.parseInt(mediaDao.createMediaTag(userName,clientId,mediaId,tag.getName())));
				tagId.setName(tag.getName());
				mediaTagIds.add(tagId);
			}
			for (Tag tag : mediaTagIds) {
				tagAssociation = mediaDao.associateMediaTag(userName,clientId,mediaId,tag.getId());
			}
			result.put("associatedTags", mediaTagIds);
			return response(200, "SUCCESS", "", result);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || createAssoMediaTag()", e);
			return response(500, "ERROR", internalServerError, null);
		}
	}
	
	public Response<Object> mediaAssociation(Map<String, String> pathVariablesMap,HttpServletRequest request, MediaBean media, BindingResult bindingResult) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : createAssoMediaTag() ||");
		Map<String,String> result = new HashMap<String,String>();
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objectNode = mapper.createObjectNode();		
		ComponentBean component = new ComponentBean();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			String mediaId = pathVariablesMap.get("mediaId");

			component.setElementInstanceId("906507");
			component.setDocumentAssociation(media.getDocumentAssociation());
			component.setDocumentTypeAssociation(media.getDocumentTypeAssociation());
			component.setAssetClassAssociation(media.getAssetClassAssociation());
			component.setFundsAssociation(media.getFundsAssociation());
			component.setType("para");
			objectNode = setComponentAssociation( userName,clientId,component);	
			result.put("associated", "success");
			return response(200, "SUCCESS", "", result);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || createAssoMediaTag()", e);
			return response(500, "ERROR", internalServerError, null);
		}
	}
	public ObjectNode setComponentAssociation(String user, String clientId,  ComponentBean component) throws Exception {
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objectNode = mapper.createObjectNode();
/*		if(!CheckString.isValidString(component.getType())) {//to get type of the component
			ComponentBean compDataFromDB =  componentDAO.getComponentData(component.getElementInstanceId());
			component.setType(compDataFromDB.getType());
		}*/
		//if(CheckString.isValidString(component.getDocumentAssociation()) || CheckString.isValidString(component.getDocumentTypeAssociation()) || CheckString.isValidString(component.getFundsAssociation()) || CheckString.isValidString(component.getAssetClassAssociation())) {
			//if(component.getOperation().equalsIgnoreCase("edit") && component.getElementInstanceId()!=null){
			mediaDao.dissociateMediaComponent(user, clientId, component.getElementInstanceId());
			//}
	//	}		
		if(CheckString.isValidString(component.getDocumentAssociation())) {
			objectNode.put("documentAssociationStatus", mediaDao.setMediaDocumentAssociation(user, component.getElementInstanceId(), component.getDocumentAssociation()));
		}
		if(CheckString.isValidString(component.getDocumentTypeAssociation())) {
			objectNode.put("docTypeAssociationStatus", mediaDao.setMediaDocTypeAssociation(user, component.getElementInstanceId(), component.getDocumentTypeAssociation()));
		}
		if(CheckString.isValidString(component.getAssetClassAssociation())) {
			objectNode.put("assetClassAssociationStatus", mediaDao.setMediaAssetClassAssociation(user, component.getElementInstanceId(), component.getAssetClassAssociation(), component.getType()));
		}
		if(CheckString.isValidString(component.getFundsAssociation())) {
			objectNode.put("fundAssociationStatus", mediaDao.setMediaFundAssociation(user, component.getElementInstanceId(), component.getFundsAssociation()));
		}
		return objectNode;
	}
	public Response<Object> editMedia(Map<String, String> pathVariablesMap, HttpServletRequest request, MediaBean media,
			BindingResult bindingResult) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : editMedia() ||");
		try {
			String tagDisassociation = null;
			String tagAssociation = null;
			Map<String, List<Tag>> response = new HashMap<String, List<Tag>>();
			Map<String,String> result = new HashMap<String,String>();
			List<Tag> mediaTagIds = new ArrayList<Tag>();
			List<Tag> newMediaTagIds = new ArrayList<Tag>();
			Tag tagId = null;
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			String mediaId = pathVariablesMap.get("mediaId");
			
			if (CheckString.isValidString(mediaId)) {
				result = mediaDao.editMedia(userName, clientId,mediaId, media);
				mediaTagIds = media.getTags();
					if (mediaTagIds !=null) {
						
					tagDisassociation = mediaDao.disassociateMediaTag(userName,clientId,mediaId,null);
					for (Tag tag : mediaTagIds) {
						tagId = new Tag();
						tagId.setId(Integer.parseInt(mediaDao.createMediaTag(userName,clientId,mediaId,tag.getName())));
						tagId.setName(tag.getName());
						newMediaTagIds.add(tagId);
					}
					for (Tag tag : newMediaTagIds) {
						tagAssociation = mediaDao.associateMediaTag(userName,clientId,mediaId,tag.getId());
					}
					response.put("tagAssociation", newMediaTagIds);
					/*tagDisassociation = mediaDao.disassociateMediaTag(userName, clientId, mediaId, null);
					for (Tag tag : mediaTagIds) {
						tagAssociation = mediaDao.associateMediaTag(userName, clientId, mediaId, tag.getId());
					}*/
					return response(200, "SUCCESS", "", response);
				}
				response.put("Media Updated", newMediaTagIds);
				return response(200, "SUCCESS", "", response);
			}
			return response(200, "ERROR", "Invalid Media Id", media);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || editMedia()", e);
			return response(500, "ERROR", internalServerError, null);
		}
	}

	public Response<Object> getAllMedia(HttpServletRequest request, Map<String, String> pathVariablesMap) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : getAllMedia() ||");
		Map<String, String> mediaMap = new HashMap<String, String>();
		MediaBean mediaObj = null;
		Tag tags = null;
		List<MediaBean> mediaFinalList = new ArrayList<MediaBean>();
		List<Tag> tagList = new ArrayList<Tag>();
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			String mediaId = pathVariablesMap.get("mediaId");


			List<MediaBean> mediaBeanList = mediaDao.getAllMedia(userName, clientId, mediaId);
			for (MediaBean mediaBean : mediaBeanList) {
				mediaMap.put(mediaBean.getName(), "");
			}
			for (Map.Entry<String, String> entry : mediaMap.entrySet()) {
				mediaObj = new MediaBean();
				tagList = new ArrayList<Tag>();
				for (MediaBean mediaBean : mediaBeanList) {
					tags = new Tag();
					if (entry.getKey().equals(mediaBean.getName())) {
						mediaObj.setName(entry.getKey());
						mediaObj.setId(mediaBean.getId());
						mediaObj.setStatus(mediaBean.getStatus());
						mediaObj.setMessage((mediaBean.getMessage()));
						mediaObj.setPath(mediaBean.getPath());
						mediaObj.setType(mediaBean.getType());
						mediaObj.setDescription(mediaBean.getDescription());
						mediaObj.setAlt(mediaBean.getAlt());
						mediaObj.setSize(mediaBean.getSize());
						mediaObj.setCreatedBy(mediaBean.getCreatedBy());
						mediaObj.setCreateTime(mediaBean.getCreateTime());
						mediaObj.setUpdatedBy(mediaBean.getUpdatedBy());
						mediaObj.setUpdateTime(mediaBean.getUpdateTime());
						if (CheckString.isValidString(mediaBean.getTagId())) {
							tags.setId(Integer.parseInt(mediaBean.getTagId()));
							tags.setName(mediaBean.getTagName());
							tagList.add(tags);
						}
					}

				}
				if (!tagList.isEmpty()) {
					mediaObj.setTags(tagList);
				}
				mediaFinalList.add(mediaObj);
			}
			
			return response(200, "SUCCESS", "", mediaFinalList);

		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getAllMedia()", e);
			return response(500, "ERROR", internalServerError, null);
		}
	}
	public Response<Object> disAssociateMediaTag(HttpServletRequest request,Map<String, String> pathVariablesMap, MediaBean media, BindingResult bindingResult) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : editMedia() ||");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			//mediaDao.createMedia(userName,clientId,media);
			//mediaDao.editMedia(userName,clientId,media)
			return response(200, "SUCCESS", "", "");
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || editMedia()", e);
			return response(500, "ERROR", internalServerError, null);
		}
	}
	public Response<Object> removeMedia(Map<String, String> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : removeMedia() ||");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			String mediaId = pathVariablesMap.get("mediaId");

			//mediaDao.createMedia(userName,clientId,media);
			return response(200, "SUCCESS", "", mediaDao.removeMedia(userName,clientId,mediaId));
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || removeMedia()", e);
			return response(500, "ERROR", internalServerError, null);
		}
	}
	public Response<Object> getMediaAssociation(Map<String, String> pathVariablesMap, HttpServletRequest request) {
		MDC.put("category","com.dci.rest.bdo.ComponentBDO");
		Map<String, Object> assocationData = new HashMap<String, Object>();
		ArrayList<Fund> fundsArray = new ArrayList<Fund>();
		ArrayList<Fund> fundsArrayFinal = new ArrayList<Fund>();
		List<Fund> found = new ArrayList<Fund>();
		try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			String mediaId = pathVariablesMap.get("mediaId");

			if(CheckString.isValidString(mediaId)) {
				assocationData.put("documentAssociationList", mediaDao.getMediaDocumentAssociation(userName, clientId, mediaId));
				assocationData.put("docTypeAssociationList", mediaDao.getMediaDocTypeAssociation(userName,clientId, mediaId));
				List<AssetClass> assetClassList = mediaDao.getMediaAssetClassAssociation(userName, clientId, mediaId);
				assocationData.put("assetClassList",assetClassList);
				fundsArray = mediaDao.getMediaContentList(userName, clientId, mediaId);
				List<Fund> fundList = null ;
				for (int i = 0;i<assetClassList.size();i++) {
					fundList = new ArrayList<Fund>();
					fundList = mediaDao.getFundListOnAssetClass(userName,clientId,assetClassList.get(i).getId());
					for (int j = 0;j<fundList.size();j++) {
						fundList.get(j).setAssetClassId(assetClassList.get(i).getId());
						fundList.get(j).setAssetClassName(assetClassList.get(i).getName());
						fundsArrayFinal.add(fundList.get(j));
					}
				}
				for (Fund fundArr : fundsArray) {
					if (fundList != null) {
						for (Fund fundLi : fundList) {
							if (fundArr.getId() == fundLi.getId()) {
								found.add(fundArr);
							}
						}
					}
				}
				fundsArray.removeAll(found);
				if (fundsArrayFinal != null) {
					fundsArray.addAll(fundsArrayFinal);
				}
				assocationData.put("fundAssociationList", fundsArray);
				return response(200,"SUCCESS","",assocationData);
			}else {
				return response(400,"ERROR",invalidCompId,mediaId);
			}
		}catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getComponentAssociation()",e);
			return response(500,"ERROR",internalServerError,"");
		}
	}

	public Response<Object> createMediaAPI(Map<String, String> pathVariablesMap,APIModel apiModel,HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : removeMedia() ||");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
	
			return response(200, "SUCCESS", "", mediaDao.createMediaAPI(userName,clientId,apiModel));
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || removeMedia()", e);
			return response(500, "ERROR", internalServerError, null);
		}
	}
	public Response<Object> getMediaAPI(Map<String, String> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : getMediaAPI() ||");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
	
			return response(200, "SUCCESS", "", mediaDao.getMediaAPI(userName,clientId));
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getMediaAPI()", e);
			return response(500, "ERROR", internalServerError, null);
		}
	}
/*	public Response<Object> getPostMediaAPI(Map<String, String> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : getMediaAPI() ||");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
	
			return response(200, "SUCCESS", "", mediaDao.getPostMediaAPI(userName,clientId));
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || getMediaAPI()", e);
			return response(500, "ERROR", internalServerError, null);
		}
	}
	*/
}

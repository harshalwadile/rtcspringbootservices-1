package com.dci.rest.service.library;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.dci.rest.bdo.ComponentBDO;
import com.dci.rest.model.Filter;
import com.dci.rest.model.Response;

@Service
public class ComponentFilterService {
	
	@Autowired
	ComponentBDO componentBDO;
	
	public Response<Object> getFilterList(HttpServletRequest request){
 		return componentBDO.getFilterList(request);
	}
	public Response<Object> createUserFilter(Map<String,Object> reqParam,HttpServletRequest request,Filter filter,BindingResult bindingResult){
 		return componentBDO.createUserFilter(reqParam,request,filter,bindingResult);
	}
	public Response<Object> editUserFilter(Map<String,Object> reqParam,HttpServletRequest request,Filter filter,BindingResult bindingResult){
 		return componentBDO.editUserFilter(reqParam,request,filter,bindingResult);
	}
	public Response<Object> getUserFilters(HttpServletRequest request) {
		return componentBDO.getUserFiltesr(request);
	}
	
}

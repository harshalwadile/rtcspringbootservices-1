package com.dci.rest.service.library;


import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.dci.rest.bdo.MediaBDO;
import com.dci.rest.model.APIModel;
import com.dci.rest.model.MediaBean;
import com.dci.rest.model.Response;

@Service
public class MediaService {
    @Autowired
	MediaBDO mediaBDO;
	public Response<Object> createMedia(HttpServletRequest request, MediaBean media, BindingResult bindingResult) {
		return mediaBDO.createMedia(request,media,bindingResult);
	}
	public Response<Object> createAssoMedia(Map<String, String> pathVariablesMap,HttpServletRequest request, MediaBean media, BindingResult bindingResult) {
		return mediaBDO.createAssoMedia(pathVariablesMap,request,media,bindingResult);
	}
	public Response<Object> mediaAssociation(Map<String, String> pathVariablesMap,HttpServletRequest request, MediaBean media, BindingResult bindingResult) {
		return mediaBDO.mediaAssociation(pathVariablesMap,request,media,bindingResult);
	}
	
	public Response<Object> removeMedia(Map<String, String> pathVariablesMap,HttpServletRequest request) {
		return mediaBDO.removeMedia(pathVariablesMap,request);
	}
	
	public Response<Object> editMedia(Map<String, String> pathVariablesMap,HttpServletRequest request, MediaBean media, BindingResult bindingResult) {
		return mediaBDO.editMedia(pathVariablesMap,request,media,bindingResult);
	}
	public Response<Object> getAllMedia(HttpServletRequest request, Map<String, String> pathVariablesMap) {
		return mediaBDO.getAllMedia(request,pathVariablesMap);
	}

	public Response<Object> disAssociateMediaTag(HttpServletRequest request, Map<String, String> pathVariablesMap, MediaBean media, BindingResult bindingResult) {
		return mediaBDO.disAssociateMediaTag(request,pathVariablesMap,media,bindingResult);
	}
	public Response<Object> getMediaAssociation(Map<String, String> pathVariablesMap,HttpServletRequest request) throws Exception {
		return mediaBDO.getMediaAssociation(pathVariablesMap,request);
	}
	public Response<Object> createMediaAPI(Map<String, String> pathVariablesMap,APIModel apiModel,HttpServletRequest request) throws Exception {
		return mediaBDO.createMediaAPI(pathVariablesMap,apiModel,request);
	}
	public Response<Object> getMediaAPI(Map<String, String> pathVariablesMap,HttpServletRequest request) throws Exception {
		return mediaBDO.getMediaAPI(pathVariablesMap,request);
	}
	
}

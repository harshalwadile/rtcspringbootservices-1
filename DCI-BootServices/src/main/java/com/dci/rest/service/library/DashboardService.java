package com.dci.rest.service.library;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dci.rest.bdo.DashboardBDO;
import com.dci.rest.bean.DashboardLayout;
import com.dci.rest.model.Response;

@Service
public class DashboardService {
	
	@Autowired DashboardBDO dashboardBDO;

	public Response<Object> getUserChartPrefrence(Map<String, String> pathVariablesMap,HttpServletRequest request) {
		return dashboardBDO.getUserChartPrefrence(pathVariablesMap,request);
	}

	public Response<Object> updateChartPrefrence(Map<String, String> pathVariablesMap,DashboardLayout dashboardLayout,HttpServletRequest request) {
		return dashboardBDO.updateChartPrefrence(pathVariablesMap,dashboardLayout,request);
	}

}

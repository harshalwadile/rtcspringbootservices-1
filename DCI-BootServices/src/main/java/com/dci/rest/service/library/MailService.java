package com.dci.rest.service.library;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dci.rest.bdo.MailBDO;
import com.dci.rest.model.Mail;
import com.dci.rest.model.Response;
@Service
public class MailService {
	@Autowired MailBDO mailBDO;
	public Response<Object> sendMail(Mail mail,HttpServletRequest request){
		return mailBDO.sendMail(mail,request);
	}
}

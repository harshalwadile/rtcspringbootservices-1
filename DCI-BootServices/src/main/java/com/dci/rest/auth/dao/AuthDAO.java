package com.dci.rest.auth.dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
/*import org.scribe.model.Token;*/
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.dci.rest.dao.DataAccessObject;
import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.model.APIModel;
import com.dci.rest.utils.CheckString;

@Component
@Repository
public class AuthDAO extends DataAccessObject {

	@Value("${APP_ID}" ) private String appId;
	
	private Logger developerLog = Logger.getLogger(AuthDAO.class);

	public Map<String, String> registerClient(String username,String client,APIModel aPIBean) {
		
		MDC.put("category","com.dci.oauth.dao");
		developerLog.warn("Entering into com.dci.rest.auth.dao.AuthDAO || Method Name : registerClient() ||");
		Connection con = null;
		CallableStatement cs = null;
		int i = 0;
		boolean bIsOk = false;
		Map<String,String> result = new HashMap<String,String>();
		try {
			con = getAuthConnection();
			developerLog.debug("{CALL ZDBXAUTH04.SP8_RTC_CREATEAPI("+username+","+client+","+aPIBean.getName()+","+ aPIBean.getDescription()+","+aPIBean.getAccess_token()+","+aPIBean.getBase_URL()+","+aPIBean.getSchemaSelector()+","+aPIBean.getStatus()+")}");
			cs = con.prepareCall("{CALL SP8_RTC_CREATEAPI (?,?,?,?,?,?,?,?,?,?)}");
			cs.setString(1, username);
			cs.setBigDecimal(2, CheckString.getBigDecimal(client));
			cs.setString(3, aPIBean.getName());
			cs.setString(4, aPIBean.getDescription());
			cs.setString(5, aPIBean.getAccess_token());
			cs.setString(6, aPIBean.getBase_URL());
			cs.setString(7, aPIBean.getSchemaSelector());
			cs.setString(8, aPIBean.getStatus());
			cs.registerOutParameter(9, java.sql.Types.VARCHAR);
			cs.registerOutParameter(10, java.sql.Types.BIGINT);
			cs.executeUpdate();
			result.put("status",cs.getString(9));
			result.put("id",cs.getString(10));

	}catch (Exception e) {
		System.out.println(e);
		 //new DCIException("com.dci.oauth.dao.UserAuthServerDAO || insertUserAuthDetails()",e);
	}		
	finally{
		releaseConStmts(null, cs, con, "com.dci.rest.auth.dao.AuthDAO  || MethodName:  registerClient()");
		developerLog.warn("Exiting from com.dci.rest.auth.dao.AuthDAO || Method Name : registerClient() ||");
	}	
	return result;
	
	}	
	
	public  List<APIModel> getApiList(String client,String user,String apiId) throws SQLException{
		MDC.put("category"," com.dci.oauth.dao.UserAuthServerDAO");
		developerLog.warn("Entering into  com.dci.rest.auth.dao.AuthDAO  || Method Name : userAuthServerDetails() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		//Token token = null;
		APIModel apiDetails = null;
		List<APIModel> apiList = new ArrayList<APIModel>();
		try {
			if (con == null || con.isClosed())
				con = getAuthConnection();
			developerLog.debug("{CALL ZDBXAUTH04.SP8_RTC_GETAPI  ("+appId+")}");
			cs = con.prepareCall("{CALL SP8_RTC_GETAPI (?,?,?,?)}");
			cs.setBigDecimal(1, CheckString.getBigDecimal(client));
			cs.setString(2, user);
			cs.setBigDecimal(3, CheckString.getBigDecimal(appId));
			cs.setBigDecimal(4, CheckString.getBigDecimal(apiId));
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {
				apiDetails = new APIModel();
				apiDetails.setName(rs.getString("FAPI_NAME"));
				apiDetails.setDescription(rs.getString("FAPI_DESCRIPTION"));
				apiDetails.setAccess_token(rs.getString("FACCESS_TOKEN"));
				apiDetails.setBase_URL(rs.getString("FBASE_URL"));
				apiDetails.setSchemaSelector(rs.getString("FSCHEMA_SELECTOR"));
				apiDetails.setStatus(rs.getString("FSTATUS_OF_API"));
				apiDetails.setCreatedBy(rs.getString("FCREATEDBY"));
				apiDetails.setUpdatedBy(rs.getString("FLASTCHANGEDBY"));
				apiDetails.setId(rs.getString("FTABLEAPIID"));
				apiList.add(apiDetails);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally{
			releaseConStmts(rs, cs, con, "com.dci.rest.auth.dao.AuthDAO || MethodName: userAuthServerDetails()");
			developerLog.warn("Exiting from com.dci.rest.auth.dao.AuthDAO || Method Name : userAuthServerDetails() ||");
		}	
		return apiList;
	}
	
	public  APIModel getApiDetailsByURL(String client,String user,String baseUrl) throws SQLException{
		MDC.put("category"," com.dci.oauth.dao.UserAuthServerDAO");
		developerLog.warn("Entering into  com.dci.rest.auth.dao.AuthDAO  || Method Name : getApiDetailsByURL() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		APIModel apiDetails = new APIModel();
		try {
			if (con == null || con.isClosed())
				con = getAuthConnection();   
			developerLog.debug("{CALL ZDBXAUTH04.SP8_RTC_GETAPIBYBASEURL  ("+appId+")}");
			cs = con.prepareCall("{CALL SP8_RTC_GETAPIBYBASEURL (?,?,?)}");
			cs.setBigDecimal(1, CheckString.getBigDecimal(client));
			cs.setString(2, user);
			//cs.setBigDecimal(3, CheckString.getBigDecimal(appId));
			cs.setString(3, baseUrl);
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {
				apiDetails.setName(rs.getString("FAPI_NAME"));
				apiDetails.setDescription(rs.getString("FAPI_DESCRIPTION"));
				apiDetails.setAccess_token(rs.getString("FACCESS_TOKEN"));
				apiDetails.setBase_URL(rs.getString("FBASE_URL"));
				apiDetails.setSchemaSelector(rs.getString("FSCHEMA_SELECTOR"));
				apiDetails.setStatus(rs.getString("FSTATUS_OF_API"));
				apiDetails.setCreatedBy(rs.getString("FCREATEDBY"));
				apiDetails.setUpdatedBy(rs.getString("FLASTCHANGEDBY"));
			}
		}catch(Exception e) {
			System.out.println("ERROR: "+e);
			e.printStackTrace();
		}finally{
			releaseConStmts(rs, cs, con, "com.dci.rest.auth.dao.AuthDAO || MethodName: getApiDetailsByURL()");
			developerLog.warn("Exiting from com.dci.rest.auth.dao.AuthDAO || Method Name : getApiDetailsByURL() ||");
		}	
		return apiDetails;
	}
	
	public  APIModel userAuthServerDetails(String client,String user,String accessKey) throws SQLException{
		MDC.put("category"," com.dci.oauth.dao.UserAuthServerDAO");
		developerLog.warn("Entering into  com.dci.rest.auth.dao.AuthDAO  || Method Name : userAuthServerDetails() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		APIModel apiDetails = new APIModel();
		try {
			if (con == null || con.isClosed())
				con = getAuthConnection();   
			developerLog.debug("{CALL ZDBXAUTH04.SP8_RTC_GETRESTSERVICE  ("+appId+")}");
			cs = con.prepareCall("{CALL SP8_RTC_GETRESTSERVICE(?)}");
			cs.setString(1, accessKey);
			rs = cs.executeQuery();
			if(rs != null && rs.next()) {
				//token = new Token(rs.getString("FACCESS_TOKEN"),rs.getString("FSECRET_TOKEN"),rs.getString("FREQUEST_TOKEN"));
				apiDetails.setApp_URL(rs.getString("FAPP_URL"));
				//userAuthDTO.setToken(token);
				apiDetails.setCallback_URL(rs.getString("FCALLBACK_URL"));
				apiDetails.setConsumer_Key(rs.getString("FCONSUMER_KEY"));
				apiDetails.setBase_URL(rs.getString("FREST_SERVICE_BASE_URL"));
				apiDetails.setPublic_Key(rs.getString("FPUBLIC_KEY"));
				apiDetails.setVerification_Key(rs.getString("FVERIFICATION_KEY"));
			}
		}catch(Exception e) {
			System.out.println("ERROR: "+e);
			e.printStackTrace();
		}finally{
			releaseConStmts(rs, cs, con, "com.dci.rest.auth.dao.AuthDAO || MethodName: userAuthServerDetails()");
			developerLog.warn("Exiting from com.dci.rest.auth.dao.AuthDAO || Method Name : userAuthServerDetails() ||");
		}	
		return apiDetails;
	}
	public String removAPI(String user,String client,String apiId) {
		MDC.put("category","com.dci.rest.dao.UserAuthServerDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;  
		Map<String,String> result = new HashMap<String,String>();
		String updateStatus = null;String id = null;
		developerLog.debug("Entering into com.dci.rest.dao.UserAuthServerDAO || Method Name : removAPI() ||");
		try {
			if(con == null || con.isClosed())
				con = getAuthConnection();
			developerLog.debug("{call SP8_RTC_REMOVEAPI  ("+user+","+client+","+apiId+",?)}");
			cs = con.prepareCall("{call SP8_RTC_REMOVEAPI(?,?,?,?)}");
			cs.setBigDecimal(1, CheckString.getBigDecimal(client));
			cs.setString(2, user);
			cs.setBigDecimal(3, CheckString.getBigDecimal(apiId));
			cs.registerOutParameter(4, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			updateStatus = cs.getString(4);
		    //result.put("id", id);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.UserAuthServerDAO || removAPI()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.MediaDAO || Method Name : removAPI() ||");
		}
		return updateStatus;	
	}
	/*
	public boolean insertRequestToken(APIBean aPIBean,String consumerKey) throws Exception{
		MDC.put("category"," com.dci.rest.auth.dao.AuthDAO");
		developerLog.warn("Entering into  com.dci.rest.auth.dao.AuthDAO || Method Name : insertRequestToken() ||");
		Connection con = null;
		CallableStatement cs = null;
		int i = 0;
		boolean bIsOk = false;
		try {
			con = getAuthConnection();
			developerLog.debug("{CALL ZDBXAUTH04.SP8_UPDATETOKENRESTSERVICE  ("+consumerKey+",null,"+aPIBean.getToken().getToken()+")}");
			cs = con.prepareCall("{CALL SP8_UPDATETOKENRESTSERVICE  (?,?,?)}");
			cs.setString(++i, consumerKey);
			cs.setString(++i, null);
			cs.setString(++i, aPIBean.getToken().getToken());
			if(cs.executeUpdate() > 0);
				bIsOk = true;
		}finally{
			releaseConStmts(null, cs, con, "com.dci.rest.auth.dao.AuthDAO || MethodName: insertRequestToken()");
			developerLog.warn("Exiting from com.dci.rest.auth.dao.AuthDAO || Method Name : insertRequestToken() ||");
		}	
		return bIsOk;
	}
	
	public boolean insertAccessToken(APIBean aPIBean,String consumerKey) throws Exception{
		MDC.put("category"," com.dci.rest.auth.dao.AuthDAO");
		developerLog.warn("Entering into  com.dci.rest.auth.dao.AuthDAO || Method Name : insertAccessToken() ||");
		Connection con = null;
		CallableStatement cs = null;
		int i = 0;
		boolean bIsOk = false;
		try {
			con = getAuthConnection();
			developerLog.debug("{CALL ZDBXAUTH04.SP8_UPDATETOKENRESTSERVICE  ("+consumerKey+","+aPIBean.getToken().getToken()+",null)}");
			cs = con.prepareCall("{CALL ZDBXAUTH04.SP8_UPDATETOKENRESTSERVICE  (?,?,?)}");
			cs.setString(++i, consumerKey);
			cs.setString(++i, aPIBean.getToken().getToken());
			cs.setString(++i, null);
			if(cs.executeUpdate() > 0);
				bIsOk = true;
		}finally{
			releaseConStmts(null, cs, con, "com.dci.rest.auth.dao.AuthDAO || MethodName: insertAccessToken()");
			developerLog.warn("Exiting from com.dci.rest.auth.dao.AuthDAO || Method Name : insertAccessToken() ||");
		}	
		return bIsOk;	
	}*/
}

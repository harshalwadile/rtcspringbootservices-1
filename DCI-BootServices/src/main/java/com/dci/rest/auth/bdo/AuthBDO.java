package com.dci.rest.auth.bdo;

import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.tidy.AttrCheckImpl.CheckAlign;

import com.dci.rest.auth.dao.AuthDAO;
import com.dci.rest.bdo.MediaBDO;
import com.dci.rest.common.ComponentCommons;
import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.model.APIModel;
import com.dci.rest.model.Response;
import com.dci.rest.utils.CheckString;

@Component
public class AuthBDO extends ComponentCommons {
	
	@Autowired
	AuthDAO authBDO;
	private Logger developerLog = Logger.getLogger(AuthBDO.class);

	public Response<Object> registerClient(HttpServletRequest request,APIModel aPIBean) {
		try {
			
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();

			return response(200, "SUCCESS", null, authBDO.registerClient(userName,clientId,aPIBean));
		} catch (Exception e) {
			return response(500, "ERROR", internalServerError, "Request Tokenn does not added");
		}
	}

	public Response<Object> getApiList(Map<String, String> pathVariablesMap,HttpServletRequest request) throws SQLException {
		try {
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			String id = pathVariablesMap.get("id");
			if(CheckString.isValidString(id)) {
				return response(200, "SUCCESS", null,authBDO.getApiList(clientId,userName,id));
			}
			return response(200, "SUCCESS", null,authBDO.getApiList(clientId,userName,null));
		} catch (Exception e) {
			return response(500, "ERROR", internalServerError, "Request Tokenn does not added");
		}
	}
	public  APIModel getApiDetailsByURL(HttpServletRequest request,String baseUrl) throws SQLException {
		try {
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			return authBDO.getApiDetailsByURL(clientId,userName,baseUrl);
		} catch (Exception e) {
			return null;
		}
	}
	public  APIModel userAuthServerDetails(HttpServletRequest request,String accessKey) throws SQLException {
		try {
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			return authBDO.userAuthServerDetails(clientId,userName,accessKey);
		} catch (Exception e) {
			return null;
		}
	}
	
	public Response<Object> removAPI(Map<String, String> pathVariablesMap,HttpServletRequest request) {
		MDC.put("category", "com.dci.rest.bdo.ComponentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.ComponentBDO || Method Name : removeMedia() ||");
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			String apiId = pathVariablesMap.get("id");
			//mediaDao.createMedia(userName,clientId,media);
			return response(200, "SUCCESS", "", authBDO.removAPI(userName,clientId,apiId));
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentBDO || removeMedia()", e);
			return response(500, "ERROR", internalServerError, null);
		}
	}
/*
	public Response<Object> addRequestToken(APIBean aPIBean) throws SQLException {
		try {
			return response(200, "SUCCESS", null, authBDO.insertRequestToken(aPIBean, aPIBean.getConsumer_Key()));
		} catch (Exception e) {
			return response(500, "ERROR", internalServerError, "Request Token does not added");
		}
	}
	public Response<Object> addAccessToken(APIBean aPIBean) throws SQLException {
		try {
			return response(200, "SUCCESS", null, authBDO.insertAccessToken(aPIBean, aPIBean.getConsumer_Key()));
		} catch (Exception e) {
			return response(500, "ERROR", internalServerError, "Access Token does not added");
		}
	}
	
*/	

}

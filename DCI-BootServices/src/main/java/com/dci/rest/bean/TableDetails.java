package com.dci.rest.bean;

import java.util.ArrayList;
import java.util.HashMap;

import com.dci.rest.utils.CheckString;
import com.dci.rest.utils.DBXmlManager;
import com.dci.rest.utils.StringUtility;

@SuppressWarnings({"rawtypes","unchecked","unused"})
public class TableDetails {	
	private String dispname = null;
	private String effdate = null;
	private String expdate = null;
	private String documentType = null;
	private String tableType = null;
	private int rows;
	private int cols;
	private String global = "1"; 
	private String[] documentTypeList =null;
	private String[] documentList =null;
	private String[] fund = null;
	private String[] fundfamily = null;//Asset Class
	private int orderRows[] = null;
	private int orderCols[] = null;
	private ArrayList tablerow = null;
	private int totalrows;
	private int totalcols;
	
	private static final String TABLE ="<table>";
	private static final String eTABLE ="</table>";	
	private static final String TBODY ="<tbody>";
	private static final String eTBODY ="</tbody>";	
	private static final String TR ="<row>";
	private static final String hTR ="<row";
	private static final String eTR ="</row>";
	private static final String TD ="<entry>";  
	private static final String hTD ="<entry";  
	private static final String eTD ="</entry>";
	private static final String cTAG =">";
	private Graphic graphic = null;
	
	private String[] selrow = null;
	private String[] selcol = null;
	private String[] tstyles = null;
	
	private String render = "0";
	private String datatype ="";
	private String primary= "";
	private String topXTable = "";
	private String style = "";
	private String maxSysentitySeqId="";
	private String cspan = null;
	
	private HashMap newAttribute = null;
	private HashMap newRowAttribute = null;
	private ArrayList newAddedColAttribute = new ArrayList();
	private ArrayList newAddedRowAttributes = new ArrayList();
	
	private ArrayList<String> newTableAttributes = new ArrayList();
	
	private String[] cellValue = null;
	
	private boolean isSelColSet = false;
		
	public int getTotalcols() {
		return totalcols;
	}

	public void setTotalcols(int totalcols) {
		this.totalcols = totalcols;
	}

	public int getTotalrows() {
		return totalrows;
	}

	public void setTotalrows(int totalrows) {
		this.totalrows = totalrows;
	}

	public TableDetails() {
		rows = 0;
		cols = 0;	
		tablerow = null;
	}
	
	public TableDetails(int p_rows,int p_cols)	{
		this.rows = p_rows;
		this.cols = p_cols;
		this.orderRows = new int[p_rows];
		this.orderCols = new int[p_cols];
		this.tablerow = new ArrayList();
		for(int i=0;i<rows;i++) {
			ArrayList al = new ArrayList();
			for(int j=0;j<cols;j++) {
				al.add(new TableCell("",null));
			}			
			tablerow.add(new TableRow(al,null));
		}
	}
	
	public String getXHtmlTable() {
		StringBuffer sb =  new StringBuffer(TABLE);
		
		sb.append(TBODY);		
		if (tablerow == null && rows == 0 && cols == 0 ) return "";
		if (tablerow == null && rows >0 && cols >0 ) {
			for(int i=0;i<rows;i++) {
				sb.append(TR);
				for(int j=0;j< cols;j++) {
					sb.append(TD);
					sb.append("");
					sb.append(eTD);
				}
				sb.append(eTR);
			}
		} else {	 
			for(int i=0;i<tablerow.size();i++) {
				TableRow tr = (TableRow)tablerow.get(i);				
				sb.append(hTR);
				sb.append(tr.getAttributes());
				
				StringBuffer addRowAttrib = new StringBuffer();
				boolean rowCheck = false;
				if(getNewRowAttribute() != null && getNewRowAttribute().containsKey(i)) {
					sb.append(" "+getNewRowAttribute().get(i));
				} else if(getNewRowAttribute() != null && (!getNewRowAttribute().containsKey(i))) {
					for(int rowAtt = 0; rowAtt < getNewAddedRowAttributes().size(); rowAtt++) {
						addRowAttrib.append(getNewAddedRowAttributes().get(rowAtt)+"=\"\" ");
						rowCheck = true;
					}
					if(rowCheck) {
						addRowAttrib.deleteCharAt(addRowAttrib.lastIndexOf(" "));
						sb.append(addRowAttrib);
					}
				}
				
				sb.append(cTAG);
				ArrayList al = tr.getTablecell();
				for(int j=0;j<al.size();j++) {
					TableCell td = (TableCell)al.get(j); 
					sb.append(hTD);						
				
						sb.append(td.getAttributes());
						StringBuffer addAttribute = new StringBuffer();
						boolean entryCheck = false;
						if(getNewAttribute() != null && getNewAttribute().containsKey(""+i+","+j))
							sb.append(" "+getNewAttribute().get(""+i+","+j));
						else if(getNewAttribute() != null && (!getNewAttribute().containsKey(""+i+","+j))) {
							for(int k = 0; k < getNewAddedColAttribute().size(); k++) {
								addAttribute.append(getNewAddedColAttribute().get(k)+"=\"\" ");
								entryCheck = true;
							}
							if(entryCheck) {
								addAttribute.deleteCharAt(addAttribute.lastIndexOf(" "));
								sb.append(" "+addAttribute);
							}
						}
				
					sb.append(cTAG);
				
					sb.append(CheckString.convertXHtmlToDocBook(CheckString.removeCRLF(CheckString.convertStyleIndents(CheckString.convertToXHTMLx(StringUtility.fixSpecialCharforWeb(td.getCellvalue())))).replaceAll("<u style=\"TEXT-DECORATION: underline\">","<u style=\"text-decoration: underline\">"))); 
					sb.append(eTD);
				}
				sb.append(eTR);
			}
		}	
		sb.append(eTBODY+eTABLE);
		String value = StringUtility.fixSpecialAttributes(sb.toString());
		return value;		
	}
	
	
	public TableDetails setTableValue(int row, int col, String gridValue) {
		String item[] = gridValue.split(",");
		int k=0;
		if(item == null) return this;
		for(int i=0;i<row;i++) {			
			TableRow tr = (TableRow) tablerow.get(i);
				ArrayList al = (ArrayList)tr.getTablecell();				
				for(int j=0;j<al.size();j++) {
					int cellColSPan = 0;
					TableCell tc = (TableCell)al.get(j);
					  String cSpanStr = tc.getCspan();
					  if(cSpanStr != null && !cSpanStr.equals(""))
						  cellColSPan = Integer.parseInt(cSpanStr);	
					  item[k] = DBXmlManager.formatXmlString(item[k]);
					tc.setCellvalue(item[k++]+getTag(tc.getCellvalue(),"input")+getTag(tc.getCellvalue(),"img") ); // get all footnotes and entities.
					al.set(j,tc);
				}
			tr.setTablecell(al);
			tablerow.set(i,tr);
		}
		return this;
	}
	
	
	private String getTag(String source, String tag) {
		if (source == null || source.length() ==0) return "";
		StringBuffer result =new StringBuffer("");
		String stag = "<"+tag;
		String etag = "</"+tag+">";
		int found = source.indexOf(stag);
		if (found == -1) return result.toString();
		source = source.substring(found);
		if (source.indexOf(etag)>-1)
			result.append(source.substring(0,source.indexOf(etag)+etag.length()));
		else {
			etag = "/>";
			if (source.indexOf(etag)>-1)
				result.append(source.substring(0,source.indexOf(etag)+etag.length()));
		}	
		source = source.substring(source.indexOf(etag)+etag.length());
		if (source.indexOf(stag) >-1) 
			result.append(getTag(source,tag));// repeat this if there elements.
		return result.toString();
	}
		
	public TableCell getCell(int row, int col) {
		if ((tablerow == null) || ((TableRow)tablerow.get(row)).getTablecell().size()<=col) return null;
		return (TableCell)((TableRow)tablerow.get(row)).getTablecell().get(col);
	}
	
	public void setCell(int row,int col, TableCell value) {
		if (tablerow == null) return;
		TableRow al = (TableRow)tablerow.get(row);
		al.getTablecell().set(col, value);
		tablerow.set(row,al);
	}
	
	public void setCellValues(TableDetails tb, String cellValuesinCSV, String mergedCellsInf) {
		String[] cellValues = cellValuesinCSV.split(",");
		String[] mergedCells = mergedCellsInf.split(",");
		String cspan = null;
		int row = 0;
		int col = 0;
		int prevRow = 0;
		int prevCspan = 0;
		for (int cellNo = 0; cellNo < cellValues.length; cellNo++) {
			String cellValue = cellValues[cellNo];
			String mergedCell = mergedCells[cellNo];
			if (mergedCell.contains("$")) {
				cspan = mergedCell.substring((mergedCell.lastIndexOf("-") + 1));
				row = Integer.parseInt(mergedCell.substring((mergedCell
						.indexOf("$") + 1), mergedCell.indexOf("-"))) - 1;
				col = Integer.parseInt(mergedCell.substring((mergedCell
						.indexOf("-") + 1), mergedCell.lastIndexOf("-"))) - 2;
				cellNo = cellNo + Integer.parseInt(cspan) - 1;
				if(prevRow==row && prevCspan!=0)
					col = col - prevCspan ;
				else 
					prevCspan = 0;
				TableCell tc = tb.getCell(row, col);
				prevCspan += (Integer.parseInt(cspan) - 1);
				prevRow = row;
				cellValue= DBXmlManager.formatXmlString(cellValue);
				if(cellValue!=null && cellValue.indexOf("ï¿½")==-1){
					cellValue = StringUtility.fixSpecialCharforWeb(cellValue);//Added by Anil for special character issue in merged cells on 12.06.12
					cellValue = CheckString.convertToXHTMLx(cellValue);
				}					
				cellValue = CheckString.convertStyleIndents(cellValue);
				if (cspan != null) {
					tb.registerCellAttribute(row, col, "colspan", cspan);
					tc.setCspan(cspan);
				}
				cellValue = StringUtility.fixSpecialCharforWeb(cellValue);
				cellValue = StringUtility.fixSpecialAttributes(cellValue);
				tc.setCellvalue(cellValue);
				tb.setCell(row, col, tc);
			}
		}

	}
	
	public void registerCellAttribute(int row, int col, String name,
			String value) {
		if (tablerow == null)
			return;
		TableRow tr = (TableRow) tablerow.get(row);
		ArrayList tds = (ArrayList) tr.getTablecell();
		if (tds.size() <= col)
			return;
		TableCell tc = (TableCell) tds.get(col);
		ArrayList al = tc.getAttribute();
		if (al == null || al.size() == 0) {
			al = new ArrayList();
			if (name.equals("colspan")
					&& (value == null || value.equals("") || value.equals("1")))
				return;
			al.add(new FieldAttribute(name, value));
			if (name.equals("colspan")) {
				for (int i = 1; i < CheckString.getInt(value); i++) {
					TableCell nTc = (TableCell) tds.get(col + 1);
					ArrayList nAl = new ArrayList();
					nAl = nTc.getAttribute();
					if (nAl != null && nAl.size() > 0) {
						for (int nCell = 0; nCell < nAl.size(); nCell++) {
							FieldAttribute nFa = (FieldAttribute) nAl
									.get(nCell);
							if (nFa.getName().equals(name)) {
								int colSpSize = Integer
										.parseInt(nFa.getValue());
								if (colSpSize != 1) {
									nTc.setCspan(colSpSize - 1 + "");
									nFa.setValue(colSpSize - 1 + "");
								} else
									tds.remove(col + 1);
							}// if getName
						}// for
					} else
						tds.remove(col + 1);
				}// for
			}// if name colspan
		} else {
			boolean notFound = true;
			for (int i = 0; i < al.size(); i++) {
				FieldAttribute fa = (FieldAttribute) al.get(i);
				if (fa.getName().equals(name)) {
					if (fa.getValue().equals(value)) {
						notFound = false;
						break;
					}
					int oldSize = Integer.parseInt((fa.getValue() == null || fa
							.getValue().equals("")) ? "1" : fa.getValue());
					int newSize = Integer.parseInt(value == null
							|| value.equals("") ? "1" : value);
					fa.setValue(value);
					if (name.equals("colspan")) {
						if (oldSize < newSize) {
							for (int j = oldSize; j < newSize; j++) {
								TableCell nTc = (TableCell) tds.get(col + 1);
								ArrayList nAl = new ArrayList();
								nAl = nTc.getAttribute();
								if (nAl != null && nAl.size() > 0) {
									for (int nCell = 0; nCell < nAl.size(); nCell++) {
										FieldAttribute nFa = (FieldAttribute) nAl
												.get(nCell);
										if (nFa.getName().equals(name)) {
											int colSpSize = Integer
													.parseInt(nFa.getValue());
											if (colSpSize != 1) {
												nFa.setValue(colSpSize - 1 + "");
												nTc.setCspan(colSpSize - 1 + "");
											} else {
												tds.remove(col + 1);
											}// end of inner if
										} // end of if
									} // end of for
								} else {
									tds.remove(col + 1);
								} // end of if
							}// end for oldSize
						} else {
							int j = newSize;
							for (int tdsSize = tds.size(); j < oldSize; j++) {
								if (col + 1 < tdsSize)
									tds.add(j, new TableCell());
								else
									tds.add(new TableCell());
							}
						}
					}// end of -- if(name.equals("colspan")){
					al.set(i, fa);
					if (name.equals("colspan") && fa.getValue().equals("1")) {
						al.remove(i);
					}
					notFound = false;
					break;
				}
			}

			if (notFound) {
				al.add(new FieldAttribute(name, value));
			}
		}
		tc.setAttributes(al); // set the attributes to the cell.
		tds.set(col, tc); // set the cell to row.
		tr.setTablecell(tds);
		tablerow.set(row, tr); // set the rows to the table
	}
	
	public void registerStyleAttributes(String[] styles) {
		if ((styles == null || styles.length ==0) ) {
		} else {
			for(int i=0;(i<rows&&i<styles.length);i++) {
					if (!styles[i].equals("default"))
						registerRowAttribute(i,"condition",styles[i]);
					else
						unRegisterRowAttribute(i,"condition",styles[i]); 
			}				 
		}		
	}
	
	public void registerRowAttribute(int row, String name, String value ) {
		if (tablerow == null) return;
		TableRow tr = (TableRow)tablerow.get(row);
		ArrayList al = tr.getFieldattribute();
		if (al == null || al.size()==0) {
			al = new ArrayList();
			al.add(new FieldAttribute(name,value));
		} else {
			boolean notFound = true;
			for(int i=0;i<al.size();i++) {
				FieldAttribute fa = (FieldAttribute) al.get(i);
				if (fa.getName().equals(name)) {
					fa.setValue(value);
					al.set(i,fa);
					notFound = false;
					break;
				}			
			}
			if (notFound) {
				al.add(new FieldAttribute(name,value));
			}
		}		
		tr.setFieldattribute(al);   // set the attribute collection.
		tablerow.set(row,tr); 		// set the row into table.	
	}
	
	public void unRegisterRowAttribute(int row, String name, String value ) {
		if (tablerow == null) return;
		TableRow tr = (TableRow)tablerow.get(row);
		ArrayList al = tr.getFieldattribute();
		if (al == null || al.size()==0) {
			return;
		} else {
			for(int i=0;i<al.size();i++) {
				FieldAttribute fa = (FieldAttribute) al.get(i);
				if (fa.getName().equals(name)) {
					al.remove(i);
					break;
				}			
			}
			tr.setFieldattribute(al);   // set the attribute collection.
			tablerow.set(row,tr); 		// set the row into table.	
		}		

	}
	
	/*public String getXHtmlTableForceTo100(String colIndex) {
		StringBuffer sb =  new StringBuffer(TABLE);
		
		sb.append(TBODY);		
		if (tablerow == null && rows == 0 && cols == 0 ) return "";
		if (tablerow == null && rows >0 && cols >0 ) {
			for(int i=0;i<rows;i++) {
				sb.append(TR);
				for(int j=0;j< cols;j++) {
					sb.append(TD);
					sb.append("");
					sb.append(eTD);
				}
				sb.append(eTR);
			}
		} else {	 
			for(int i=0;i<tablerow.size();i++) {
				TableRow tr = (TableRow)tablerow.get(i);				
				sb.append(hTR);
				sb.append(tr.getAttributes());
				
				StringBuffer addRowAttrib = new StringBuffer();
				boolean rowCheck = false;
				if(getNewRowAttribute() != null && getNewRowAttribute().containsKey(i)) {
					sb.append(" "+getNewRowAttribute().get(i));
				} else if(getNewRowAttribute() != null && (!getNewRowAttribute().containsKey(i))) {
					for(int rowAtt = 0; rowAtt < getNewAddedRowAttributes().size(); rowAtt++) {
						addRowAttrib.append(getNewAddedRowAttributes().get(rowAtt)+"=\"\" ");
						rowCheck = true;
					}
					if(rowCheck) {
						addRowAttrib.deleteCharAt(addRowAttrib.lastIndexOf(" "));
						sb.append(addRowAttrib);
					}
				}
				
				sb.append(cTAG);
				ArrayList al = tr.getTablecell();
				for(int j=0;j<al.size();j++) {
					TableCell td = (TableCell)al.get(j); 
					sb.append(hTD);
					if(colIndex != null && colIndex.equals(Integer.toString(j))){
						sb.append(" "+"fcol=\"true\"");
					}
				
						sb.append(td.getAttributes());
						StringBuffer addAttribute = new StringBuffer();
						boolean entryCheck = false;
						if(getNewAttribute() != null && getNewAttribute().containsKey(""+i+","+j))
							sb.append(" "+getNewAttribute().get(""+i+","+j));
						else if(getNewAttribute() != null && (!getNewAttribute().containsKey(""+i+","+j))) {
							for(int k = 0; k < getNewAddedColAttribute().size(); k++) {
								addAttribute.append(getNewAddedColAttribute().get(k)+"=\"\" ");
								entryCheck = true;
							}
							if(entryCheck) {
								addAttribute.deleteCharAt(addAttribute.lastIndexOf(" "));
								sb.append(" "+addAttribute);
							}
						}
				
					sb.append(cTAG);
				
					sb.append(CheckString.convertXHtmlToDocBook(CheckString.removeCRLF(CheckString.convertStyleIndents(CheckString.convertToXHTMLx(StringUtility.fixSpecialCharforWeb(td.getCellvalue())))).replaceAll("<u style=\"TEXT-DECORATION: underline\">","<u style=\"text-decoration: underline\">"))); 
					sb.append(eTD);
				}
				sb.append(eTR);
			}
		}	
		sb.append(eTBODY+eTABLE);
		String value = StringUtility.fixSpecialAttributes(sb.toString());
		return value;		
	}
	
	private String getSelRowColAttributeFromXml(String colAttributeXml, String attr){
		String attrVal = "";
		if(colAttributeXml != null && !colAttributeXml.trim().equals("") && !attr.equals("")){
			try{
				Document colAttributeXmlDoc = DBXmlManager.getDOMFromXMLString("<table>"+colAttributeXml+"</table>");			
				Element rootElement = colAttributeXmlDoc.getDocumentElement();
				NodeList tableAttributeNode = rootElement.getElementsByTagName("tableattribute");	
				Element nodeElement = (Element) tableAttributeNode.item(0);		
				NamedNodeMap nodeElementAttributes = nodeElement.getAttributes();
				if(attr.equals("rowattr")){
					for(int attribute=0; attribute < nodeElementAttributes.getLength(); attribute++){
						if(nodeElementAttributes.item(attribute).getNodeName().trim().equals("selrows")){
							attrVal = nodeElementAttributes.item(attribute).getNodeValue();
							break;
						
						}
					}
				}else if(attr.equals("colattr")){
					NodeList colAttributeTags = nodeElement.getElementsByTagName("colattribute");
					String selColAttrVal = "";
					for(int index=0; index < colAttributeTags.getLength(); index++){
						Element colAttrNodeElement = (Element) colAttributeTags.item(index);		
						NamedNodeMap colAttrNodeElementAttributes = colAttrNodeElement.getAttributes();
						for(int attribute=0; attribute < colAttrNodeElementAttributes.getLength(); attribute++){
							if(colAttrNodeElementAttributes.item(attribute).getNodeName().trim().equals("selcol")){
								String selColValue = colAttrNodeElementAttributes.item(attribute).getNodeValue().trim();
								if(selColValue != null && selColValue.equals("1")){
									if(selColAttrVal.equals("")){
										selColAttrVal = Integer.toString(index);
									}else{
										selColAttrVal += "," + Integer.toString(index);
									}
								}
							}
						}
					}
					if(selColAttrVal != null && !selColAttrVal.equals("")){
						attrVal = selColAttrVal;
					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		return attrVal;
	}
	
	private int getNoOfColAttributesFromXml(String colAttributeXml){
		int noOfColAttributes = 0;
		if(colAttributeXml != null && !colAttributeXml.trim().equals("")){
			try{
				Document colAttributeXmlDoc = DBXmlManager.getDOMFromXMLString("<table>"+colAttributeXml+"</table>");			
				Element rootElement = colAttributeXmlDoc.getDocumentElement();
				NodeList tableAttributeNode = rootElement.getElementsByTagName("tableattribute");	
				Element nodeElement = (Element) tableAttributeNode.item(0);		
				NamedNodeMap nodeElementAttributes = nodeElement.getAttributes();
				NodeList colAttributeTags = nodeElement.getElementsByTagName("colattribute");
				String selColAttrVal = "";
				noOfColAttributes = colAttributeTags.getLength();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		return noOfColAttributes;
	}
	
	private boolean getRtypeFromXml(String colAttributeXml){
		boolean rTypeAttrFlag = false;
		if(colAttributeXml != null && !colAttributeXml.trim().equals("")){
			try{
				Document colAttributeXmlDoc = DBXmlManager.getDOMFromXMLString("<table>"+colAttributeXml+"</table>");			
				Element rootElement = colAttributeXmlDoc.getDocumentElement();
				NodeList tableAttributeNode = rootElement.getElementsByTagName("tableattribute");	
				Element nodeElement = (Element) tableAttributeNode.item(0);		
				NamedNodeMap nodeElementAttributes = nodeElement.getAttributes();
				for(int attribute=0; attribute < nodeElementAttributes.getLength(); attribute++){
					if(nodeElementAttributes.item(attribute).getNodeName().trim().equals("rtype")){
						if(nodeElementAttributes.item(attribute).getNodeValue().trim().equals("1")){
							rTypeAttrFlag = true;
							break;
						}
					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		return rTypeAttrFlag;
	}
	
	public String setTable(String value,Hashtable hsXBRLTagedCells, String colAttributeXml) {
		
		System.out.println("======================== colAttributeXml inside setTable is =============================");
	
		int noOfColAttributes = getNoOfColAttributesFromXml(colAttributeXml);
		boolean rTypeFlag = getRtypeFromXml(colAttributeXml);
		String selColAttr = "", selRowAttr = "";
		boolean selColAttrFlag = false, selRowAttrFlag = false;
		String[] selectedColArr = null, selectedRowArr = null;
		
		if(noOfColAttributes != 0 && rTypeFlag){
			selRowAttr = getSelRowColAttributeFromXml(colAttributeXml, "rowattr");
		
			selRowAttr = StringUtility.getNumberSequenceFromRange(selRowAttr);
		
			if(selRowAttr != null && !selRowAttr.trim().equals("")){
				selectedRowArr = selRowAttr.split(",");
				selRowAttrFlag = true;
			}
			selColAttr = getSelRowColAttributeFromXml(colAttributeXml, "colattr");
			if(selColAttr != null && !selColAttr.trim().equals("")){
				selectedColArr = selColAttr.split(",");
				selColAttrFlag = true;
			}
		
		
		}
		
		isSelColSet = false;
		String mergedCellsInfo = null;
		newAttribute = new HashMap();
		newRowAttribute = new HashMap();
		StringBuffer addAttribute = null;
		newAddedColAttribute = new ArrayList();
		try {
			if (value==null || value.length()==0) return mergedCellsInfo;
			String xmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE book SYSTEM 'http://dev.datacom-usa.com/dtd//dcidocbook.dtd'>";
		
		
			System.out.println("Valur for xmk is "+value);
			value = CheckString.replaceStringX(value,"value="," value=");
			value = CheckString.replaceStringX(value,"title="," title=");
			value = CheckString.replaceStringX(value,"idref="," idref=");
			value = CheckString.replaceStringX(value,"src="," src=");
			value = CheckString.replaceStringX(value,"order="," order=");
		
			Document doc = DBXmlManager.getDOMFromXMLString(xmlHeader+"<body>"+StringUtility.substituteEntityName(value)+"</body>");			
			Element rootElement = doc.getDocumentElement();
			NodeList ntable = rootElement.getElementsByTagName("table");			
			Element tble = (Element) ntable.item(0);
			String renderType = "";
			
			NamedNodeMap tableAttributes = tble.getAttributes();	
			ArrayList knownAttributes = new ArrayList(Arrays.asList(new String []{"primary","rows","cols","tabletype","render","style","datatype","graphics"}));
			for(int attribute=0;attribute<tableAttributes.getLength();attribute++){
				if(tableAttributes.item(attribute).getNodeName().trim().equals("render")){
					renderType = tableAttributes.item(attribute).getNodeValue().trim();
				}
				if(!knownAttributes.contains(tableAttributes.item(attribute).getNodeName())){
					newTableAttributes.add( tableAttributes.item(attribute).getNodeName()+"=\""+tableAttributes.item(attribute).getNodeValue()+"\"");
				}
			}
		
		
			if(renderType != null && !renderType.equals("") && !(Table.isRenderTypePartial(renderType, null, false, false)) ){
				selRowAttrFlag = false;
				selColAttrFlag = false;
			}
			NodeList nl = rootElement.getElementsByTagName("row");
			setTotalrows(CheckString.getInt(tble.getAttribute("rows")));
			setTotalcols(CheckString.getInt(tble.getAttribute("cols")));
			setTableType(tble.getAttribute("tabletype"));
			if (getTotalrows()==0 || getTotalrows() != nl.getLength()){
				if(nl!=null)
					setTotalrows(nl.getLength());
			}
			if (getTotalcols()==0){
				if(nl!=null){
					Element nd = (Element)nl.item(0);
					if(nd!=null)
						setTotalcols(nd.getElementsByTagName("entry").getLength());
				}
			}
			if(nl!=null)
				setRows(nl.getLength());
			selrow = new String[getTotalrows()];
			tstyles = new String[getTotalrows()];						
			tablerow = new ArrayList();
			cellValue = new String[getTotalrows()*getTotalcols()];
		
		
			String strRowCol = null;
			boolean rowCheck = false;
			selcol = new String[getTotalcols()];	
			for(int i=0;i<getTotalrows();i++) {				
				Element tr = (Element) nl.item(i);
				
				StringBuffer newRowAttrib = new StringBuffer();
				NamedNodeMap rowAttributes = tr.getAttributes();
				ArrayList rowAttrib = new ArrayList(Arrays.asList(new String[]{"condition","selrow"}));
				for(int attribute=0;attribute<rowAttributes.getLength();attribute++){
					if(!rowAttrib.contains(rowAttributes.item(attribute).getNodeName())){
						newRowAttrib.append(rowAttributes.item(attribute).getNodeName()+"=\""+rowAttributes.item(attribute).getNodeValue()+"\" ");
						rowCheck = true;
						if(i == 0)
							newAddedRowAttributes.add(rowAttributes.item(attribute).getNodeName());							
					}
				}
				if(rowCheck) {
					newRowAttrib.deleteCharAt(newRowAttrib.lastIndexOf(" "));
					newRowAttribute.put(i, newRowAttrib);
				}
				

				NodeList nltd = tr.getElementsByTagName("entry");
				if(nltd!=null)
					setCols(nltd.getLength());																	

				ArrayList alrow = new ArrayList();
				int columnNo = 0;
				for(int j=0;(nltd!=null && j<nltd.getLength());j++) {					
					strRowCol=""+i+","+j;
					if(j<nltd.getLength()){
						Element elem = (Element)nltd.item(j);
						addAttribute = new StringBuffer();
						String[] attributs = new String[elem.getAttributes().getLength()];
						boolean check = false;
						for(int k =0;k < elem.getAttributes().getLength(); k++) {
							if(!(elem.getAttributes().item(k).toString().substring(0, 7).equals("colspan") || elem.getAttributes().item(k).toString().substring(0, 6).equals("selcol"))){
								addAttribute.append(elem.getAttributes().item(k).toString()+" ");
								check = true;
								if(i == 0 && j == 0) {
									attributs = elem.getAttributes().item(k).toString().split("=");
									if(!newAddedColAttribute.contains(attributs[0].toString()))
										newAddedColAttribute.add(attributs[0].toString());
								}
							}							
						}
						if(check == true) {
							addAttribute.deleteCharAt(addAttribute.lastIndexOf(" "));
							newAttribute.put(strRowCol, addAttribute.toString());
						}
					
						String strCellValue = StringUtility.fixSpecialCharforWeb(CheckString.converStyleIndentReverse(CheckString.xdocBookToHtml((DBXmlManager.getNodeValue( nltd.item(j))).replace("<para/>", "").replaceAll("<superscript\\s*\\/>", "<superscript></superscript>").replaceAll("<subscript\\s*\\/>", "<subscript></subscript>"))));
						strCellValue = CheckString.xmlToText(strCellValue);
						
						cellValue[(i*getTotalcols())+j+columnNo] = strCellValue;
						if(((i*getTotalcols())+j+columnNo)==0)
							mergedCellsInfo = " ";
						else
							mergedCellsInfo = mergedCellsInfo + "," +" ";
					
						
						TableCell tc = new TableCell(strCellValue,DBXmlManager.getElementAttributes(elem));					
						if(elem.getAttribute("colspan")!= null){
							cspan = elem.getAttribute("colspan");
							if(!cspan.equals("")){
								cellValue[(i*getTotalcols())+j+columnNo] = strCellValue;
								if(((i*getTotalcols())+j+columnNo)==0)
									mergedCellsInfo = "$"+(i+1)+"-"+(j+columnNo+2)+"-"+cspan;
								else
									mergedCellsInfo = mergedCellsInfo + "," + "$"+(i+1)+"-"+(j+columnNo+2)+"-"+cspan;
							
								columnNo = columnNo + Integer.parseInt(cspan)-1;
							}
							tc.setCspan(cspan);						
						}
						if(hsXBRLTagedCells !=null && !hsXBRLTagedCells.isEmpty() && hsXBRLTagedCells.containsKey(strRowCol)){
							tc.setIsxbrlTaged("y");
							tc.setXbrlTag(hsXBRLTagedCells.get(strRowCol).toString());
						}
					
					
						if(nltd.getLength()==(selcol.length)){
							if(!selColAttrFlag){
								if(elem.getAttribute("selcol").equals("1"))
									selcol[j]=j+"";
								else
									selcol[j]="";
							}else if(selColAttrFlag){
							
							
								boolean isColSelected = false;
								for(int selectedColArrIndex = 0; selectedColArrIndex < selectedColArr.length; selectedColArrIndex++ ){
									if( j == ( Integer.parseInt(selectedColArr[selectedColArrIndex]) ) ){
										isColSelected = true;
										break;
									}
								
								
								}
								if( isColSelected ){
									selcol[j]=j+"";
								}else{
									selcol[j]="";
								}
							}
							isSelColSet = true;
						}
						if( i==(getTotalrows()-1) && isSelColSet == false ){
							if(!selColAttrFlag){
								if(elem.getAttribute("selcol").equals("1"))
									selcol[j]=j+"";
								else
									selcol[j]="";
							}else if(selColAttrFlag){
							
							
								boolean isColSelected = false;
								for(int selectedColArrIndex = 0; selectedColArrIndex < selectedColArr.length; selectedColArrIndex++ ){
									if( j == ( Integer.parseInt(selectedColArr[selectedColArrIndex]) ) ){
										isColSelected = true;
										break;
									}
								
								
								}
								if( isColSelected ){
									selcol[j]=j+"";
								}else{
									selcol[j]="";
								}
							}
						}

						alrow.add(tc);
					} else {
						cellValue[(i*getTotalcols())+j+columnNo] = "";
						if(((i*getTotalcols())+j+columnNo)==0)
							mergedCellsInfo = " ";
						else
							mergedCellsInfo = mergedCellsInfo +"," + " ";
					
					}
				}
				
				tablerow.add(new TableRow(alrow,DBXmlManager.getElementAttributes(tr)));								
			
				if(!selRowAttrFlag){
					if(tr.getAttribute("selrow").equals("1"))
						selrow[i]=i+"";
					else
						selrow[i]="";
				}else if(selRowAttrFlag){
				
				
					boolean isRowSelected = false;
					for(int selectedRowArrIndex = 0; selectedRowArrIndex < selectedRowArr.length; selectedRowArrIndex++ ){
						if( i == ( Integer.parseInt(selectedRowArr[selectedRowArrIndex]) ) ){
							isRowSelected = true;
							break;
						}
					
					
					}
					if( isRowSelected ){
						selrow[i]=i+"";
					}else{
						selrow[i]="";
					}
				}
				if(tr.getAttribute("condition")!=null){				
					tstyles[i]=tr.getAttribute("condition");					
				} else
					tstyles[i]="";
			}
			setNewAttribute(newAttribute);
			setNewAddedColAttribute(newAddedColAttribute);
			nl = null;
			nl = rootElement.getElementsByTagName("graphic");
			if (nl != null && nl.getLength() > 0 ) {
				Element elem = (Element)nl.item(0);
				graphic = new Graphic();
				graphic.setFileref(elem.getAttribute("fileref"));
				graphic.setCharttype(elem.getAttribute("charttype"));
				graphic.setHeight(elem.getAttribute("height"));
				graphic.setWidth(elem.getAttribute("width"));
			}
			nl = null;
			nl = rootElement.getElementsByTagName("table");
			if (nl != null && nl.getLength() > 0 ) {
				Element elem = (Element)nl.item(0);
				render = elem.getAttribute("render");
				datatype = elem.getAttribute("datatype");
				primary = elem.getAttribute("primary");
				style = elem.getAttribute("style");
			}
			return mergedCellsInfo;
		} catch(Exception e) {
			e.printStackTrace();
			return mergedCellsInfo;
		}
	}
	
	public String setTableForForce200(String value) {
		String mergedCellsInfo = null;
		try {
			if (value==null || value.length()==0) return mergedCellsInfo;
			String xmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE book SYSTEM 'http://dev.datacom-usa.com/dtd//dcidocbook.dtd' []>";
		
			value = CheckString.replaceStringX(value,"value="," value=");
			value = CheckString.replaceStringX(value,"title="," title=");
			value = CheckString.replaceStringX(value,"idref="," idref=");
			value = CheckString.replaceStringX(value,"src="," src=");
			value = CheckString.replaceStringX(value,"order="," order=");
		
			Document doc = DBXmlManager.getDOMFromXMLString(xmlHeader+"<body>"+StringUtility.substituteEntityName(value)+"</body>");			
			Element rootElement = doc.getDocumentElement();
			NodeList ntable = rootElement.getElementsByTagName("table");			
			Element tble = (Element) ntable.item(0);
			
			NodeList nl = rootElement.getElementsByTagName("row");
			setTotalrows(CheckString.getInt(tble.getAttribute("rows")));
			setTotalcols(CheckString.getInt(tble.getAttribute("cols")));
			
			if (getTotalrows()==0 || getTotalrows() != nl.getLength()){
				if(nl!=null)
					setTotalrows(nl.getLength());
			}
			if (getTotalcols()==0){
				if(nl!=null){
					Element nd = (Element)nl.item(0);
					if(nd!=null)
						setTotalcols(nd.getElementsByTagName("entry").getLength());
					
				}
			}
			
			NodeList nl = rootElement.getElementsByTagName("row");
			if (nl!=null && nl.getLength() != 0){
				setTotalrows(nl.getLength());
				Element nd = (Element)nl.item(0);
				setTotalcols(nd.getElementsByTagName("entry").getLength());
			}
			if(nl!=null)
				setRows(nl.getLength());
			
		
			tstyles = new String[getTotalrows()];						
			tablerow = new ArrayList();
			cellValue = new String[getTotalrows()*getTotalcols()];
		
		
			String strRowCol = null;
			boolean rowCheck = false;
			selcol = new String[getTotalcols()];	
			for(int i=0;i<getTotalrows();i++) {				
				Element tr = (Element) nl.item(i);
				
				tr.getAttribute("primary");				
				NodeList nltd = tr.getElementsByTagName("entry");
				if(nltd!=null)
					setCols(nltd.getLength());																	

				ArrayList alrow = new ArrayList();
				int columnNo = 0;
				for(int j=0;(nltd!=null && j<nltd.getLength());j++) {					
					strRowCol=""+i+","+j;
					if(j<nltd.getLength()){
						Element elem = (Element)nltd.item(j);
						String strCellValue = StringUtility.fixSpecialCharforWeb(CheckString.converStyleIndentReverse(CheckString.xdocBookToHtml((DBXmlManager.getNodeValue( nltd.item(j))).replace("<para/>", ""))));					
						strCellValue = CheckString.xmlToText(strCellValue);
						
						cellValue[(i*getTotalcols())+j+columnNo] = strCellValue;
						if(((i*getTotalcols())+j+columnNo)==0)
							mergedCellsInfo = " ";
						else
							mergedCellsInfo = mergedCellsInfo + "," +" ";
					
						
						TableCell tc = new TableCell(strCellValue,DBXmlManager.getElementAttributes(elem));					
						if(elem.getAttribute("colspan")!= null){
							cspan = elem.getAttribute("colspan");
							if(!cspan.equals("")){
								cellValue[(i*getTotalcols())+j+columnNo] = strCellValue;
								if(((i*getTotalcols())+j+columnNo)==0)
									mergedCellsInfo = "$"+(i+1)+"-"+(j+columnNo+1)+"-"+cspan;
								else
									mergedCellsInfo = mergedCellsInfo + "," + "$"+(i+1)+"-"+(j+columnNo+1)+"-"+cspan;
							
								columnNo = columnNo + Integer.parseInt(cspan)-1;
							}
							tc.setCspan(cspan);						
						}
					
						alrow.add(tc);
					} else {
						cellValue[(i*getTotalcols())+j+columnNo] = "";
						if(((i*getTotalcols())+j+columnNo)==0)
							mergedCellsInfo = " ";
						else
							mergedCellsInfo = mergedCellsInfo +"," + " ";
					
					}
				}
				
				tablerow.add(new TableRow(alrow,DBXmlManager.getElementAttributes(tr)));								
			
				if(tr.getAttribute("condition")!=null){				
					tstyles[i]=tr.getAttribute("condition");					
				} else
					tstyles[i]="";
			}
			nl = null;
			return mergedCellsInfo;
		} catch(Exception e) {
			e.printStackTrace();
			return mergedCellsInfo;
		}
	}
	
	public String getForceTo100SelectedColIndex(String value) {
		String colIndex = "";
		try {
			if (value==null || value.length()==0) return colIndex;
			String xmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE book SYSTEM 'http://dev.datacom-usa.com/dtd//dcidocbook.dtd' []>";
		
			value = CheckString.replaceStringX(value,"value="," value=");
			value = CheckString.replaceStringX(value,"title="," title=");
			value = CheckString.replaceStringX(value,"idref="," idref=");
			value = CheckString.replaceStringX(value,"src="," src=");
			value = CheckString.replaceStringX(value,"order="," order=");
		
			Document doc = DBXmlManager.getDOMFromXMLString(xmlHeader+"<body>"+StringUtility.substituteEntityName(value)+"</body>");			
			Element rootElement = doc.getDocumentElement();
			NodeList ntable = rootElement.getElementsByTagName("table");			
			NodeList nl = rootElement.getElementsByTagName("row");
			if( nl != null ){
				for(int i=0;i<nl.getLength();i++) {				
					Element tr = (Element) nl.item(i);
					NodeList nltd = tr.getElementsByTagName("entry");
					for(int j=0;(nltd!=null && j<nltd.getLength());j++) {					
						if(j<nltd.getLength()){
							Element elem = (Element)nltd.item(j);
							for(int k =0;k < elem.getAttributes().getLength(); k++) {
								if(elem.getAttributes().item(k).toString().indexOf("fcol=") != -1){
									colIndex = Integer.toString(j);
									break;
								}
								if( ( elem.getAttributes().item(k).toString().substring(0, 7).equals("fcol") ) ){
									
								}							
							}
						} 
					}
				}
			}
			nl = null;
			return colIndex;
		} catch(Exception e) {
			e.printStackTrace();
			return colIndex;
		}
	}
	
	
	 * set the content, take the old footnoteref and entity references and add it to the new content.
	 
	public TableBean setTableValue(int row, int col, String gridValue) {
		
		String item[] = gridValue.split(",");
		int k=0;
		if(item == null) return this;

		for(int i=0;i<row;i++) {			
			TableRow tr = (TableRow) tablerow.get(i);
				ArrayList al = (ArrayList)tr.getTablecell();				
				for(int j=0;j<al.size();j++) {
					int cellColSPan = 0;
					TableCell tc = (TableCell)al.get(j);
					  String cSpanStr = tc.getCspan();
					  if(cSpanStr != null && !cSpanStr.equals(""))
						  cellColSPan = Integer.parseInt(cSpanStr);	
					 
					
					  item[k] = DBXmlManager.formatXmlString(item[k]);
					tc.setCellvalue(item[k++]+getTag(tc.getCellvalue(),"input")+getTag(tc.getCellvalue(),"img") );
					al.set(j,tc);
				}
			tr.setTablecell(al);
			tablerow.set(i,tr);
		}
		return this;
	}
	
	private String getTag(String source, String tag) {
		if (source == null || source.length() ==0) return "";
	
	
		StringBuffer result =new StringBuffer("");
		String stag = "<"+tag;
		String etag = "</"+tag+">";
		int found = source.indexOf(stag);
		if (found == -1) return result.toString();
			source = source.substring(found);
		if (source.indexOf(etag)>-1)
			result.append(source.substring(0,source.indexOf(etag)+etag.length()));
		else {
			etag = "/>";
			if (source.indexOf(etag)>-1)
				result.append(source.substring(0,source.indexOf(etag)+etag.length()));
		}	
		source = source.substring(source.indexOf(etag)+etag.length());
		if (source.indexOf(stag) >-1) 
			result.append(getTag(source,tag));// repeat this if there elements.
		return result.toString();
	}
		
	public TableCell getCell(int row, int col) {
	
		if ((tablerow == null) || ((TableRow)tablerow.get(row)).getTablecell().size()<=col) return null;
		return (TableCell)((TableRow)tablerow.get(row)).getTablecell().get(col);
	}
	
	public void setCell(int row,int col, TableCell value) {
		if (tablerow == null) return;
		TableRow al = (TableRow)tablerow.get(row);
		al.getTablecell().set(col, value);
		tablerow.set(row,al);
	}
	
	public void registerCellAttribute(int row, int col, String name,
			String value) {
		if (tablerow == null)
			return;
		TableRow tr = (TableRow) tablerow.get(row);
		ArrayList tds = (ArrayList) tr.getTablecell();
	

		if (tds.size() <= col)
			return;
		TableCell tc = (TableCell) tds.get(col);
		ArrayList al = tc.getAttribute();
		if (al == null || al.size() == 0) {
			al = new ArrayList();
			if (name.equals("colspan")
					&& (value == null || value.equals("") || value.equals("1")))
				return;
			al.add(new FieldAttribute(name, value));
			if (name.equals("colspan")) {
				for (int i = 1; i < CheckString.getInt(value); i++) {
					TableCell nTc = (TableCell) tds.get(col + 1);
					ArrayList nAl = new ArrayList();
					nAl = nTc.getAttribute();
					if (nAl != null && nAl.size() > 0) {
						for (int nCell = 0; nCell < nAl.size(); nCell++) {
							FieldAttribute nFa = (FieldAttribute) nAl
									.get(nCell);
							if (nFa.getName().equals(name)) {
								int colSpSize = Integer
										.parseInt(nFa.getValue());
								if (colSpSize != 1) {
									nTc.setCspan(colSpSize - 1 + "");
									nFa.setValue(colSpSize - 1 + "");
								} else
									tds.remove(col + 1);
							}// if getName
						}// for
					} else
						tds.remove(col + 1);
				}// for
			}// if name colspan
		} else {
			boolean notFound = true;
			for (int i = 0; i < al.size(); i++) {
				FieldAttribute fa = (FieldAttribute) al.get(i);
				if (fa.getName().equals(name)) {
					if (fa.getValue().equals(value)) {
						notFound = false;
						break;
					}
					int oldSize = Integer.parseInt((fa.getValue() == null || fa
							.getValue().equals("")) ? "1" : fa.getValue());
					int newSize = Integer.parseInt(value == null
							|| value.equals("") ? "1" : value);
					fa.setValue(value);
					if (name.equals("colspan")) {
						if (oldSize < newSize) {
							for (int j = oldSize; j < newSize; j++) {

								TableCell nTc = (TableCell) tds.get(col + 1);
								ArrayList nAl = new ArrayList();
								nAl = nTc.getAttribute();
								if (nAl != null && nAl.size() > 0) {
									for (int nCell = 0; nCell < nAl.size(); nCell++) {
										FieldAttribute nFa = (FieldAttribute) nAl
												.get(nCell);
										if (nFa.getName().equals(name)) {
											int colSpSize = Integer
													.parseInt(nFa.getValue());
											if (colSpSize != 1) {
												nFa.setValue(colSpSize - 1 + "");
											
												nTc.setCspan(colSpSize - 1 + "");
											} else {
												tds.remove(col + 1);
											}// end of inner if
										}
									}
								} else {
									tds.remove(col + 1);
								}
							}// end for oldSize
						} else {
							int j = newSize;
							for (int tdsSize = tds.size(); j < oldSize; j++) {
							
								if (col + 1 < tdsSize)
									tds.add(j, new TableCell());

							
								else
									tds.add(new TableCell());
							}
						}
					}// end of -- if(name.equals("colspan")){
					al.set(i, fa);
					if (name.equals("colspan") && fa.getValue().equals("1")) {
						al.remove(i);
					}
					notFound = false;
					break;
				}
			}

			if (notFound) {
				al.add(new FieldAttribute(name, value));
			}
		}
		tc.setAttributes(al);
		tds.set(col, tc);
		tr.setTablecell(tds);
		tablerow.set(row, tr);
	}
	
	public void registerRowAttribute(int row, String name, String value ) {
		if (tablerow == null) return;
		TableRow tr = (TableRow)tablerow.get(row);
		ArrayList al = tr.getFieldattribute();
		if (al == null || al.size()==0) {
			al = new ArrayList();
			al.add(new FieldAttribute(name,value));
		} else {
			boolean notFound = true;
			for(int i=0;i<al.size();i++) {
				FieldAttribute fa = (FieldAttribute) al.get(i);
				if (fa.getName().equals(name)) {
					fa.setValue(value);
					al.set(i,fa);
					notFound = false;
					break;
				}			
			}
			if (notFound) {
				al.add(new FieldAttribute(name,value));
			}
		}		
		tr.setFieldattribute(al);  
		tablerow.set(row,tr); 	
	}
	
	public void unRegisterRowAttribute(int row, String name, String value ) {
		if (tablerow == null) return;
		TableRow tr = (TableRow)tablerow.get(row);
		ArrayList al = tr.getFieldattribute();
		if (al == null || al.size()==0) {
			return;
		} else {
			for(int i=0;i<al.size();i++) {
				FieldAttribute fa = (FieldAttribute) al.get(i);
				if (fa.getName().equals(name)) {
					al.remove(i);
					break;
				}			
			}
			tr.setFieldattribute(al);  
			tablerow.set(row,tr); 	
		}		

	}
	
	public void registerSelAttributes(String[] selrow, String[]selcol ) {
		if ((selrow == null || selrow.length ==0) && (selcol == null || selcol.length ==0)) {
		
			for(int i=0;i<rows;i++) {
				for(int j=0;j<cols;j++) {
					registerCellAttribute(i,j,"selcol","0");							
				}			
			}
		
			for(int i=0;i<rows;i++) {
				this.registerRowAttribute(i,"selrow","0");
			}				
		} else {
		
		
		
			
			for(int i=0;i<rows;i++) {
				for(int j=0;j<cols;j++) {
				
					if(CheckString.lookup(j+"",selcol)) {
						registerCellAttribute(i,j,"selcol","1");
					}else {
						registerCellAttribute(i,j,"selcol","0");
					}
				}			
			}
		
			for(int i=0;i<rows;i++) {
				if(CheckString.lookup(i+"",selrow))
					registerRowAttribute(i,"selrow","1");
				else
					registerRowAttribute(i,"selrow","0");
			}				 
		}		
	}
	

	public void registerSelAttributesWithSelCol(String[] selrow, String[]selcol, TableBean tb) {
		if ((selrow == null || selrow.length ==0) && (selcol == null || selcol.length ==0)) {
		
			for(int i=0;i<rows;i++) {
				for(int j=0;j<cols;j++) {
					registerCellAttribute(i,j,"selcol","0");							
				}			
			}
		
			for(int i=0;i<rows;i++) {
				this.registerRowAttribute(i,"selrow","0");
			}				
		} else {
		
		
		
			
			for(int i=0;i<rows;i++) {
				for(int j=0, k=0;j<cols;j++,k++) {
					
				
					if(CheckString.lookup(k+"",selcol)) {
						registerCellAttribute(i,j,"selcol","1");
					}else {
						registerCellAttribute(i,j,"selcol","0");
					}
					if(tb.getCell(i,j)!=null && tb.getCell(i,j).getCspan()!=null){
						k = k + (Integer.parseInt(tb.getCell(i,j).getCspan())-1) ;
					}

				}			
			}
		
			for(int i=0;i<rows;i++) {
				if(CheckString.lookup(i+"",selrow))
					registerRowAttribute(i,"selrow","1");
				else
					registerRowAttribute(i,"selrow","0");
			}				 
		}		
	}

	
	public void registerStyleAttributes(String[] styles) {
		if ((styles == null || styles.length ==0) ) {
		} else {
			for(int i=0;(i<rows&&i<styles.length);i++) {
					if (!styles[i].equals("default"))
						registerRowAttribute(i,"condition",styles[i]);
					else
						unRegisterRowAttribute(i,"condition",styles[i]); 
			}				 
		}		
	}
	
	public String getGraphicXml() {
		String string = "";
		if (graphic != null)
			string = "<graphic fileref=\""+ graphic.getFileref() +"\" charttype=\""+graphic.getCharttype()+"\" width=\"144\" height=\"16\" />";
		return string;		
	}
	*//**
	 * added by karan for xbrlsection 
	 * @param row
	 * @param col
	 * @param name
	 *//*
	public void unRegisterCellAttribute(int row, int col, String name) {
		if (tablerow == null) return;
		TableRow tr = (TableRow)tablerow.get(row);
	
		ArrayList tds = (ArrayList)tr.getTablecell(); 
	
		TableCell tc = (TableCell)tds.get(col);
		ArrayList al = tc.getAttribute();		
		if (al == null || al.size()==0) {
			return;
		} else {
			for(int i=0;i<al.size();i++) {
				FieldAttribute fa = (FieldAttribute) al.get(i);
				if (fa.getName().equals(name)) {
					al.remove(i);
					break;
				}			
			}
			tc.setAttributes(al);	
		}		
	}
	
	public void getColumnWidthFromXML(Table table) throws Exception {
		if(table.getColAttributeXML()!=null && !table.getColAttributeXML().equals("")) {
			Document doc = DBXmlManager.getDOMFromXMLString("<table>"+table.getColAttributeXML()+"</table>");			
			Element rootElement = doc.getDocumentElement();
			NodeList tableAttributes = rootElement.getElementsByTagName("tableattribute");	
			Element nodeElement = (Element) tableAttributes.item(0);			
			NodeList colAttributes = nodeElement.getElementsByTagName("colattribute");
			
			float eachColWidth = 0;
			int totalColNo =0;
			float lastColWidth = 0;
			if(colAttributes.getLength()!=table.getFinalCol()) {
				String maxWidth = nodeElement.getAttribute("maxwidth");	
				table.setRenderWidth(nodeElement.getAttribute("renderwidth"));
				eachColWidth = Integer.parseInt(maxWidth)/table.getFinalCol();
				lastColWidth = eachColWidth + (Integer.parseInt(maxWidth) - (eachColWidth*table.getFinalCol()));
				totalColNo = table.getFinalCol();
			} else {
				totalColNo = colAttributes.getLength();
				table.setRenderWidth(nodeElement.getAttribute("renderwidth"));
			}			
			int colIndex = 0;				
			while(colIndex<totalColNo) {
				if(colAttributes.getLength()==table.getFinalCol()){
					eachColWidth  = Float.parseFloat(((Element)colAttributes.item(colIndex)).getAttribute("maxwidth"));
				}
				if((colIndex==totalColNo-1) && (colAttributes.getLength()!=table.getFinalCol())) {
					table.getEachColumnWidth().add(Float.toString(lastColWidth));
				} else {
					table.getEachColumnWidth().add(Float.toString(eachColWidth));
				}				
				
			
				String asciiChar_string = "";
				if (colIndex < 26) {
					char[] asciiCode = { (char) (65 + colIndex) };
					asciiChar_string = new String(asciiCode);
				} else {
					String temp_AsciiCode1 = "";
					String temp_AsciiCode2 = "";
					int count = 0;
					int count1 = 0;
					int count2 = 0;
	
					for (int j = 0; j < (colIndex - 26); j++) {
						if (count == 26 * count1) {
							char[] asciiCode1 = { (char) (65 + count1) };
							temp_AsciiCode1 = new String(asciiCode1);
							count1++;
							count2 = 0;
						}
						char[] asciiCode2 = { (char) (65 + count2) };
						temp_AsciiCode2 = new String(asciiCode2);
						count++;
						count2++;
						asciiChar_string = temp_AsciiCode1 + temp_AsciiCode2;
					}
				}
				table.getEachColumnName().add(asciiChar_string);
				colIndex++;						
			}
		
		}
	}
	
	public void getColumnWidthFromXMLWithDefaultColumnWidthCheck(Table table, String user, String clientId, String tableType) throws Exception {
		if(table.getColAttributeXML()!=null && !table.getColAttributeXML().equals("")) {
			Document doc = DBXmlManager.getDOMFromXMLString("<table>"+table.getColAttributeXML()+"</table>");			
			Element rootElement = doc.getDocumentElement();
			NodeList tableAttributes = rootElement.getElementsByTagName("tableattribute");	
			Element nodeElement = (Element) tableAttributes.item(0);			
			NodeList colAttributes = nodeElement.getElementsByTagName("colattribute");
			
			float eachColWidth = 0;
			int totalColNo =0;
			float lastColWidth = 0;
			if(colAttributes.getLength()!=table.getFinalCol()) {
				String defaultColumnWidth = new TableBDO().getDefaultColumnWidthForGivenNoOfColumns(user, clientId, tableType, table.getFinalCol());
				if(defaultColumnWidth != null && !defaultColumnWidth.trim().equals("")){
					doc = DBXmlManager.getDOMFromXMLString("<table>"+defaultColumnWidth+"</table>");			
					rootElement = doc.getDocumentElement();
					tableAttributes = rootElement.getElementsByTagName("tableattribute");	
					nodeElement = (Element) tableAttributes.item(0);			
					colAttributes = nodeElement.getElementsByTagName("colattribute");
					totalColNo = colAttributes.getLength();
					table.setRenderWidth(nodeElement.getAttribute("renderwidth"));
				}else{
					String maxWidth = nodeElement.getAttribute("maxwidth");	
					table.setRenderWidth(nodeElement.getAttribute("renderwidth"));
					eachColWidth = Integer.parseInt(maxWidth)/table.getFinalCol();
					lastColWidth = eachColWidth + (Integer.parseInt(maxWidth) - (eachColWidth*table.getFinalCol()));
					totalColNo = table.getFinalCol();
				}
			} else {
				totalColNo = colAttributes.getLength();
				table.setRenderWidth(nodeElement.getAttribute("renderwidth"));
			}			
			int colIndex = 0;				
			while(colIndex<totalColNo) {
				if(colAttributes.getLength()==table.getFinalCol()){
					eachColWidth  = Float.parseFloat(((Element)colAttributes.item(colIndex)).getAttribute("maxwidth"));
				}
				if((colIndex==totalColNo-1) && (colAttributes.getLength()!=table.getFinalCol())) {
					table.getEachColumnWidth().add(Float.toString(lastColWidth));
				} else {
					table.getEachColumnWidth().add(Float.toString(eachColWidth));
				}				
				
			
				String asciiChar_string = "";
				if (colIndex < 26) {
					char[] asciiCode = { (char) (65 + colIndex) };
					asciiChar_string = new String(asciiCode);
				} else {
					String temp_AsciiCode1 = "";
					String temp_AsciiCode2 = "";
					int count = 0;
					int count1 = 0;
					int count2 = 0;
	
					for (int j = 0; j < (colIndex - 26); j++) {
						if (count == 26 * count1) {
							char[] asciiCode1 = { (char) (65 + count1) };
							temp_AsciiCode1 = new String(asciiCode1);
							count1++;
							count2 = 0;
						}
						char[] asciiCode2 = { (char) (65 + count2) };
						temp_AsciiCode2 = new String(asciiCode2);
						count++;
						count2++;
						asciiChar_string = temp_AsciiCode1 + temp_AsciiCode2;
					}
				}
				table.getEachColumnName().add(asciiChar_string);
				colIndex++;						
			}
		
		}
	}
	
	public void getColumnPartialWidthFromXML(Table table) throws Exception {
		if(table.getColAttributeXML()!=null && !table.getColAttributeXML().equals("")) {
			Document doc = DBXmlManager.getDOMFromXMLString("<table>"+table.getColAttributeXML()+"</table>");			
			Element rootElement = doc.getDocumentElement();
			NodeList tableAttributes = rootElement.getElementsByTagName("tableattribute");	
			Element nodeElement = (Element) tableAttributes.item(0);			
			NodeList colAttributes = nodeElement.getElementsByTagName("colattribute");
			
			float eachColWidth = 0;
			int totalColNo =0;
			float lastColWidth = 0;
			if(colAttributes.getLength()!=table.getFinalCol()) {
				String maxWidth = nodeElement.getAttribute("maxwidth");	
				table.setRenderWidth(nodeElement.getAttribute("renderwidth"));
				eachColWidth = Integer.parseInt(maxWidth)/table.getFinalCol();
				lastColWidth = eachColWidth + (Integer.parseInt(maxWidth) - (eachColWidth*table.getFinalCol()));
				totalColNo = table.getFinalCol();
			} else {
				totalColNo = colAttributes.getLength();
				table.setRenderWidth(nodeElement.getAttribute("renderwidth"));
			}			
			int colIndex = 0;				
			while(colIndex<totalColNo) {
				if(colAttributes.getLength()==table.getFinalCol()){
					eachColWidth  = Float.parseFloat(((Element)colAttributes.item(colIndex)).getAttribute("p_maxwidth"));
				}
				if((colIndex==totalColNo-1) && (colAttributes.getLength()!=table.getFinalCol())) {
					table.getEachColumnWidth().add(Float.toString(lastColWidth));
				} else {
					table.getEachColumnWidth().add(Float.toString(eachColWidth));
				}				
				
			
				String asciiChar_string = "";
				if (colIndex < 26) {
					char[] asciiCode = { (char) (65 + colIndex) };
					asciiChar_string = new String(asciiCode);
				} else {
					String temp_AsciiCode1 = "";
					String temp_AsciiCode2 = "";
					int count = 0;
					int count1 = 0;
					int count2 = 0;
	
					for (int j = 0; j < (colIndex - 26); j++) {
						if (count == 26 * count1) {
							char[] asciiCode1 = { (char) (65 + count1) };
							temp_AsciiCode1 = new String(asciiCode1);
							count1++;
							count2 = 0;
						}
						char[] asciiCode2 = { (char) (65 + count2) };
						temp_AsciiCode2 = new String(asciiCode2);
						count++;
						count2++;
						asciiChar_string = temp_AsciiCode1 + temp_AsciiCode2;
					}
				}
				table.getEachColumnName().add(asciiChar_string);
				colIndex++;						
			}
		
		}
	}*/
	
	public int getCols() {
		return cols;
	}
	public void setCols(int cols) {
		this.cols = cols;
	}
	public String getDispname() {
		return dispname;
	}
	public void setDispname(String dispname) {
		this.dispname = dispname;
	}
	public String[] getDocumentList() {
		return documentList;
	}
	public void setDocumentList(String[] documentList) {
		this.documentList = documentList;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public String[] getDocumentTypeList() {
		return documentTypeList;
	}
	public void setDocumentTypeList(String[] documentTypeList) {
		this.documentTypeList = documentTypeList;
	}
	public String getEffdate() {
		return effdate;
	}
	public void setEffdate(String effdate) {
		this.effdate = effdate;
	}
	public String getExpdate() {
		return expdate;
	}
	public void setExpdate(String expdate) {
		this.expdate = expdate;
	}
	public String[] getFund() {
		return fund;
	}
	public void setFund(String[] fund) {
		this.fund = fund;
	}
	public String[] getFundfamily() {
		return fundfamily;
	}
	public void setFundfamily(String[] fundfamily) {
		this.fundfamily = fundfamily;
	}
	public String getGlobal() {
		return global;
	}
	public void setGlobal(String global) {
		this.global = global;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public String getTableType() {
		if(tableType == null)
			tableType = "";
		
		return tableType;
	}
	public void setTableType(String tableType) {
		this.tableType = tableType;
	}

	public int[] getOrderRows() {
		return orderRows;
	}

	public void setOrderRows(int[] orderRows) {
		this.orderRows = orderRows;
	}

	public int[] getOrderCols() {
		return orderCols;
	}

	public void setOrderCols(int[] orderCols) {
		this.orderCols = orderCols;
	}

	public Graphic getGraphic() {
		return graphic;
	}

	public void setGraphic(Graphic graphic) {
		this.graphic = graphic;
	}

	public String getCspan() {
		return cspan;
	}

	public void setCspan(String cspan) {
		this.cspan = cspan;
	}

	public String getDatatype() {
		return datatype;
	}

	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}

	public String getMaxSysentitySeqId() {
		return maxSysentitySeqId;
	}

	public void setMaxSysentitySeqId(String maxSysentitySeqId) {
		this.maxSysentitySeqId = maxSysentitySeqId;
	}

	public String getPrimary() {
		return primary;
	}

	public void setPrimary(String primary) {
		this.primary = primary;
	}

	public String getRender() {
		return render;
	}

	public void setRender(String render) {
		if(render == null) 
			render = "0";
		this.render = render;
	}

	public String[] getSelcol() {
		return selcol;
	}

	public void setSelcol(String[] selcol) {
		this.selcol = selcol;
	}

	public String[] getSelrow() {
		return selrow;
	}

	public void setSelrow(String[] selrow) {
		this.selrow = selrow;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public ArrayList getTablerow() {
		return tablerow;
	}

	public void setTablerow(ArrayList tablerow) {
		this.tablerow = tablerow;
	}

	public String getTstyle(int i) {
		if (i>rows)
			return "";
		if (tstyles==null || i>=tstyles.length)
			return "";	
		return tstyles[i];
	}

	public String[] getCellValue() {
		return cellValue;
	}

	public void setCellValue(String[] cellValue) {
		this.cellValue = cellValue;
	}

	/**
	 * @return the topXTable
	 */
	public String getTopXTable() {
		return topXTable;
	}

	/**
	 * @param topXTable the topXTable to set
	 */
	public void setTopXTable(String topXTable) {
		this.topXTable = topXTable;
	}

	public HashMap getNewAttribute() {
		return newAttribute;
	}

	public void setNewAttribute(HashMap newAttribute) {
		this.newAttribute = newAttribute;
	}

	public ArrayList getNewAddedColAttribute() {
		return newAddedColAttribute;
	}

	public void setNewAddedColAttribute(ArrayList newAddedColAttribute) {
		this.newAddedColAttribute = newAddedColAttribute;
	}

	public ArrayList<String> getNewTableAttributes() {
		return newTableAttributes;
	}

	public void setNewTableAttributes(ArrayList<String> newTableAttributes) {
		this.newTableAttributes = newTableAttributes;
	}

	public HashMap getNewRowAttribute() {
		return newRowAttribute;
	}

	public void setNewRowAttribute(HashMap newRowAttribute) {
		this.newRowAttribute = newRowAttribute;
	}

	public ArrayList getNewAddedRowAttributes() {
		return newAddedRowAttributes;
	}

	public void setNewAddedRowAttributes(ArrayList newAddedRowAttributes) {
		this.newAddedRowAttributes = newAddedRowAttributes;
	}
}

package com.dci.rest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.*;

@SpringBootApplication
@ComponentScan("com.dci.rest")
public class DciBootServicesApplication {

	private static final Logger LOGGER = LogManager.getLogger(DciBootServicesApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(DciBootServicesApplication.class, args);
		LOGGER.info("url to access service in Swagger:http://localhost:8081/swagger-ui.html#/");
		LOGGER.info("Spring Boot Service");
	}
    
}

package com.dci.rest.common;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import com.dci.rest.exception.ErrorMessagesBean;
import com.dci.rest.exception.ErrorMessagesBean.ErrorDetail;

public class DciCommon {
	public static final String CONTENTS = "/rest/LibraryService/contents";
	public static final String SEARCH = "/rest/LibraryService/search";
	public static final String LEFTPANELJSON = "/rest/LibraryService/leftPanelJson";
	public static final String EFF_DATE = "01/01/1800";
	public static final String EXP_DATE = "01/01/2599";
	public static String PUBLIC_KEY = null;
	private Logger developerLog = Logger.getLogger(DciCommon.class);
	/**
	  *	Method Name: replaceSpecialCharacters(String inputString)
	  * @param inputString
	  *	Description: by vilas to replace special character
	  **/	
		public static String replaceSpecialCharacters(String inputString) throws Exception 
		{
			try
			{
				if(inputString!=null)
				{
					// inputString = inputString.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("'", "&apos;").replaceAll("\"", "&quot;").replace("&nbsp;"," ").replace(",","&#44;"); 	commented and added line for PB-337
					inputString = inputString.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("'", "&apos;").replaceAll("\"", "&quot;").replace("&nbsp;"," ").replace(",","&#44;").replace("&#039;", "'").replace("&amp;#039;", "'");
					
				}
			}catch(Exception e){
				//new DCIException("com.dci.db4.common.DciCommon || MethodName:replaceSpecialCharacters()",e);
			}
			return(inputString);
		}
		
		
		public String getErrorMessages(String clientId,String errorType,String errorId)
			{
					MDC.put("category","com.dci.rest.common");
					developerLog.warn("Entering into com.dci.rest.common || Method Name : getErrorMessages() ||");
					HashMap clientIds=new HashMap();
				    HashMap msgErrorObj = new HashMap();
				    HashMap msgObj = new HashMap();
				    ErrorMessagesBean errMsg = new ErrorMessagesBean();
				 
							
					errMsg = (ErrorMessagesBean)msgErrorObj.get(errorType);
					msgObj = errMsg.getErrorDetail();
					String errorMessage = "";
					
					ErrorDetail errorDetail = (ErrorDetail)(msgObj.get(errorId));
					errorMessage = errorDetail.getMessagevalue();
					
					developerLog.warn("Exiting from com.dci.db4.common.DciCommon || Method Name : getErrorMessages() ||");
					if(errorMessage!="")
					{
				      return errorMessage;
				    }else
					{	
					  return "Error while Processing";
				    }
			}	
		public static String addSpecialCharactersInTextualString(String inputString)throws Exception 
		{
			try
			{
				if(null==inputString )
				{
					inputString="";
				}
				else
				{
					inputString = inputString.replaceAll("&#39;", "'").replaceAll("&#38;", "&").replaceAll("&lt;", "<").replaceAll("&gt;", ">").replace("&#44;",",").replaceAll("&#35;","#").replaceAll("&#37;","%").replaceAll("&#58;",":").replaceAll("&#95;","_");
				}
			}
			catch(Exception e)
			{
				//new DCIException("com.dci.db4.common.DciCommon || MethodName:addSpecialCharactersInTextualString()",e);
			}
			return inputString;
		}
			
}

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registration</title>
</head>
<body>
	<form:form id="authUser" modelAttribute="authUser" action="register"
		method="post">
		<table align="center">
			<tr>
				<td><form:label path="userName">Username</form:label></td>
				<td><form:input path="userName" name="userName" id="userName" />
				</td>
			</tr>
			<tr>
				<td><form:label path="base_URL">Rest Service Base URL</form:label>
				</td>
				<td><form:input path="base_URL"
						name="base_URL" id="base_URL" /></td>
			</tr>
			<tr>
				<td><form:label path="app_URL">Application URL</form:label></td>
				<td><form:input path="app_URL" name="app_URL" id="app_URL" /></td>
			</tr>
			<tr>
				<td><form:label path="callback_URL">Call Back URL</form:label>
				</td>
				<td><form:input path="callback_URL" name="callback_URL"
						id="callback_URL" /></td>
			</tr>
			<tr>
				<td><form:label path="auth_URL">Auth URL</form:label></td>
				<td><form:input path="auth_URL" name="auth_URL" id="auth_URL" />
				</td>
			</tr>
			<tr>
				<td><form:label path="request_token">Request Token</form:label>
				</td>
				<td><form:input path="request_token" name="request_token"
						id="request_token" /></td>
			</tr>
			<tr>
				<td><form:label path="secrete_token">Secrete Token</form:label>
				</td>
				<td><form:input path="secrete_token" name="secrete_token"
						id="secrete_token" /></td>
			</tr>
			<tr>
				<td><form:label path="access_token">Access Token</form:label></td>
				<td><form:input path="access_token" name="access_token"
						id="access_token" /></td>
			</tr>
			<tr>
				<td><form:label path="consumer_Key">Consumer Key</form:label></td>
				<td><form:input path="consumer_Key" name="consumer_Key"
						id="consumer_Key" /></td>
			</tr>
			<tr>
				<td><form:label path="verification_Key">Verification Key</form:label></td>
				<td><form:input path="verification_Key" name="verification_Key"
						id="verification_Key" /></td>
			</tr>
			<tr>
				<td><form:label path="public_Key">Public Key</form:label></td>
				<td><form:input path="public_Key" name="public_Key"
						id="public_Key" /></td>
			</tr>
			<tr>
				<td><form:label path="private_Key">Private Key</form:label></td>
				<td><form:input path="private_Key" name="private_Key"
						id="private_Key" /></td>
			</tr>
			<tr>
				<td></td>
				<td><form:button id="register" name="register">Register</form:button>
				</td>
			</tr>
			<tr></tr>
<!-- 			<tr>
				<td></td>
				<td><a href="register.jsp">Home</a></td>
			</tr> -->
		</table>
	</form:form>
</body>
</html>